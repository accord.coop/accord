#!/bin/sh

echo "[Running pre-commit hook…]"

WWW_FILES=$(git diff --cached --name-only --diff-filter=ACMR "www/*.js" "www/*.json" "www/*.ts" "www/**/*.js" "www/**/*.jsx" "www/**/*.ts" "www/**/*.tsx" "www/**/*.json" | sed 's| |\\ |g')
CMS_FILES=$(git diff --cached --name-only --diff-filter=ACMR "cms/*.js" "cms/*.json" "cms/*.ts" "cms/**/*.js" "cms/**/*.jsx" "cms/**/*.ts" "cms/**/*.tsx" "cms/**/*.json" | sed 's| |\\ |g')

[[ -z "$WWW_FILES" && -z "$CMS_FILES" ]] && exit 0

if [ -n "$WWW_FILES" ]
then
  echo "$WWW_FILES" | xargs ./www/node_modules/.bin/prettier --write
  echo "$WWW_FILES" | xargs git add
fi

if [ -n "$CMS_FILES" ]
then
  echo "$CMS_FILES" | xargs ./cms/node_modules/.bin/prettier --write
  echo "$CMS_FILES" | xargs git add
fi

exit 0
