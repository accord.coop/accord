const baseSize = 16

const exponent = 1.333
const mobileExponent = 1.25

const sizes = ['xs', 'sm', 'md', 'lg', 'xl', '2xl', '3xl', '4xl', '5xl', '6xl']

module.exports = sizes.reduce((acc, size, s)=>{
  acc[size] = `${(baseSize * Math.pow(exponent, s - 2)).toFixed(3)}px`
  acc[`m-${size}`] = `${(baseSize * Math.pow(mobileExponent, s - 2)).toFixed(3)}px`
  return acc
}, {})
