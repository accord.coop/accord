module.exports = (function(){
  const exponent = 1.2

  const levels = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

  return levels.reduce((acc, level, l)=>{
    acc[`t${level}`] = `${(Math.pow(exponent, l) - 1).toFixed(3)}em`
    return acc
  }, {})
})()
