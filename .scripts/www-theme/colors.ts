const { generateAdaptiveTheme } = require('@adobe/leonardo-contrast-colors');

const contrasts = {
  50: 1.06,
  100: 1.2,
  150: 1.4,
  200: 1.9,
  300: 3.1,
  400: 5.5,
  500: 7.5,
  600: 10,
  700: 12,
  800: 14.5,
  850: 15.2,
  900: 16,
  950: 16.8,
}

const hues = {
  gray: ['#DADCDF', '#323536'],
  red: ['#FF8570',  '#FF5B45', '#560105'],
  orange: ['#FE7349', '#A14E01'],
  yellow: ['#F5C968', '#F5B71E'],
  green: ['#36FE8A', '#0EB681'],
  teal: ['#85BFC9', '#41ACBB','#508F9F'],
  cyan: ['#BDF6F0', '#75F5E9', '#71C6BE'],
  blue: ['#597CDC', '#375EED', '#0F2A81'],
  purple: ['#CDB2ED','#4D018C'],
  pink: ['#FE9BA2', '#F33A69'],
}

const separator = '--'

// returns theme colors as JSON
const leonardoTheme = generateAdaptiveTheme({
  colorScales: Object.keys(hues).map(name => ({
    name,
    colorspace: "CAM02",
    colorKeys: hues[name],
    ratios: Object.keys(contrasts).reduce((acc, shade) => {
      acc[`${name}${separator}${shade}`] = contrasts[shade]
      return acc
    }, {})
  })),
  baseScale: 'gray',
  brightness: 96
})

module.exports = leonardoTheme.reduce((acc, swatch)=>{
  if(swatch.name){
    acc[swatch.name] = swatch.values.reduce((valuesAcc, {name, value})=>{
      const [_name, shadeString] = name.split(separator)
      const shade = parseInt(shadeString)
      valuesAcc[shade] = value
      return valuesAcc
    }, {})
    return acc
  }
  else return acc
}, {
  white: '#fff',
  black: '#000'
})
