#! /bin/sh

echo "〘Test〙 API"
cd api
mix deps.get --only test
if ! MIX_ENV="ci-test" mix test --raise; then
  exit 1
fi
cd ..
