git config --local http.version HTTP/1.1
echo "〘Deploy〙 Deploying to Gigalixir from CI"

git remote add gigalixir "https://${GIGALIXIR_USER}@git.gigalixir.com/${GIGALIXIR_APP}.git"
git push gigalixir `git subtree split --prefix api`:refs/heads/master --force
