git config --local http.version HTTP/1.1

deploy () {
  service=$1
  echo "〘Deploy〙 ${service} service…"
  git push "${service}-deploy" `git subtree split --prefix api`:refs/heads/master --force
}

echo "〘Deploy〙 Deploying:"

# deploy api-production
deploy api-staging

echo "〘Deploy〙 done."
