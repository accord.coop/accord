./.scripts/gigalixir/pause.sh

echo "〘Destroy〙 Accord domains…"
#gigalixir domains:remove api.accord.tools -a $api_production
gigalixir domains:remove api-staging.accord.coop -a humming-spicy-goose

echo "〘Destroy〙 API service…"
#git remote remove api_production-deploy
git remote remove api-staging-deploy
#gigalixir apps:destroy -a $api_production -y
gigalixir apps:destroy -a humming-spicy-goose -y

echo "〘Destroy〙 All done."
