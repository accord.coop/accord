#echo "" > .scripts/gigalixir/.appnames

create () {
  service=$1
  appname=$2
  echo "〘Create〙 ${service} service…"
  gigalixir apps:create -n $appname --cloud gcp --region europe-west1
  sleep 1
  git remote add "${service}-deploy" $(git remote get-url gigalixir)
  git remote remove gigalixir
  gigalixir domains:add "${service}.accord.coop" -a $appname
  echo "${service}=${appname}" >> .scripts/gigalixir/.appnames
#  gigalixir config:set -a $appname $(tr '\n' ' ' < .scripts/gigalixir/.secrets)
}

#create api-production
create api-staging accord-staging-5

echo "〘Create〙 All done. Don’t forget to deploy and resume!"
