import React, { useState, useRef, useEffect } from 'react'
import { Box, useColorModeValue, useMediaQuery } from '@chakra-ui/react'
import { ClassNames } from '@emotion/react'
import T from './T'

interface FigureProps {
  uid: string
  light: { url: string }
  dark: { url: string }
  width: number
  height: number
  eager?: boolean
  alt?: string
  inSurface?: boolean
}

const rSVG = /\.svg$/

function isVector(path) {
  return rSVG.test(path)
}

const Figure = ({
  uid,
  light,
  dark,
  width,
  height,
  eager,
  alt,
  inSurface,
}: FigureProps) => {
  const imgSrc = `${process.env.NEXT_PUBLIC_IMAGE_HOST}${useColorModeValue(
    light.url,
    dark.url
  )}`

  const [isRetina] = useMediaQuery(
    '(-webkit-min-device-pixel-ratio: 2), (min-resolution: 192dpi)'
  )
  const [loaded, setLoaded] = useState(false)
  const $img = useRef(null)

  useEffect(() => {
    if ($img.current?.complete) setLoaded(true)
  })

  return (
    <T
      children={({ t }) => (
        <ClassNames
          children={({ css }) => (
            <Box
              as='figure'
              {...{
                position: 'relative',
                zIndex: 'base',
                ...(inSurface && {
                  borderRadius: 'lg',
                  boxShadow: 'base',
                  overflow: 'hidden',
                }),
                ...(!isVector(imgSrc) && {
                  ...{ [inSurface ? 'me' : 'mx']: 'auto' },
                  maxW: `${width / (isRetina ? 2 : 1)}px`,
                }),
              }}
            >
              <Box role='none' pb={`${(100 * height) / width}%`} />
              <img
                alt={alt || t(`figure--${uid}`)}
                src={imgSrc}
                width={width}
                height={height}
                ref={$img}
                className={css({
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  width: '100%',
                  height: 'auto',
                  opacity: loaded ? 1 : 0,
                  transition: 'opacity 180ms linear',
                })}
                onLoad={() => setLoaded(true)}
                loading={eager ? 'eager' : 'lazy'}
              />
            </Box>
          )}
        />
      )}
    />
  )
}

export default Figure
