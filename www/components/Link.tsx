import { Link as ChakraLink, LinkProps, Icon, Text } from '@chakra-ui/react'
import { ExternalLink, Mail, FileText } from 'react-feather'
import { default as NextLink, LinkProps as NextLinkProps } from 'next/link'

import { Button, IconButton, IButtonProps, IIconButtonProps } from './Button'

const isExternal = /^http.+/
const isMailto = /^mailto.+/
const isDoc = /^\/docs/

const lastWordSep = /(.*?)(\s\S+)?$/
const firstWordSep = /^(\S+)(\s.*)?/

const linkIconProps = (atEnd) => ({
  mb: 1,
  [atEnd ? 'ms' : 'me']: 'px',
})

const LinkText = ({ children, ...props }) => {
  return (
    <Text
      {...{
        'as': 'span',
        whiteSpace: 'normal',
        lineHeight: 'inherit',
        ...props,
      }}
    >
      {children}
    </Text>
  )
}

const AnnotatedLinkContent = ({ href, target, children }) => {
  let _, initial, last, nowrapContent
  switch (true) {
    case isExternal.test(href) || !!target:
      ;[_, initial, last] = lastWordSep.exec(children)
      nowrapContent = last || initial
      return (
        <LinkText>
          {last && initial}
          <span
            {...(nowrapContent.length < 32 && {
              style: { whiteSpace: 'nowrap' },
            })}
          >
            {nowrapContent}
            <Icon role='none' as={ExternalLink} {...linkIconProps(true)} />
          </span>
        </LinkText>
      )
    case isDoc.test(href):
      ;[_, initial, last] = firstWordSep.exec(children)
      nowrapContent = initial
      return (
        <LinkText>
          <span
            {...(nowrapContent.length < 32 && {
              style: { whiteSpace: 'nowrap' },
            })}
          >
            <Icon role='none' as={FileText} {...linkIconProps(false)} />
            {nowrapContent}
          </span>
          {last}
        </LinkText>
      )
    case isMailto.test(href):
      return (
        <LinkText {...(children.length < 32 && { whiteSpace: 'nowrap' })}>
          <Icon role='none' as={Mail} {...linkIconProps(false)} />
          {children}
        </LinkText>
      )
    default:
      return <LinkText {...{ children }} />
  }
}

type TNextLinkPickedProps =
  | 'href'
  | 'replace'
  | 'scroll'
  | 'shallow'
  | 'prefetch'

export interface ILinkButtonProps
  extends Pick<NextLinkProps, TNextLinkPickedProps>,
    Omit<IButtonProps, 'href'> {}

export const LinkButton = ({
  href,
  prefetch,
  replace,
  scroll,
  shallow,
  children,
  ...chakraProps
}: ILinkButtonProps) => {
  return (
    <NextLink passHref {...{ href, prefetch, replace, scroll, shallow }}>
      <Button
        as='a'
        {...{ py: 2, height: 'auto', lineHeight: 1 }}
        {...chakraProps}
      >
        <LinkText lineHeight='shorter'>{children}</LinkText>
      </Button>
    </NextLink>
  )
}

export interface ILinkIconButtonProps
  extends Pick<NextLinkProps, TNextLinkPickedProps>,
    Omit<IIconButtonProps, 'href'> {}

export const LinkIconButton = ({
  href,
  prefetch,
  replace,
  scroll,
  shallow,
  ...chakraProps
}: ILinkIconButtonProps) => {
  return (
    <NextLink passHref {...{ href, prefetch, replace, scroll, shallow }}>
      <IconButton as='a' {...chakraProps} />
    </NextLink>
  )
}

export interface ILinkProps
  extends Pick<NextLinkProps, TNextLinkPickedProps>,
    Omit<LinkProps, 'href'> {
  noIcon?: boolean
}

export const Link = ({
  href,
  target,
  prefetch,
  replace,
  scroll,
  shallow,
  children,
  noIcon,
  ...chakraProps
}: ILinkProps) => {
  return (
    <NextLink passHref {...{ href, prefetch, replace, scroll, shallow }}>
      <ChakraLink {...{ target }} {...chakraProps}>
        {noIcon ? (
          <LinkText {...{ children }} />
        ) : (
          <AnnotatedLinkContent {...{ href, target, children }} />
        )}
      </ChakraLink>
    </NextLink>
  )
}

export default Link
