import React, { LegacyRef, forwardRef } from 'react'
import {
  Button as ChakraButton,
  IconButton as ChakraIconButton,
  ButtonProps,
  IconButtonProps,
  useColorMode,
} from '@chakra-ui/react'

export enum buttonColorSchemes {
  primary = 'primary',
}

const buttonColorScheme = (colorScheme) => {
  const { colorMode } = useColorMode()
  switch (colorScheme) {
    case buttonColorSchemes.primary:
      return colorMode === 'light' ? 'blue' : 'yellow'
    default:
      return null
  }
}

export interface IButtonProps extends Omit<ButtonProps, 'colorScheme'> {
  colorScheme?: buttonColorSchemes
}

export const Button = forwardRef(
  ({ colorScheme, ...props }: IButtonProps, ref) => {
    return (
      <ChakraButton
        colorScheme={buttonColorScheme(colorScheme)}
        ref={ref as LegacyRef<any>}
        {...props}
      />
    )
  }
)

export interface IIconButtonProps extends Omit<IconButtonProps, 'colorScheme'> {
  colorScheme?: buttonColorSchemes
}

export const IconButton = forwardRef(
  ({ colorScheme, ...props }: IIconButtonProps, ref) => {
    return (
      <ChakraIconButton
        colorScheme={buttonColorScheme(colorScheme)}
        ref={ref as LegacyRef<any>}
        {...props}
      />
    )
  }
)

export default Button
