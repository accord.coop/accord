import React, { createContext } from 'react'
import { TFunction } from 'i18next'
import { ChakraProvider, extendTheme, ThemeDirection } from '@chakra-ui/react'

import theme, { getDirection } from '../theme'
import initT from '../lib/init-t'

const TContext = createContext({
  t: ((key) => key) as TFunction,
  direction: 'ltr' as ThemeDirection,
  inlineStart: 'left' as 'left' | 'right',
  inlineEnd: 'right' as 'left' | 'right',
})

TContext.displayName = 'TranslationsContext'

export const TProvider = ({ t10s, locale, children }) => {
  const direction = getDirection(locale)
  const inlineStart = direction === 'ltr' ? 'left' : 'right'
  const inlineEnd = direction === 'ltr' ? 'right' : 'left'
  return (
    <ChakraProvider theme={extendTheme({ ...theme, direction })}>
      <TContext.Provider
        value={{
          t: initT({ locale, t10s }),
          direction,
          inlineStart,
          inlineEnd,
        }}
        children={children}
      />
    </ChakraProvider>
  )
}

export default TContext.Consumer
