import React from 'react'
import { default as NextHead } from 'next/head'

const defaults = {
  image: '/social.jpg',
}

const Head = ({ title, url, abstract, image = defaults.image }) => {
  return (
    <NextHead>
      <title>{title}</title>
      <meta key='description' name='description' content={abstract} />
      <meta key='image' itemProp='image' content={image} />

      <meta key='og__title' property='og:title' content={title} />
      <meta
        key='og__description'
        property='og:description'
        content={abstract}
      />
      <meta key='og__image' property='og:image' content={image} />
      <meta key='og__url' property='og:url' content={url} />

      <meta
        key='viewport'
        name='viewport'
        content='minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no'
      />
    </NextHead>
  )
}

export default Head
