import React from 'react'
import { useColorMode } from '@chakra-ui/react'

import { Box } from '@chakra-ui/react'
import Figure from './Figure'
import { supportsBackdrop } from '../theme'

const LPSegment = ({ children, figure, ...props }) => {
  const { colorMode } = useColorMode()
  return (
    <Box mb={{ base: 0, md: '-6rem' }} {...props}>
      <Box px={{ base: 0, sm: '10%' }}>
        <Figure {...figure} />
      </Box>
      <Box
        {...{
          maxW: 'sm',
          position: 'relative',
          top: { base: '-2rem', md: '-10rem' },
          m: { base: '0 auto', md: 0 },
          px: 8,
          pt: 1,
          pb: 5,
          zIndex: 1,
          css: {
            backdropFilter: 'blur(6px)',
          },
        }}
      >
        <Box
          {...{
            // display: {base: 'none', md: 'block'},
            position: 'absolute',
            top: 0,
            bottom: 0,
            left: 0,
            right: 0,
            zIndex: -1,
            background: colorMode === 'light' ? 'white' : 'gray.900',
            borderRadius: 'base',
            opacity: 0.94,
            css: { [supportsBackdrop]: { opacity: 0.82 } },
          }}
        />
        {children}
      </Box>
    </Box>
  )
}

export default LPSegment
