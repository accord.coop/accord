import React, { createContext } from 'react'
import hydrate from 'next-mdx-remote/hydrate'
import { TFunction } from 'i18next'

import {
  Box,
  Code,
  Heading,
  Icon,
  OrderedList,
  UnorderedList,
  ListItem,
  Text,
  useColorMode,
  BoxProps,
} from '@chakra-ui/react'

import Figure from './Figure'
import Link from './Link'
import LPSegment from './LPSegment'
import { LinkButton } from './Link'

import { fontSize } from '../theme'

import { AccordDevice } from '../icons/Accord'
import IconSlack from '../icons/Slack'

import { ArrowRight } from 'react-feather'
import { MdxRemote } from 'next-mdx-remote/types'

const headerProps = {
  mb: 't3',
  mt: 't4',
  textStyle: 'heading',
}

const assetsDefault = {}
const tDefault = ((key) => key) as TFunction

const MDXContext = createContext({
  colorMode: 'light',
  assets: assetsDefault,
  t: tDefault,
})

export const mdxOptions = ({
  assets = assetsDefault,
  t = tDefault,
  colorMode,
}) => ({
  components: {
    // HTML substitutes
    a: Link,
    inlineCode: Code,
    code: (props) => <Code display='block' py={1} {...props} />,
    h1: (props) => (
      <Heading
        as='h1'
        {...fontSize('3xl')}
        {...headerProps}
        fontWeight='bold'
        {...props}
      />
    ),
    h2: (props) => (
      <Heading as='h2' {...fontSize('2xl')} {...headerProps} {...props} />
    ),
    h3: (props) => (
      <Heading as='h3' {...fontSize('xl')} {...headerProps} {...props} />
    ),
    h4: (props) => (
      <Heading as='h4' {...fontSize('lg')} {...headerProps} {...props} />
    ),
    h5: (props) => (
      <Heading as='h5' {...fontSize('md')} {...headerProps} {...props} />
    ),
    h6: (props) => (
      <Heading as='h6' {...fontSize('md')} {...headerProps} {...props} />
    ),
    p: (props) => <Text as='p' my='t3' {...props} />,
    hr: (props) => <Box role='none' as='hr' my='t7' {...props} />,
    ul: (props) => <UnorderedList ms='1em' {...props} />,
    ol: (props) => <OrderedList ms='1em' {...props} />,
    li: ListItem,
    // Provider
    Provider: MDXContext.Consumer,
    // Chakra components
    Heading,
    Icon,
    Link,
    // Accord components
    Figure,
    LPSegment,
    LinkButton,
    // Accord assets
    IconSlack,
    AccordDevice,
    // Generic assets
    ArrowRight: (props) => <Icon as={ArrowRight} {...props} />,
  },
  provider: {
    component: MDXContext.Provider,
    props: {
      value: {
        colorMode,
        assets,
        t,
      },
    },
  },
})

interface IMDXProps extends Omit<BoxProps, 'children'> {
  assets?: any
  t?: TFunction
  children: MdxRemote.Source
}

const MDX = ({ children, assets, t, ...props }: IMDXProps) => {
  const { colorMode } = useColorMode()
  return (
    <Box pb={{ base: 16, md: 4 }} {...props}>
      {hydrate(children, mdxOptions({ assets, t, colorMode }))}
    </Box>
  )
}

export default MDX
