import React from 'react'

import { Flex, useColorMode } from '@chakra-ui/react'

const Card = (props) => {
  const { colorMode } = useColorMode()
  return (
    <Flex
      {...{
        width: 'full',
        shadow: 'sm',
        borderRadius: 'base',
        py: { base: 2, md: 4 },
        px: { base: 4, md: 6 },
        backgroundColor: colorMode === 'light' ? 'white' : 'gray.850',
      }}
      {...props}
    />
  )
}

export default Card
