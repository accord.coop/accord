import React, { useEffect, useState } from 'react'
import throttle from 'lodash/throttle'
import {
  Box,
  ButtonGroup,
  Icon,
  IconButton,
  Spacer,
  useColorMode,
} from '@chakra-ui/react'
import { Moon, Sun } from 'react-feather'

import { LinkButton, LinkIconButton } from './Link'
import Accord, { AccordDevice } from '../icons/Accord'
import Surface from './Surface'
import T from './T'
import { supportsBackdrop } from '../theme'

const buttonProps = {
  color: 'inherit',
  fontWeight: 500,
  height: 8,
  px: 3,
}

const logoProps = {
  variant: 'ghost',
  p: 2,
  py: 1,
  ms: -2,
  height: 'auto',
}

const TopBar = () => {
  const { colorMode, toggleColorMode } = useColorMode()
  useEffect(() => {
    const onScroll = throttle(() => setScrolled(window.scrollY > 6), 100)
    window.addEventListener('scroll', onScroll)
    return () => {
      window.removeEventListener('scroll', onScroll)
    }
  }, [])
  const [isScrolled, setScrolled] = useState<boolean>(false)
  return (
    <T
      children={({ t }) => (
        <Box
          as='nav'
          {...{
            position: 'fixed',
            left: 0,
            right: 0,
            top: 0,
            zIndex: 'docked',
            backgroundColor: 'transparent',
            boxShadow: isScrolled ? 'xl' : 'none',
            transition: 'box-shadow .2s linear',
            css: { backdropFilter: isScrolled ? 'blur(12px)' : 'blur(0)' },
          }}
        >
          <Box
            {...{
              role: 'none',
              position: 'absolute',
              top: 0,
              bottom: 0,
              left: 0,
              right: 0,
              zIndex: -1,
              opacity: 0,
              backgroundColor: 'transparent',
              transition: 'background-color .2s linear, opacity .2s linear',
              ...(isScrolled && {
                backgroundColor: colorMode === 'light' ? 'white' : 'gray.800',
                opacity: 0.96,
                css: {
                  [supportsBackdrop]: {
                    opacity: 0.68,
                  },
                },
              }),
            }}
          />
          <Surface flexbox alignItems='center' py={3}>
            <LinkIconButton
              href='/'
              color='inherit'
              me={2}
              {...logoProps}
              aria-label={t('top-bar__cta--home')}
              display={{ base: 'inherit', sm: 'none' }}
              icon={<AccordDevice />}
            />
            <LinkIconButton
              href='/'
              color='inherit'
              me={2}
              {...logoProps}
              aria-label={t('top-bar__cta--home')}
              display={{ base: 'none', sm: 'inherit' }}
              icon={<Accord />}
            />
            <Spacer />
            <ButtonGroup variant='ghost' spacing={2} me={-3}>
              <LinkButton href='/docs' {...buttonProps}>
                {t('top-bar__link--docs')}
              </LinkButton>
              {/*<LinkButton href='/login' {...buttonProps}>*/}
              {/*  {t('top-bar__link--login')}*/}
              {/*</LinkButton>*/}
              <IconButton
                aria-label={t('top-bar__cta--toggle-color-mode')}
                onClick={toggleColorMode}
                icon={
                  colorMode === 'light' ? <Icon as={Moon} /> : <Icon as={Sun} />
                }
                {...buttonProps}
              />
            </ButtonGroup>
          </Surface>
        </Box>
      )}
    />
  )
}

export default TopBar
