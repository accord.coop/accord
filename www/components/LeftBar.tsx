import React, { useRef, useState } from 'react'
import set from 'lodash/set'
import get from 'lodash/get'
import {
  Accordion,
  AccordionButton,
  AccordionIcon,
  AccordionItem,
  AccordionPanel,
  Box,
  Flex,
  Icon,
  IconButton,
  Spacer,
  useBreakpointValue,
  useColorMode,
} from '@chakra-ui/react'

import { Menu, Moon, Sun, X } from 'react-feather'
import Accord, { AccordDevice } from '../icons/Accord'
import { supportsBackdrop } from '../theme'

import Footer from './Footer'
import T from './T'
import { LinkIconButton, Link } from './Link'

import { useInteractionOutside } from '../lib/hooks'

const dims = {
  baseWidth: 18,
  gap: 1,
}

export const SurfaceWithLeftBarWrapper = ({ children }) => (
  <Box ps={{ base: 0, md: `${dims.baseWidth + dims.gap}rem` }}>{children}</Box>
)

const buttonProps = {
  ms: 1,
  width: 8,
  minW: null,
  variant: 'ghost',
}

function setSlugTreeOrders(slugTree) {
  if (slugTree && slugTree.children) {
    slugTree.childrenOrder = Object.keys(slugTree.children).sort(
      (a, b) =>
        get(slugTree, ['children', a, 'doc', 'order']) -
        get(slugTree, ['children', b, 'doc', 'order'])
    )
    slugTree.childrenOrder.forEach((child) =>
      setSlugTreeOrders(slugTree.children[child])
    )
  }
}

const DocsTree = ({ docsDocs, pageSlug, isHidden }) => {
  const slugTree = Object.keys(docsDocs).reduce((acc, fullSlug) => {
    const pathComponents = fullSlug.split('/')
    let path = pathComponents[0]
    for (let i = 1; i < pathComponents.length; i++) {
      path += '.children.' + pathComponents.slice(0, i + 1).join('/')
    }
    return set(acc, path, {
      doc: docsDocs[fullSlug],
      children: get(acc, path + '.children', null),
    })
  }, {})

  setSlugTreeOrders(slugTree['docs'])

  return (
    <DocsSubtree
      {...{
        rootSlug: 'docs',
        slugTree: slugTree['docs'],
        index: 0,
        prefix: [],
        suppressOrdinal: true,
        pageSlug,
        isHidden,
      }}
    />
  )
}

const DocsSubtree = ({
  rootSlug,
  slugTree,
  index,
  prefix,
  suppressOrdinal,
  pageSlug,
  isHidden,
}) => {
  const ordinal = index + 1
  return (
    <>
      <Link
        {...{
          display: 'block',
          ms: `${0.5 * prefix.length}rem`,
          mt: suppressOrdinal ? 0 : prefix.length < 1 ? 4 : 2,
          href: `/${rootSlug}`,
          noIcon: true,
          variant: 'ghost',
          children:
            (suppressOrdinal ? '' : `${prefix.concat(ordinal).join('.')}. `) +
            get(slugTree, 'doc.title'),
          prefetch: false,
          ...(pageSlug === rootSlug && {
            fontWeight: 700,
          }),
          ...(isHidden && { tabIndex: -1 }),
        }}
      />
      {get(slugTree, 'childrenOrder', []).map((child, i) => (
        <DocsSubtree
          {...{
            key: `left-bar__link--${child}`,
            rootSlug: child,
            slugTree: get(slugTree, 'children.' + child),
            index: i,
            prefix: suppressOrdinal ? prefix : prefix.concat(ordinal),
            suppressOrdinal: false,
            pageSlug,
            isHidden,
          }}
        />
      ))}
    </>
  )
}

const fabRadius = {
  borderTopRadius: '2rem',
  borderBottomStartRadius: 'md',
  borderBottomEndRadius: '2rem',
}

const LeftBar = ({ basicDocs, docsDocs, pageSlug }) => {
  const { colorMode, toggleColorMode } = useColorMode()
  const [open, setOpen] = useState(false)
  const isOpenable = useBreakpointValue({ base: true, md: false })
  const isHidden = isOpenable && !open
  const $leftBar = useRef(null)

  useInteractionOutside($leftBar, () => setOpen(false))

  return (
    <T
      children={({ t, inlineStart, inlineEnd }) => {
        return (
          <Box
            {...{
              ref: $leftBar,
              position: 'fixed',
              zIndex: 'overlay',
              top: 4,
              bottom: 4,
              [inlineEnd]: {
                base: open
                  ? `calc(100% - ${dims.baseWidth + dims.gap}rem)`
                  : '100%',
                md: `calc(100% - ${dims.baseWidth + dims.gap}rem)`,
              },
              borderRadius: '2xl',
              boxShadow: 'xl',
              css: { backdropFilter: 'blur(12px)' },
              width: `${dims.baseWidth}rem`,
              transition: 'right .2s ease-in-out',
            }}
          >
            <Box
              {...{
                display: { base: 'block', md: 'none' },
                position: 'absolute',
                zIndex: 'overlay',
                [inlineStart]: `calc(100% + ${dims.gap}rem)`,
                bottom: 0,
                opacity: open ? 0 : 1,
                visibility: open ? 'hidden' : 'visible',
                transition: `opacity .2s linear, visibility 0s linear ${
                  open ? 0.2 : 0
                }s`,
              }}
            >
              <IconButton
                {...{
                  display: 'block',
                  icon: (
                    <div style={{ transform: 'translateY(-1px)' }}>
                      <AccordDevice display='block' width='full' />
                      <Icon as={Menu} display='block' width='full' />
                    </div>
                  ),
                  'aria-label': t('left-bar__open'),
                  onClick: () => setOpen(true),
                  height: 'auto',
                  px: 4,
                  py: 5,
                  width: 16,
                  variant: 'solid',
                  ...fabRadius,
                  boxShadow: 'xl',
                  colorScheme: colorMode === 'light' ? 'blue' : 'yellow',
                }}
              />
            </Box>
            <Flex
              as='nav'
              {...{
                direction: 'column',
                height: 'full',
                borderRadius: '2xl',
                background:
                  colorMode === 'light'
                    ? 'rgba(255, 255, 255, .96)'
                    : 'rgba(31, 33, 33, .96)',
                ...(isHidden && { 'aria-hidden': true }),
                css: {
                  [supportsBackdrop]: {
                    background:
                      colorMode === 'light'
                        ? 'rgba(255, 255, 255, .72)'
                        : 'rgba(31, 33, 33, .72)',
                  },
                },
              }}
            >
              <Flex
                {...{
                  flex: '0 0 auto',
                  py: 2,
                  px: 3,
                  background:
                    colorMode === 'light' ? 'whiteAlpha.500' : 'whiteAlpha.100',
                  borderTopRadius: '2xl',
                  borderBottomRadius: 'sm',
                }}
              >
                <LinkIconButton
                  href='/'
                  {...{
                    color: 'inherit',
                    me: 2,
                    variant: 'ghost',
                    height: 'auto',
                    'aria-label': t('top-bar__cta--home'),
                    icon: <Accord />,
                    ...(isHidden && { tabIndex: -1 }),
                  }}
                />
                <Spacer />
                <IconButton
                  {...{
                    color: 'inherit',
                    'aria-label': t('top-bar__cta--toggle-color-mode'),
                    onClick: toggleColorMode,
                    icon:
                      colorMode === 'light' ? (
                        <Icon as={Moon} />
                      ) : (
                        <Icon as={Sun} />
                      ),
                    ...buttonProps,
                    ...(isHidden && { tabIndex: -1 }),
                  }}
                />
                <IconButton
                  {...{
                    color: 'inherit',
                    icon: <Icon as={X} />,
                    'aria-label': t('left-bar__close'),
                    onClick: () => setOpen(false),
                    display: { base: 'block', md: 'none' },
                    ...buttonProps,
                    ...(isHidden && { tabIndex: -1 }),
                  }}
                />
              </Flex>

              <Box {...{ flexGrow: 1, overflowY: 'auto' }}>
                <Accordion
                  {...(pageSlug.startsWith('docs') && { defaultIndex: 0 })}
                >
                  <AccordionItem index={0}>
                    <AccordionButton {...(isHidden && { tabIndex: -1 })}>
                      <Box flex='1' textAlign={inlineStart}>
                        {t('top-bar__link--docs')}
                      </Box>
                      <AccordionIcon />
                    </AccordionButton>
                    <AccordionPanel>
                      <DocsTree {...{ docsDocs, pageSlug, isHidden }} />
                    </AccordionPanel>
                  </AccordionItem>
                </Accordion>

                <Footer {...{ basicDocs, isHidden }} />
              </Box>
            </Flex>
          </Box>
        )
      }}
    />
  )
}

export default LeftBar
