import React from 'react'
import { Box } from '@chakra-ui/react'
import Link from './Link'

interface FooterProps {
  basicDocs: { [slug: string]: any }
  isHidden?: boolean
}

const Footer = ({ basicDocs, isHidden }: FooterProps) => (
  <Box as='footer' {...{ w: 'full', p: 8, textAlign: 'center' }}>
    {Object.keys(basicDocs).map((slug, s) => (
      <React.Fragment key={`footer__link-fragment--${slug}`}>
        {s > 0 && (
          <Box display='inline-block' mx={1}>
            &#xb7;
          </Box>
        )}
        <Link
          variant='subdued'
          href={`/${slug}`}
          {...(isHidden && { tabIndex: -1 })}
        >
          {basicDocs[slug].title}
        </Link>
      </React.Fragment>
    ))}
  </Box>
)

export default Footer
