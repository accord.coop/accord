import React from 'react'
import omit from 'lodash/omit'
import {
  Box,
  BoxProps,
  Flex,
  FlexProps,
  Grid,
  GridProps,
} from '@chakra-ui/react'

import { SurfaceWithLeftBarWrapper } from './LeftBar'

const dimensions = (withTopBar, withLeftBar) => ({
  maxW: '3xl',
  m: `0 auto`,
  px: { base: 8, lg: 12 },
  pt: withTopBar ? 20 : withLeftBar ? 4 : 0,
  pb: withLeftBar ? 12 : 0,
})

interface IFlexWidthProps extends FlexProps {
  flexbox: true
  withTopBar?: boolean
  withLeftBar?: boolean
}

interface IGridWidthProps extends GridProps {
  grid: true
  withTopBar?: boolean
  withLeftBar?: boolean
}

interface IBoxWidthProps extends BoxProps {
  withTopBar?: boolean
  withLeftBar?: boolean
}

export type TWidthProps = IFlexWidthProps | IGridWidthProps | IBoxWidthProps

const Surface = ({ withTopBar, withLeftBar, ...props }: TWidthProps) => {
  const content = (function () {
    switch (true) {
      case (props as IFlexWidthProps).flexbox:
        return (
          <Flex
            {...dimensions(withTopBar, withLeftBar)}
            {...omit(props, ['flexbox'])}
          />
        )
      case (props as IGridWidthProps).grid:
        return (
          <Grid
            {...dimensions(withTopBar, withLeftBar)}
            {...omit(props, ['grid'])}
          />
        )
      default:
        return <Box {...dimensions(withTopBar, withLeftBar)} {...props} />
    }
  })()

  return withLeftBar ? (
    <SurfaceWithLeftBarWrapper>{content}</SurfaceWithLeftBarWrapper>
  ) : (
    content
  )
}

export default Surface
