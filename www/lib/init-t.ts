import i18next from 'i18next'

const initT = ({ locale, t10s }) => {
  i18next.init({
    resources: { [locale]: t10s },
    lng: locale,
    defaultNS: 'common',
    initImmediate: false,
  })
  return i18next.t.bind(i18next)
}

export default initT
