const docsSlugsReplacePattern = /^docs\/?(.*)/

export function fullSlugToDocsPageSlug(fullSlug) {
  return docsSlugsReplacePattern.exec(fullSlug)[1]
}

export function docsPageSlugToFullSlug(docsPageSlug) {
  return docsPageSlug && docsPageSlug.length
    ? `docs/${docsPageSlug.join('/')}`
    : 'docs'
}
