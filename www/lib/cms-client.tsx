import { ApolloClient, InMemoryCache, gql } from '@apollo/client'

const CMSClient = new ApolloClient({
  uri: `${process.env.NEXT_PUBLIC_CMS_HOST}/graphql`,
  cache: new InMemoryCache(),
})

export async function getDocument(slug, localeCode) {
  const {
    data: { documentBySlugAndLocale: document },
  } = await CMSClient.query({
    query: gql`
      query Document($slug: String, $localeCode: String) {
        documentBySlugAndLocale(slug: $slug, localeCode: $localeCode) {
          slug
          title
          abstract
          body
        }
      }
    `,
    variables: { slug, localeCode },
  })
  return document
}

export async function getStaticDocuments(tagUID, localeCode) {
  if (!tagUID) return {}
  const {
    data: { documentsByTagAndLocale },
  } = await CMSClient.query({
    query: gql`
      query Documents($tagUID: String, $localeCode: String) {
        documentsByTagAndLocale(tagUID: $tagUID, localeCode: $localeCode) {
          slug
          title
          order
        }
      }
    `,
    variables: { tagUID, localeCode },
  })
  return documentsByTagAndLocale.reduce((acc, { slug, ...doc }) => {
    acc[slug] = doc
    return acc
  }, {})
}

export async function getTranslations(slugs, localeCode) {
  if (slugs.length < 1) return {}
  const {
    data: { translationsBySlugsAndLocale: t10s },
  } = await CMSClient.query({
    query: gql`
      query Translations($slugs: [String], $localeCode: String) {
        translationsBySlugsAndLocale(slugs: $slugs, localeCode: $localeCode) {
          slug
          values
        }
      }
    `,
    variables: { slugs, localeCode },
  })
  return t10s.reduce((acc, { slug, values }) => {
    acc[slug] = values
    return acc
  }, {})
}

export async function getFigures(uids) {
  if (uids.length < 1) return {}
  const {
    data: { figuresByUids: figures },
  } = await CMSClient.query({
    query: gql`
      query Figures($uids: [String]) {
        figuresByUids(uids: $uids) {
          uid
          light {
            url
          }
          dark {
            url
          }
          width
          height
        }
      }
    `,
    variables: { uids },
  })
  return figures.reduce((acc, figure) => {
    acc[figure.uid] = figure
    return acc
  }, {})
}

export async function getFiguresByTag(tag) {
  const {
    data: { figuresByTag: figures },
  } = await CMSClient.query({
    query: gql`
      query Figures($tagUID: String) {
        figuresByTag(tagUID: $tagUID) {
          uid
          light {
            url
          }
          dark {
            url
          }
          width
          height
        }
      }
    `,
    variables: { tagUID: tag },
  })
  return figures.reduce((acc, figure) => {
    acc[figure.uid] = figure
    return acc
  }, {})
}
