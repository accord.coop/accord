import renderToString from 'next-mdx-remote/render-to-string'
import {
  getDocument,
  getStaticDocuments,
  getTranslations,
  getFigures,
  getFiguresByTag,
} from './cms-client'
import { mdxOptions } from '../components/MDX'
import initT from './init-t'

async function getMDXDocument({ title, abstract, body }, providerProps) {
  return renderToString(body, mdxOptions(providerProps)).then((mdxBody) => ({
    title,
    abstract,
    mdxBody,
  }))
}

interface staticPageQueryProps {
  pageSlug: string
  pageTag: string
  t10sSlugs: string[]
  figureUids?: string[]
  figureTag?: string
  locale: string
}

async function getStaticPageProps({
  pageSlug,
  pageTag,
  t10sSlugs,
  figureUids,
  figureTag,
  locale,
}: staticPageQueryProps) {
  const [doc, docsOfTag, basicDocs, t10s, figures] = await Promise.all([
    getDocument(pageSlug, locale),
    getStaticDocuments(pageTag, locale),
    getStaticDocuments('basic', locale),
    getTranslations(t10sSlugs, locale),
    figureTag ? getFiguresByTag(figureTag) : getFigures(figureUids),
  ])

  const mdxDoc = await getMDXDocument(doc, {
    t: initT({ t10s, locale }),
    assets: { figures },
  })

  return {
    props: { mdxDoc, basicDocs, docsOfTag, figures, t10s, pageSlug, locale },
  }
}

export default getStaticPageProps
