import { useEffect } from 'react'

export function useInteractionOutside(ref, callback) {
  useEffect(() => {
    function onMouseDown(event) {
      if (ref.current && !ref.current.contains(event.target))
        callback.apply(null, [event])
    }

    document.addEventListener('mousedown', onMouseDown)
    return () => {
      document.removeEventListener('mousedown', onMouseDown)
    }
  }, [ref])
}
