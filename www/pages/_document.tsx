import Document, { Html, Head, Main, NextScript } from 'next/document'
import { ColorModeScript } from '@chakra-ui/react'
import atoms from '../theme/computed-atoms'
import config from '../theme/config'
import direction from '../theme/direction'

class AccordAppDoc extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return { ...initialProps }
  }

  render() {
    const { locale } = this.props.__NEXT_DATA__
    return (
      <Html dir={direction(locale)}>
        <Head>
          <meta key='charset' charSet='utf-8' />
          <link
            key='fontfaces'
            rel='stylesheet'
            href='https://use.typekit.net/bqn6hbj.css'
          />
          <meta key='og__sitename' property='og:site_name' content='Accord' />
          <link key='icon' rel='icon' type='image/x-icon' href='/favicon.ico' />
          <link
            key='icon180'
            rel='apple-touch-icon'
            sizes='180x180'
            href='/apple-touch-icon.png'
          />
          <link
            key='icon32'
            rel='icon'
            type='image/png'
            sizes='32x32'
            href='/favicon-32x32.png'
          />
          <link
            key='icon16'
            rel='icon'
            type='image/png'
            sizes='16x16'
            href='/favicon-16x16.png'
          />
          <link
            key='iconmask'
            rel='mask-icon'
            href='/safari-pinned-tab.svg'
            color={atoms.colors.gray['900']}
          />
          <meta
            key='tilecolor'
            name='msapplication-TileColor'
            content={atoms.colors.gray['900']}
          />
          <meta
            key='themecolor'
            name='theme-color'
            content={atoms.colors.gray['900']}
          />
        </Head>
        <body>
          <ColorModeScript initialColorMode={config.initialColorMode} />
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default AccordAppDoc
