import React from 'react'
import { useRouter } from 'next/router'

import { getStaticDocuments } from '../../lib/cms-client'
import getStaticPageProps from '../../lib/get-static-page-props'
import {
  fullSlugToDocsPageSlug,
  docsPageSlugToFullSlug,
} from '../../lib/slug-helpers'
import T, { TProvider } from '../../components/T'
import Head from '../../components/Head'
import LeftBar from '../../components/LeftBar'
import Surface from '../../components/Surface'
import MDX from '../../components/MDX'

export async function getStaticPaths({ locale }) {
  const docs = await getStaticDocuments('docs', locale)
  return {
    paths: Object.keys(docs).map((fullSlug) => {
      const docsPageSlug = fullSlugToDocsPageSlug(fullSlug)
      return {
        params: docsPageSlug.length
          ? { docsPageSlug: docsPageSlug.split('/') }
          : { docsPageSlug: [] },
      }
    }),
    fallback: false,
  }
}

export async function getStaticProps({ locale, params }) {
  const fullSlug = docsPageSlugToFullSlug(params.docsPageSlug)
  return getStaticPageProps({
    pageTag: 'docs',
    pageSlug: fullSlug,
    t10sSlugs: ['common'],
    figureTag: 'docs',
    locale,
  })
}

const DocsPage = ({
  mdxDoc: { title, mdxBody, abstract },
  basicDocs,
  docsOfTag,
  t10s,
  pageSlug,
  locale,
  figures,
}) => {
  const { asPath } = useRouter()
  const url = process.env.NEXT_PUBLIC_WWW_HOST + asPath
  return (
    <TProvider {...{ t10s, locale }}>
      <Head {...{ title: `${title} | Accord documentation`, abstract, url }} />
      <LeftBar {...{ basicDocs, pageSlug, docsDocs: docsOfTag }} />
      <Surface as='article' withLeftBar>
        <T
          children={({ t }) => (
            <MDX {...{ assets: { figures }, t }}>{mdxBody}</MDX>
          )}
        />
      </Surface>
    </TProvider>
  )
}

export default DocsPage
