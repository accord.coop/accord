import React from 'react'
import { useRouter } from 'next/router'

import { getStaticDocuments } from '../lib/cms-client'
import getStaticPageProps from '../lib/get-static-page-props'

import T, { TProvider } from '../components/T'
import Footer from '../components/Footer'
import Head from '../components/Head'
import MDX from '../components/MDX'
import TopBar from '../components/TopBar'
import Surface from '../components/Surface'

export async function getStaticPaths({ locale }) {
  const docs = await getStaticDocuments('basic', locale)
  return {
    paths: Object.keys(docs).map((basicPageSlug) => ({
      params: { basicPageSlug },
    })),
    fallback: false,
  }
}

export async function getStaticProps({ locale, params: { basicPageSlug } }) {
  console.log('[basic page static props]', locale, basicPageSlug)
  return getStaticPageProps({
    pageTag: null,
    pageSlug: basicPageSlug,
    t10sSlugs: ['common'],
    figureUids: ['lp-1', 'en__new-delegation__step-2'],
    locale,
  })
}

const BasicPage = ({
  mdxDoc: { title, mdxBody, abstract },
  basicDocs,
  t10s,
  locale,
  figures,
}) => {
  const { asPath } = useRouter()
  const url = process.env.NEXT_PUBLIC_WWW_HOST + asPath
  return (
    <TProvider {...{ t10s, locale }}>
      <Head {...{ title: `${title} | Accord`, abstract, url }} />
      <TopBar />
      <Surface as='article' withTopBar>
        <T
          children={({ t }) => (
            <MDX {...{ assets: { figures }, t }}>{mdxBody}</MDX>
          )}
        />
      </Surface>
      <Footer {...{ basicDocs }} />
    </TProvider>
  )
}

export default BasicPage
