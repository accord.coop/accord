import React, { useState } from 'react'
import get from 'lodash/get'
import { useRouter } from 'next/router'
import { useAsyncCallback } from 'react-async-hook'
import renderToString from 'next-mdx-remote/render-to-string'

import { Checkbox, CheckboxGroup, Icon, Text, VStack } from '@chakra-ui/react'

import { ArrowRight } from 'react-feather'

import getStaticPageProps from '../../lib/get-static-page-props'
import setMultiple from '../../lib/set-multiple'

import T, { TProvider } from '../../components/T'
import Button, { buttonColorSchemes } from '../../components/Button'
import Card from '../../components/Card'
import Footer from '../../components/Footer'
import Head from '../../components/Head'
import MDX, { mdxOptions } from '../../components/MDX'
import TopBar from '../../components/TopBar'
import Surface from '../../components/Surface'

const agreementKeys = ['terms', 'privacy']

export async function getStaticProps({ locale }) {
  const page = await getStaticPageProps({
    pageTag: null,
    pageSlug: 'auth/slack',
    t10sSlugs: ['common'],
    figureUids: [],
    locale,
  })

  const agreementBodies = await Promise.all(
    agreementKeys.map((agreementKey) =>
      renderToString(
        page.props.t10s.common[`auth--slack__${agreementKey}`],
        mdxOptions({ colorMode: 'light' })
      ).then((body) => ({ agreementKey, body }))
    )
  )

  return setMultiple(page, {
    'props.agreements': agreementBodies,
  })
}

const SlackAuthPage = ({
  mdxDoc: { title, mdxBody, abstract },
  basicDocs,
  t10s,
  locale,
  agreements,
}) => {
  const [agreementsChecked, setAgreementsChecked] = useState([])
  const [navigating, setNavigating] = useState(false)
  const router = useRouter()
  const url = process.env.NEXT_PUBLIC_WWW_HOST + router.asPath

  const addTeamRequest = useAsyncCallback(async () =>
    fetch(`${process.env.NEXT_PUBLIC_API_HOST}/v1/auth/slack`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        code: get(router, 'query.code'),
        state: get(router, 'query.state'),
      }),
    }).then((response) => {
      if (response.ok) {
        setNavigating(true)
        return router.push('/docs/slack')
      } else {
        throw new Error('Error adding Team')
      }
    })
  )

  return (
    <TProvider {...{ t10s, locale }}>
      <Head {...{ title: `${title} | Accord`, abstract, url }} />
      <TopBar />
      <Surface as='article' withTopBar>
        <T
          children={({ t }) => (
            <>
              <MDX {...{ t }}>{mdxBody}</MDX>
              <CheckboxGroup onChange={setAgreementsChecked}>
                <VStack spacing={4} mt={8}>
                  {agreements.map(({ agreementKey, body }) => {
                    return (
                      <Card key={`checkbox__${agreementKey}`}>
                        <MDX flex={1} mr={2} {...{ t }}>
                          {body}
                        </MDX>
                        <Checkbox value={agreementKey}>
                          {t('auth__accept')}
                        </Checkbox>
                      </Card>
                    )
                  })}
                </VStack>
              </CheckboxGroup>
              <Button
                {...{
                  width: 'full',
                  mt: 4,
                  mb: 8,
                  rightIcon: <Icon as={ArrowRight} />,
                  colorScheme: buttonColorSchemes.primary,
                  disabled:
                    agreementsChecked.length !== agreementKeys.length ||
                    addTeamRequest.loading ||
                    navigating,
                  onClick: addTeamRequest.execute,
                  isLoading: addTeamRequest.loading,
                }}
              >
                {t('auth__next')}
              </Button>
              {addTeamRequest.error && <Text>{t('auth_error')}</Text>}
            </>
          )}
        />
      </Surface>
      <Footer {...{ basicDocs }} />
    </TProvider>
  )
}

export default SlackAuthPage
