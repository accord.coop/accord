import React from 'react'
import { useRouter } from 'next/router'

import {
  Box,
  Flex,
  Heading,
  Icon,
  Text,
  VStack,
  useColorMode,
} from '@chakra-ui/react'

import { ArrowRight } from 'react-feather'
import IconSlack from '../icons/Slack'
import { AccordDevice } from '../icons/Accord'

import getStaticPageProps from '../lib/get-static-page-props'
import { fontSize } from '../theme'

import T, { TProvider } from '../components/T'
import { buttonColorSchemes } from '../components/Button'
import Surface from '../components/Surface'
import Footer from '../components/Footer'
import MDX from '../components/MDX'
import TopBar from '../components/TopBar'
import Figure from '../components/Figure'
import Head from '../components/Head'
import { LinkButton } from '../components/Link'

export async function getStaticProps({ locale }) {
  return getStaticPageProps({
    pageSlug: 'home',
    pageTag: null,
    t10sSlugs: ['common'],
    figureUids: ['hero-1', 'lp-1', 'lp-2', 'lp-4'],
    locale,
  })
}

const heroSpacing = { base: 6, md: 10 }

const Hero = ({ figure, ...props }) => {
  const { colorMode } = useColorMode()
  return (
    <T
      children={({ t }) => (
        <Box
          {...{
            bgGradient: `linear(160deg, ${
              colorMode === 'light'
                ? 'yellow.50, yellow.150'
                : 'blue.800, blue.600'
            })`,
            pb: heroSpacing,
          }}
          {...props}
        >
          <Surface
            as='figure'
            withTopBar
            {...{
              maxW: '4xl',
              px: 4,
              mb: heroSpacing,
            }}
          >
            <Figure layout='intrinsic' {...figure} />
          </Surface>
          <Surface>
            <Heading
              {...{
                textTransform: 'uppercase',
                textAlign: 'center',
                fontWeight: 400,
                lineHeight: 1,
                textShadow: `hero--${colorMode}`,
                mb: heroSpacing,
              }}
            >
              {t('copy--hero-1')}
            </Heading>
            <Flex
              {...{
                background:
                  colorMode === 'light' ? 'whiteAlpha.600' : 'blackAlpha.400',
                py: { base: 3, md: 5 },
                px: { base: 5, md: 7 },
                mx: -3,
                borderRadius: 'base',
                direction: 'row',
                wrap: 'wrap',
                align: 'center',
              }}
            >
              <Text
                {...{
                  ...fontSize('lg'),
                  flex: '2 0 0',
                  minW: '3xs',
                  mr: 5,
                }}
              >
                {t('copy--hero-2')}
              </Text>
              <VStack
                {...{
                  width: 'full',
                  flex: '1 0 0',
                  spacing: 3,
                  py: 3,
                }}
              >
                <LinkButton
                  href='https://api.accord.coop/slack'
                  colorScheme={buttonColorSchemes.primary}
                  width='full'
                  leftIcon={<IconSlack height={6} />}
                  rightIcon={<Icon as={ArrowRight} />}
                >
                  {t('hero-2__cta--add-to-slack')}
                </LinkButton>
                <LinkButton
                  href='/'
                  disabled
                  width='full'
                  leftIcon={<AccordDevice height={6} />}
                  rightIcon={<Icon as={ArrowRight} />}
                >
                  {t('hero-2__cta--register')}
                </LinkButton>
              </VStack>
            </Flex>
          </Surface>
        </Box>
      )}
    />
  )
}

const Home = ({
  mdxDoc: { title, abstract, mdxBody },
  basicDocs,
  figures,
  t10s,
  locale,
}) => {
  const { asPath } = useRouter()
  const url = process.env.NEXT_PUBLIC_WWW_HOST + asPath
  return (
    <TProvider {...{ t10s, locale }}>
      <Head {...{ title, abstract, url }} />
      <TopBar />
      <Hero figure={figures['hero-1']} />
      <Surface as='article'>
        <T
          children={({ t }) => (
            <MDX {...{ assets: { figures }, t, py: 8 }}>{mdxBody}</MDX>
          )}
        />
      </Surface>
      <Footer {...{ basicDocs }} />
    </TProvider>
  )
}

export default Home
