export default {
  initialColorMode: 'dark' as 'dark',
  useSystemColorMode: true,
}
