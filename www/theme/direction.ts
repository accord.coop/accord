const isRTL = /ar|fa|ps|ur|sd/

const direction = (locale) => {
  return isRTL.test(locale) ? 'rtl' : 'ltr'
}

export default direction
