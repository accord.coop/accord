import { mode } from '@chakra-ui/theme-tools'
import computedAtoms from './computed-atoms'
import components from './components'
import config from './config'
export { default as getDirection } from './direction'

export const fontSize = (size) => ({
  fontSize: { base: `m-${size}`, md: size },
})

export const supportsBackdrop =
  '@supports (backdrop-filter: blur(12px)) or (-webkit-backdrop-filter: blur(12px))'

const theme = {
  config,
  components,
  ...computedAtoms,
  fonts: {
    body: 'urw-din, sans-serif',
    heading: 'urw-din-semi-condensed, sans-serif',
    mono: 'dico-mono-script, mono',
  },
  textStyles: {
    heading: {
      fontWeight: 'semibold',
      letterSpacing: 'wide',
      lineHeight: 'shorter',
    },
  },
  lineHeights: {
    base: 1.58,
  },
  shadows: {
    'hero--light': `0 2px 0 ${computedAtoms.colors.gray[50]}`,
    'hero--dark': `0 2px 0 ${computedAtoms.colors.gray[900]}`,
  },
  styles: {
    global: (props) => ({
      body: {
        bg: mode('gray.50', 'gray.950')(props),
      },
    }),
  },
}

export default theme
