import { theme } from '@chakra-ui/theme'
import setMultiple from '../lib/set-multiple'

const components = {
  Accordion: {
    baseStyle: ({ colorMode }) =>
      setMultiple(theme.components.Accordion.baseStyle, {
        'container.borderTopWidth': 0,
        'container._last.borderBottomWidth': 0,
      }),
  },
  Code: {
    baseStyle: ({ colorMode }) =>
      setMultiple(theme.components.Code.baseStyle, {
        'fontSize': '1em',
      }),
  },
  Link: {
    baseStyle: ({ colorMode }) => ({
      color: colorMode === 'dark' ? 'yellow.150' : 'blue.500',
      '&:hover': { textDecoration: 'underline' },
      '&:focus': {
        outlineColor: colorMode === 'dark' ? 'yellow.150' : 'blue.500',
      },
    }),
    variants: {
      subdued: ({ colorMode }) => ({
        color: colorMode === 'dark' ? 'gray.200' : 'gray.700',
      }),
    },
  },
}

export default components
