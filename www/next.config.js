module.exports = {
  distDir: 'build',
  i18n: {
    locales: ['en'],
    defaultLocale: 'en',
  },
  images: {
    domains: ['storage.googleapis.com', 'localhost'],
  },
  async redirects() {
    return [
      {
        source: '/slack',
        destination: '/docs/slack',
        permanent: true,
      },
    ]
  },
}
