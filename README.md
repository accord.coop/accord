# Accord

Decide together! Accord helps you and your team propose decisions and vote on them using a variety of adjustable democratic methods.

This is the monorepo for the Accord apps, APIs, and other tools. Each of the modules here have their own documentation:

### `api`

The central service for processing requests from apps and bots.

### `www`

The web app for Accord.

### `.scripts`

Repeatable operations needed for building, deploying, and testing these modules.

## License

All software which runs on a server (`api`, `www`, and `.scripts`) is licensed under the GNU Affero General Public License v3.0. 
