const { sanitizeEntity } = require('strapi-utils')

module.exports = {
  query: `
    translationsBySlugsAndLocale(slugs: [String], localeCode: String): [Translation]
  `,
  resolver: {
    Query: {
      translationsBySlugsAndLocale: {
        resolverOf: 'Translation.find',
        async resolver(_, { slugs, localeCode }) {
          const entity = await strapi.services.translation.find({
            slug_in: slugs,
            'locale.code': localeCode,
          })
          return sanitizeEntity(entity, { model: strapi.models.translation })
        },
      },
    },
  },
}
