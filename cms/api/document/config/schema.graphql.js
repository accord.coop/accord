const { sanitizeEntity } = require('strapi-utils')

module.exports = {
  query: `
    documentBySlugAndLocale(slug: String, localeCode: String): Document
    documentsByTagAndLocale(tagUID: String, localeCode: String): [Document]
  `,
  resolver: {
    Query: {
      documentBySlugAndLocale: {
        resolverOf: 'Document.findOne',
        async resolver(_, { slug, localeCode }) {
          const entity = await strapi.services.document.findOne({
            slug,
            'locale.code': localeCode,
          })
          return sanitizeEntity(entity, { model: strapi.models.document })
        },
      },
      documentsByTagAndLocale: {
        resolverOf: 'Document.find',
        async resolver(_, { tagUID, localeCode }) {
          const entity = await strapi.services.document.find({
            'tags.uid': tagUID,
            'locale.code': localeCode,
          })
          return sanitizeEntity(entity, { model: strapi.models.document })
        },
      },
    },
  },
}
