const { sanitizeEntity } = require('strapi-utils')

module.exports = {
  query: `
    figuresByUids(uids: [String]): [Figure]
    figuresByTag(tagUID: String): [Figure]
  `,
  resolver: {
    Query: {
      figuresByUids: {
        resolverOf: 'Figure.find',
        async resolver(_, { uids }) {
          const entity = await strapi.services.figure.find({ uid_in: uids })
          return sanitizeEntity(entity, { model: strapi.models.figure })
        },
      },
      figuresByTag: {
        resolverOf: 'Figure.find',
        async resolver(_, { tagUID }) {
          const entity = await strapi.services.figure.find({
            'tags.uid': tagUID,
          })
          return sanitizeEntity(entity, { model: strapi.models.figure })
        },
      },
    },
  },
}
