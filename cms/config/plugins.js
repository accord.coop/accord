module.exports = ({ env }) => ({
  upload: (function () {
    switch (env('NODE_ENV')) {
      case 'development':
        return {
          provider: 'local',
          providerOptions: {
            'sizeLimit': 100000,
          },
        }
      default:
        return {
          provider: 'google-cloud-storage',
          providerOptions: {
            bucketName: 'accord-cms-media-1',
            basePath: '',
          },
        }
    }
  })(),
})
