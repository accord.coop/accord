# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Use time zone data
config :elixir, :time_zone_database, Tzdata.TimeZoneDatabase

# Add Ecto repos
config :accord,
  ecto_repos: [Accord.Repo]

# Configures the endpoint
config :accord, AccordWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "SQgb0M6fAcmsI34ShPmr0fDXah5k/I29Y0nIerZHsihK1Yd8qhyzNNICucUQqZQ4",
  pubsub_server: Accord.PubSub

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Configure Gettext and CLDR
config :gettext, :default_locale, "en"

# (CLDR uses Unicode-style hyphens, not underscores, for locale identifiers)
config :accord, AccordWeb.Cldr,
  default_locale: "en",
  gettext: AccordWeb.Gettext,
  data_dir: "./priv/cldr",
  json_library: Jason

config :accord, Accord.Schedule, stack_size: 32

config :accord, AccordSlack,
  scopes:
    [
      "channels:join",
      "channels:read",
      "chat:write",
      "chat:write.public",
      "commands",
      "groups:read",
      "im:history",
      "im:read",
      "mpim:read",
      "team:read",
      "users:read"
    ]
    |> Enum.join(",")

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
