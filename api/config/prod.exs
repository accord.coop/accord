use Mix.Config

defmodule SSLFromEnv do
  @moduledoc """
    Implementation for pulling SSL keys & certs from env variables based on:
    https://elixirforum.com/t/using-client-certificates-from-a-string-with-httposion/8631/7

    Newlines are substituted with pipes '|'
    Spaces are substituted with splats '*'
  """

  defp from_env(name) do
    System.get_env(name)
    |> String.replace("@", " ")
    |> String.replace(":", "\n")
  end

  defp decode_pem_bin(pem_bin) do
    pem_bin |> :public_key.pem_decode() |> hd()
  end

  defp decode_pem_entry(pem_entry) do
    :public_key.pem_entry_decode(pem_entry)
  end

  defp decode_pem_entry(pem_entry, password) do
    password = String.to_charlist(password)
    :public_key.pem_entry_decode(pem_entry, password)
  end

  defp encode_der(ans1_type, ans1_entity) do
    :public_key.der_encode(ans1_type, ans1_entity)
  end

  defp split_type_and_entry(ans1_entry) do
    ans1_type = elem(ans1_entry, 0)
    {ans1_type, ans1_entry}
  end

  defp get_cert_der(from) do
    {cert_type, cert_entry} =
      from_env(from)
      |> decode_pem_bin()
      |> decode_pem_entry()
      |> split_type_and_entry()

    {cert_type, encode_der(cert_type, cert_entry)}
  end

  defp get_key_der(from) do
    {key_type, key_entry} =
      from_env(from)
      |> decode_pem_bin()
      |> decode_pem_entry("password")
      |> split_type_and_entry()

    {key_type, encode_der(key_type, key_entry)}
  end

  def get_ssl_opts do
    {_ans1_type, client_cert} = get_cert_der("SQL_CLIENT_CERT")
    {_ans1_type, server_cert} = get_cert_der("SQL_SERVER_CA")
    key = get_key_der("SQL_CLIENT_KEY")

    [
      cacerts: [server_cert],
      key: key,
      cert: client_cert
    ]
  end
end

# Configure database
config :accord, Accord.Repo,
  url: System.get_env("PG_URL"),
  show_sensitive_data_on_connection_error: false,
  ssl: true,
  pool_size: 2,
  ssl_opts: SSLFromEnv.get_ssl_opts(),
  types: Accord.PostgresTypes

# Configure HTTPS
config :accord, AccordWeb.Endpoint,
  load_from_system_env: true,
  url: [host: "api.accord.coop", port: 443],
  http: [port: {:system, "PORT"}],
  force_ssl: [rewrite_on: [:x_forwarded_proto]],
  secret_key_base: System.get_env("SECRET_KEY_BASE"),
  server: true,
  origins: ["https://accord.coop", "https://api.accord.coop", "https://www.accord.coop"]

# Configure Logger

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Configure Guardian
config :accord, AccordWeb.Auth.Guardian,
  issuer: "accord-api",
  secret_key: System.get_env("GUARDIAN_KEY")

# Configure Sendgrid
config :sendgrid,
  api_key: System.get_env("SENDGRID_API_KEY")

# Configure Slack router
config :accord, AccordWeb.Router.Slack, signing_secrets: System.get_env("SLACK_SIGNING_SECRETS")

# Configure Slack controller
config :accord, AccordSlack,
  url: "https://slack.com/api/",
  dev_team: System.get_env("SLACK_DEVELOPMENT_TEAM"),
  client_id: System.get_env("SLACK_CLIENT_ID"),
  client_secret: System.get_env("SLACK_CLIENT_SECRET"),
  redirect_uri: "https://accord.coop/auth/slack"

# Configure Urne
config :accord, Accord.Vote, system_key: System.get_env("URNE_KEY")
