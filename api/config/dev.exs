use Mix.Config

# Configure your database
config :accord, Accord.Repo,
  username: "postgres",
  password: "postgres",
  database: "accord_dev",
  hostname: "localhost",
  show_sensitive_data_on_connection_error: true,
  pool_size: 10,
  log: false,
  types: Accord.PostgresTypes

# Configure HTTPS
config :accord, AccordWeb.Endpoint,
  https: [
    port: 4001,
    cipher_suite: :strong,
    keyfile: "priv/cert/selfsigned_key.pem",
    certfile: "priv/cert/selfsigned.pem"
  ],
  debug_errors: true,
  check_origin: false,
  watchers: [],
  origins: ["http://localhost:3000", "https://localhost:4001"]

# Configure Elixir's logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Set a higher stacktrace during development. Avoid configuring such
# in production as building large stacktraces may be expensive.
config :phoenix, :stacktrace_depth, 20

# Initialize plugs at runtime for faster development compilation
config :phoenix, :plug_init_mode, :runtime

# Configure Guardian
config :accord, AccordWeb.Auth.Guardian,
  issuer: "accord-api",
  secret_key: "ZoGexF/kgJrKWxn+eAMzkw31/F/0u5/B7sJHEO8Fe+mR79NDDN8Iw86rGkS+0HHr"

# Configure Sendgrid
config :sendgrid,
  api_key: {:system, "SENDGRID_API_KEY"}

# Configure Slack router
config :accord, AccordWeb.Router.Slack, signing_secrets: {:system, "SLACK_SIGNING_SECRETS"}

# Configure Slack controller
config :accord, AccordSlack,
  url: "https://slack.com/api/",
  client_id: 1_234_567_890,
  dev_team: {:system, "SLACK_DEVELOPMENT_TEAM"}

# Configure Urne
config :accord, Accord.Vote, system_key: {:system, "URNE_KEY"}
