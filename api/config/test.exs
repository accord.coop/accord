use Mix.Config

# Configure your database
config :accord, Accord.Repo,
  username: "postgres",
  password: "postgres",
  database: "accord_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox,
  types: Accord.PostgresTypes

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :accord, AccordWeb.Endpoint,
  http: [port: 4002],
  server: false,
  origins: ["http://localhost:3000", "https://localhost:4001"]

# Print only warnings and errors during test
config :logger, level: :warn

# Configure Guardian
config :accord, AccordWeb.Auth.Guardian,
  issuer: "accord-api",
  secret_key: "ZoGexF/kgJrKWxn+eAMzkw31/F/0u5/B7sJHEO8Fe+mR79NDDN8Iw86rGkS+0HHr"

# Configure Slack router
config :accord, AccordWeb.Router.Slack, signing_secrets: nil

# Configure Slack controller
config :accord, AccordSlack,
  url: "http://localhost:4004/",
  dev_team: "TR4QXERCG:xoxb-123234950938"

# Configure Urne
config :accord, Accord.Vote,
  system_key: "Dv3dh9qjlh4kCDKYas2G3fGJ5fhfIqj3lZbZd8fZUqHVgoZnhPPqp2P09j+S9+AR"
