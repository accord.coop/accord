defmodule Accord.MixProject do
  use Mix.Project

  def project do
    [
      app: :accord,
      version: "1.0.0",
      elixir: "~> 1.11",
      licenses: ["AGPL-3.0-only"],
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Accord.Application, [env: Mix.env()]},
      extra_applications: extra_applications(Mix.env()),
      start_phases: [bootstrap: []]
    ]
  end

  defp extra_test_applications(), do: extra_applications(nil) ++ [:plug_cowboy]

  defp extra_applications(:test), do: extra_test_applications()
  defp extra_applications(:"ci-test"), do: extra_test_applications()
  defp extra_applications(_), do: [:crypto, :logger, :plug, :runtime_tools]

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(:"ci-test"), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:argon2_elixir, "~> 2.4"},
      {:absinthe, "~> 1.4"},
      {:absinthe_plug, "~> 1.4"},
      {:absinthe_phoenix, "~> 2.0"},
      {:blockbox, "~> 1.1"},
      {:broadway, "~> 0.6"},
      {:cors_plug, "~> 2.0"},
      {:ecto_ltree, "~> 0.3.0"},
      {:ecto_sql, "~> 3.4"},
      {:elixir_uuid, "~> 1.2"},
      {:ex_cldr_dates_times, "~> 2.0"},
      {:gettext, "~> 0.11"},
      {:gproc, "~> 0.8.0"},
      {:guardian, "~> 1.2"},
      {:jason, "~> 1.0"},
      {:phoenix, "~> 1.5"},
      {:phoenix_ecto, "~> 4.0"},
      {:phoenix_html, "~> 2.11"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:phoenix_pubsub, "~> 2.0"},
      {:plug_cowboy, "~> 2.3"},
      {:postgrex, ">= 0.0.0"},
      {:sendgrid, "~> 2.0"},
      {:tesla, "~> 1.4"},
      {:tzdata, "~> 1.0"},
      {:wormwood, "~> 0.1", only: [:"ci-test", :test]},
      {:zxcvbn, "~> 0.1"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate", "test"]
    ]
  end
end
