defmodule Accord.Repo.Migrations.SlackCache do
  use Ecto.Migration

  def change do
    create table(:cached_slack_users) do
      add :service_id, :string, primary_key: true
      add :is_bot, :boolean
      timestamps()
    end

    create unique_index(:cached_slack_users, [:service_id])

    create table(:oauth_states, primary_key: false) do
      add :state, :uuid, primary_key: true
      timestamps()
    end
  end
end
