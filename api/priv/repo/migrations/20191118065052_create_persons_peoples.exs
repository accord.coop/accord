defmodule Accord.Repo.Migrations.CreatePeoples do
  @moduledoc false
  use Ecto.Migration

  def change do
    create table(:persons, primary_key: false) do
      add :uuid, :uuid, primary_key: true
      add :preferred_name, :string
      add :locale, :map
      add :locale_updated_at, :naive_datetime
      timestamps()
    end

    create table(:peoples, primary_key: false) do
      add :uuid, :uuid, primary_key: true
      add :title, :string
      add :service_id, :string
      add :service_type, :string
      add :locale, :map
      add :locale_updated_at, :naive_datetime
      add :app_token, :string
      add :bot_token, :string
      timestamps()
    end

    create unique_index(:peoples, [:service_id])

    create table(:persons_peoples) do
      add :person_uuid, references(:persons, type: :uuid, column: :uuid, on_delete: :delete_all)
      add :people_uuid, references(:peoples, type: :uuid, column: :uuid, on_delete: :delete_all)
      timestamps()
    end

    create unique_index(
             :persons_peoples,
             [:person_uuid, :people_uuid],
             name: :persons_peoples_relation
           )

    create table(:peoples_peoples) do
      add :child_uuid, references(:peoples, type: :uuid, column: :uuid, on_delete: :delete_all)
      add :parent_uuid, references(:peoples, type: :uuid, column: :uuid, on_delete: :delete_all)
      timestamps()
    end

    create unique_index(
             :peoples_peoples,
             [:child_uuid, :parent_uuid],
             name: :peoples_peoples_child_parent_relation
           )

    create unique_index(
             :peoples_peoples,
             [:parent_uuid, :child_uuid],
             name: :peoples_peoples_parent_child_relation
           )

    create table(:personas, primary_key: false) do
      add :uuid, :uuid, primary_key: true
      add :service_id, :string
      add :service_type, :string
      add :service_properties, :map
      add :native_credentials, :map
      add :person_uuid, references(:persons, type: :uuid, column: :uuid, on_delete: :delete_all)
      timestamps()
    end

    create unique_index(:personas, [:service_id])

    create table(:sessions) do
      add :token, :text, primary_key: true
      add :expires, :naive_datetime
      add :persona_uuid, references(:personas, type: :uuid, column: :uuid, on_delete: :delete_all)
      timestamps()
    end
  end
end
