defmodule Accord.Repo.Migrations.STVScott do
  use Ecto.Migration

  def change do
    execute(
      "CREATE EXTENSION ltree",
      "DROP EXTENSION ltree"
    )

    create table(:cached_stv_scott_ballot_trees, primary_key: false) do
      add :decision_version_moment_uuid,
          references(:decision_version_moments, type: :uuid, column: :uuid, on_delete: :delete_all),
          null: false,
          primary_key: true

      add :path, :ltree, null: false, primary_key: true

      add :vote_power, :float, null: false
    end

    create table(:cached_stv_scott_allocations, primary_key: false) do
      add :decision_version_moment_uuid,
          references(:decision_version_moments, type: :uuid, column: :uuid, on_delete: :delete_all),
          primary_key: true

      add :iteration, :integer, primary_key: true

      add :recipient, :uuid, primary_key: true

      add :selection, :string, primary_key: true

      add :as_quotient, :float, null: false

      add :net_vote_power, :float, null: false
    end
  end
end
