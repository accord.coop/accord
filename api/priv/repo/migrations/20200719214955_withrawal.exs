defmodule Accord.Repo.Migrations.Withrawal do
  use Ecto.Migration

  def up do
    alter table(:decision_versions) do
      add_if_not_exists(:state_addendum, :text)
    end
  end

  def down do
    alter table(:decision_versions) do
      remove_if_exists(:state_addendum, :text)
    end
  end
end
