defmodule Accord.Repo.Migrations.Statics do
  use Ecto.Migration

  alias Accord.DecisionVersion.Types, as: DVTypes
  alias Accord.DecisionRole.Types, as: DRTypes
  alias Accord.DecisionSettings, as: Settings
  alias Settings.Types, as: DSTypes

  @replacements %{
    {"decision_roles", "role"} => [
      {"votable", DRTypes.votable(:str)},
      {"visible", DRTypes.visible(:str)}
    ],
    {"decision_versions", "state"} => [
      {"new", DVTypes.new(:str)},
      {"published", DVTypes.published(:str)},
      {"withdrawn", DVTypes.withdrawn(:str)},
      {"closed", DVTypes.closed(:str)}
    ]
  }

  @embed_replacements %{
    {"decision_versions", "settings"} => [
      # DecisionSettings methods
      %{
        up:
          {"'method' = 'approval'",
           """
             '{"method": "#{DSTypes.cardinal(:str)}"}'::jsonb
           """},
        down:
          {"'method' = '#{DSTypes.cardinal(:str)}'",
           """
             '{"method": "approval"}'::jsonb
           """}
      },
      %{
        up:
          {"'method' = 'plurality'",
           """
             '{"method": "#{DSTypes.plurality(:str)}"}'::jsonb
           """},
        down:
          {"'method' = '#{DSTypes.plurality(:str)}'",
           """
             '{"method": "plurality"}'::jsonb
           """}
      },
      %{
        up:
          {"'method' = 'consensus'",
           """
             '{
               "method": "#{DSTypes.plurality(:str)}",
               "parameter0_quantification": "#{DSTypes.proportional(:str)}",
               "parameter0_value": 1.0
             }'::jsonb
           """},
        down:
          {"'method' = '#{DSTypes.plurality(:str)}'",
           """
             '{"method": "consensus"}'::jsonb - 'parameter0_quantification'::text - 'parameter0_value'::text
           """}
      },
      %{
        up:
          {"'method' = 'twothirds'",
           """
             '{
               "method": "#{DSTypes.plurality(:str)}",
               "parameter0_quantification": "#{DSTypes.proportional(:str)}",
               "parameter0_value": #{2 / 3}
             }'::jsonb
           """},
        down:
          {"'method' = '#{DSTypes.plurality(:str)}' AND settings ->> 'parameter0_quantification' = '#{
             DSTypes.proportional(:str)
           }' AND settings ->> 'parameter0_value' BETWEEN 0.66 AND 0.67",
           """
             '{"method": "twothirds"}'::jsonb - 'parameter0_quantification'::text - 'parameter0_value'::text
           """}
      },
      # DecisionSettings quorum
      %{
        up:
          {"'quorum' = 'any'",
           """
             '{"quorum_quantification": "#{DSTypes.absolute(:str)}", "quorum_value": 1.0}'::jsonb - 'quorum'::text
           """},
        down:
          {"'quorum_quantification' = '#{DSTypes.absolute(:str)}' AND settings ->> 'quorum_value' = 1.0",
           """
             '{"quorum": "any"}'::jsonb - 'quorum_quantification'::text - 'quorum_value'::text
           """}
      },
      %{
        up:
          {"'quorum' = 'half'",
           """
             '{"quorum_quantification": "#{DSTypes.proportional(:str)}", "quorum_value": 0.5}'::jsonb - 'quorum'::text
           """},
        down:
          {"'quorum_quantification' = '#{DSTypes.proportional(:str)}' AND settings ->> 'quorum_value' = 0.5",
           """
             '{"quorum": "half"}'::jsonb - 'quorum_quantification'::text - 'quorum_value'::text
           """}
      },
      %{
        up:
          {"'quorum' = 'twothirds'",
           """
             '{"quorum_quantification": "#{DSTypes.proportional(:str)}", "quorum_value": #{2 / 3}}'::jsonb - 'quorum'::text
           """},
        down:
          {"'quorum_quantification' = '#{DSTypes.proportional(:str)}' AND settings ->> 'quorum_value' BETWEEN 0.66 AND 0.67",
           """
             '{"quorum": "twothirds"}'::jsonb - 'quorum_quantification'::text - 'quorum_value'::text
           """}
      },
      %{
        up:
          {"'quorum' = 'all'",
           """
             '{"quorum_quantification": "#{DSTypes.proportional(:str)}", "quorum_value": 1.0}'::jsonb - 'quorum'::text
           """},
        down:
          {"'quorum_quantification' = '#{DSTypes.proportional(:str)}' AND settings ->> 'quorum_value' = 1.0",
           """
             '{"quorum": "all"}'::jsonb - 'quorum_quantification'::text - 'quorum_value'::text
           """}
      }
      # todo: migrate Vote ballot column as needed…
    ]
  }

  def up do
    Enum.each(@replacements, fn {{table, column}, values} ->
      Enum.each(values, fn {from, to} ->
        execute "UPDATE #{table} SET #{column} = regexp_replace(#{column}, '^#{from}$', '#{to}')"
      end)
    end)

    Enum.each(@embed_replacements, fn {{table, column}, operations} ->
      Enum.each(operations, fn %{up: {where, set}} ->
        execute "UPDATE #{table} SET #{column} = #{column} || #{set} WHERE #{column} ->> #{where}"
      end)
    end)
  end

  def down do
    Enum.each(@replacements, fn {{table, column}, values} ->
      Enum.each(values, fn {from, to} ->
        execute "UPDATE #{table} SET #{column} = regexp_replace(#{column}, '^#{to}$', '#{from}')"
      end)
    end)

    Enum.each(@embed_replacements, fn {{table, column}, operations} ->
      Enum.each(operations, fn %{down: {where, set}} ->
        execute "UPDATE #{table} SET #{column} = #{column} || #{set} WHERE #{column} ->> #{where}"
      end)
    end)
  end
end
