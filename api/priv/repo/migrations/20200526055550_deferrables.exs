defmodule Accord.Repo.Migrations.Deferrables do
  @moduledoc """
    This migration drops Ecto's automatic foreign key constraints, which don’t support DEFERRABLE,
    and adds constraints manually which do. This became necessary when switching to using UUID
    primary keys on major entities, since non-deferred fkey constraints would halt any transactions
    inserting associated entities.
  """
  use Ecto.Migration

  defp drop_all_fkey_constraints() do
    # Humans ontology

    drop constraint("peoples_peoples", "peoples_peoples_child_uuid_fkey")
    drop constraint("peoples_peoples", "peoples_peoples_parent_uuid_fkey")

    drop constraint("persons_peoples", "persons_peoples_person_uuid_fkey")
    drop constraint("persons_peoples", "persons_peoples_people_uuid_fkey")

    drop constraint("personas", "personas_person_uuid_fkey")

    # Decisions ontology

    drop constraint("decision_moments", "decision_moments_decision_uuid_fkey")

    drop constraint("decision_roles", "decision_roles_decision_uuid_fkey")
    drop constraint("decision_roles", "decision_roles_people_uuid_fkey")

    drop constraint("decision_versions", "decision_versions_author_uuid_fkey")
    drop constraint("decision_versions", "decision_versions_decision_uuid_fkey")

    drop constraint("votes", "votes_person_uuid_fkey")
    drop constraint("votes", "votes_decision_version_uuid_fkey")
  end

  defp add_all_fkey_constraints(addendum) do
    execute("""
      ALTER TABLE peoples_peoples

        ADD CONSTRAINT peoples_peoples_child_uuid_fkey
          FOREIGN KEY (child_uuid) REFERENCES peoples (uuid)
            ON DELETE CASCADE#{addendum},

        ADD CONSTRAINT peoples_peoples_parent_uuid_fkey
          FOREIGN KEY (parent_uuid) REFERENCES peoples (uuid)
            ON DELETE CASCADE#{addendum}
    """)

    execute("""
      ALTER TABLE persons_peoples

        ADD CONSTRAINT persons_peoples_person_uuid_fkey
          FOREIGN KEY (person_uuid) REFERENCES persons (uuid)
            ON DELETE CASCADE#{addendum},

        ADD CONSTRAINT persons_peoples_people_uuid_fkey
          FOREIGN KEY (people_uuid) REFERENCES peoples (uuid)
            ON DELETE CASCADE#{addendum}
    """)

    execute("""
      ALTER TABLE personas

        ADD CONSTRAINT personas_person_uuid_fkey
          FOREIGN KEY (person_uuid) REFERENCES persons (uuid)
            ON DELETE CASCADE#{addendum}
    """)

    # `sessions`, which contains `sessions_persona_uuid_fkey`, is skipped since the Persona should
    # certainly exist before a Session is created.

    # Decisions ontology

    execute("""
      ALTER TABLE decision_moments

        ADD CONSTRAINT decision_moments_decision_uuid_fkey
          FOREIGN KEY (decision_uuid) REFERENCES decisions (uuid)
            ON DELETE CASCADE#{addendum}
    """)

    execute("""
      ALTER TABLE decision_roles

        ADD CONSTRAINT decision_roles_decision_uuid_fkey
          FOREIGN KEY (decision_uuid) REFERENCES decisions (uuid)
            ON DELETE CASCADE#{addendum},

        ADD CONSTRAINT decision_roles_people_uuid_fkey
          FOREIGN KEY (people_uuid) REFERENCES peoples (uuid)
            ON DELETE CASCADE#{addendum}
    """)

    execute("""
      ALTER TABLE decision_versions

        ADD CONSTRAINT decision_versions_author_uuid_fkey
          FOREIGN KEY (author_uuid) REFERENCES persons (uuid)#{addendum},

        ADD CONSTRAINT decision_versions_decision_uuid_fkey
          FOREIGN KEY (decision_uuid) REFERENCES decisions (uuid)
            ON DELETE CASCADE#{addendum}
    """)

    execute("""
      ALTER TABLE votes

        ADD CONSTRAINT votes_person_uuid_fkey
          FOREIGN KEY (person_uuid) REFERENCES persons (uuid)#{addendum},

        ADD CONSTRAINT votes_decision_version_uuid_fkey
          FOREIGN KEY (decision_version_uuid) REFERENCES decision_versions (uuid)
            ON DELETE CASCADE#{addendum}
    """)
  end

  def up do
    drop_all_fkey_constraints()
    add_all_fkey_constraints(" DEFERRABLE INITIALLY DEFERRED")
  end

  def down do
    drop_all_fkey_constraints()
    add_all_fkey_constraints("")
  end
end
