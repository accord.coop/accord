defmodule Accord.Repo.Migrations.Delegations do
  use Ecto.Migration

  def up do
    create table(:delegations, primary_key: false) do
      add :uuid, :uuid, primary_key: true

      add :delegator_uuid,
          references(:persons, type: :uuid, column: :uuid, on_delete: :delete_all),
          null: false

      add :delegatee_uuid,
          references(:persons, type: :uuid, column: :uuid, on_delete: :delete_all),
          null: false

      add :scope_people_uuid,
          references(:peoples, type: :uuid, column: :uuid, on_delete: :delete_all),
          null: false

      add :starts, :naive_datetime, null: false
      add :expires, :naive_datetime, null: false
      add :check, :string, null: false
      add :precedence, :integer, null: false

      timestamps()
    end

    create unique_index(:delegations, [:delegator_uuid, :precedence])

    alter table(:votes) do
      modify :person_uuid, :uuid, null: false
      modify :decision_version_uuid, :uuid, null: false
      modify :check, :string, null: false
    end

    rename table(:decision_moments), to: table(:backup__decision_moments)

    create table(:decision_version_moments, primary_key: false) do
      add :uuid, :uuid, primary_key: true

      add :type, :string
      add :moment, :naive_datetime
      add :executed, :integer

      add :decision_version_uuid,
          references(:decision_versions, type: :uuid, column: :uuid, on_delete: :delete_all)

      timestamps()
    end

    create table(:decision_version_results) do
      add :summary, :map

      add :decision_version_moment_uuid,
          references(:decision_version_moments, type: :uuid, column: :uuid, on_delete: :delete_all)

      timestamps()
    end

    create unique_index(:decision_version_results, [:decision_version_moment_uuid],
             name: :decision_version_moment_relation
           )
  end

  def down do
    drop_if_exists unique_index(:decision_version_results, [:decision_version_moment_uuid],
                     name: :decision_version_moment_relation
                   )

    drop_if_exists table(:decision_version_results)

    drop_if_exists table(:decision_version_moments)

    rename table(:backup__decision_moments), to: table(:decision_moments)

    drop_if_exists unique_index(:delegations, [:delegator_uuid, :precedence])

    drop_if_exists table(:delegations)

    alter table(:votes) do
      modify :person_uuid, :uuid, null: true
      modify :decision_version_uuid, :uuid, null: true
      modify :check, :string, null: true
    end
  end
end
