defmodule Accord.Repo.Migrations.CreateDecisions do
  use Ecto.Migration

  def change do
    create table(:decisions, primary_key: false) do
      add :uuid, :uuid, primary_key: true
      timestamps()
    end

    create table(:decision_versions, primary_key: false) do
      add :uuid, :uuid, primary_key: true
      add :language, :string
      add :title, :text
      add :body, :text
      add :options, :map
      add :settings, :map
      add :state, :string
      add :service_id, :string

      add :decision_uuid,
          references(:decisions, type: :uuid, column: :uuid, on_delete: :delete_all)

      add :author_uuid,
          references(:persons, type: :uuid, column: :uuid)

      timestamps()
    end

    create unique_index(:decision_versions, [:service_id])

    create table(:decision_moments) do
      add :decision_uuid,
          references(:decisions, type: :uuid, column: :uuid, on_delete: :delete_all)

      add :type, :string
      add :moment, :naive_datetime
    end

    create table(:decision_roles) do
      add :decision_uuid,
          references(:decisions, type: :uuid, column: :uuid, on_delete: :delete_all)

      add :role, :string

      add :people_uuid,
          references(:peoples, type: :uuid, column: :uuid, on_delete: :delete_all)
    end

    create table(:votes, primary_key: false) do
      add :uuid, :uuid, primary_key: true

      add :person_uuid,
          references(:persons, type: :uuid, column: :uuid)

      add :decision_version_uuid,
          references(:decision_versions, type: :uuid, column: :uuid, on_delete: :delete_all)

      add :ballot, :map
      add :check, :string
      timestamps()
    end

    create unique_index(:votes, [:person_uuid, :decision_version_uuid])
  end
end
