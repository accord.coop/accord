Postgrex.Types.define(
  Accord.PostgresTypes,
  [EctoLtree.Postgrex.Lquery, EctoLtree.Postgrex.Ltree] ++
    Ecto.Adapters.Postgres.extensions()
)
