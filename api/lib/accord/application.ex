defmodule Accord.Application do
  @moduledoc false

  use Application

  def start(_type, args) do
    # Start app children
    opts = [strategy: :one_for_one, name: Accord.Supervisor]
    Supervisor.start_link(children(args), opts)
  end

  defp assure_dev_team(dev_team) do
    [team_id | [app_token]] = String.split(dev_team, ":")

    case Accord.People.assure(:team, {:slack, team_id}, app_token: app_token)
         |> Accord.Repo.commit() do
      {:ok, _} -> :ok
      err -> err
    end
  end

  def start_phase(:bootstrap, _, _) do
    case Application.get_env(:accord, AccordSlack)[:dev_team] do
      "" <> dev_team -> assure_dev_team(dev_team)
      _ -> :ok
    end
  end

  defp children(env: :test), do: test_children()
  defp children(env: :"ci-test"), do: test_children()

  defp children(_),
    do: [
      Accord.Repo,
      Accord.Schedule,
      AccordWeb.Endpoint,
      AccordWeb.Pipeline,
      {Phoenix.PubSub, [name: Accord.PubSub, adapter: Phoenix.PubSub.PG2]}
    ]

  defp test_children(),
    do:
      children(nil) ++
        [
          Plug.Cowboy.child_spec(
            scheme: :http,
            plug: AccordWeb.MockServer,
            options: [port: 4004]
          )
        ]

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    AccordWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
