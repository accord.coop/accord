defmodule Accord.Repo do
  @moduledoc false
  use Ecto.Repo,
    otp_app: :accord,
    adapter: Ecto.Adapters.Postgres

  alias Ecto.Multi

  @type multi_method :: :insert | :update | :delete | :run
  @type multi_payload ::
          Ecto.Changeset.t()
          | Ecto.Schema.t()
          | Ecto.Multi.fun(Ecto.Changeset.t() | Ecto.Schema.t())
  @type multi_payload_with_opts :: {multi_payload, Keyword.t()}

  @type independent_action :: {atom, multi_method, multi_payload_with_opts}
  @type dependent_action :: {atom, [atom], multi_method, multi_payload_with_opts}

  @type action :: independent_action | dependent_action

  @spec now() :: NaiveDateTime.t()
  def now(), do: NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)

  @spec to_unix_seconds(NaiveDateTime.t()) :: NaiveDateTime.t()
  def to_unix_seconds(moment), do: NaiveDateTime.diff(moment, ~N[1970-01-01 00:00:00], :second)

  @spec assign(term, Map.t(), Map.t()) :: any
  def assign(key, map1, _) when is_map_key(map1, key), do: Map.get(map1, key)
  def assign(key, _, map2) when is_map_key(map2, key), do: Map.get(map2, key)

  @spec get_rel(Ecto.Schema.t() | nil, atom) :: Ecto.Schema.t() | nil
  def get_rel(nil, _key), do: nil
  def get_rel(struct, key), do: struct |> preload(key) |> Map.get(key)

  @doc """
    This establishes a convenience API for working with Multi. It accepts a List of “actions”,
    each of which contains information needed to carry out a Multi operation.
  """
  # todo: support atomic actions, like {:update, changeset}
  @spec commit([action]) :: {:ok, any()} | {:error, any()}
  def commit(actions) do
    action_groups =
      Map.merge(
        %{independent: [], dependent: []},
        Enum.group_by(actions, &action_type/1)
      )

    Multi.new()
    |> Multi.run(:__defer_constraints__, fn repo, _ ->
      repo.query!("SET CONSTRAINTS ALL DEFERRED;")
      {:ok, :ok}
    end)
    |> apply_actions(action_groups.independent)
    |> apply_actions(action_groups.dependent)
    |> Accord.Repo.transaction()
  end

  defp apply_actions(multi, actions) do
    Enum.reduce(
      actions,
      multi,
      &apply_action/2
    )
  end

  defp apply_action({name, function_name, {payload_fn, opts}}, multi) do
    case function_name do
      :run -> apply(Multi, function_name, [multi, name, payload_fn])
      :error -> apply(Multi, function_name, [multi, name, payload_fn])
      _ -> apply(Multi, function_name, [multi, name, payload_fn, opts])
    end
  end

  defp apply_action({name, _with_names, function_name, {payload_fn, opts}}, multi) do
    # [wsn] maybe dependencies can be resolved using `with_names`?
    case function_name do
      :run -> apply(Multi, function_name, [multi, name, payload_fn])
      :error -> apply(Multi, function_name, [multi, name, payload_fn])
      _ -> apply(Multi, function_name, [multi, name, payload_fn, opts])
    end
  end

  defp action_type({_, _, _}), do: :independent
  defp action_type({_, _, _, _}), do: :dependent

  def nothing_to_delete(name), do: [{name, :run, {fn _, _ -> {:ok, nil} end, []}}]
  def nothing_to_insert(name, struct), do: [{name, :run, {fn _, _ -> {:ok, struct} end, []}}]
  def error(name, err), do: [{name, :error, {err, []}}]
end

defmodule Accord.Schema do
  defmacro __using__(_) do
    quote do
      use Ecto.Schema
      @primary_key {:uuid, :binary_id, autogenerate: true}
      @foreign_key_type :binary_id
      @derive {Phoenix.Param, key: :uuid}
    end
  end
end
