defmodule Accord.Statics do
  defmacro __using__(_) do
    quote do
      import Accord.Statics, only: [declare: 2, declare_namespaced: 2]
    end
  end

  defmacro declare(namespace, name) do
    static_value =
      "accord__#{Atom.to_string(namespace)}__#{Atom.to_string(name)}" |> String.to_atom()

    quote do
      def unquote(name)(type \\ :atom)
      def unquote(name)(:str), do: unquote(Atom.to_string(static_value))
      def unquote(name)(_), do: unquote(static_value)
    end
  end

  defmacro declare_namespaced({method_namespace, value_namespace}, name) do
    method_name =
      "#{Atom.to_string(method_namespace)}_#{Atom.to_string(name)}" |> String.to_atom()

    static_value =
      "accord__#{Atom.to_string(value_namespace)}__#{Atom.to_string(name)}" |> String.to_atom()

    quote do
      def unquote(method_name)(type \\ :atom)
      def unquote(method_name)(:str), do: unquote(Atom.to_string(static_value))
      def unquote(method_name)(_), do: unquote(static_value)
    end
  end
end
