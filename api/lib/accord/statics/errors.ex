defmodule Accord.Errors do
  use Accord.Statics

  declare(:error, :not_found)
  declare(:error, :bad_args)
  declare(:error, :circular)
  declare(:error, :delegates_to_self)
  declare(:error, :no_access)
  declare(:error, :save_failed)
  declare(:error, :weak_pw)
  declare(:error, :get_slack_channel_users_failed)
  declare(:error, :no_slack_dm)
end
