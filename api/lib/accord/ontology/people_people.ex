defmodule Accord.PeoplePeople do
  @moduledoc """
    The schema and changeset for the associations between People entities.
  """

  alias Accord.{Repo, People, PeoplePeople}

  import Ecto.Changeset
  import Ecto.Query, only: [{:from, 2}]

  use Ecto.Schema

  @type t :: %__MODULE__{}

  schema "peoples_peoples" do
    belongs_to :parent, People, foreign_key: :parent_uuid, references: :uuid, type: :binary_id
    belongs_to :child, People, foreign_key: :child_uuid, references: :uuid, type: :binary_id
    timestamps()
  end

  @doc false
  def changeset(child, parent) do
    %PeoplePeople{}
    |> cast(%{child_uuid: child.uuid, parent_uuid: parent.uuid}, [:child_uuid, :parent_uuid])
    |> put_assoc(:child, child)
    |> put_assoc(:parent, parent)
    |> unique_constraint(:child, name: :peoples_peoples_child_parent_relation)
    |> unique_constraint(:parent, name: :peoples_peoples_parent_child_relation)
  end

  @spec query(People.t(), People.t()) :: Ecto.Query.t()
  defp query(%People{} = child, %People{} = parent) do
    from(
      pp in PeoplePeople,
      where: pp.child_uuid == ^child.uuid and pp.parent_uuid == ^parent.uuid
    )
  end

  @spec query(People.t(), :children) :: Ecto.Query.t()
  defp query(%People{} = people, :children) do
    from(
      p in People,
      join: pp in PeoplePeople,
      on: pp.parent_uuid == ^people.uuid and pp.child_uuid == p.uuid
    )
  end

  @spec query(People.t(), :parents) :: Ecto.Query.t()
  defp query(%People{} = people, :parents) do
    from(
      p in People,
      join: pp in PeoplePeople,
      on: pp.child_uuid == ^people.uuid and pp.parent_uuid == p.uuid
    )
  end

  @spec associated?(People.t() | nil, People.t() | nil) ::
          false | {true, %{child: People.t(), parent: People.t(), assoc: PeoplePeople.t()}}

  def associated?(nil, _), do: false
  def associated?(_, nil), do: false

  def associated?(%People{} = people_x, %People{} = people_y) do
    x_as_parent = query(people_y, people_x) |> Repo.one()
    y_as_parent = query(people_x, people_y) |> Repo.one()

    unless x_as_parent !== nil or y_as_parent !== nil do
      false
    else
      if x_as_parent !== nil do
        {true, %{child: people_y, parent: people_x, assoc: x_as_parent}}
      else
        {true, %{child: people_x, parent: people_y, assoc: y_as_parent}}
      end
    end
  end

  @spec associate(atom, People.t(), People.t()) :: [Repo.action()]
  def associate(name \\ :assoc, %People{} = child, %People{} = parent) do
    child_uuid = child.uuid
    parent_uuid = parent.uuid

    case associated?(child, parent) do
      {true, %{child: %{uuid: ^child_uuid}, parent: %{uuid: ^parent_uuid}, assoc: assoc}} ->
        Repo.nothing_to_insert(name, assoc)

      {true, _} ->
        Repo.error(name, :opposite_relation_found)

      false ->
        [{name, :insert, {changeset(child, parent), []}}]
    end
  end

  @spec dissociate(atom, People.t(), People.t()) :: [Repo.action()]
  def dissociate(name \\ :assoc, %People{} = people_x, %People{} = people_y) do
    case associated?(people_x, people_y) do
      false -> Repo.nothing_to_delete(name)
      {true, %{assoc: assoc}} -> [{name, :delete, {assoc, []}}]
    end
  end

  @spec get_child_peoples(People.t()) :: [People.t()]
  def get_child_peoples(%People{} = people) do
    query(people, :children) |> Repo.all()
  end

  @spec get_parent_peoples(People.t()) :: [People.t()]
  def get_parent_peoples(%People{} = people) do
    query(people, :parents) |> Repo.all()
  end
end
