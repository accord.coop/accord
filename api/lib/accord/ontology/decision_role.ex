defmodule Accord.DecisionRole.Types do
  use Accord.Statics

  declare(:"decision-role", :votable)
  declare(:"decision-role", :visible)
end

defmodule Accord.DecisionRole do
  @moduledoc """
    The schema and changeset for Decision role entities.
  """

  alias Accord.{Repo, Decision, People, PersonPeople, DecisionRole, DecisionVersion, Vote}
  alias Accord.DecisionRole.Types

  import Ecto.Changeset
  import Ecto.Query

  use Ecto.Schema

  @type t :: %__MODULE__{}

  @default_role Types.votable(:str)

  schema "decision_roles" do
    belongs_to :decision, Decision,
      foreign_key: :decision_uuid,
      references: :uuid,
      type: :binary_id

    belongs_to :people, People, foreign_key: :people_uuid, references: :uuid, type: :binary_id
    field :role, :string, default: @default_role
  end

  @doc false
  def changeset(role, attrs \\ %{}) do
    role
    |> cast(attrs, [:role])
    |> assoc_constraint(:decision)
    |> assoc_constraint(:people)
  end

  @spec new(Decision.t(), People.t(), String.t()) :: Ecto.Changeset.t()
  def new(%Decision{} = decision, %People{} = people, role) do
    %DecisionRole{}
    |> changeset(%{role: role})
    |> put_assoc(:decision, decision)
    |> put_assoc(:people, people)
  end

  @spec add_to_decision(Decision.t(), People.t(), String.t()) :: [Repo.action()]
  def add_to_decision(%Decision{} = decision, %People{} = people, role \\ @default_role) do
    [{:decision_role, :insert, {new(decision, people, role), []}}]
  end

  @spec people_uuids_with_role(Decision.t(), String.t(), atom) :: Ecto.Query.t()
  def people_uuids_with_role(%Decision{} = decision, role, which_select) do
    case which_select do
      :struct ->
        from(
          dr in DecisionRole,
          where: dr.decision_uuid == ^decision.uuid and dr.role == ^role,
          select: [:people_uuid]
        )

      _ ->
        from(
          dr in DecisionRole,
          where: dr.decision_uuid == ^decision.uuid and dr.role == ^role,
          select: dr.people_uuid
        )
    end
  end

  @spec people_service_ids_votable(Decision.t()) :: [String.t()]
  def people_service_ids_votable(%Decision{} = decision) do
    from(
      p in People,
      join: dr in subquery(people_uuids_with_role(decision, Types.votable(:str), :struct)),
      on: dr.people_uuid == p.uuid,
      select: p.service_id
    )
    |> Repo.all()
  end

  @spec decision_uuids_with_role_from_person_uuid(Ecto.UUID.t(), String.t()) :: Ecto.SubQuery.t()
  defp decision_uuids_with_role_from_person_uuid(person_uuid, role) do
    people_uuids = PersonPeople.get_people_uuids_for_person_uuid(person_uuid)

    subquery(
      from(
        dr in DecisionRole,
        join: p in ^people_uuids,
        on: dr.people_uuid == p.uuid and dr.role == ^role,
        select: [:decision_uuid]
      )
    )
  end

  @spec votable_decision_versions_for_person(String.t(), String.t() | nil) ::
          %{voted: [DecisionVersion.t()], unvoted: [DecisionVersion.t()]}
  def votable_decision_versions_for_person(person_uuid, nil),
    do: votable_decision_versions_for_person(person_uuid, DecisionVersion.Types.published(:str))

  def votable_decision_versions_for_person(person_uuid, state) do
    decision_uuids = decision_uuids_with_role_from_person_uuid(person_uuid, Types.votable(:str))

    inclusive_query =
      from(
        dv in DecisionVersion,
        join: dr in ^decision_uuids,
        on: dv.decision_uuid == dr.decision_uuid and dv.state == ^state
      )

    inclusive_subquery = subquery(inclusive_query)

    voted_decision_version_uuids =
      from(
        v in Vote,
        join: dv in ^inclusive_subquery,
        on: v.person_uuid == ^person_uuid and v.decision_version_uuid == dv.uuid,
        select: v.decision_version_uuid
      )
      |> Repo.all()
      |> MapSet.new()

    %{voted: [], unvoted: []}
    |> Map.merge(
      inclusive_query
      |> Repo.all()
      |> Enum.group_by(fn %DecisionVersion{uuid: uuid} ->
        if MapSet.member?(voted_decision_version_uuids, uuid) do
          :voted
        else
          :unvoted
        end
      end)
    )
  end
end
