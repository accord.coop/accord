defmodule Accord.People do
  @moduledoc """
    The schema and changeset for People entities.
  """

  alias Accord.{Repo, Person, People, PersonPeople, PeoplePeople, Decision, DecisionRole, Locale}

  import Ecto.Changeset
  import Ecto.Query, only: [{:from, 2}]

  use Accord.Schema

  @type t :: %__MODULE__{}

  @empty_title ""

  @type signature ::
          {:accord, String.t()}
          | {:slack, String.t()}
          | {:slack, String.t(), String.t()}

  schema "peoples" do
    field :title, :string, default: @empty_title
    field :service_id, :string
    field :service_type, :string
    field :bot_token, :string
    field :app_token, :string

    embeds_one :locale, Locale, on_replace: :delete
    field :locale_updated_at, :naive_datetime

    many_to_many :persons, Person,
      join_through: PersonPeople,
      join_keys: [person_uuid: :uuid, people_uuid: :uuid],
      unique: true

    many_to_many :child_peoples, People,
      join_through: PeoplePeople,
      join_keys: [parent_uuid: :uuid, child_uuid: :uuid],
      unique: true

    many_to_many :parent_peoples, People,
      join_through: PeoplePeople,
      join_keys: [child_uuid: :uuid, parent_uuid: :uuid],
      unique: true

    many_to_many :decisions, Decision,
      join_through: DecisionRole,
      join_keys: [decision_uuid: :uuid, people_uuid: :uuid]

    has_many :decision_roles, DecisionRole

    timestamps()
  end

  @doc false
  def changeset(people, attrs) do
    people
    |> cast(attrs, [
      :title,
      :service_id,
      :service_type,
      :locale_updated_at,
      :bot_token,
      :app_token
    ])
    |> validate_required(:service_id)
    |> unique_constraint(:service_id)
    |> cast_embed(:locale, with: &Locale.changeset/2)
    |> put_locale_timestamp
  end

  @spec put_locale_timestamp(Ecto.Changeset.t()) :: Ecto.Changeset.t()
  defp put_locale_timestamp(%Ecto.Changeset{changes: %{locale: _}} = changeset) do
    changeset |> put_change(:locale_updated_at, Repo.now())
  end

  defp put_locale_timestamp(%Ecto.Changeset{} = changeset), do: changeset

  @spec service_id(signature) :: String.t()
  defp service_id({:accord, people_uuid}), do: "accord︰#{people_uuid}"
  defp service_id({:slack, team_id}), do: "slack︰#{team_id}"
  defp service_id({:slack, team_id, channel_id}), do: "slack︰#{team_id}︰#{channel_id}"

  @spec get_by_service_id(String.t()) :: People.t() | nil
  def get_by_service_id(service_id) do
    from(p in People, where: p.service_id == ^service_id) |> Repo.one()
  end

  @spec get_slack_channel_id(People.t()) :: String.t()
  def get_slack_channel_id(%People{service_id: service_id}), do: get_slack_channel_id(service_id)

  @spec get_slack_channel_id(String.t()) :: String.t()
  def get_slack_channel_id("slack︰" <> team_scoped_channel) do
    [_team_id | [channel_id]] = team_scoped_channel |> String.split("︰")
    channel_id
  end

  @spec get_slack_team_id(People.t()) :: String.t()
  def get_slack_team_id(%People{service_id: service_id}), do: get_slack_team_id(service_id)

  @spec get_slack_team_id(String.t()) :: String.t()
  def get_slack_team_id("slack︰" <> team_scoped_channel) do
    [team_id | _] = team_scoped_channel |> String.split("︰")
    team_id
  end

  @spec get(signature) :: People.t() | nil
  def get({:accord, people_id}), do: get_by_service_id(service_id({:accord, people_id}))

  def get({:slack, team_id}), do: get_by_service_id(service_id({:slack, team_id}))

  def get({:slack, team_id, channel_id}),
    do: get_by_service_id(service_id({:slack, team_id, channel_id}))

  @spec get(term()) :: People.t() | nil
  def get(id), do: Repo.get(People, id)

  @spec new(String.t(), String.t(), Keyword.t()) :: Ecto.Changeset.t()
  def new(service_id, service_type, opts) do
    changeset(
      %People{},
      %{
        service_id: service_id,
        service_type: service_type,
        title: opts[:title],
        locale: opts[:locale],
        app_token: opts[:app_token],
        bot_token: opts[:bot_token]
      }
    )
  end

  @spec new(signature, Keyword.t()) :: Ecto.Changeset.t()
  def new(people_signature, opts \\ nil)

  def new({:slack, team_id}, opts) do
    new(service_id({:slack, team_id}), "slack︰team", opts)
  end

  def new({:slack, team_id, channel_id}, opts) do
    new(service_id({:slack, team_id, channel_id}), "slack︰channel", opts)
  end

  def new({:accord, title}, _opts) do
    new(service_id({:accord, Ecto.UUID.generate()}), "accord︰people", title: title)
  end

  @spec create(atom, signature, Keyword.t()) :: [Repo.action()]
  def create(name \\ :people, signature, opts \\ []) do
    [{name, :insert, {People.new(signature, opts), [on_conflict: :nothing]}}]
  end

  @spec assure_on_conflict(Keyword.t() | nil) :: [
          on_conflict: {:replace, [atom]},
          conflict_target: [atom],
          returning: [atom] | boolean
        ]

  defp assure_on_conflict(nil), do: assure_on_conflict([])

  defp assure_on_conflict(opts) do
    [
      on_conflict: {:replace, [:updated_at | Keyword.keys(opts)]},
      conflict_target: [:service_id],
      returning: true
    ]
  end

  @spec assure_associated({:slack, String.t(), String.t()}, Keyword.t()) :: [Repo.action()]
  def assure_associated({:slack, team_id, channel_id}, opts \\ []) do
    assure(:team, {:slack, team_id}, opts[:team_opts]) ++
      assure(:channel, {:slack, team_id, channel_id}, opts[:channel_opts]) ++
      [
        {:people_people_assoc, [:team, :channel], :insert,
         {fn %{channel: channel, team: team} -> PeoplePeople.changeset(channel, team) end,
          [on_conflict: :nothing]}}
      ]
  end

  @spec assure(atom, signature, Keyword.t()) :: [Repo.action()]
  def assure(name, signature, opts \\ []) do
    [
      {
        name,
        :insert,
        {
          People.new(signature, opts),
          assure_on_conflict(opts)
        }
      }
    ]
  end

  @behaviour Access

  @impl true
  def fetch(%People{} = people, key), do: Map.fetch(people, key)

  @impl true
  def get_and_update(%People{} = people, key, fun), do: Map.get_and_update(people, key, fun)

  @impl true
  def pop(%People{} = people, key), do: Map.pop(people, key, nil)
end
