defmodule Accord.Person do
  @moduledoc """
  The schema and changeset for Person entities.
  """

  alias Accord.{Repo, Person, Persona, Locale, DecisionVersion, Caches}

  import Ecto.Changeset

  use Accord.Schema

  @type t :: %__MODULE__{}

  @empty_preferred_name ""

  schema "persons" do
    field :preferred_name, :string, default: @empty_preferred_name

    embeds_one :locale, Locale, on_replace: :update
    field :locale_updated_at, :naive_datetime

    has_many :personas, Accord.Persona

    has_many :authored_decision_versions, DecisionVersion, foreign_key: :author_uuid

    many_to_many :peoples, Accord.People,
      join_through: Accord.PersonPeople,
      join_keys: [person_uuid: :uuid, people_uuid: :uuid],
      unique: true

    timestamps()
  end

  @doc false
  def changeset(person, attrs \\ %{}) do
    person
    |> cast(attrs, [
      :preferred_name
    ])
    |> cast_embed(:locale, with: &Locale.changeset/2)
    |> put_locale_timestamp
  end

  @spec put_locale_timestamp(Ecto.Changeset.t()) :: Ecto.Changeset.t()
  defp put_locale_timestamp(%Ecto.Changeset{changes: %{locale: _}} = changeset) do
    changeset |> put_change(:locale_updated_at, Repo.now())
  end

  defp put_locale_timestamp(%Ecto.Changeset{} = changeset), do: changeset

  @spec get(term()) :: Person.t() | nil
  def get(id), do: Repo.get(Person, id)

  @spec get_by_email(String.t()) :: Person.t() | nil
  def get_by_email(email) do
    Persona.get({:accord, email, nil}) |> Repo.get_rel(:person)
  end

  @spec get_slack_user_id(Person.t()) :: String.t() | nil
  def get_slack_user_id(%Person{} = person) do
    case Repo.get_rel(person, :personas)
         |> Enum.find(fn %Persona{service_type: service_type} -> service_type === "slack︰user" end) do
      nil -> nil
      %Persona{service_id: service_id} -> Persona.get_slack_user_id(service_id)
    end
  end

  @spec new(Keyword.t()) :: Ecto.Changeset.t()
  def new(opts) do
    changeset(%Person{locale: opts[:locale]}, %{
      preferred_name: opts[:preferred_name],
      locale_updated_at: Repo.now()
    })
  end

  @spec create_or_assure_with_persona(
          :assure | :create,
          Persona.signature(),
          Keyword.t()
        ) :: [Repo.action()]

  defp create_or_assure_with_persona(which, persona_signature, person_opts) do
    persona_service_id = Persona.service_id(persona_signature)

    case person_opts[:is_slack_bot] do
      nil ->
        Repo.nothing_to_insert(:slack_user_cached, Caches.SlackUser.get(persona_service_id))

      is_bot ->
        Caches.SlackUser.create(%{
          service_id: persona_service_id,
          is_bot: is_bot
        })
    end ++
      case Persona.get(persona_signature) do
        %Persona{} = persona ->
          case which do
            :assure ->
              Repo.nothing_to_insert(:persona, persona) ++
                case person_opts[:locale] do
                  nil ->
                    Repo.nothing_to_insert(:person, persona |> Repo.get_rel(:person))

                  locale ->
                    [
                      {:person, :update,
                       {persona
                        |> Repo.get_rel(:person)
                        |> change()
                        |> put_embed(:locale, locale, with: &Locale.changeset/2)
                        |> put_locale_timestamp, []}}
                    ]
                end

            :create ->
              Repo.error(:persona, :service_id_exists)
          end

        nil ->
          [
            {:person, :insert, {Person.new(person_opts), []}},
            {:persona, :insert,
             {fn %{person: person} -> Persona.new(persona_signature, person) end,
              [on_conflict: :raise]}}
          ]
      end
  end

  @spec assure_with_persona(Persona.signature(), Keyword.t()) :: [Repo.action()]
  def assure_with_persona(persona_signature, person_opts \\ nil),
    do: create_or_assure_with_persona(:assure, persona_signature, person_opts)

  @spec create_with_persona(Persona.signature(), Keyword.t()) :: [Repo.action()]
  def create_with_persona(persona_signature, person_opts \\ nil),
    do: create_or_assure_with_persona(:create, persona_signature, person_opts)

  @behaviour Access

  @impl true
  def fetch(%Person{} = person, key), do: Map.fetch(person, key)

  @impl true
  def get_and_update(%Person{} = person, key, fun), do: Map.get_and_update(person, key, fun)

  @impl true
  def pop(%Person{} = person, key), do: Map.pop(person, key, nil)
end
