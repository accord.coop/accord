defmodule Accord.Vote do
  @moduledoc """
    The schema and changeset for Vote entities.
  """

  alias Accord.{Repo, Person, DecisionVersion, Vote, PersonPeople, Errors}

  import Ecto.Changeset
  import Ecto.Query, only: [{:from, 2}]

  use Accord.Schema

  @type t :: %__MODULE__{}

  @type choice_id :: Ecto.UUID.t() | nil

  @type choices :: %{
          choice_id => %{
            required(:value) => float,
            optional(:comment) => String.t()
          }
        }

  schema "votes" do
    field :ballot, :map
    field :check, :string

    belongs_to :person, Person,
      foreign_key: :person_uuid,
      references: :uuid,
      type: :binary_id

    belongs_to :decision_version, DecisionVersion,
      foreign_key: :decision_version_uuid,
      references: :uuid,
      type: :binary_id

    timestamps()
  end

  @doc false
  defp changeset(vote, attrs) do
    vote
    |> cast(attrs, [:ballot, :person_uuid, :decision_version_uuid])
    |> assoc_constraint(:person)
    |> assoc_constraint(:decision_version)
    |> unique_constraint([:person_uuid, :decision_version_uuid])
    |> validate_access()
    |> put_check()
  end

  @spec validate_access(Ecto.Changeset) :: Ecto.Changeset.t()
  defp validate_access(%Ecto.Changeset{changes: changes, data: data} = changeset) do
    voter = Repo.assign(:person_uuid, changes, data) |> Person.get()
    version = Repo.assign(:decision_version_uuid, changes, data) |> DecisionVersion.get()
    decision = version |> Repo.get_rel(:decision)

    if DecisionVersion.still_open?(version) and
         voter |> PersonPeople.can_vote_in_decision?(decision) do
      changeset
    else
      changeset |> add_error(:person, Errors.no_access(:str))
    end
  end

  @spec put_check(Ecto.Changeset.t()) :: Ecto.Changeset.t()
  defp put_check(
         %Ecto.Changeset{
           changes: changes,
           data: data
         } = changeset
       ) do
    changeset
    |> put_change(
      :check,
      :crypto.hmac(
        :sha256,
        Application.get_env(:accord, Accord.Vote)[:system_key],
        Jason.encode!(%{
          ballot: Repo.assign(:ballot, changes, data),
          person_uuid: Repo.assign(:person_uuid, changes, data),
          dv_uuid: Repo.assign(:decision_version_uuid, changes, data)
        })
      )
      |> Base.encode16()
    )
  end

  defp put_check(%Ecto.Changeset{} = changeset), do: changeset

  defp make_ballot(choices), do: %{"choices" => choices}

  @spec new(choices, Person.t(), DecisionVersion.t()) :: Ecto.Changeset.t()
  def new(
        choices,
        %Person{uuid: person_uuid} = person,
        %DecisionVersion{uuid: dv_uuid} = version
      ) do
    changeset(
      %Vote{},
      %{
        ballot: make_ballot(choices),
        person_uuid: person_uuid,
        decision_version_uuid: dv_uuid
      }
    )
    |> put_assoc(:person, person)
    |> put_assoc(:decision_version, version)
  end

  @create_on_conflict [
    on_conflict: {:replace, [:ballot, :updated_at]},
    conflict_target: [:person_uuid, :decision_version_uuid],
    returning: true
  ]
  @spec create(choices, Person.t(), DecisionVersion.t()) :: [Repo.action()]
  def create(choices, %Person{} = voter, %DecisionVersion{} = version, opts \\ []) do
    [
      {Keyword.get(opts, :name, :vote), :insert,
       {new(choices, voter, version), @create_on_conflict}}
    ]
  end

  @spec change_choices(Vote.t(), List.t()) :: Ecto.Changeset.t()
  def change_choices(%Vote{} = vote, choices) do
    vote |> changeset(%{ballot: make_ballot(choices)})
  end

  @spec get(%{voter: Person.t(), decision_version: DecisionVersion.t()}) :: Vote.t() | nil
  def get(%{voter: %Person{} = voter, decision_version: %DecisionVersion{} = version}) do
    from(
      vv in Vote,
      where: vv.person_uuid == ^voter.uuid and vv.decision_version_uuid == ^version.uuid
    )
    |> Repo.one()
  end

  @spec get_choices(Vote.t()) :: choices
  def get_choices(%Vote{ballot: %{"choices" => choices}}), do: choices
end
