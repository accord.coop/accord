defmodule Accord.PersonPeople do
  @moduledoc """
    The schema and changeset for the associations between Person and People entities.
  """

  alias Accord.{Repo, Person, People, PersonPeople, Decision, DecisionVersion, DecisionRole, Vote}

  import Ecto.Changeset
  import Ecto.Query

  use Ecto.Schema

  @type t :: %__MODULE__{}

  schema "persons_peoples" do
    belongs_to :person, Person, foreign_key: :person_uuid, references: :uuid, type: :binary_id
    belongs_to :people, People, foreign_key: :people_uuid, references: :uuid, type: :binary_id
    timestamps()
  end

  @doc false
  def changeset(person, people) do
    %PersonPeople{}
    |> cast(%{person_uuid: person.uuid, people_uuid: people.uuid}, [:person_uuid, :people_uuid])
    |> put_assoc(:person, person)
    |> put_assoc(:people, people)
    |> unique_constraint(:person, name: :persons_peoples_relation)
  end

  @spec query(Person.t(), People.t()) :: Ecto.Query.t()
  defp query(%Person{} = person, %People{} = people) do
    from(
      pp in PersonPeople,
      where: pp.people_uuid == ^people.uuid and pp.person_uuid == ^person.uuid
    )
  end

  @spec query(term(), term()) :: Ecto.Query.t()
  defp query(person_uuid, people_uuid) do
    from(
      pp in PersonPeople,
      where: pp.people_uuid == ^people_uuid and pp.person_uuid == ^person_uuid
    )
  end

  @spec get(Person.t() | term(), People.t() | term()) ::
          PersonPeople.t() | nil
  defp get(person, people) do
    query(person, people) |> Repo.one()
  end

  @spec associated?(Person.t() | term(), People.t() | term()) :: boolean
  def associated?(person, people) do
    query(person, people) |> Repo.exists?()
  end

  @spec associate(atom, Person.t(), People.t()) :: [Repo.independent_action()]
  def associate(name \\ :assoc, %Person{} = person, %People{} = people) do
    [{name, :insert, {changeset(person, people), [on_conflict: :nothing]}}]
  end

  def associate(name \\ :assoc)

  @spec associate({atom, atom, atom}) :: [Repo.dependent_action()]
  def associate({name, person_name, people_name}) do
    [
      {
        name,
        [person_name, people_name],
        :insert,
        {
          fn %{^person_name => person, ^people_name => people} -> changeset(person, people) end,
          [on_conflict: :nothing]
        }
      }
    ]
  end

  @spec associate(atom) :: [Repo.dependent_action()]
  def associate(name), do: associate({name, :person, :people})

  @spec dissociate(atom, Person.t(), People.t()) :: [Repo.action()]
  def dissociate(name \\ :assoc, %Person{} = person, %People{} = people) do
    case get(person, people) do
      nil -> [{name, :run, {fn _, _ -> {:ok, nil} end, []}}]
      %PersonPeople{} = assoc -> [{name, :delete, {assoc, []}}]
    end
  end

  @spec get_persons_for_people(People.t()) :: [Person.t()]
  def get_persons_for_people(%People{} = people),
    do: get_persons_for_people(people.uuid)

  @spec get_persons_for_people(Ecto.UUID.t()) :: [Person.t()]
  def get_persons_for_people(people_uuid) do
    from(
      p in Person,
      join: pp in PersonPeople,
      on: pp.people_uuid == ^people_uuid and pp.person_uuid == p.uuid
    )
    |> Repo.all()
  end

  @spec get_peoples_for_person(Person.t()) :: [People.t()]
  def get_peoples_for_person(%Person{} = person),
    do: get_peoples_for_person(person.uuid)

  @spec get_peoples_for_person(Ecto.UUID.t()) :: [People.t()]
  def get_peoples_for_person(person_uuid) do
    from(
      p in People,
      join: pp in PersonPeople,
      on: pp.person_uuid == ^person_uuid and pp.people_uuid == p.uuid
    )
    |> Repo.all()
  end

  @spec get_people_uuids_for_person_uuid(Ecto.UUID.t()) :: Ecto.SubQuery.t()
  def get_people_uuids_for_person_uuid(person_uuid) do
    subquery(
      from(
        p in People,
        join: pp in PersonPeople,
        on: pp.person_uuid == ^person_uuid and pp.people_uuid == p.uuid,
        select: [:uuid]
      )
    )
  end

  @spec can_vote_in_decision?(Person.t(), Decision.t()) :: boolean
  def can_vote_in_decision?(%Person{} = person, %Decision{} = decision) do
    from(
      pp in PersonPeople,
      join:
        dr in subquery(
          decision
          |> DecisionRole.people_uuids_with_role(DecisionRole.Types.votable(:str), :struct)
        ),
      on: pp.person_uuid == ^person.uuid and dr.people_uuid == pp.people_uuid
    )
    |> Repo.exists?()
  end

  def get_known_absent_voters(%DecisionVersion{uuid: dv_uuid} = version) do
    voted_person_uuids =
      from(
        v in Vote,
        where: v.decision_version_uuid == ^dv_uuid,
        order_by: v.person_uuid,
        select: v.person_uuid
      )

    from(
      pp in PersonPeople,
      join:
        dr in subquery(
          version
          |> Repo.get_rel(:decision)
          |> DecisionRole.people_uuids_with_role(DecisionRole.Types.votable(:str), :struct)
        ),
      on: dr.people_uuid == pp.people_uuid,
      order_by: pp.person_uuid,
      select: pp.person_uuid,
      except: ^voted_person_uuids
    )
  end
end
