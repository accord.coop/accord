defmodule Accord.DecisionVersion.Types do
  use Accord.Statics

  declare(:"dv-state", :new)
  declare(:"dv-state", :published)
  declare(:"dv-state", :withdrawn)
  declare(:"dv-state", :closed)
end

defmodule Accord.DecisionVersion do
  @moduledoc """
    The schema and changeset for Decision version entities.
  """

  alias Accord.{
    Repo,
    Decision,
    DecisionVersion,
    DecisionVersionMoment,
    Person,
    Vote,
    Option,
    DecisionSettings
  }

  alias Accord.DecisionVersion.Types

  import Ecto.Changeset
  import Ecto.Query, only: [{:from, 2}]

  use Accord.Schema

  require Logger

  @type t :: %__MODULE__{}

  @default_state Types.new(:str)

  @type signature :: {:slack, String.t()} | {:slack, String.t(), String.t()}

  schema "decision_versions" do
    field :language, :string
    field :title, :string
    field :body, :string
    field :state, :string, default: @default_state
    field :state_addendum, :string
    field :service_id, :string

    embeds_many :options, Option, on_replace: :delete

    embeds_one :settings, DecisionSettings, on_replace: :delete

    belongs_to :decision, Decision,
      foreign_key: :decision_uuid,
      references: :uuid,
      type: :binary_id

    belongs_to :author, Person, foreign_key: :author_uuid, references: :uuid, type: :binary_id

    has_many :votes, Vote
    has_many :moments, DecisionVersionMoment

    timestamps()
  end

  @doc false
  def changeset(version, attrs \\ %{}) do
    version
    |> cast(attrs, [
      :language,
      :title,
      :body,
      :state,
      :state_addendum,
      :service_id
    ])
    |> validate_required([:title, :service_id, :state])
    |> cast_embed(:options, with: &Option.changeset/2)
    |> cast_embed(:settings, with: &DecisionSettings.changeset/2)
    |> validate_length(:title, max: 255)
    |> validate_length(:body, max: 1_000_000)
    |> assoc_constraint(:decision)
    |> assoc_constraint(:author)
  end

  @spec service_id(signature) :: String.t()
  def service_id({:slack, team_id, view_id}), do: "slack︰#{team_id}︰#{view_id}"
  def service_id({:slack, team_id, channel_id, ts}), do: "slack︰#{team_id}︰#{channel_id}︰#{ts}"

  @spec get_ids_from_service_id(String.t()) :: {String.t(), String.t()} | String.t()
  def get_ids_from_service_id("slack︰" <> ids) do
    case String.split(ids, "︰") do
      [team_id | [channel_id | [ts | _]]] -> {team_id, channel_id, ts}
      [team_id | [view_id | _]] -> {team_id, view_id}
    end
  end

  @spec get(signature) :: DecisionVersion.t() | nil
  def get({:slack, team_id, view_id}),
    do: service_id({:slack, team_id, view_id}) |> get_by_service_id()

  def get({:slack, team_id, channel_id, ts}),
    do: service_id({:slack, team_id, channel_id, ts}) |> get_by_service_id()

  @spec get(term()) :: DecisionVersion.t() | nil
  def get(id), do: Repo.get(DecisionVersion, id)

  @spec get_by_service_id(String.t()) :: DecisionVersion.t() | nil
  def get_by_service_id(service_id) do
    from(d in DecisionVersion, where: d.service_id == ^service_id) |> Repo.one()
  end

  @spec get_drafts_for_author(Ecto.UUID.t()) :: [DecisionVersion.t()]
  def get_drafts_for_author(author_uuid) do
    state_new = Types.new(:str)

    from(dv in DecisionVersion, where: dv.author_uuid == ^author_uuid and dv.state == ^state_new)
    |> Repo.all()
  end

  @spec new(%{author: Person.t(), decision: Decision.t()}) :: Ecto.Changeset.t()
  def new(%{author: author, decision: decision} = params),
    do:
      changeset(%DecisionVersion{}, params)
      |> put_assoc(:author, author)
      |> put_assoc(:decision, decision)

  @spec change_options(DecisionVersion.t(), [Option.t()]) :: Ecto.Changeset.t()
  def change_options(version, options) do
    version
    |> change
    |> put_embed(:options, options)
  end

  @spec change_settings(DecisionVersion.t(), DecisionSettings.t()) :: Ecto.Changeset.t()
  def change_settings(version, settings) do
    version
    |> change
    |> put_embed(:settings, settings)
  end

  @spec change_state(DecisionVersion.t(), String.t(), String.t()) :: Ecto.Changeset.t()
  def change_state(version, state, state_addendum) do
    version
    |> change(state: state, state_addendum: state_addendum)
  end

  @spec change_state(DecisionVersion.t(), String.t()) :: Ecto.Changeset.t()
  def change_state(version, state) do
    version
    |> change(state: state)
  end

  @spec close(DecisionVersion.t()) :: {:ok, DecisionVersion.t()} | {:error, any}
  def close(%DecisionVersion{state: state} = decision_version) do
    state_withdrawn = Types.withdrawn(:str)
    state_closed = Types.closed(:str)

    case state do
      ^state_withdrawn ->
        {:ok, decision_version}

      ^state_closed ->
        {:ok, decision_version}

      _ ->
        decision_version |> change_state(state_closed) |> Repo.update()
    end
  end

  @spec vote_count(DecisionVersion.t()) :: integer
  def vote_count(version) do
    case version |> Repo.get_rel(:votes) do
      nil -> 0
      votes -> length(votes)
    end
  end

  @spec get_option_by_id(String.t(), DecisionVersion.t()) :: Option.t() | nil
  def get_option_by_id(option_id, %DecisionVersion{options: options}) do
    options |> Enum.find(nil, fn %Option{id: id} -> id === option_id end)
  end

  @spec on_conflict() :: [
          on_conflict: {:replace, [atom]},
          conflict_target: [atom],
          returning: [atom] | boolean
        ]
  def on_conflict() do
    [
      on_conflict: {
        :replace,
        [
          :updated_at,
          :language,
          :title,
          :body,
          :options,
          :settings
        ]
      },
      conflict_target: [:service_id],
      returning: true
    ]
  end

  @spec get_expiry(DecisionVersion.t()) :: NaiveDateTime.t() | nil
  def get_expiry(%DecisionVersion{} = dv) do
    case dv
         |> Repo.get_rel(:moments)
         |> Enum.find(fn moment -> moment.type === DecisionVersionMoment.Types.close(:str) end) do
      %DecisionVersionMoment{moment: moment} -> moment
      _ -> nil
    end
  end

  @spec still_open?(DecisionVersion.t()) :: boolean
  def still_open?(%DecisionVersion{} = dv) do
    case dv |> get_expiry do
      nil -> true
      %NaiveDateTime{} = expiry -> NaiveDateTime.compare(expiry, Repo.now()) === :gt
    end
  end

  @spec get_winning_options(DecisionVersion.t(), [Ecto.UUID.t()]) :: [Option.t()]
  def get_winning_options(%DecisionVersion{} = dv, winning_option_uuids) do
    dv.options |> Enum.filter(fn %Option{id: uuid} -> uuid in winning_option_uuids end)
  end

  @behaviour Access

  @impl true
  def fetch(%DecisionVersion{} = version, key), do: Map.fetch(version, key)

  @impl true
  def get_and_update(%DecisionVersion{} = version, key, fun),
    do: Map.get_and_update(version, key, fun)

  @impl true
  def pop(%DecisionVersion{} = version, key), do: Map.pop(version, key, nil)
end
