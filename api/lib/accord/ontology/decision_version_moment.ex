defmodule Accord.DecisionVersionMoment.Types do
  use Accord.Statics

  declare(:"dv-moment-type", :close)
  declare(:"dv-moment-type", :service_update)
  declare(:"dv-moment-type", :async_count)
end

defmodule Accord.DecisionVersionMoment do
  @moduledoc """
    The schema and changeset for Decision moment entities.
  """

  alias Accord.{Repo, Errors, DecisionVersion}
  alias Accord.DecisionVersionMoment, as: DVMoment
  alias Accord.DecisionVersionMoment.Types, as: Types

  import Ecto.Changeset
  import Ecto.Query, only: [{:from, 2}]

  use Accord.Schema

  @type t :: %__MODULE__{}

  @types [Types.close(:str), Types.service_update(:str), Types.async_count(:str)]
  @default_type Types.service_update(:str)

  schema "decision_version_moments" do
    field :type, :string, default: @default_type
    field :moment, :naive_datetime
    field :executed, :integer, default: -1

    belongs_to :decision_version, DecisionVersion,
      foreign_key: :decision_version_uuid,
      references: :uuid,
      type: :binary_id

    timestamps()
  end

  @doc false
  def changeset(dv_moment, attrs \\ %{}) do
    dv_moment
    |> cast(attrs, [:moment, :type, :executed])
    |> assoc_constraint(:decision_version)
  end

  @spec get(term()) :: DVMoment.t() | nil
  def get(id), do: Repo.get(DVMoment, id)

  @spec new(DecisionVersion.t(), NaiveDateTime.t(), String.t()) :: Ecto.Changeset.t()
  def new(%DecisionVersion{} = dv, moment, type) when type in @types do
    %DVMoment{}
    |> changeset(%{moment: moment, type: type})
    |> put_assoc(:decision_version, dv)
  end

  @spec create(DecisionVersion.t(), NaiveDateTime.t() | nil, String.t()) :: [Repo.action()]
  def create(%DecisionVersion{} = dv, moment, type \\ @default_type) when type in @types do
    [{:decision_version_moment, :insert, {new(dv, moment, type), []}}]
  end

  @spec get_upcoming(Integer.t()) :: [DVMoment.t()]
  def get_upcoming(limit \\ 16) do
    from(
      dm in DVMoment,
      order_by: [asc: dm.moment],
      where: not is_nil(dm.moment) and dm.moment >= ^Repo.now(),
      limit: ^limit
    )
    |> Repo.all()
  end

  @dvm_type_close DVMoment.Types.close(:str)
  @dvm_type_service_update DVMoment.Types.service_update(:str)
  @dvm_type_async_count DVMoment.Types.async_count(:str)

  @spec execute(DVMoment.t()) :: :ok | {:error, any}
  def execute(%DVMoment{type: type} = dv_moment) do
    case type do
      @dvm_type_close -> dv_moment |> close_dv()
      @dvm_type_service_update -> dv_moment |> service_update_dv()
      @dvm_type_async_count -> :ok
      _ -> {:error, Errors.bad_args()}
    end
  end

  @spec execute(term()) :: :ok | {:error, any}
  def execute(dv_moment_uuid) do
    case DVMoment.get(dv_moment_uuid) do
      nil ->
        {:error, Errors.not_found()}

      %DVMoment{} = moment ->
        execute(moment)
    end
  end

  @dv_state_closed DecisionVersion.Types.closed(:str)

  def close_dv(%DVMoment{} = dv_moment) do
    case dv_moment |> Repo.get_rel(:decision_version) |> DecisionVersion.close() do
      {:ok, %DecisionVersion{service_id: service_id, state: @dv_state_closed}} ->
        case service_id do
          "" <> _ -> AccordWeb.Pipeline.Jobs.AssessDecisionVersion.start(dv_moment)
          _ -> :ok
        end

      err ->
        {:error, err}
    end
  end

  def service_update_dv(%DVMoment{} = dv_moment) do
    AccordWeb.Pipeline.Jobs.AssessDecisionVersion.start(dv_moment)
  end

  def set_executed(%DVMoment{} = dv_moment, :ok), do: dv_moment |> change(executed: 0)
end
