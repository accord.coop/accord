defmodule Accord.Locale do
  @moduledoc """
    Manages a locale for a Person or a People
  """

  import Ecto.Changeset

  use Ecto.Schema

  @type t :: %__MODULE__{}

  embedded_schema do
    field :accept_language, :string, default: AccordWeb.Gettext.default_localization()
    field :formality_style, :string, default: "casual"
    field :tz, :string, default: "Etc/UTC"
  end

  @doc false
  def changeset(locale, attrs \\ %{}) do
    locale
    |> cast(attrs, [
      :accept_language,
      :formality_style,
      :tz
    ])
  end

  @behaviour Access

  @impl true
  def fetch(locale, key), do: Map.fetch(locale, key)

  @impl true
  def get_and_update(locale, key, fun), do: Map.get_and_update(locale, key, fun)

  @impl true
  def pop(locale, key), do: Map.pop(locale, key, nil)
end
