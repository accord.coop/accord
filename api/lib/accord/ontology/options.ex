defmodule Accord.Option do
  @moduledoc """
    Manages an option for a DecisionVersion
  """

  import Ecto.Changeset

  use Ecto.Schema

  @type t :: %__MODULE__{}

  @derive {Jason.Encoder, only: [:id, :title]}

  embedded_schema do
    field :title, :string
  end

  @doc false
  def changeset(option, attrs \\ %{}) do
    option
    |> cast(attrs, [:title])
  end
end
