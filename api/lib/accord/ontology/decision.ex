defmodule Accord.Decision do
  @moduledoc """
    The schema and changeset for Decision entities.
  """

  alias Accord.{
    Repo,
    Decision,
    People,
    DecisionRole,
    DecisionVersion,
    DecisionSettings,
    Delegation,
    Vote
  }

  import Ecto.Changeset
  import Ecto.Query, only: [{:from, 2}]

  use Accord.Schema

  use Accord.Counter.Plurality
  use Accord.Counter.Cardinal
  use Accord.Counter.OrdinalSTV

  @type t :: %__MODULE__{}
  @type participation :: {number, number, number}
  @type breakdown :: %{Vote.choice_id() => term}

  schema "decisions" do
    many_to_many :peoples, People,
      join_through: DecisionRole,
      join_keys: [decision_uuid: :uuid, people_uuid: :uuid]

    has_many :people_roles, DecisionRole

    has_many :versions, DecisionVersion

    timestamps()
  end

  @doc false
  def changeset(decision, attrs \\ %{}) do
    decision
    |> cast(attrs, [])
  end

  @spec get(term()) :: Decision.t() | nil
  def get(id), do: Repo.get(Decision, id)

  @spec new() :: Ecto.Changeset.t()
  defp new(), do: changeset(%Decision{})

  @spec create_with_version(Map.t()) :: [Repo.action()]
  def create_with_version(version_params) do
    case DecisionVersion.get_by_service_id(version_params.service_id) do
      %DecisionVersion{} = version ->
        [
          {:decision, :run, {fn _, _ -> {:ok, version |> Repo.get_rel(:decision)} end, []}},
          {:decision_version, :insert,
           {DecisionVersion.new(
              version_params
              |> Map.put(:decision, version |> Repo.get_rel(:decision))
            ), DecisionVersion.on_conflict()}}
        ]

      nil ->
        [
          {:decision, :insert, {new(), []}},
          {:decision_version, [:decision], :insert,
           {fn decision ->
              DecisionVersion.new(version_params |> Map.merge(decision))
            end, []}}
        ]
    end
  end

  @spec get_latest_version(Decision.t()) :: DecisionVersion.t() | nil
  def get_latest_version(%Decision{uuid: decision_uuid}) do
    from(
      dv in DecisionVersion,
      where: dv.decision_uuid == ^decision_uuid,
      order_by: [desc: dv.inserted_at]
    )
    |> Repo.one()
  end

  @quant_absolute DecisionSettings.Types.absolute(:str)
  @quant_proportional DecisionSettings.Types.proportional(:str)
  @quant_prop_exclusive DecisionSettings.Types.proportional_exclusive(:str)

  @spec count(DecisionVersion.t(), integer, MapSet.t(Ecto.UUID.t())) :: %{
          breakdown: breakdown,
          winning_option_uuids: [Ecto.UUID.t()],
          n_delegations: float,
          n_votes: float,
          n_voters: float
        }
  def(
    count(
      %DecisionVersion{settings: settings} = version,
      n_possible_voters,
      known_votable_person_uuids
    )
  ) do
    votes = version |> Repo.get_rel(:votes)

    total_direct_vote_power = DecisionVersion.vote_count(version)

    voted_person_uuids =
      votes
      |> Enum.map(fn %Vote{person_uuid: person_uuid} -> person_uuid end)
      |> MapSet.new()

    absent_person_uuids = known_votable_person_uuids |> MapSet.difference(voted_person_uuids)

    delegation_vote_augmentations =
      absent_person_uuids
      # for each absent Person,
      |> Enum.reduce(Map.new(), fn absent_person_uuid, acc ->
        # fully resolve any Delegation they initiate for the DV's votable people,
        case Delegation.resolve(absent_person_uuid, version) do
          nil ->
            acc

          final_delegatee_uuid ->
            case Map.get(acc, final_delegatee_uuid) do
              nil -> Map.put(acc, final_delegatee_uuid, 1.0)
              aug -> Map.put(acc, final_delegatee_uuid, aug + 1.0)
            end
        end
      end)

    total_delegated_vote_power =
      delegation_vote_augmentations |> Enum.reduce(0, fn {_, aug}, acc -> acc + aug end)

    # if the Person at the end of the resolved Delegation has a Vote for this DV,
    vote_power_by_person_uuid =
      Repo.get_rel(version, :votes)
      |> Enum.reduce(Map.new(), fn %Vote{person_uuid: person_uuid} = vote, acc ->
        Map.put(acc, person_uuid, {vote,
         1 +
           case Map.get(delegation_vote_augmentations, person_uuid) do
             nil -> 0.0
             # add the absent Persons' voting power to that Vote
             aug -> aug
           end})
      end)

    score_breakdown =
      votes
      |> Enum.reduce(
        initial_acc(version, participation: {total_direct_vote_power, total_delegated_vote_power}),
        fn %Vote{person_uuid: person_uuid} = vote, acc ->
          {_, power} = vote_power_by_person_uuid |> Map.get(person_uuid)

          accumulate_choices(
            vote |> Vote.get_choices(),
            acc,
            power,
            version
          )
        end
      )
      |> resolve_breakdown(version)

    {winning_option_uuids, annotated_breakdown} =
      score_breakdown
      |> get_winning_option_uuids(
        {total_direct_vote_power, total_delegated_vote_power, n_possible_voters},
        {votes, vote_power_by_person_uuid},
        settings
      )

    %{
      breakdown: annotated_breakdown,
      winning_option_uuids: winning_option_uuids,
      n_votes: total_direct_vote_power,
      n_delegations: total_delegated_vote_power,
      n_voters: n_possible_voters
    }
  end

  @spec get_winning_option_uuids(breakdown, participation, [Vote.t()], DecisionSettings.t()) :: [
          Ecto.UUID.t()
        ]
  # Does not meet absolute quorum
  defp get_winning_option_uuids(breakdown, {direct, delegated, _possible}, _, %DecisionSettings{
         quorum_quantification: @quant_absolute,
         quorum_value: quorum_value
       })
       when direct + delegated < quorum_value,
       do: {[], standard_annotated_breakdown(breakdown)}

  # Does not meet proportional quorum
  defp get_winning_option_uuids(breakdown, {direct, delegated, possible}, _, %DecisionSettings{
         quorum_quantification: @quant_proportional,
         quorum_value: quorum_value
       })
       when (direct + delegated) / possible < quorum_value,
       do: {[], standard_annotated_breakdown(breakdown)}

  defp get_winning_option_uuids(
         breakdown,
         participation,
         votes_and_vote_power,
         settings
       ) do
    option_uuids_by_score =
      breakdown
      |> Enum.group_by(fn {_, score} -> score end, fn {option_uuid, _} ->
        option_uuid
      end)
      |> Map.drop([:blocked])

    select_winning_option_uuids(
      {
        breakdown,
        option_uuids_by_score,
        participation
      },
      votes_and_vote_power,
      settings
    )
  end

  def meets_min_score?(score, _, %DecisionSettings{
        parameter0_quantification: @quant_absolute,
        parameter0_value: p0v
      }),
      do: score >= p0v

  def meets_min_score?(score, {direct, delegated, _}, %DecisionSettings{
        parameter0_quantification: @quant_proportional,
        parameter0_value: p0v
      }),
      do: score / (direct + delegated) >= p0v

  def meets_min_score?(score, {direct, delegated, _}, %DecisionSettings{
        parameter0_quantification: @quant_prop_exclusive,
        parameter0_value: p0v
      }),
      do: score / (direct + delegated) > p0v

  def meets_min_score?(_, _, _), do: true

  defp standard_annotated_breakdown(breakdown),
    do:
      breakdown
      |> Enum.reduce(Map.new(), fn {option_uuid, score}, acc ->
        Map.put(acc, option_uuid, %{score: score, annotation: nil})
      end)

  def standard_winning_option_uuids(
        {breakdown, option_uuids_by_score, participation},
        _,
        %DecisionSettings{n_winners_value: n_winners_value} = settings
      ) do
    winning_option_uuids =
      Map.keys(option_uuids_by_score)
      |> Enum.sort(&(&1 >= &2))
      |> Enum.reduce_while([], fn score, acc ->
        if score |> Decision.meets_min_score?(participation, settings) and
             length(acc) < n_winners_value do
          {:cont, acc ++ Map.get(option_uuids_by_score, score)}
        else
          {:halt, acc}
        end
      end)

    {winning_option_uuids, standard_annotated_breakdown(breakdown)}
  end
end
