defmodule Accord.Delegation do
  @moduledoc """
  The schema and changeset for Delegation entities.
  """

  alias Accord.{
    Repo,
    Person,
    People,
    Delegation,
    Errors,
    DecisionRole,
    DecisionVersion,
    Vote,
    PeoplePeople
  }

  import Ecto.Changeset
  import Ecto.Query, only: [{:from, 2}]

  use Accord.Schema

  @type t :: %__MODULE__{}

  schema "delegations" do
    belongs_to :delegator, Person,
      foreign_key: :delegator_uuid,
      references: :uuid,
      type: :binary_id

    belongs_to :delegatee, Person,
      foreign_key: :delegatee_uuid,
      references: :uuid,
      type: :binary_id

    belongs_to :scope_people, People,
      foreign_key: :scope_people_uuid,
      references: :uuid,
      type: :binary_id

    field :starts, :naive_datetime
    field :expires, :naive_datetime

    field :check, :string

    field :precedence, :integer

    timestamps()
  end

  defp default_start(), do: Repo.now()

  defp default_precedence(delegator_uuid) do
    case Repo.all(
           from d in Delegation,
             where: d.delegator_uuid == ^delegator_uuid,
             select: max(d.precedence)
         )
         |> List.first() do
      nil -> 0
      last_precedence -> last_precedence + 1
    end
  end

  @doc false
  def changeset(delegation, attrs) do
    delegation
    |> cast(attrs, [
      :delegator_uuid,
      :delegatee_uuid,
      :scope_people_uuid,
      :starts,
      :expires,
      :precedence
    ])
    |> assoc_constraint(:delegator)
    |> assoc_constraint(:delegatee)
    |> assoc_constraint(:scope_people)
    |> validate_time_range()
    |> unique_for_time_range()
    |> validate_resolvable()
    |> put_check()
  end

  defp traverse_delegations(_direction, acc, [], _now), do: acc

  defp traverse_delegations(:forward, %MapSet{} = acc, current_uuids, now) do
    traverse_delegations(
      :forward,
      MapSet.union(acc, MapSet.new(current_uuids)),
      from(d in Delegation,
        where: d.delegator_uuid in ^current_uuids and d.expires > ^now and d.starts <= ^now,
        select: d.delegatee_uuid
      )
      |> Repo.all(),
      now
    )
  end

  defp traverse_delegations(:reverse, %MapSet{} = acc, current_uuids, now) do
    traverse_delegations(
      :reverse,
      MapSet.union(acc, MapSet.new(current_uuids)),
      from(d in Delegation,
        where: d.delegatee_uuid in ^current_uuids and d.expires > ^now and d.starts <= ^now,
        select: d.delegator_uuid
      )
      |> Repo.all(),
      now
    )
  end

  defp unique_for_time_range(%Ecto.Changeset{changes: changes, data: data} = changeset) do
    delegator_uuid = Repo.assign(:delegator_uuid, changes, data)
    scope_people_uuid = Repo.assign(:scope_people_uuid, changes, data)
    starts = Repo.assign(:starts, changes, data)
    expires = Repo.assign(:expires, changes, data)

    if from(d in Delegation,
         where:
           d.delegator_uuid == ^delegator_uuid and d.scope_people_uuid == ^scope_people_uuid and
             d.expires >= ^starts and d.starts <= ^expires
       )
       |> Repo.exists?() do
      changeset |> add_error(:scope_people, Errors.bad_args(:str))
    else
      changeset
    end
  end

  defp validate_resolvable(%Ecto.Changeset{changes: changes, data: data} = changeset),
    do:
      validate_resolvable(
        changeset,
        Repo.assign(:delegatee_uuid, changes, data),
        Repo.assign(:delegator_uuid, changes, data)
      )

  defp validate_resolvable(changeset, delegatee_uuid, delegator_uuid)
       when delegatee_uuid == delegator_uuid,
       do: add_error(changeset, :delegatee, Errors.delegates_to_self(:str))

  defp validate_resolvable(changeset, delegatee_uuid, delegator_uuid) do
    now = Repo.now()
    originating_nodes = traverse_delegations(:reverse, MapSet.new(), [delegator_uuid], now)
    target_nodes = traverse_delegations(:forward, MapSet.new(), [delegatee_uuid], now)

    case MapSet.intersection(originating_nodes, target_nodes) |> MapSet.size() do
      n when n > 0 -> add_error(changeset, :delegatee, Errors.circular(:str))
      n when n === 0 -> changeset
    end
  end

  defp validate_time_range(%Ecto.Changeset{changes: changes, data: data} = changeset) do
    starts = Repo.assign(:starts, changes, data)
    expires = Repo.assign(:expires, changes, data)

    case NaiveDateTime.compare(starts, expires) do
      :lt -> changeset
      _ -> add_error(changeset, :expires, Errors.bad_args(:str))
    end
  end

  def new(
        %Person{uuid: delegator_uuid} = delegator,
        %Person{uuid: delegatee_uuid} = delegatee,
        %People{uuid: scope_people_uuid} = scope_people,
        opts \\ []
      ) do
    changeset(
      %Delegation{},
      %{
        delegator_uuid: delegator_uuid,
        delegatee_uuid: delegatee_uuid,
        scope_people_uuid: scope_people_uuid,
        starts:
          case opts[:starts] do
            nil ->
              default_start()

            %NaiveDateTime{} = dt ->
              case NaiveDateTime.compare(dt, Repo.now()) do
                # prevent setting retroactive delegations (for now)
                :lt -> default_start()
                _ -> dt
              end
          end,
        expires: opts[:expires],
        precedence:
          case opts[:precedence] do
            nil -> default_precedence(delegator_uuid)
            precedence -> precedence
          end
      }
    )
    |> put_assoc(:delegator, delegator)
    |> put_assoc(:delegatee, delegatee)
    |> put_assoc(:scope_people, scope_people)
  end

  @spec put_check(Ecto.Changeset.t()) :: Ecto.Changeset.t()
  defp put_check(
         %Ecto.Changeset{
           changes: changes,
           data: data
         } = changeset
       ) do
    changeset
    |> put_change(
      :check,
      :crypto.hmac(
        :sha256,
        Application.get_env(:accord, Accord.Vote)[:system_key],
        Jason.encode!(%{
          delegator_uuid: Repo.assign(:delegator_uuid, changes, data),
          delegatee_uuid: Repo.assign(:delegatee_uuid, changes, data),
          scope_uuid: Repo.assign(:scope_people_uuid, changes, data),
          starts: Repo.assign(:starts, changes, data) |> Repo.to_unix_seconds(),
          expires: Repo.assign(:expires, changes, data) |> Repo.to_unix_seconds(),
          precedence: Repo.assign(:precedence, changes, data)
        })
      )
      |> Base.encode16()
    )
  end

  def end_now(%Delegation{} = delegation) do
    delegation
    |> change(expires: Repo.now())
    |> put_check()
  end

  defp change_precedence(%Delegation{} = delegation, precedence) do
    delegation
    |> change(precedence: precedence)
    |> put_check()
  end

  def apply_precedence_order(delegations, start_value \\ 0) do
    {_, precedence_actions} =
      delegations
      |> Enum.reduce({start_value, []}, fn delegation, {precedence, actions} ->
        update_action =
          {"delegation_precedence__#{precedence}", :update,
           {change_precedence(delegation, precedence), []}}

        {
          precedence + 1,
          [update_action | actions]
        }
      end)

    precedence_actions
  end

  def create(%Person{} = delegator, %Person{} = delegatee, scope, opts \\ []) do
    [
      {:delegation, :insert, {new(delegator, delegatee, scope, opts), []}}
      | case opts[:precedence] do
          nil ->
            []

          # Apply new precedence to all items with specified precedence and greater
          precedence ->
            from(d in Delegation,
              where: d.delegator_uuid == ^delegator.uuid and d.precedence >= ^precedence,
              order_by: [asc: d.precedence]
            )
            |> Repo.all()
            |> apply_precedence_order(precedence + 1)
        end
    ]
  end

  @spec get(term()) :: Delegation.t() | nil
  def get(uuid), do: Repo.get(Delegation, uuid)

  def get_active_by_delegator(delegator, offset \\ 0)

  @spec get_active_by_delegator(Person.t(), integer) :: [Delegation.t()]
  def get_active_by_delegator(%Person{uuid: delegator_uuid}, offset),
    do: get_active_by_delegator(delegator_uuid, offset)

  @spec get_active_by_delegator(String.t(), integer) :: [Delegation.t()]
  def get_active_by_delegator("" <> delegator_uuid, offset) do
    now = NaiveDateTime.add(Repo.now(), offset, :second)

    Repo.all(
      from d in Delegation,
        where:
          d.delegator_uuid == ^delegator_uuid and
            d.expires > ^now and
            d.starts <= ^now
    )
  end

  @spec get_scope(Delegation.t()) :: People.t()
  def get_scope(%Delegation{} = delegation),
    do: Repo.get_rel(delegation, :scope_people)

  @spec get_scope_levels(List.t([Ecto.UUID.t()]), List.t(Ecto.UUID.t())) :: %{
          Ecto.UUID.t() => integer
        }
  defp get_scope_levels(scope_levels_array, []) do
    # this function receives the scope_levels array, but returns a mapping between
    # scope People UUIDs and their level to facilitate querying
    {_, final_scope_levels} =
      scope_levels_array
      |> Enum.reverse()
      |> Enum.reduce({0, Map.new()}, fn people_uuids, {level, acc} ->
        next_acc =
          people_uuids
          |> Enum.reduce(acc, fn people_uuid, acc ->
            acc |> Map.put(people_uuid, level)
          end)

        {level + 1, next_acc}
      end)

    final_scope_levels
  end

  defp get_scope_levels(scope_levels, current_scope) do
    get_scope_levels(
      [current_scope | scope_levels],
      from(pp in PeoplePeople, where: pp.child_uuid in ^current_scope, select: pp.parent_uuid)
      |> Repo.all()
    )
  end

  @spec get_scope_levels(DecisionVersion.t()) :: %{Ecto.UUID.t() => integer}
  defp get_scope_levels(%DecisionVersion{} = version) do
    get_scope_levels(
      [],
      version
      |> Repo.get_rel(:decision)
      |> DecisionRole.people_uuids_with_role(DecisionRole.Types.votable(:str), :uuids)
      |> Repo.all()
    )
  end

  defp best_path(paths, _, _) when length(paths) == 1 do
    {voted_delegatee_uuid, _path} = List.first(paths)
    voted_delegatee_uuid
  end

  defp best_path(paths, :precedence, _) do
    best_precedence =
      Enum.min_by(paths, fn {_d_uuid, path} ->
        {_, precedence} = List.last(path)
        precedence
      end)

    best_path([best_precedence], :precedence, 1)
  end

  defp best_path(paths, :scope, index) do
    {_, prototype_path} =
      Enum.min_by(paths, fn {_d_uuid, path} ->
        {scope, _} = Enum.at(path, index)
        scope
      end)

    {min_scope, _} = Enum.at(prototype_path, index)

    next_paths =
      Enum.filter(paths, fn {_d_uuid, path} ->
        {scope, _} = Enum.at(path, index)
        scope <= min_scope
      end)

    {next_param, next_index} =
      case Enum.at(prototype_path, index + 1) do
        nil -> {:precedence, 0}
        _ -> {:scope, index + 1}
      end

    best_path(next_paths, next_param, next_index)
  end

  defp get_resolution_step_delegations(delegator_uuids, scope_people_uuids, now) do
    from(
      d in Delegation,
      where:
        d.delegator_uuid in ^delegator_uuids and d.scope_people_uuid in ^scope_people_uuids and
          d.expires > ^now and d.starts <= ^now
    )
    |> Repo.all()
  end

  defp get_resolution_step_votes(delegations, current_delegators, scope_levels, dv_uuid) do
    delegatee_uuids =
      Enum.map(delegations, fn %Delegation{delegatee_uuid: delegatee_uuid} -> delegatee_uuid end)

    voted_delegatee_uuids =
      from(
        v in Vote,
        where: v.person_uuid in ^delegatee_uuids and v.decision_version_uuid == ^dv_uuid,
        select: v.person_uuid
      )
      |> Repo.all()

    next_delegators =
      delegations
      |> Enum.reduce(
        Map.new(),
        fn %Delegation{
             delegator_uuid: delegator_uuid,
             delegatee_uuid: delegatee_uuid,
             scope_people_uuid: scope_people_uuid,
             precedence: precedence
           },
           acc ->
          acc
          |> Map.put(
            delegatee_uuid,
            [
              {scope_levels |> Map.get(scope_people_uuid), precedence}
              | current_delegators |> Map.get(delegator_uuid)
            ]
          )
        end
      )

    {voted_delegatee_uuids, next_delegators}
  end

  defp resolve([], current_delegators, _, _, _) when map_size(current_delegators) <= 0, do: nil

  defp resolve([], current_delegators, scope_levels, now, dv_uuid) do
    delegator_uuids = Map.keys(current_delegators)
    scope_people_uuids = Map.keys(scope_levels)

    case get_resolution_step_delegations(delegator_uuids, scope_people_uuids, now) do
      [] ->
        resolve([], %{}, scope_levels, now, dv_uuid)

      delegations ->
        case get_resolution_step_votes(delegations, current_delegators, scope_levels, dv_uuid) do
          {[], next_delegators} ->
            resolve([], next_delegators, scope_levels, now, dv_uuid)

          {voted_delegatee_uuids, next_delegators} ->
            resolve(voted_delegatee_uuids, next_delegators, scope_levels, now, dv_uuid)
        end
    end
  end

  defp resolve(voted_delegatee_uuids, current_delegators, _, _, _) do
    voted_delegatee_paths = current_delegators |> Map.take(voted_delegatee_uuids)

    {_, shortest_paths} =
      voted_delegatee_paths
      |> Enum.group_by(fn {_d_uuid, path} -> length(path) end)
      |> Enum.min_by(fn {l, _} -> l end)

    best_path(shortest_paths, :scope, 0)
  end

  @doc """
    The aim of Delegation resolution is to find a Vote by a Person who is as local as possible to
    the original delegator (ODo) as possible.

    Every Delegation found is characterized by a “preference path”: a series of level-precedence
    values, where the values characterize the current Delegation and all of the predecessors traced
    back to the ODo by the level of scope broader than the DecisionVersion's original scope and the
    precedence for that Delegation’s own delegator, e.g.:

      [{2, 0}, {1, 0}, {0, 2}]

    If multiple Votes by delegatees are found, the preference paths leading to those delegatees is
    compared. The shortest path with the lowest level and precedence values determines the
    resultant Vote.
  """

  @spec resolve(Ecto.UUID.t(), DecisionVersion.t(), integer) :: Ecto.UUID.t() | nil
  def resolve(delegator_uuid, version, offset \\ 0) do
    resolve(
      [],
      %{delegator_uuid => [{0, -1}]},
      get_scope_levels(version),
      NaiveDateTime.add(Repo.now(), offset, :second),
      version.uuid
    )
  end
end
