defmodule Accord.Counter.Plurality do
  @behaviour Accord.Counter

  alias Accord.{Decision, DecisionVersion, DecisionSettings, Option}
  alias Accord.Counter.Plurality

  def initial_acc(%DecisionVersion{options: options}, _) do
    options
    |> Enum.reduce(Map.new(), fn %Option{id: option_uuid}, acc ->
      Map.put(acc, option_uuid, 0.0)
    end)
  end

  def accumulate_choices(choices, acc, power, _version) do
    case Map.keys(choices) |> List.first() do
      nil -> acc
      option_uuid -> acc |> Map.put(option_uuid, Map.get(acc, option_uuid) + power)
    end
  end

  def resolve_breakdown(accumulated_votes, _version),
    do: accumulated_votes

  defmacro __using__(_) do
    quote do
      @method_plurality DecisionSettings.Types.plurality(:str)

      defp initial_acc(
             %DecisionVersion{settings: %{method: @method_plurality}} = version,
             opts
           ),
           do: Plurality.initial_acc(version, opts)

      defp accumulate_choices(
             choices,
             acc,
             power,
             %DecisionVersion{
               settings: %{method: @method_plurality}
             } = version
           ),
           do: Plurality.accumulate_choices(choices, acc, power, version)

      defp resolve_breakdown(
             accumulated_votes,
             %DecisionVersion{
               settings: %{method: @method_plurality}
             } = version
           ),
           do: Plurality.resolve_breakdown(accumulated_votes, version)

      defp select_winning_option_uuids(
             summary_parameters,
             votes_and_vote_power,
             %DecisionSettings{method: @method_plurality} = settings
           ),
           do:
             Decision.standard_winning_option_uuids(
               summary_parameters,
               votes_and_vote_power,
               settings
             )
    end
  end
end
