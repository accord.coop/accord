defmodule Accord.Counter do
  alias Accord.{Decision, DecisionVersion, DecisionSettings, Vote}

  @type count_accumulator :: any

  @callback initial_acc(DecisionVersion.t(), Keyword.t()) :: count_accumulator

  @callback accumulate_choices(Vote.choices(), count_accumulator, float, DecisionVersion.t()) ::
              any

  @callback resolve_breakdown(count_accumulator, DecisionVersion.t()) :: Decision.breakdown()

  @callback select_winning_option_uuids(
              {Decision.breakdown(), %{number => [Vote.choice_id()]}, Decision.participation()},
              {[Vote.t()], %{Vote.choice_id() => number}},
              DecisionSettings.t()
            ) :: [Vote.choice_id()]

  @optional_callbacks select_winning_option_uuids: 3
end
