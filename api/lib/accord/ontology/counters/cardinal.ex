defmodule Accord.Counter.Cardinal do
  @behaviour Accord.Counter

  alias Accord.{Decision, DecisionVersion, DecisionSettings, Option, Vote}
  alias Accord.Counter.Cardinal

  @quant_absolute DecisionSettings.Types.absolute(:str)
  @quant_proportional DecisionSettings.Types.proportional(:str)

  def initial_acc(%DecisionVersion{options: options}, _) do
    options
    |> Enum.reduce(Map.new(), fn %Option{id: option_uuid}, acc ->
      Map.put(acc, option_uuid, {0.0, 0.0, 0.0})
    end)
  end

  defp block_value(_, %DecisionSettings{parameter3_value: nil}),
    do: 0

  defp block_value(value, %DecisionSettings{parameter3_value: p3v}),
    do: if(value < p3v, do: 1, else: 0)

  defp block_value(_, _),
    do: 0

  def accumulate_choices(choices, acc, power, %DecisionVersion{
        options: options,
        settings: %{parameter2_value: p2v} = settings
      }) do
    options
    |> Enum.reduce(acc, fn %Option{id: option_uuid}, acc ->
      {sum, n_votes, sum_blocks} = Map.get(acc, option_uuid)

      %{"value" => value} =
        Map.get(choices, option_uuid, %{"value" => if(p2v, do: p2v, else: 0.0)})

      acc
      |> Map.put(
        option_uuid,
        {
          sum + value * power,
          n_votes + power,
          sum_blocks + block_value(value, settings) * power
        }
      )
    end)
  end

  defp is_blocked?(block_value, _n_votes, %DecisionSettings{
         parameter4_value: p4v,
         parameter4_quantification: @quant_absolute
       })
       when block_value >= p4v,
       do: true

  defp is_blocked?(block_value, n_votes, %DecisionSettings{
         parameter4_value: p4v,
         parameter4_quantification: @quant_proportional
       })
       when block_value / n_votes >= p4v,
       do: true

  defp is_blocked?(_, _, _), do: false

  def resolve_breakdown(accumulated_votes, %DecisionVersion{settings: settings}) do
    accumulated_votes
    |> Enum.reduce(Map.new(), fn {option_uuid, {sum, n_votes, block_value}}, acc ->
      acc
      |> Map.put(
        option_uuid,
        if(is_blocked?(block_value, n_votes, settings), do: :blocked, else: sum)
      )
    end)
  end

  defp select_star_runoff_contestants({score_groups, option_uuids_by_score}, winners) do
    Enum.reduce_while(score_groups, MapSet.new(), fn score, contestants ->
      if MapSet.size(contestants) <= 1 do
        {:cont,
         Map.get(option_uuids_by_score, score)
         |> MapSet.new()
         |> MapSet.difference(winners)
         |> MapSet.union(contestants)}
      else
        {:halt, contestants}
      end
    end)
    |> MapSet.to_list()
  end

  defp compute_condorcet_pairs(contestants) do
    contestants
    |> Enum.reduce(MapSet.new(), fn left_contestant, pairs ->
      contestants
      |> Enum.reduce(pairs, fn
        right_contestant, pairs when left_contestant !== right_contestant ->
          pairs |> MapSet.put(MapSet.new([left_contestant, right_contestant]))

        _, pairs ->
          pairs
      end)
    end)
    |> MapSet.to_list()
  end

  defp count_pairwise_score(pair, {votes, vote_power_by_person_uuid}, %DecisionSettings{
         parameter2_value: p2v
       }) do
    [left | [right | []]] = pair |> MapSet.to_list()

    votes
    |> Enum.reduce(%{left => 0.0, right => 0.0}, fn %Vote{person_uuid: person_uuid} = vote,
                                                    scores_for_pair ->
      choices = vote |> Vote.get_choices()
      {_, power} = vote_power_by_person_uuid |> Map.get(person_uuid)

      %{"value" => left_value} = Map.get(choices, left, %{"value" => if(p2v, do: p2v, else: 0.0)})

      %{"value" => right_value} =
        Map.get(choices, right, %{"value" => if(p2v, do: p2v, else: 0.0)})

      delta = left_value - right_value

      cond do
        delta > 0 ->
          Map.put(scores_for_pair, left, Map.get(scores_for_pair, left) + 1.0 * power)

        delta < 0 ->
          Map.put(scores_for_pair, right, Map.get(scores_for_pair, right) + 1.0 * power)

        true ->
          scores_for_pair
      end
    end)
  end

  defp run_condorcet_contest(pairs, votes_and_vote_power, settings) do
    pairs
    |> Enum.map(fn pair -> count_pairwise_score(pair, votes_and_vote_power, settings) end)
    |> Enum.reduce(Map.new(), fn scores_for_pair, acc ->
      Enum.reduce(scores_for_pair, acc, fn {option_uuid, preference_score}, acc ->
        Map.put(acc, option_uuid, Map.get(acc, option_uuid, 0.0) + preference_score)
      end)
    end)
  end

  defp get_runoff_winners(contest_scores, breakdown, annotations, round) do
    contestants_by_score =
      Enum.group_by(
        contest_scores,
        fn {_, preference_score} -> preference_score end,
        fn {option_uuid, _} -> option_uuid end
      )

    max_contest_score = Enum.max(Map.keys(contestants_by_score))
    runoff_winners = Map.get(contestants_by_score, max_contest_score)

    runoff_winners_by_score =
      Enum.group_by(runoff_winners, fn option_uuid -> Map.get(breakdown, option_uuid) end)

    max_runoff_winner_score = Enum.max(Map.keys(runoff_winners_by_score))
    final_runoff_winners = Map.get(runoff_winners_by_score, max_runoff_winner_score)

    {
      final_runoff_winners |> MapSet.new(),
      final_runoff_winners
      |> Enum.reduce(annotations, fn option_uuid, acc ->
        Map.put(acc, option_uuid, %{round: round, contest: contest_scores})
      end)
    }
  end

  def star_winning_option_uuids(
        {score_breakdown, option_uuids_by_score, _participation},
        votes_and_vote_power,
        %DecisionSettings{n_winners_value: n_winners_value} = settings
      ) do
    score_groups = Map.keys(option_uuids_by_score) |> Enum.sort(&(&1 >= &2))

    {winners, annotations, _} =
      Enum.reduce_while(
        0..ceil(n_winners_value),
        {MapSet.new(), Map.new(), 0},
        fn _, {winners, annotations, round} = acc ->
          if MapSet.size(winners) < n_winners_value do
            # pick options not already in acc from top score group(s) until contestants > 1
            {runoff_winners, annotations} =
              select_star_runoff_contestants({score_groups, option_uuids_by_score}, winners)
              # compute collection of all pairwise combinations of contestants
              |> compute_condorcet_pairs()
              # run a condorcet contest for each pair and produce a breakdown mapping the sum of
              # contest scores to each option
              |> run_condorcet_contest(votes_and_vote_power, settings)
              # return contestants from top score and resolve ties
              |> get_runoff_winners(score_breakdown, annotations, round)

            {:cont, {winners |> MapSet.union(runoff_winners), annotations, round + 1}}
          else
            {:halt, acc}
          end
        end
      )

    {
      winners |> MapSet.to_list(),
      score_breakdown
      |> Enum.reduce(Map.new(), fn {option_uuid, option_score}, acc ->
        Map.put(acc, option_uuid, %{
          score: option_score,
          annotation: Map.get(annotations, option_uuid, nil)
        })
      end)
    }
  end

  defmacro __using__(_) do
    quote do
      @method_cardinal DecisionSettings.Types.cardinal(:str)
      @cardinal_resolution_sum DecisionSettings.Types.sum(:str)
      @cardinal_resolution_star DecisionSettings.Types.star(:str)

      defp initial_acc(
             %DecisionVersion{settings: %{method: @method_cardinal}} = version,
             opts
           ),
           do: Cardinal.initial_acc(version, opts)

      defp accumulate_choices(
             choices,
             acc,
             power,
             %DecisionVersion{
               options: _,
               settings: %{method: @method_cardinal, parameter2_value: _}
             } = version
           ),
           do: Cardinal.accumulate_choices(choices, acc, power, version)

      defp resolve_breakdown(
             accumulated_votes,
             %DecisionVersion{
               settings: %{method: @method_cardinal}
             } = version
           ),
           do: Cardinal.resolve_breakdown(accumulated_votes, version)

      defp select_winning_option_uuids(
             summary_parameters,
             votes_and_vote_power,
             %DecisionSettings{method: @method_cardinal, resolution: @cardinal_resolution_sum} =
               settings
           ),
           do:
             Decision.standard_winning_option_uuids(
               summary_parameters,
               votes_and_vote_power,
               settings
             )

      defp select_winning_option_uuids(
             summary_parameters,
             votes_and_vote_power,
             %DecisionSettings{
               method: @method_cardinal,
               resolution: @cardinal_resolution_star
             } = settings
           ),
           do:
             Cardinal.star_winning_option_uuids(
               summary_parameters,
               votes_and_vote_power,
               settings
             )
    end
  end
end
