defmodule Accord.Counter.OrdinalSTV do
  @behaviour Accord.Counter

  alias Accord.{Repo, Decision, DecisionVersion, DecisionVersionMoment, DecisionSettings, Option}
  alias Accord.Counter.OrdinalSTV
  alias Accord.Caches.STVScottBallotTree, as: BallotTree
  alias Accord.Caches.STVScottAllocation, as: Allocation

  @stv_quota_droop DecisionSettings.Types.droop(:str)
  @stv_quota_hare DecisionSettings.Types.hare(:str)

  defp quota_value(total_participation, %DecisionSettings{
         n_winners_value: n_winners,
         quota: @stv_quota_droop
       }),
       do: floor(total_participation / (n_winners + 1)) + 1

  defp quota_value(total_participation, %DecisionSettings{
         n_winners_value: n_winners,
         quota: @stv_quota_hare
       }),
       do: floor(total_participation / n_winners)

  def initial_acc(
        %DecisionVersion{options: options, settings: settings} = version,
        {direct_power, delegated_power}
      ) do
    {:ok, %{decision_version_moment: %{uuid: moment_uuid}}} =
      DecisionVersionMoment.create(
        version,
        Repo.now(),
        DecisionVersionMoment.Types.async_count(:str)
      )
      |> Repo.commit()

    %{
      moment_uuid: moment_uuid,
      iteration: 0,
      quota_value: quota_value(direct_power + delegated_power, settings),
      continuing: options |> Enum.map(fn %Option{id: option_uuid} -> option_uuid end),
      selected: [],
      rejected: []
    }
  end

  def accumulate_choices(choices, %{moment_uuid: moment_uuid} = acc, power, _version) do
    choice_ids_in_order =
      choices
      |> Enum.filter(fn {_, %{"value" => value}} -> is_float(value) end)
      |> Enum.sort_by(fn {_, %{"value" => value}} -> value end, &<=/2)
      |> Enum.map(fn {choice_id, _} -> choice_id end)

    BallotTree.put(
      moment_uuid,
      choice_ids_in_order |> Enum.join(".") |> BallotTree.uuid_to_label(),
      power
    )

    acc
  end

  defp build_breakdown(%{moment_uuid: moment_uuid}, _version) do
    Allocation.get_totals(moment_uuid) |> Enum.into(%{})
  end

  defp make_initial_allocations(
         %{moment_uuid: moment_uuid, iteration: iteration} = acc,
         %DecisionVersion{options: options} = version
       ) do
    options
    |> Enum.reduce([], fn %Option{id: option_uuid}, acc ->
      selection = "#{option_uuid |> BallotTree.uuid_to_label()}"
      quotient = 1.0

      [
        Allocation.create(
          moment_uuid,
          iteration,
          option_uuid,
          selection,
          quotient,
          BallotTree.vote_power_at_path(moment_uuid, selection <> ".*") * quotient,
          name: "allocation#{iteration}__#{option_uuid}"
        )
        |> List.first()
        | acc
      ]
    end)
    |> Repo.commit()

    acc |> Map.put(:iteration, 1) |> resolve_breakdown(version)
  end

  # Base case: return breakdown when enough winners are selected.

  def resolve_breakdown(
        %{selected: selected, continuing: continuing} = acc,
        %DecisionVersion{settings: %{n_winners_value: n_winners}} = version
      )
      when length(selected) >= n_winners or length(continuing) <= 0,
      do: build_breakdown(acc, version)

  # Initial case: make any preparations that aren't part of the usual STV flow.

  def resolve_breakdown(
        %{iteration: 0} = acc,
        %DecisionVersion{} = version
      ),
      do: make_initial_allocations(acc, version)

  # Typical case: follow usual STV flow here.

  def resolve_breakdown(
        %{
          moment_uuid: moment_uuid,
          continuing: continuing,
          rejected: rejected,
          selected: selected,
          quota_value: quota_value,
          iteration: iteration
        } = acc,
        %DecisionVersion{settings: %{n_winners_value: n_winners}} = version
      ) do
    next_iteration = iteration + 1

    allocation_totals = Allocation.get_totals(moment_uuid)

    %{true => options_with_surplus, false => options_without_surplus} =
      %{true => [], false => []}
      |> Map.merge(
        allocation_totals
        |> Enum.filter(fn {option_uuid, _} -> option_uuid in continuing end)
        |> Enum.group_by(fn {_, allocation} -> allocation > quota_value end)
      )

    # 1) of the continuing candidates, do any have a greater allocation than quota?

    if length(options_with_surplus) > 0 do
      # 1.1) if yes, add candidate as selected and distribute whole allocation as the surplus's fraction
      {srs_uuid, srs_allocation} =
        options_with_surplus
        |> Enum.sort_by(fn {_, allocation} -> allocation end, &>=/2)
        |> List.first()

      next_continuing = continuing |> List.delete(srs_uuid)
      surplus_allocation_quotient = (srs_allocation - quota_value) / srs_allocation

      Allocation.reallocate(
        {{srs_uuid, srs_allocation}, next_continuing, surplus_allocation_quotient},
        {moment_uuid, iteration}
      )

      resolve_breakdown(
        acc
        |> Map.merge(%{
          iteration: next_iteration,
          continuing: next_continuing,
          selected: [srs_uuid | selected]
        }),
        version
      )
    else
      # 2) if no continuing candidate has a greater allocation than quota, is the size of continuing candidates greater than the selected candidates plus the number of winners?
      if length(options_without_surplus) > n_winners - length(selected) do
        # 2.1) if yes, redistribute the continuing candidate with the smallest allocation
        {rrs_uuid, rrs_allocation} =
          options_without_surplus
          |> Enum.sort_by(fn {_, allocation} -> allocation end, &<=/2)
          |> List.first()

        # 2.1.1) select any candidates that happen to have exactly the quota

        selected_during_rejection =
          options_without_surplus
          |> Enum.filter(fn {_, allocation} -> allocation >= quota_value end)
          |> Enum.map(fn {option_uuid, _} -> option_uuid end)

        next_selected = selected ++ selected_during_rejection

        remove_from_continuing = [rrs_uuid | selected_during_rejection]

        next_continuing =
          continuing
          |> Enum.filter(fn option_uuid -> option_uuid not in remove_from_continuing end)

        Allocation.reallocate(
          {{rrs_uuid, rrs_allocation}, next_continuing, 1.0},
          {moment_uuid, iteration}
        )

        resolve_breakdown(
          acc
          |> Map.merge(%{
            iteration: next_iteration,
            selected: next_selected,
            continuing: next_continuing,
            rejected: [rrs_uuid | rejected]
          }),
          version
        )
      else
        # 2.2) if no, add continuing candidates to selection
        resolve_breakdown(
          acc
          |> Map.merge(%{
            iteration: next_iteration,
            continuing: [],
            selected: selected ++ continuing
          }),
          version
        )
      end
    end
  end

  defmacro __using__(_) do
    quote do
      @method_stv DecisionSettings.Types.ordinal_stv(:str)

      defp initial_acc(%DecisionVersion{settings: %{method: @method_stv}} = version, opts),
        do: OrdinalSTV.initial_acc(version, opts[:participation])

      defp accumulate_choices(
             choices,
             acc,
             power,
             %DecisionVersion{
               settings: %{method: @method_stv}
             } = version
           ),
           do: OrdinalSTV.accumulate_choices(choices, acc, power, version)

      defp resolve_breakdown(
             acc,
             %DecisionVersion{
               settings: %{method: @method_stv}
             } = version
           ),
           do: OrdinalSTV.resolve_breakdown(acc, version)

      defp select_winning_option_uuids(
             summary_parameters,
             votes_and_vote_power,
             %DecisionSettings{method: @method_stv} = settings
           ),
           do:
             Decision.standard_winning_option_uuids(
               summary_parameters,
               votes_and_vote_power,
               settings
             )
    end
  end
end
