defmodule Accord.NativeCredentials do
  @moduledoc """
    Manages credentials for native Personas
  """

  alias Accord.{Repo, NativeCredentials, Errors}
  alias AccordWeb.PasswordStrength

  import Ecto.Changeset
  use Ecto.Schema

  @type t :: %__MODULE__{}

  @expired ~N[1970-01-01 00:00:00]

  embedded_schema do
    field :pw, :string
    field :pw_reset_token, :string, default: ""
    field :pw_reset_token_expiry, :naive_datetime, default: @expired
  end

  @doc false
  def changeset(native_credentials, attrs \\ %{}) do
    native_credentials
    |> cast(attrs, [:pw, :pw_reset_token, :pw_reset_token_expiry])
    |> validate_required([:pw])
    |> check_password_strength(Map.get(attrs, :email), Map.get(attrs, :preferred_name))
    |> put_password_hash
  end

  @spec new(String.t()) :: Ecto.Changeset.t()
  def new(pw) do
    changeset(
      %NativeCredentials{},
      %{pw: pw}
    )
  end

  # Check password complexity

  @spec check_password_strength(Ecto.Changeset.t(), any, any) :: Ecto.Changeset.t()
  defp check_password_strength(
         %Ecto.Changeset{changes: %{pw: pw}} = changeset,
         provided_email,
         provided_name
       ) do
    case PasswordStrength.check(pw, email: provided_email, name: provided_name).passes do
      false -> add_error(changeset, :pw, Errors.weak_pw(:str))
      true -> changeset
    end
  end

  defp check_password_strength(%Ecto.Changeset{} = changeset, _, _), do: changeset

  # Put password hash

  @spec put_password_hash(Ecto.Changeset.t()) :: Ecto.Changeset.t()
  defp put_password_hash(%Ecto.Changeset{changes: %{pw: pw}} = changeset) do
    changeset |> put_change(:pw, Argon2.hash_pwd_salt(pw))
  end

  defp put_password_hash(%Ecto.Changeset{} = changeset), do: changeset

  # Facilitate password reset

  @spec set_pw_reset_token(NativeCredentials.t()) :: Ecto.Changeset.t()
  def set_pw_reset_token(%NativeCredentials{} = credentials) do
    credentials
    |> change(%{
      pw_reset_token: Ecto.UUID.generate(),
      pw_reset_token_expiry:
        NaiveDateTime.add(
          Repo.now(),
          86_400,
          :second
        )
    })
  end

  @spec has_valid_pw_reset_token?(NativeCredentials.t()) :: boolean
  defp has_valid_pw_reset_token?(%NativeCredentials{
         pw_reset_token: token,
         pw_reset_token_expiry: expiry
       }) do
    String.length(token) === 36 and
      NaiveDateTime.diff(expiry, Repo.now(), :second) > 0
  end

  @spec update_pw(NativeCredentials.t(), String.t(), String.t()) ::
          {:ok, Ecto.Changeset.t()} | {:error, :token_invalid}
  def update_pw(%NativeCredentials{} = credentials, received_token, new_pw) do
    if(
      has_valid_pw_reset_token?(credentials) and credentials.pw_reset_token === received_token
    ) do
      {:ok, new(new_pw)}
    else
      {:error, :token_invalid}
    end
  end
end
