defmodule Accord.Caches.OAuthState do
  @moduledoc """
    The schema and changeset for cached OAuth state values.
    These improve the security of OAuth flows.
  """

  alias Accord.{Repo, Caches}

  import Ecto.Changeset
  import Ecto.Query, only: [{:from, 2}]

  use Ecto.Schema

  @type t :: %__MODULE__{}

  @primary_key {:state, :binary_id, autogenerate: true}

  schema "oauth_states" do
    timestamps()
  end

  def new() do
    %Caches.OAuthState{} |> change
  end

  defp expiry() do
    Repo.now() |> NaiveDateTime.add(-7_200, :second)
  end

  @spec expired_states() :: Ecto.Query.t()
  defp expired_states() do
    from(
      s in Caches.OAuthState,
      where: s.inserted_at < ^expiry()
    )
  end

  @spec prune_and_new() :: [Repo.action()]
  def prune_and_new() do
    [
      {:expireds, :delete_all, {expired_states(), []}},
      {:state, :insert, {new(), []}}
    ]
  end
end
