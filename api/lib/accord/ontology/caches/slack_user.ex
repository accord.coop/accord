defmodule Accord.Caches.SlackUser do
  @moduledoc """
    The schema and changeset for cached Slack users.
    This is primarily for storing presumably immutable properties of a Slack user to reduce reliance on API calls
    for determining e.g. whether a user is a bot.
  """

  alias Accord.{Repo, Caches}

  import Ecto.Changeset
  import Ecto.Query, only: [{:from, 2}]

  use Ecto.Schema

  @type t :: %__MODULE__{}

  schema "cached_slack_users" do
    field :service_id, :string
    field :is_bot, :boolean

    timestamps()
  end

  @doc false
  def changeset(slack_user, attrs \\ %{}) do
    slack_user
    |> cast(attrs, [:service_id, :is_bot])
  end

  def new(attrs) do
    changeset(%Caches.SlackUser{}, attrs)
  end

  @on_conflict [
    on_conflict: {:replace, [:is_bot, :updated_at]},
    conflict_target: [:service_id]
  ]

  @spec put(Map.t()) :: {:ok, Caches.SlackUser.t()} | {:error, Ecto.Changeset.t()}
  def put(attrs) do
    new(attrs) |> Repo.insert(@on_conflict)
  end

  @spec create(Map.t()) :: [Repo.action()]
  def create(attrs) do
    [{:slack_user_cached, :insert, {new(attrs), @on_conflict}}]
  end

  @spec get(String.t()) :: Caches.SlackUser.t()
  def get(service_id) do
    from(
      csu in Caches.SlackUser,
      where: csu.service_id == ^service_id
    )
    |> Repo.one()
  end

  @spec get_cached(MapSet.t(String.t())) :: MapSet.t(String.t())
  def get_cached(service_ids) do
    service_ids_list = MapSet.to_list(service_ids)

    from(
      csu in Caches.SlackUser,
      where: csu.service_id in ^service_ids_list,
      select: csu.service_id
    )
    |> Repo.all()
    |> MapSet.new()
  end

  @spec get_missing(MapSet.t(String.t())) :: MapSet.t(String.t())
  def get_missing(service_ids) do
    MapSet.difference(
      service_ids,
      get_cached(service_ids)
    )
  end

  @spec annotate(MapSet.t(String.t())) :: %{String.t() => %{missing?: bool, is_bot: bool | nil}}
  def annotate(service_ids) do
    service_ids_list = MapSet.to_list(service_ids)

    annotated_cached =
      from(
        csu in Caches.SlackUser,
        where: csu.service_id in ^service_ids_list,
        select: {csu.service_id, csu.is_bot}
      )
      |> Repo.all()
      |> Enum.reduce(
        Map.new(),
        fn {service_id, is_bot}, acc ->
          Map.put(acc, service_id, %{missing?: false, is_bot: is_bot})
        end
      )

    Enum.reduce(service_ids, annotated_cached, fn service_id, acc ->
      if Map.has_key?(acc, service_id),
        do: acc,
        else: Map.put(acc, service_id, %{missing?: true, is_bot: nil})
    end)
  end
end
