defmodule Accord.Caches.DecisionVersionResult.Summary do
  @moduledoc """
    Summarizes a result
  """

  import Ecto.Changeset

  use Ecto.Schema

  @type t :: %__MODULE__{}

  embedded_schema do
    field :breakdown, :map
    field :winning_option_uuids, {:array, :binary_id}
    field :n_votes, :float
    field :n_voters, :float
    field :n_delegations, :float
  end

  @doc false
  def changeset(summary, attrs \\ %{}) do
    summary
    |> cast(attrs, [:breakdown, :winning_option_uuids, :n_votes, :n_voters])
  end
end

defmodule Accord.Caches.DecisionVersionResult do
  alias Accord.{
    Repo,
    Decision,
    DecisionVersionMoment
  }

  alias Accord.Caches.DecisionVersionResult, as: Result
  alias Accord.Caches.DecisionVersionResult.Summary, as: Summary

  import Ecto.Changeset

  use Ecto.Schema

  @type t :: %__MODULE__{}

  schema("decision_version_results") do
    embeds_one :summary, Accord.Caches.DecisionVersionResult.Summary, on_replace: :delete

    belongs_to :decision_version_moment, DecisionVersionMoment,
      foreign_key: :decision_version_moment_uuid,
      references: :uuid,
      type: :binary_id

    timestamps()
  end

  @spec new(DecisionVersionMoment.t(), integer, MapSet.t(Ecto.UUID.t())) :: Result.t()
  def new(dv_moment, n_possible_voters, known_votable_person_uuids) do
    %Result{}
    |> change()
    |> put_embed(
      :summary,
      dv_moment
      |> Repo.get_rel(:decision_version)
      |> Decision.count(n_possible_voters, known_votable_person_uuids),
      with: &Summary.changeset/2
    )
    |> put_assoc(:decision_version_moment, dv_moment)
  end

  @create_on_conflict [
    on_conflict: :replace_all,
    conflict_target: [:decision_version_moment_uuid],
    returning: true
  ]

  @spec create(DecisionVersionMoment.t(), integer, MapSet.t(Ecto.UUID.t())) :: [Repo.action()]
  def create(%DecisionVersionMoment{} = dv_moment, n_voters, known_votable_person_uuids) do
    [
      {:decision_version_result, :insert,
       {new(dv_moment, n_voters, known_votable_person_uuids), @create_on_conflict}}
    ]
  end
end
