defmodule Accord.Caches.STVScottBallotTree do
  @moduledoc """
    The schema and changeset for cached OAuth state values.
    These improve the security of OAuth flows.
  """

  alias Accord.{Repo, DecisionVersionMoment}
  alias Accord.Caches.STVScottBallotTree, as: BallotTree
  alias EctoLtree.LabelTree, as: Ltree

  import Ecto.Changeset
  import Ecto.Query, only: [{:from, 2}]

  use Ecto.Schema

  @primary_key false

  @type t :: %__MODULE__{}

  schema "cached_stv_scott_ballot_trees" do
    belongs_to :decision_version_moment, DecisionVersionMoment,
      foreign_key: :decision_version_moment_uuid,
      references: :uuid,
      type: :binary_id,
      primary_key: true

    field :path, Ltree, primary_key: true

    field :vote_power, :float
  end

  def uuid_to_label(uuid), do: uuid |> String.replace("-", "_")

  @doc false
  def changeset(ballot_tree, attrs \\ %{}) do
    ballot_tree
    |> cast(attrs, [:decision_version_moment_uuid, :path, :vote_power])
  end

  def new(attrs) do
    changeset(%BallotTree{}, attrs)
  end

  def get(_, ""), do: nil

  def get(moment_uuid, path) do
    from(
      cbt in BallotTree,
      where: cbt.decision_version_moment_uuid == ^moment_uuid and cbt.path == ^path,
      select: cbt.vote_power
    )
    |> Repo.one()
  end

  @on_conflict [
    on_conflict: {:replace, [:vote_power]},
    conflict_target: [:decision_version_moment_uuid, :path]
  ]

  def put(moment_uuid, path, vote_power) do
    new(%{
      decision_version_moment_uuid: moment_uuid,
      path: path,
      vote_power:
        case get(moment_uuid, path) do
          nil -> vote_power
          extant_power -> extant_power + vote_power
        end
    })
    |> Repo.insert(@on_conflict)
  end

  def vote_power_at_path(moment_uuid, lquery) do
    case from(
           cbt in BallotTree,
           where:
             cbt.decision_version_moment_uuid == ^moment_uuid and
               fragment("? ~ ?", cbt.path, ^lquery),
           select: sum(cbt.vote_power)
         )
         |> Repo.all()
         |> List.first() do
      nil -> 0.0
      vote_power -> vote_power
    end
  end

  def vote_power_at_paths(moment_uuid, paths) do
    #    lqueries = paths |> Enum.map(fn path -> "#{path}.*" end)
    #
    #    vote_power_for_continuing =
    #      case from(
    #             cbt in BallotTree,
    #             where:
    #               cbt.decision_version_moment_uuid == ^moment_uuid and
    #                 fragment("? \\? ?", cbt.path, ^lqueries),
    #             select: sum(cbt.vote_power)
    #           )
    #           |> Repo.all()
    #           |> List.first() do
    #        nil -> 0.0
    #        vote_power -> vote_power
    #      end

    {:ok, %{rows: [[vote_power]]}} =
      Ecto.Adapters.SQL.query(
        Repo,
        """
        SELECT sum(c0."vote_power")
        FROM "cached_stv_scott_ballot_trees" AS c0
        WHERE ((c0."decision_version_moment_uuid" = $1) AND c0."path" ? array[#{
          paths
          |> Enum.map(fn path -> "'#{path}.*'" end)
          |> Enum.join(",")
        }]::lquery[])
        """,
        [UUID.string_to_binary!(moment_uuid)]
      )

    case vote_power do
      nil -> 0.0
      _ -> vote_power
    end
  end

  def vote_power_for_paths(moment_uuid, paths) do
    {:ok, %{rows: rows}} =
      Ecto.Adapters.SQL.query(
        Repo,
        """
        SELECT c0."path", sum(c0."vote_power")
        FROM "cached_stv_scott_ballot_trees" AS c0
        WHERE ((c0."decision_version_moment_uuid" = $1) AND c0."path" ? array[#{
          paths
          |> Enum.map(fn path -> "'#{path}.*'" end)
          |> Enum.join(",")
        }]::lquery[])
        GROUP BY c0."path"
        """,
        [UUID.string_to_binary!(moment_uuid)]
      )

    rows |> Enum.map(fn [path, vote_power] -> {path, vote_power} end)
  end

  def next_preference_summary(moment_uuid, path_inventory, continuing) do
    {sum, summary} =
      continuing
      |> Enum.reduce({0.0, %{}}, fn continuing_uuid, {sum, summary} ->
        vote_power_for_continuing =
          vote_power_at_paths(
            moment_uuid,
            path_inventory
            |> Enum.map(fn path -> "#{path}.#{continuing_uuid |> uuid_to_label()}" end)
          )

        {sum + vote_power_for_continuing,
         Map.put(summary, continuing_uuid, vote_power_for_continuing)}
      end)

    summary
    |> Enum.reduce(%{}, fn {continuing_uuid, total_vote_power}, acc ->
      Map.put(
        acc,
        continuing_uuid,
        {total_vote_power, if(sum <= 0, do: 1.0, else: total_vote_power / sum)}
      )
    end)
  end
end
