# Accord caches

The entities here should only be used to store information that is short-lived & trivial and/or derived completely from more definitive sources.
