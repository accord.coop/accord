defmodule Accord.Caches.STVScottAllocation do
  @moduledoc """
    The schema and changeset for cached OAuth state values.
    These improve the security of OAuth flows.
  """

  alias Accord.{Repo, DecisionVersionMoment}
  alias Accord.Caches.STVScottAllocation, as: Allocation
  alias Accord.Caches.STVScottBallotTree, as: BallotTree

  import Ecto.Changeset
  import Ecto.Query, only: [{:from, 2}]

  use Ecto.Schema

  @primary_key false

  @type t :: %__MODULE__{}

  @null_recipient "00000000-0000-0000-0000-000000000000"

  schema "cached_stv_scott_allocations" do
    belongs_to :decision_version_moment, DecisionVersionMoment,
      foreign_key: :decision_version_moment_uuid,
      references: :uuid,
      type: :binary_id,
      primary_key: true

    field :iteration, :integer, primary_key: true

    field :recipient, :binary_id, primary_key: true

    field :selection, :string, primary_key: true

    field :as_quotient, :float

    field :net_vote_power, :float
  end

  @doc false
  defp changeset(allocation, attrs) do
    allocation
    |> cast(attrs, [
      :decision_version_moment_uuid,
      :iteration,
      :recipient,
      :selection,
      :as_quotient,
      :net_vote_power
    ])
    |> validate_required([
      :decision_version_moment_uuid,
      :iteration,
      :recipient,
      :selection,
      :as_quotient,
      :net_vote_power
    ])
  end

  def new(moment_uuid, iteration, recipient, selection, as_quotient, net_vote_power) do
    changeset(
      %Allocation{},
      %{
        decision_version_moment_uuid: moment_uuid,
        iteration: iteration,
        recipient: recipient,
        selection: selection,
        as_quotient: as_quotient,
        net_vote_power: net_vote_power
      }
    )
  end

  @create_on_conflict [on_conflict: :raise]
  def create(
        moment_uuid,
        iteration,
        recipient,
        selection,
        as_quotient,
        net_vote_power,
        opts \\ []
      ) do
    [
      {Keyword.get(opts, :name, :allocation), :insert,
       {new(moment_uuid, iteration, recipient, selection, as_quotient, net_vote_power),
        @create_on_conflict}}
    ]
  end

  def get_totals(moment_uuid) do
    from(
      a in Allocation,
      where: a.decision_version_moment_uuid == ^moment_uuid,
      group_by: a.recipient,
      select: {a.recipient, sum(a.net_vote_power)}
    )
    |> Repo.all()
  end

  def reallocate(
        {{subject_uuid, subject_net_vote_power}, continuing, quotient},
        {moment_uuid, iteration}
      ) do
    # first, allocate the negative quotient to the subject
    allocations =
      create(
        moment_uuid,
        iteration,
        subject_uuid,
        @null_recipient,
        quotient * -1.0,
        subject_net_vote_power * quotient * -1.0,
        name: "allocation_negative"
      )

    # then, get all positive allocations for subject…

    path_inventory =
      from(
        a in Allocation,
        where: a.decision_version_moment_uuid == ^moment_uuid and a.recipient == ^subject_uuid,
        group_by: a.selection,
        select: a.selection
      )
      |> Repo.all()

    # finally sum the voting power for all continuing candidates next on those ballots

    next_preference_summary =
      BallotTree.next_preference_summary(moment_uuid, path_inventory, continuing)

    Enum.reduce(continuing, allocations, fn continuing_uuid, acc ->
      {_continuing_vote_power, continuing_quotient} =
        Map.get(next_preference_summary, continuing_uuid)

      BallotTree.vote_power_for_paths(
        moment_uuid,
        path_inventory
        |> Enum.map(fn path -> "#{path}.#{continuing_uuid |> BallotTree.uuid_to_label()}" end)
      )
      |> Enum.reduce(acc, fn {selection, vote_power}, acc ->
        [
          create(
            moment_uuid,
            iteration,
            continuing_uuid,
            selection,
            quotient * continuing_quotient,
            quotient * vote_power,
            name: "allocation_positive__#{length(acc)}"
          )
          |> List.first()
          | acc
        ]
      end)
    end)
    |> Repo.commit()
  end
end
