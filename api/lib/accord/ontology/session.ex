defmodule Accord.Session do
  @moduledoc """
  The schema and changeset for Session entities.
  """

  alias Accord.{Repo, Persona, Session}

  import Ecto.Changeset
  import Ecto.Query, only: [{:from, 2}]

  use Ecto.Schema

  @type t :: %__MODULE__{}

  @primary_key {:token, :string, autogenerate: false}

  schema "sessions" do
    belongs_to :persona, Persona, foreign_key: :persona_uuid, references: :uuid, type: :binary_id
    field :expires, :naive_datetime
    timestamps()
  end

  @doc false
  def changeset(session, attrs) do
    session
    |> cast(attrs, [:expires, :token])
    |> validate_required([:expires, :token])
    |> unique_constraint(:token)
    |> assoc_constraint(:persona)
  end

  @spec new(Persona.t(), NaiveDateTime.t(), String.t()) :: Ecto.Changeset.t()
  def new(%Persona{} = persona, %NaiveDateTime{} = expires, token) do
    changeset(
      %Session{},
      %{
        expires: expires,
        token: token
      }
    )
    |> put_assoc(:persona, persona)
  end

  @spec create(atom, Persona.t(), NaiveDateTime.t(), String.t()) :: [Repo.action()]
  def create(name \\ :session, %Persona{} = persona, %NaiveDateTime{} = expires, token) do
    [{name, :insert, {new(persona, expires, token), []}}]
  end

  @spec expired_sessions_for_persona(Persona.t()) :: Ecto.Query.t()
  defp expired_sessions_for_persona(%Persona{} = persona) do
    from(
      s in Session,
      where: s.persona_uuid == ^persona.uuid and s.expires < ^Repo.now()
    )
  end

  @spec prune_and_create(atom, Persona.t(), NaiveDateTime.t(), String.t()) :: [Repo.action()]
  def prune_and_create(name \\ :session, persona, expires, new_token) do
    [
      {:expireds, :delete_all, {expired_sessions_for_persona(persona), []}},
      {name, :insert, {new(persona, expires, new_token), []}}
    ]
  end
end
