defmodule Accord.Persona do
  @moduledoc """
    The schema and changeset for Persona entities, which track a Person's account on third-party services.
  """

  alias Accord.{Repo, Person, Persona, NativeCredentials, Errors}

  import Ecto.Changeset
  import Ecto.Query, only: [{:from, 2}]

  use Accord.Schema

  @type t :: %__MODULE__{}

  @email ~r/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

  @type signature :: {:accord, String.t(), String.t() | nil} | {:slack, String.t(), String.t()}

  schema "personas" do
    field :service_id, :string
    field :service_type, :string
    field :service_properties, :map, default: %{}

    embeds_one :native_credentials, NativeCredentials, on_replace: :delete

    belongs_to :person, Person, foreign_key: :person_uuid, references: :uuid, type: :binary_id

    timestamps()
  end

  @doc false
  def changeset(persona, attrs \\ %{}) do
    persona
    |> cast(attrs, [:service_id, :service_type, :service_properties])
    |> cast_embed(:native_credentials)
    |> validate_required([:service_id, :service_type])
    |> validate_service_id
    |> unique_constraint(:service_id)
    |> make_credentials(attrs)
    |> assoc_constraint(:person)
  end

  @spec validate_service_id(Ecto.Changeset.t()) :: Ecto.Changeset.t()
  # Checks if email is correct for native ID's, otherwise leaves it be
  defp validate_service_id(%Ecto.Changeset{changes: %{service_id: service_id}} = changeset) do
    [service | [id | _]] = String.split(service_id, "︰")

    case service do
      "accord" -> validate_email(changeset, id)
      _ -> changeset
    end
  end

  defp validate_service_id(%Ecto.Changeset{} = changeset), do: changeset

  @spec validate_email(Ecto.Changeset.t(), String.t()) :: Ecto.Changeset.t()
  defp validate_email(changeset, email) do
    unless String.match?(email, @email) do
      add_error(changeset, :email, Errors.bad_args(:str))
    else
      changeset
    end
  end

  # Check credentials:
  # If this is a new native Persona, create new credentials from attrs.pw, error if this isn’t provided
  @spec make_credentials(Ecto.Changeset.t(), Map.t()) :: Ecto.Changeset.t()
  defp make_credentials(
         %Ecto.Changeset{changes: %{service_id: "accord︰" <> _id}} = changeset,
         %{pw: pw}
       ) do
    changeset |> put_embed(:native_credentials, NativeCredentials.new(pw))
  end

  defp make_credentials(
         %Ecto.Changeset{changes: %{service_id: "accord︰" <> _id}} = changeset,
         _attrs
       ) do
    changeset |> add_error(:service_id, Errors.bad_args(:str))
  end

  defp make_credentials(%Ecto.Changeset{} = changeset, _), do: changeset

  # Changeset tools

  @spec service_id(signature) :: String.t()
  def service_id({:accord, email}), do: "accord︰#{email}"
  def service_id({:accord, email, _pw}), do: "accord︰#{email}"
  def service_id({:slack, team_id, user_id}), do: "slack︰#{team_id}︰#{user_id}"

  @spec get_slack_user_id(String.t()) :: String.t()
  def get_slack_user_id("slack︰" <> team_scoped_user_id) do
    [_team_id | [user_id]] = team_scoped_user_id |> String.split("︰")
    user_id
  end

  @spec get(signature) :: Persona.t() | nil
  def get({:slack, team_id, user_id}),
    do: get_by_service_id(service_id({:slack, team_id, user_id}))

  def get({:accord, email, _pw}), do: get_by_service_id(service_id({:accord, email}))

  @spec get(term()) :: Persona.t() | nil
  def get(id), do: Repo.get(Persona, id)

  @spec get_by_service_id(String.t()) :: Persona.t() | nil
  def get_by_service_id(service_id) do
    from(p in Persona, where: p.service_id == ^service_id) |> Repo.one()
  end

  @spec get_known_service_ids(MapSet.t(String.t())) :: MapSet.t(String.t())
  def get_known_service_ids(%MapSet{} = service_ids),
    do: get_known_service_ids(MapSet.to_list(service_ids))

  @spec get_known_service_ids([String.t()]) :: MapSet.t(String.t())
  def get_known_service_ids(service_ids) do
    from(
      p in Persona,
      where: p.service_id in ^service_ids,
      select: p.service_id
    )
    |> Repo.all()
    |> MapSet.new()
  end

  @spec new(Map.t()) :: Ecto.Changeset.t()
  defp new(params), do: changeset(%Persona{}, params)

  @spec new(signature, Person.t()) :: Ecto.Changeset.t()
  def new({:slack, team_id, user_id}, person) do
    new(%{
      service_id: service_id({:slack, team_id, user_id}),
      service_type: "slack︰user",
      service_properties: %{"home_opened" => false}
    })
    |> put_assoc(:person, person)
  end

  def new({:accord, email, pw}, person) do
    new(%{
      service_id: service_id({:accord, email}),
      service_type: "accord︰person",
      pw: pw
    })
    |> put_assoc(:person, person)
  end

  @spec change_service_properties(Persona.t(), Map.t()) :: Ecto.Changeset.t() | {:error, any}
  def change_service_properties(%Persona{} = persona, %{} = service_properties) do
    persona |> changeset(%{service_properties: service_properties})
  end

  @spec change_pw_reset_token(Persona.t()) :: Ecto.Changeset.t()
  def change_pw_reset_token(%Persona{} = persona) do
    persona
    |> change
    |> put_embed(
      :native_credentials,
      NativeCredentials.set_pw_reset_token(persona.native_credentials)
    )
  end

  @spec change_pw(Persona.t(), String.t(), String.t()) :: Ecto.Changeset.t() | {:error, any()}
  def change_pw(%Persona{} = persona, received_token, new_pw) do
    case NativeCredentials.update_pw(persona.native_credentials, received_token, new_pw) do
      {:ok, credentials_changeset} ->
        persona
        |> change
        |> put_embed(
          :native_credentials,
          credentials_changeset
        )

      {:error, _} ->
        persona
        |> change
        |> add_error(:native_credentials, Errors.save_failed(:str))
    end
  end

  @behaviour Access

  @impl true
  def fetch(%Persona{} = persona, key), do: Map.fetch(persona, key)

  @impl true
  def get_and_update(%Persona{} = persona, key, fun), do: Map.get_and_update(persona, key, fun)

  @impl true
  def pop(%Persona{} = persona, key), do: Map.pop(persona, key, nil)
end
