defmodule Accord.DecisionSettings.Templates do
  import AccordWeb.Gettext
  alias Accord.DecisionSettings.Types

  def plurality(opts \\ []) do
    %{
      method: Types.plurality(:str)
    }
    |> Map.merge(opts |> Enum.into(%{}))
  end

  def approval(opts \\ []) do
    %{
      method: Types.cardinal(:str),
      resolution: Types.sum(:str),
      parameter0_quantification: Types.absolute(:str),
      parameter0_value: 1.0,
      parameter1_quantification: Types.absolute(:str),
      parameter1_value: 1.0,
      parameter2_quantification: Types.absolute(:str),
      parameter2_value: 0.0,
      parameter3_quantification: Types.absolute(:str),
      parameter3_value: 0.0
    }
    |> Map.merge(opts |> Enum.into(%{}))
  end

  def consensus(opts \\ []) do
    %{
      method: Types.cardinal(:str),
      resolution: Types.sum(:str),
      parameter0_quantification: Types.proportional_exclusive(:str),
      parameter0_value: 1.0,
      parameter1_quantification: Types.absolute(:str),
      parameter1_value: 2.0,
      parameter2_quantification: Types.absolute(:str),
      parameter2_value: 1.0,
      parameter3_quantification: Types.absolute(:str),
      parameter3_value: 1.0,
      parameter4_quantification: Types.absolute(:str),
      parameter4_value: 1.0,
      value_titles: %{
        Float.to_string(0.0) => gettext("Block"),
        Float.to_string(1.0) => gettext("Abstain"),
        Float.to_string(2.0) => gettext("Support")
      }
    }
    |> Map.merge(opts |> Enum.into(%{}))
  end

  def score(opts \\ []) do
    %{
      method: Types.cardinal(:str),
      resolution: Types.sum(:str),
      parameter0_quantification: Types.proportional_exclusive(:str),
      parameter0_value: 0.0,
      parameter1_quantification: Types.absolute(:str),
      parameter1_value: 5.0,
      parameter2_quantification: Types.absolute(:str),
      parameter2_value: 0.0,
      parameter3_quantification: Types.absolute(:str),
      parameter3_value: 0.0,
      parameter4_quantification: Types.absolute(:str),
      parameter4_value: 1.0
    }
    |> Map.merge(opts |> Enum.into(%{}))
  end

  def stv(opts \\ []) do
    %{
      method: Types.ordinal_stv(:str),
      resolution: Types.scott(:str),
      quota: Types.droop(:str),
      parameter0_quantification: Types.proportional_exclusive(:str),
      parameter0_value: 0.0
    }
    |> Map.merge(opts |> Enum.into(%{}))
  end

  def star(opts \\ []) do
    %{
      method: Types.cardinal(:str),
      resolution: Types.star(:str),
      parameter0_quantification: Types.proportional_exclusive(:str),
      parameter0_value: 1.0,
      parameter1_quantification: Types.absolute(:str),
      parameter1_value: 5.0,
      parameter2_quantification: Types.absolute(:str),
      parameter2_value: 0.0,
      parameter3_quantification: Types.absolute(:str),
      parameter3_value: 0.0,
      parameter4_quantification: Types.absolute(:str),
      parameter4_value: 1.0,
      value_titles: %{
        Float.to_string(0.0) => gettext("(no stars)"),
        Float.to_string(1.0) => gettext("⭐️"),
        Float.to_string(2.0) => gettext("⭐️⭐️"),
        Float.to_string(3.0) => gettext("⭐️⭐️⭐️"),
        Float.to_string(4.0) => gettext("⭐️⭐️⭐️⭐️"),
        Float.to_string(5.0) => gettext("⭐️⭐️⭐️⭐️⭐️")
      }
    }
    |> Map.merge(opts |> Enum.into(%{}))
  end
end

defmodule Accord.DecisionSettings.Types do
  use Accord.Statics

  declare(:"decision-method", :plurality)
  declare(:"decision-method", :cardinal)
  declare(:"decision-method", :ordinal_stv)

  declare(:quantification, :absolute)
  declare(:quantification, :absolute_exclusive)
  declare(:quantification, :proportional)
  declare(:quantification, :proportional_exclusive)

  declare(:continuity, :discrete)
  #  declare(:continuity, :continuous)

  declare(:"stv-quota", :droop)
  declare(:"stv-quota", :hare)

  declare(:"stv-resolution", :scott)
  # declare(:"stv-resolution", :wright)
  #  declare(:"stv-resolution", :gregory)

  declare(:"cardinal-resolution", :sum)
  declare(:"cardinal-resolution", :star)
  #  declare(:"cardinal-resolution", :mean)
  #  declare(:"cardinal-resolution", :median)
end

defmodule Accord.DecisionSettings do
  @moduledoc """
    Manages settings for a DecisionVersion
  """

  alias Accord.DecisionSettings.Types

  import Ecto.Changeset

  use Ecto.Schema

  @type t :: %__MODULE__{}

  embedded_schema do
    # Determines how Votes are cast
    field :method, :string, default: Types.plurality(:str)

    # Determines how votes are counted
    field :resolution, :string

    # Ordinal: Determines quotas for ordinal iterations
    field :quota, :string

    # Determines whether any options can win based on participation
    # Cardinal: Also determines which individual options can win based on participation (if an option receives participation less than this score, it can't win)
    field :quorum_quantification, :string, default: Types.absolute(:str)
    field :quorum_value, :float, default: 1.0

    # Determines the number of options that can win; always absolute
    field :n_winners_quantification, :string, default: Types.absolute(:str)
    field :n_winners_value, :float, default: 1.0

    # Cardinal: Determines whether scores are recorded as integers ("discrete") or floats ("continuous")
    field :parameter_continuity, :string, default: Types.discrete(:str)
    # Cardinal, discrete continuity: The names of the scores, %{"n" => "name"}
    # Cardinal, continuous continuity: The names of the scores, %{"min" => "name", "max" => "name"}
    field :value_titles, {:map, :string}

    # Plurality: Determines the minimum number of votes an option needs to win; if proportional, relative to the total voting power
    # Cardinal: Determines the minimum score an option needs to win; if proportional, relative to the total voting power
    field :parameter0_quantification, :string
    field :parameter0_value, :float

    # Cardinal: Determines the upper bound, inclusive, of possible scores; always absolute
    field :parameter1_quantification, :string
    field :parameter1_value, :float

    # Cardinal: Determines the default value assigned options receiving an abstention; if proportional, relative to parameter1
    field :parameter2_quantification, :string
    field :parameter2_value, :float

    # Cardinal: Determines the score below which an option receiving such a vote can be blocked; if proportional, relative to parameter1
    field :parameter3_quantification, :string
    field :parameter3_value, :float

    # Cardinal: Determines how many votes below parameter3 are needed to block an option; if proportional, relative to the total voting power
    field :parameter4_quantification, :string
    field :parameter4_value, :float
  end

  @doc false
  def changeset(settings, attrs \\ %{}) do
    settings
    |> cast(attrs, [
      :method,
      :resolution,
      :quota,
      :quorum_quantification,
      :quorum_value,
      :n_winners_quantification,
      :n_winners_value,
      :parameter_continuity,
      :value_titles,
      :parameter0_quantification,
      :parameter0_value,
      :parameter1_quantification,
      :parameter1_value,
      :parameter2_quantification,
      :parameter2_value,
      :parameter3_quantification,
      :parameter3_value,
      :parameter4_quantification,
      :parameter4_value
    ])
  end
end
