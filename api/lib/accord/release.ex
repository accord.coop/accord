defmodule Accord.ReleaseTasks do
  @start_apps [
    :crypto,
    :ssl,
    :postgrex,
    :ecto,
    :ecto_sql
  ]

  @app :accord
  @repos Application.fetch_env!(@app, :ecto_repos)

  def migrate(_argv \\ []) do
    start_services()
    up_all_migrations()
    stop_services()
  end

  def reset(_argv \\ []) do
    start_services()
    down_all_migrations()
    up_all_migrations()
    stop_services()
  end

  defp start_services do
    IO.puts("[Release] Starting apps…")
    Enum.each(@start_apps, &Application.ensure_all_started/1)
    Application.load(@app)
    IO.puts("[Release] Starting repos…")
    Enum.each(@repos, & &1.start_link(pool_size: 2))
  end

  defp stop_services do
    IO.puts("[Release] Done.")
    :init.stop()
  end

  defp up_all_migrations do
    Enum.each(@repos, &up_all_migrations_for/1)
  end

  defp up_all_migrations_for(repo) do
    app = Keyword.get(repo.config(), :otp_app)
    IO.puts("[Release] Up migrations for #{app}…")
    migrations_path = priv_path_for(repo, "migrations")
    Ecto.Migrator.run(repo, migrations_path, :up, all: true)
  end

  defp down_all_migrations do
    Enum.each(@repos, &down_all_migrations_for/1)
  end

  defp down_all_migrations_for(repo) do
    app = Keyword.get(repo.config(), :otp_app)
    IO.puts("[Release] Down migrations for #{app}…")
    migrations_path = priv_path_for(repo, "migrations")
    Ecto.Migrator.run(repo, migrations_path, :down, all: true)
  end

  defp priv_path_for(repo, filename) do
    app = Keyword.get(repo.config(), :otp_app)

    repo_underscore =
      repo
      |> Module.split()
      |> List.last()
      |> Macro.underscore()

    priv_dir = "#{:code.priv_dir(app)}"

    Path.join([priv_dir, repo_underscore, filename])
  end
end
