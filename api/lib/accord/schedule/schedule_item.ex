defmodule Accord.Schedule.Item do
  use GenServer

  alias Accord.{DecisionVersionMoment, Schedule}

  def via(id), do: {:via, :gproc, {:n, :l, {:accord_schedule_item, id}}}

  def start_link(dv_moment_uuid, delay) do
    GenServer.start_link(__MODULE__, [dv_moment_uuid, delay], name: via(dv_moment_uuid))
  end

  @impl true
  def init([dv_moment_uuid, delay]) do
    Process.send_after(self(), :run, delay)

    {
      :ok,
      %{dv_moment_uuid: dv_moment_uuid}
    }
  end

  @impl true
  def handle_info(:run, %{dv_moment_uuid: dv_moment_uuid} = state) do
    DecisionVersionMoment.execute(dv_moment_uuid)
    Schedule.on_finished_running(dv_moment_uuid)
    {:stop, :normal, state}
  end

  @impl true
  def handle_info(:shutdown, state) do
    {:stop, :normal, state}
  end
end
