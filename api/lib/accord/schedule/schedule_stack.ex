defmodule Accord.Schedule.Stack do
  use GenServer

  alias Accord.{Schedule}
  import NaiveDateTime, only: [{:compare, 2}]

  defp max_stack_size(), do: Application.get_env(:accord, Schedule)[:stack_size]

  def via(), do: {:via, :gproc, {:n, :l, :accord_schedule_stack}}

  def start_link() do
    GenServer.start_link(__MODULE__, [], name: via())
  end

  def schedule_moment(%{moment: moment, uuid: uuid}) do
    GenServer.call(via(), {:schedule, moment, uuid})
  end

  def drop(uuid) do
    GenServer.cast(via(), {:drop, uuid})
  end

  @impl true
  def init([]) do
    Process.send_after(self(), :pack_stack, 0)
    {:ok, Map.new()}
  end

  defp latest(state) do
    Enum.reduce(
      state,
      fn {_id, moment}, acc ->
        case compare(moment, acc) do
          :gt -> moment
          _ -> acc
        end
      end
    )
  end

  defp schedule(state, moment, dv_moment_uuid),
    do: {:reply, {:scheduled, dv_moment_uuid}, Map.put(state, dv_moment_uuid, moment)}

  defp swap(state, latest_moment, moment, dv_moment_uuid) do
    {r_id, _moment} = Enum.find(state, fn {_id, l_moment} -> l_moment == latest_moment end)

    {:reply, {:swapped, r_id, dv_moment_uuid},
     state |> Map.drop([r_id]) |> Map.put(dv_moment_uuid, moment)}
  end

  @impl true
  def handle_call({:schedule, moment, dv_moment_uuid}, _from, state) do
    if map_size(state) >= max_stack_size() do
      latest_moment = latest(state)

      case compare(moment, latest_moment) do
        # Swap it with the furthest out in the stack if it's sooner
        :lt -> swap(state, latest_moment, moment, dv_moment_uuid)
        # Add to stack if it's at the exact same time
        :eq -> schedule(state, moment, dv_moment_uuid)
        # Don't add to stack if it's later
        :gt -> {:reply, :deferred, state}
      end
    else
      schedule(state, moment, dv_moment_uuid)
    end
  end

  @impl true
  def handle_cast({:drop, dv_moment_uuid}, state) do
    {:noreply, Map.drop(state, [dv_moment_uuid])}
  end

  @impl true
  def handle_info(:pack_stack, state) do
    Task.start_link(&Schedule.pack_stack/0)
    {:noreply, state}
  end

  @impl true
  def handle_info(:shutdown, state) do
    {:stop, :normal, state}
  end
end
