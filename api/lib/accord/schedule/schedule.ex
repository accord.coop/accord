defmodule Accord.Schedule do
  use Supervisor

  alias Accord.{DecisionVersionMoment, Schedule}

  require Logger

  defp max_stack_size(), do: Application.get_env(:accord, Schedule)[:stack_size]

  def via(), do: {:via, :gproc, {:n, :l, :accord_schedule}}

  @impl true
  def init(_) do
    Supervisor.init(
      [
        %{
          id: :accord_schedule_stack,
          start: {Schedule.Stack, :start_link, []},
          restart: :permanent,
          shutdown: 200
        }
      ],
      strategy: :one_for_one
    )
  end

  def pack_stack() do
    max_stack_size()
    |> DecisionVersionMoment.get_upcoming()
    |> Enum.reduce_while([], fn dv_moment, acc ->
      case Schedule.Stack.schedule_moment(dv_moment) do
        {:scheduled, _} -> init_start_child(acc, dv_moment)
        {:swapped, r_id, _} -> init_swap_child(acc, r_id, dv_moment)
        :deferred -> {:halt, acc}
      end
    end)
    |> Enum.map(fn spec -> Supervisor.start_child(via(), spec) end)
  end

  defp init_swap_child(acc, r_id, %DecisionVersionMoment{} = dv_moment) do
    Logger.warn("Swap child called on init… this probably shouldn’t happen.")

    {:cont,
     [
       child_for_dv_moment(dv_moment)
       | List.delete(acc, Enum.find(acc, fn %{id: {:decision_moment, l_id}} -> r_id == l_id end))
     ]}
  end

  defp init_start_child(acc, %DecisionVersionMoment{} = dv_moment) do
    {:cont, [child_for_dv_moment(dv_moment) | acc]}
  end

  defp swap_child(r_id, %DecisionVersionMoment{} = dv_moment) do
    Supervisor.terminate_child(via(), child_id(r_id))
    start_child(dv_moment)
  end

  defp start_child(%DecisionVersionMoment{} = dv_moment) do
    Supervisor.start_child(via(), child_for_dv_moment(dv_moment))
  end

  def start_link(_) do
    Supervisor.start_link(__MODULE__, [], name: via())
  end

  defp ms_from_now(%NaiveDateTime{} = moment),
    do: NaiveDateTime.diff(moment, NaiveDateTime.utc_now(), :millisecond)

  defp child_id(dv_moment_uuid), do: {:decision_moment, dv_moment_uuid}

  defp child_for_dv_moment(%DecisionVersionMoment{} = dv_moment) do
    %{
      id: child_id(dv_moment.uuid),
      start: {
        Schedule.Item,
        :start_link,
        [
          dv_moment.uuid,
          dv_moment.moment |> ms_from_now()
        ]
      },
      restart: :temporary,
      shutdown: 200
    }
  end

  def on_moment_created(%DecisionVersionMoment{moment: nil}), do: :ok

  def on_moment_created(%DecisionVersionMoment{} = dv_moment) do
    case Schedule.Stack.schedule_moment(dv_moment) do
      {:scheduled, _} -> start_child(dv_moment)
      {:swapped, r_id, _} -> swap_child(r_id, dv_moment)
      :deferred -> :ok
    end
  end

  def on_finished_running(dv_moment_uuid) do
    Schedule.Stack.drop(dv_moment_uuid)

    if stack_size() <= 1,
      do: pack_stack()
  end

  def stack_size(), do: Supervisor.count_children(via()) |> Map.get(:workers)
end
