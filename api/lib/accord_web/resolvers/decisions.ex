defmodule AccordWeb.Resolvers.Decisions do
  @moduledoc """
    Absinthe Resolvers for the human ontology Persons, Peoples, and their associations.
  """

  alias Accord.{DecisionRole, DecisionVersion}

  @c404 {:error, :not_found}

  @doc """
    Gets votable decisions as long as the request is auth'd.
  """
  def get_decision_versions(
        _parent,
        %{state: state},
        %{context: %{person_uuid: requester_person_uuid}}
      ) do
    case DecisionRole.votable_decision_versions_for_person(requester_person_uuid, state) do
      result -> {:ok, result}
    end
  end

  def get_votable(_parent, _args, _resolution), do: @c404

  def get_options_for_decision_version(
        %DecisionVersion{options: options},
        _args,
        _context
      ),
      do: {:ok, options}
end
