defmodule AccordWeb.Resolvers.Humans do
  @moduledoc """
    Absinthe Resolvers for the human ontology Persons, Peoples, and their associations.
  """

  alias Accord.{Repo, Person, People, PersonPeople}

  @c404 {:error, :not_found}
  @c401 {:error, :unauthorized}

  @doc """
    Gets any Person as long as the request is auth'd.
  """
  def get_person(
        _parent,
        %{uuid: person_uuid},
        %{context: %{person_uuid: _requester_person_uuid}}
      ) do
    case Person.get(person_uuid) do
      nil -> {:error, "Failed to get Person"}
      person -> {:ok, person}
    end
  end

  def get_person(_parent, _args, _resolution), do: @c404

  @doc """
    Gets the Peoples for any Person as long as the request is auth'd.
  """
  def get_peoples_for_person(
        %Person{} = person,
        _args,
        %{context: %{person_uuid: _requester_person_uuid}}
      ) do
    case PersonPeople.get_peoples_for_person(person) do
      [] -> {:ok, nil}
      peoples -> {:ok, peoples}
    end
  end

  def get_peoples_for_person(_parent, _args, _resolution), do: @c404

  @doc """
    Gets the Personas for any Person as long as the request is auth'd.
  """
  def get_personas_for_person(
        %Person{} = person,
        _args,
        %{context: %{person_uuid: _requester_person_uuid}}
      ) do
    case person |> Repo.get_rel(:personas) do
      [] -> {:ok, nil}
      personas -> {:ok, personas}
    end
  end

  def get_personas_for_person(_parent, _args, _resolution), do: @c404

  @doc """
    Creates a People and adds the requester-Person to the People.
  """
  def create_people(
        _parent,
        %{title: title},
        %{context: %{person_uuid: requester_person_uuid}}
      ) do
    with %Person{} = requester_person <- Person.get(requester_person_uuid),
         {:ok, %{people: people}} <-
           (Repo.nothing_to_insert(:person, requester_person) ++
              People.create({:accord, title}) ++
              PersonPeople.associate())
           |> Repo.commit() do
      {:ok, people}
    else
      err -> {:error, err}
    end
  end

  def create_people(_parent, _args, _resolution), do: @c401

  @doc """
    Gets a People as long as the requester-Person belongs to the People.
  """
  def get_people(
        _parent,
        %{uuid: people_uuid},
        %{context: %{person_uuid: requester_person_uuid}}
      ) do
    if Accord.PersonPeople.associated?(requester_person_uuid, people_uuid) do
      case Accord.People.get(people_uuid) do
        nil -> {:error, "Failed to get People"}
        people -> {:ok, people}
      end
    else
      @c404
    end
  end

  def get_people(_parent, _args, _resolution), do: @c404

  @doc """
    Gets Persons for a People as long as the requester-Person belongs to the People.
  """
  def get_persons_for_people(
        _parent,
        %{id: people_uuid},
        %{context: %{person_uuid: requester_person_uuid}}
      ) do
    get_persons_for_people(people_uuid, requester_person_uuid)
  end

  def get_persons_for_people(
        %Accord.People{} = people,
        _args,
        %{context: %{person_uuid: requester_person_uuid}}
      ) do
    get_persons_for_people(people.uuid, requester_person_uuid)
  end

  def get_persons_for_people(_parent, _args, _resolution), do: @c404

  defp get_persons_for_people(people_uuid, requester_person_uuid) do
    if Accord.PersonPeople.associated?(requester_person_uuid, people_uuid) do
      case Accord.PersonPeople.get_persons_for_people(people_uuid) do
        [] -> {:ok, nil}
        persons -> {:ok, persons}
      end
    else
      @c404
    end
  end
end
