defmodule AccordWeb.Resolvers.Auth do
  @moduledoc """
    Absinthe Resolvers for authentication.
  """

  alias Accord.{Repo, Person, Persona, Session}

  require Logger

  @c400pw {:error, :wrong_pw}
  @c400email {:error, :wrong_email}

  defp log_unhandled_error(err) do
    IO.inspect(err)
    Logger.error("Unhandled error in Resolvers.Auth", event: err)
    {:error, :unknown}
  end

  defp verify_persona_with_pass(_pw, nil), do: @c400email

  defp verify_persona_with_pass(pw, %Persona{} = persona) do
    if Argon2.verify_pass(pw, persona.native_credentials.pw) do
      :ok
    else
      @c400pw
    end
  end

  defp process_login(email, pw) do
    with persona <- Persona.get({:accord, email, pw}),
         :ok <- verify_persona_with_pass(pw, persona),
         expires <- NaiveDateTime.add(NaiveDateTime.utc_now(), 518_400, :second),
         {:ok, token, _class} <- AccordWeb.Auth.Guardian.encode_and_sign({persona, expires}),
         {:ok, %{session: session}} <- Session.create(persona, expires, token) |> Repo.commit() do
      {:ok, session}
    else
      @c400email -> @c400email
      @c400pw -> @c400pw
      err -> log_unhandled_error(err)
    end
  end

  def register(
        _parent,
        %{email: email, pw: pw},
        _resolution
      ) do
    with {:ok, _person} <- Person.assure_with_persona({:accord, email, pw}) |> Repo.commit(),
         {:ok, session} <- process_login(email, pw) do
      {:ok, session}
    else
      {:error, %Ecto.Changeset{errors: [email: {"has already been taken", _constraint}]}} ->
        {:error, :email_not_unique}

      err ->
        log_unhandled_error(err)
    end
  end

  def login(
        _parent,
        %{email: email, pw: pw},
        _resolution
      ) do
    process_login(email, pw)
  end

  def get_persona_for_session(%Session{} = session, _args, _resolution) do
    case session |> Repo.get_rel(:persona) do
      nil -> {:error, :not_found}
      persona -> {:ok, persona}
    end
  end
end
