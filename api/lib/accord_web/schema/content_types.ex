defmodule AccordWeb.Schema.ContentTypes do
  @moduledoc """
    The ontology, as far as the GraphQL part of the API is concerned.
  """

  use Absinthe.Schema.Notation
  alias AccordWeb.Resolvers.{Humans, Auth, Decisions}
  import_types(AccordWeb.Schema.Types.Custom.JSON)

  object :person do
    field :uuid, :id

    field :peoples, list_of(:people) do
      resolve(&Humans.get_peoples_for_person/3)
    end

    field :personas, list_of(:persona) do
      resolve(&Humans.get_personas_for_person/3)
    end
  end

  object :persona do
    field :uuid, :id
    field :service_id, :string
    field :service_type, :string
  end

  object :people do
    field :uuid, :id
    field :title, :string

    field :persons, list_of(:person) do
      resolve(&Humans.get_persons_for_people/3)
    end
  end

  object :session do
    field :expires, :string,
      description: "An ISO8601 formatted string for when this session will expire."

    field :token, :string,
      description:
        "The token that should be used in the `authorization` HTTP header for subsequent authorized requests."

    field :persona, :persona, description: "The Persona this session is for." do
      resolve(&Auth.get_persona_for_session/3)
    end
  end

  object :decision_version do
    field :uuid, :id
    field :decision_uuid, :id
    field :title, :string
    field :body, :string
    field :state, :string

    field :options, list_of(:option) do
      resolve(&Decisions.get_options_for_decision_version/3)
    end

    field :settings, :settings
  end

  object :votable_decision_versions do
    field :voted, list_of(:decision_version)
    field :unvoted, list_of(:decision_version)
  end

  object :option do
    field :id, :id
    field :title, :string
  end

  object :settings do
    field :method, :string

    field :resolution, :string

    field :quorum_quantification, :string
    field :quorum_value, :float

    field :n_winners_quantification, :string
    field :n_winners_value, :float

    field :paramter_continuity, :string
    field :value_titles, :json

    field :parameter0_quantification, :string
    field :parameter0_value, :float

    field :parameter1_quantification, :string
    field :parameter1_value, :float
  end
end
