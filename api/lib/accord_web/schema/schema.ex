defmodule AccordWeb.Schema do
  @moduledoc """
    Accord's API surface.
  """
  use Absinthe.Schema
  import_types(AccordWeb.Schema.ContentTypes)

  alias AccordWeb.Resolvers

  query do
    @desc "Get a Person"
    field :person, :person do
      arg(:uuid, non_null(:id))
      resolve(&Resolvers.Humans.get_person/3)
    end

    @desc "Get a People"
    field :people, :people do
      arg(:uuid, non_null(:id))
      resolve(&Resolvers.Humans.get_people/3)
    end

    @desc "Get DecisionVersions"
    field :decision_versions, :votable_decision_versions do
      arg(:state, :string, default_value: Accord.DecisionVersion.Types.published(:str))
      resolve(&Resolvers.Decisions.get_decision_versions/3)
    end
  end

  mutation do
    @desc "Create a People"
    field :create_people, type: :people do
      arg(:title, non_null(:string))

      resolve(&Resolvers.Humans.create_people/3)
    end

    @desc "Create an Accord-native Person, i.e. register"
    field :create_person, type: :session do
      arg(:email, non_null(:string))
      arg(:pw, non_null(:string))

      resolve(&Resolvers.Auth.register/3)
    end

    @desc "Create a Session, i.e. log in"
    field :create_session, type: :session do
      arg(:email, non_null(:string))
      arg(:pw, non_null(:string))

      resolve(&Resolvers.Auth.login/3)
    end
  end
end
