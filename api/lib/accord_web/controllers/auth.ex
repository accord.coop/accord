defmodule AccordWeb.AuthController do
  use AccordWeb, :controller

  alias Accord.{Repo, Persona, People, Caches}
  alias AccordWeb.PasswordStrength
  alias AccordSlack.IO, as: SlackIO
  alias SendGrid.{Mail, Email}
  require Logger

  @c404 {:error, :not_found}

  defp log_unhandled_err(err, conn) do
    Logger.error("〘AuthController〙 Unanticipated error")
    IO.inspect(err)
    conn |> send_resp(500, "unknown")
  end

  defp check_email_exists(email) do
    case Persona.get({:accord, email}) do
      %Persona{} = persona -> persona
      nil -> @c404
      err -> err
    end
  end

  def reset_password(conn, params) do
    Logger.debug("〘Security:reset password〙Received reset request")
    person_email = get_in(params, ["email"])

    with %Persona{} = persona <- check_email_exists(person_email),
         {:ok, %Persona{native_credentials: %{pw_reset_token: token}}} <-
           Persona.change_pw_reset_token(persona) |> Repo.update(),
         :ok =
           Email.build()
           |> Email.put_from("security@accord.coop", "Accord security")
           # todo: get their name somehow
           |> Email.add_to(person_email, "Accord user")
           |> Email.put_subject("Your password reset link")
           |> Email.put_phoenix_view(AccordWeb.EmailView)
           |> Email.put_phoenix_template("reset_password.html",
             reset_token_href:
               "https://accord.coop/reset-password?email=#{person_email}&token=#{token}"
           )
           |> Mail.send() do
      conn |> send_resp(:ok, "")
    else
      @c404 -> conn |> send_resp(404, "no_such_email")
      err -> log_unhandled_err(err, conn)
    end
  end

  def set_password(conn, params) do
    person_email = get_in(params, ["email"])
    received_token = get_in(params, ["pw_reset_token"])
    new_pw = get_in(params, ["new_pw"])

    with %Persona{} = persona <- check_email_exists(person_email),
         {:ok, _} <- persona |> Accord.Persona.change_pw(received_token, new_pw) |> Repo.update() do
      conn |> send_resp(:ok, "")
    else
      @c404 -> conn |> send_resp(404, "no_such_email")
      {:error, :token_invalid} -> conn |> send_resp(400, "token_invalid")
      err -> log_unhandled_err(err, conn)
    end
  end

  def slack_oauth(conn, %{"code" => code, "state" => state}) do
    with %Caches.OAuthState{state: ^state} <- Repo.get(Caches.OAuthState, state),
         {:ok, %{app_token: app_token, team_id: team_id, team_name: team_name}} <-
           SlackIO.oauth_access(code),
         {:ok, %{team: _}} <-
           People.assure(:team, {:slack, team_id}, title: team_name, app_token: app_token)
           |> Repo.commit() do
      conn |> send_resp(:ok, "")
    else
      err -> log_unhandled_err(err, conn)
    end
  end

  def zxcvbn(conn, %{"pw" => pw} = params) do
    conn
    |> Plug.Conn.put_resp_header("content-type", "application/json")
    |> Plug.Conn.send_resp(
      :ok,
      PasswordStrength.check(pw,
        email: Map.get(params, "email"),
        name: Map.get(params, "name")
      )
      |> Jason.encode!()
    )
  end
end
