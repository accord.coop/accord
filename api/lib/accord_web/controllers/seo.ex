defmodule AccordWeb.SEOController do
  use AccordWeb, :controller

  def robots(conn, _params) do
    conn
    |> put_resp_content_type("text/plain")
    |> send_resp(200, "User-agent: *\nDisallow: /")
  end
end
