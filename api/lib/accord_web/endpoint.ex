defmodule AccordWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :accord

  plug Plug.RequestId
  plug Plug.Telemetry, event_prefix: [:phoenix, :endpoint]

  plug Plug.Parsers,
    parsers: [:urlencoded, :json],
    pass: ["*/*"],
    body_reader: {AccordWeb.BodyReader, :read_body, []},
    json_decoder: Phoenix.json_library()

  plug Plug.MethodOverride
  plug Plug.Head

  plug CORSPlug, origin: Application.get_env(:accord, AccordWeb.Endpoint)[:origins]

  plug AccordWeb.Router
end
