defmodule AccordWeb.Pipeline.Producer do
  @callback id() :: atom

  defmacro __using__(_) do
    quote do
      def child_spec() do
        %{
          id: id(),
          start: {__MODULE__, :start_link, []},
          restart: :permanent,
          shutdown: 200
        }
      end

      def start_link() do
        GenStage.start_link(__MODULE__, [], name: __MODULE__)
      end

      def init(_) do
        {:producer, {[], 0}}
      end

      defp emit(queue, total_demand) do
        {queue_to_emit, queue_remaining} = Enum.split(queue, total_demand)

        {
          :noreply,
          queue_to_emit,
          {
            queue_remaining,
            total_demand - length(queue_to_emit)
          }
        }
      end

      # Drain
      def handle_demand(incoming_demand, {queue, pending_demand}) do
        emit(queue, incoming_demand + pending_demand)
      end

      # Fill
      def handle_cast({:enqueue, events}, {queue, pending_demand}) do
        emit(queue ++ events, pending_demand)
      end
    end
  end
end
