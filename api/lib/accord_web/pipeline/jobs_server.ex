defmodule AccordWeb.Pipeline.JobsServer do
  @moduledoc """
    Keeps track of all ongoing jobs being processed by the pipes.
  """

  alias Broadway.{Acknowledger, Message}

  use GenServer

  require Logger

  @behaviour Acknowledger

  def via(), do: {:via, :gproc, {:n, :l, :accord_pipeline_jobs}}

  def child_spec(_) do
    %{
      id: :accord_pipeline_jobs,
      start: {__MODULE__, :start_link, []},
      restart: :permanent,
      shutdown: 200
    }
  end

  def start_link() do
    GenServer.start_link(__MODULE__, [], name: via())
  end

  @impl true
  def init([]) do
    {:ok, %{}}
  end

  @spec enqueue([Message.t()], term, atom) :: :ok
  def enqueue(messages, job_signature, pipe) do
    via() |> GenServer.call({:extend, job_signature, length(messages)})

    pipe.id() |> Broadway.producer_names() |> List.first() |> GenStage.cast({:enqueue, messages})
  end

  @spec start_job(term, {atom, atom}, any) :: :ok
  def start_job(job_signature, {_, _} = resolution, payload \\ nil) do
    via() |> GenServer.call({:start_job, job_signature, 0, resolution, payload})
  end

  @spec start_and_enqueue_job([Message.t()], term, atom, {atom, atom}, any) :: :ok
  def start_and_enqueue_job(messages, job_signature, pipe, {_, _} = resolution, payload \\ nil) do
    via() |> GenServer.call({:start_job, job_signature, length(messages), resolution, payload})

    pipe.id() |> Broadway.producer_names() |> List.first() |> GenStage.cast({:enqueue, messages})
  end

  @spec abort_job(term) :: :ok
  def abort_job(job_signature) do
    via() |> GenServer.call({:abort, job_signature})
  end

  @spec acknowledge_job(term, [Message.t()]) :: :ok
  def acknowledge_job(job_signature, messages) do
    via() |> GenServer.call({:acknowledge, job_signature, messages})
  end

  @impl true
  def handle_call({:start_job, job_signature, n_awaiting, resolution, payload}, _from, state) do
    {:reply, :ok,
     state
     |> Map.put(job_signature, %{
       n_awaiting: n_awaiting,
       messages: [],
       resolution: resolution,
       payload: payload
     })}
  end

  @impl true
  def handle_call({:abort, job_signature}, _from, state) do
    with %{resolution: {module, function}, payload: payload} <- state |> Map.get(job_signature) do
      apply(module, function, [job_signature, {:error, :message_failed}, payload])
      {:reply, :ok, state |> Map.drop([job_signature])}
    else
      _ -> {:reply, :no_such_job, state}
    end
  end

  @impl true
  def handle_call({:extend, job_signature, additional_size}, _from, state) do
    with %{n_awaiting: n_awaiting} = job <- state |> Map.get(job_signature) do
      {
        :reply,
        :ok,
        state
        |> Map.put(job_signature, job |> Map.put(:n_awaiting, n_awaiting + additional_size))
      }
    else
      _ -> {:reply, :no_such_job, state}
    end
  end

  @impl true
  def handle_call({:acknowledge, job_signature, received_messages}, _from, state) do
    with %{
           n_awaiting: n_awaiting,
           messages: messages,
           resolution: {module, function},
           payload: payload
         } = job <- state |> Map.get(job_signature) do
      if length(received_messages) >= n_awaiting do
        apply(module, function, [job_signature, {:ok, messages ++ received_messages}, payload])
        {:reply, :done, state |> Map.drop([job_signature])}
      else
        {
          :reply,
          :pending,
          state
          |> Map.put(
            job_signature,
            job
            |> Map.put(:messages, messages ++ received_messages)
            |> Map.put(:n_awaiting, n_awaiting - length(received_messages))
          )
        }
      end
    else
      _ -> {:reply, :no_such_job, state}
    end
  end

  @impl true
  def ack(job_signature, successful, failed) do
    if(length(failed) > 0) do
      abort_job(job_signature)
    else
      acknowledge_job(job_signature, successful)
    end
  end
end
