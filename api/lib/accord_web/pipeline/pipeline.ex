defmodule AccordWeb.Pipeline do
  alias AccordWeb.Pipeline.JobsServer
  alias AccordSlack.Pipes, as: SlackPipes

  use Supervisor
  require Logger

  def via(), do: {:via, :gproc, {:n, :l, :accord_slack_pipeline}}

  @impl true
  def init(_) do
    Supervisor.init(
      [
        JobsServer,
        SlackPipes.Tier4,
        SlackPipes.Repo
      ],
      strategy: :one_for_one
    )
  end

  def start_link(_) do
    Supervisor.start_link(__MODULE__, [], name: via())
  end
end
