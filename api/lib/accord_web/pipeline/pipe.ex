defmodule AccordWeb.Pipeline.Pipe do
  @callback id() :: atom
  @callback producer_spec() :: Keyword.t()
  @callback processor_concurrency() :: non_neg_integer
  @callback batchers() :: nil | Keyword.t()

  defmacro __using__(_) do
    quote do
      def child_spec(_) do
        %{
          id: id(),
          start:
            {__MODULE__, :start_link,
             [
               [
                 producer: producer_spec(),
                 processor_concurrency: processor_concurrency(),
                 batchers: batchers(),
                 name: id()
               ]
             ]},
          restart: :permanent,
          shutdown: 5_000
        }
      end

      def start_link(opts) do
        Broadway.start_link(
          __MODULE__,
          [
            name: opts[:name],
            producer: opts[:producer],
            processors: [
              default: [concurrency: opts[:processor_concurrency]]
            ]
          ] ++
            case opts[:batchers] do
              nil -> []
              _ -> [batchers: opts[:batchers]]
            end
        )
      end
    end
  end
end
