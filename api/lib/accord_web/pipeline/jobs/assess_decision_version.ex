defmodule AccordWeb.Pipeline.Jobs.AssessDecisionVersion do
  @moduledoc """
    This is the main `AssessDecisionVersion` job. It delegates everything to jobs with logic
    specific to the integration requested; there is or will be a `AssessDecisionVersion` for
    `AccordWeb`, `AccordSlack`, and any other future integrations.

    Since this delegates all of its pipeline work to other jobs, it will never invoke
    `JobsServer.enqueue` or any other function that interacts with `Broadway`, though it does
    track progress using the `JobsServer`.
  """

  alias AccordWeb.Pipeline.JobsServer
  alias AccordSlack.PipelineJobs.AssessDecisionVersion, as: SlackAssessDecisionVersion
  alias Accord.{Repo, DecisionVersion, DecisionVersionMoment, DecisionRole, Persona}
  alias Accord.Caches.DecisionVersionResult, as: Result

  require Logger

  @resolve {__MODULE__, :resolve}

  @spec start(DecisionVersionMoment.t()) :: :ok
  def start(%DecisionVersionMoment{uuid: dv_moment_uuid} = dv_moment) do
    job_signature = {:adv, dv_moment_uuid}
    dv = dv_moment |> Repo.get_rel(:decision_version)
    Logger.info("Started AssessDecisionVersion for #{dv.service_id}")

    votable_people_service_ids =
      dv
      |> Repo.get_rel(:decision)
      |> DecisionRole.people_service_ids_votable()

    votable_people_service_groups =
      Enum.group_by(votable_people_service_ids, fn service_id ->
        case service_id do
          "slack" <> _ -> :slack
          "accord" <> _ -> :accord
        end
      end)

    JobsServer.start_job(
      job_signature,
      @resolve
    )

    JobsServer.via()
    |> GenServer.call({:extend, job_signature, map_size(votable_people_service_groups)})

    if Map.has_key?(votable_people_service_groups, :slack) do
      SlackAssessDecisionVersion.start(
        dv_moment_uuid,
        votable_people_service_groups |> Map.get(:slack)
      )
    end

    if Map.has_key?(votable_people_service_groups, :accord) do
      # This is a no-op since native People is yet to be implemented.
      JobsServer.acknowledge_job(job_signature, [%{}])
    end

    :ok
  end

  def resolve(_, {:error, _}, _), do: Logger.error("Assess DecisionVersion failed.")

  @spec resolve(
          {:adv, String.t()},
          {:ok, [%{String.t() => %{is_bot: bool, persona?: bool}}]},
          any
        ) :: :ok
  def resolve({:adv, dv_moment_uuid}, {:ok, messages}, _) do
    summary =
      Enum.reduce(
        messages,
        %{person_uuids: MapSet.new(), unregistered_non_bots: MapSet.new()},
        fn annotated_service_ids, results_acc ->
          Enum.reduce(
            annotated_service_ids,
            results_acc,
            fn {service_id, %{is_bot: is_bot, persona?: persona?}}, acc ->
              cond do
                persona? and not is_bot ->
                  acc
                  |> Map.put(
                    :person_uuids,
                    Map.get(acc, :person_uuids)
                    |> MapSet.put(
                      # todo: this almost definitely needs to be optimized:
                      Persona.get_by_service_id(service_id)
                      |> Repo.get_rel(:person)
                      |> Map.get(:uuid)
                    )
                  )

                not is_bot ->
                  acc
                  |> Map.put(
                    :unregistered_non_bots,
                    Map.get(acc, :unregistered_non_bots) |> MapSet.put(service_id)
                  )

                true ->
                  acc
              end
            end
          )
        end
      )

    n_voters = MapSet.size(summary.unregistered_non_bots) + MapSet.size(summary.person_uuids)

    with %DecisionVersionMoment{} = dv_moment <- DecisionVersionMoment.get(dv_moment_uuid),
         %DecisionVersion{service_id: dv_service_id} = decision_version <-
           dv_moment |> Repo.get_rel(:decision_version),
         {:ok, %{decision_version_result: result}} <-
           Result.create(dv_moment, n_voters, summary.person_uuids) |> Repo.commit() do
      Logger.info("AssessDecisionVersion finished for #{dv_service_id}")

      dv_moment |> DecisionVersionMoment.set_executed(:ok) |> Repo.update()

      case dv_service_id do
        "slack" <> _ ->
          update_slack(decision_version, result)

        "accord" <> _ ->
          :ok

        _ ->
          "DecisionVersion service_id not supported: #{dv_service_id}"
          |> Logger.warn()
      end
    else
      err -> IO.inspect(err)
    end
  end

  defp update_slack(
         %DecisionVersion{state: state, service_id: service_id} = decision_version,
         %Result{summary: %{n_voters: n_voters}} = result
       ) do
    state_closed = DecisionVersion.Types.closed(:str)
    Logger.info("Updating message in Slack for DecisionVersion #{service_id}")

    AccordSlack.Router.Decisions.update_decision_message(decision_version, %{n_voters: n_voters})

    case state do
      ^state_closed -> AccordSlack.Router.Decisions.send_summary(decision_version, result)
      _ -> :ok
    end
  end
end
