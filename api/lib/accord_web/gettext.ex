defmodule AccordWeb.Gettext do
  @moduledoc """
    A module providing Internationalization with a gettext-based API.
  """
  use Gettext, otp_app: :accord, priv: "priv/t10s"

  @default_localization Gettext.get_locale()

  def default_localization, do: @default_localization

  defp parse(%{accept_language: accept_language} = _locale) do
    String.split(accept_language, ",")
    |> Enum.map(&String.trim/1)
    |> Enum.map(fn s -> s |> String.split(";") |> List.first() end)
  end

  defp lang_part(lang), do: lang |> String.split(["-", "_"]) |> List.first()

  defp resolve(accept_localizations) do
    Enum.find(Gettext.known_locales(AccordWeb.Gettext), @default_localization, fn l ->
      Enum.any?(accept_localizations, fn al ->
        al === l or lang_part(al) === l or lang_part(al) === lang_part(l)
      end)
    end)
  end

  def put_locale(locale) do
    locale |> parse |> resolve |> Gettext.put_locale()
  end

  def get_l10n(), do: Gettext.get_locale()
end
