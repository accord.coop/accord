defmodule AccordWeb.Cldr do
  use Cldr,
    otp_app: :accord,
    providers: [Cldr.Number, Cldr.Calendar, Cldr.DateTime]
end
