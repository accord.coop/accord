defmodule AccordWeb.Auth.Guardian do
  use Guardian, otp_app: :accord

  def subject_for_token({%Accord.Persona{} = persona, %NaiveDateTime{} = expires}, _claims) do
    {:ok, "#{persona.service_id}︙#{NaiveDateTime.to_string(expires)}"}
  end

  def resource_from_claims(%{"sub" => subject}) do
    [service_id, _] = String.split(subject, "︙")

    case Accord.Persona.get_by_service_id(service_id) do
      nil -> {:error, :not_found}
      persona -> {:ok, persona}
    end
  end
end
