defmodule AccordWeb.PasswordStrength do
  @pw_score_threshold 2

  @spec extend_pw_dict(List.t(), any) :: List.t()
  defp extend_pw_dict(dict, str) when is_bitstring(str), do: dict ++ String.split(str, ~r{\W})
  defp extend_pw_dict(dict, _), do: dict

  def check(pw, opts \\ []) do
    dict =
      []
      |> extend_pw_dict(opts[:email])
      |> extend_pw_dict(opts[:name])

    pw_score = ZXCVBN.zxcvbn(pw, dict).score

    %{
      score: pw_score,
      passes: pw_score >= @pw_score_threshold
    }
  end
end
