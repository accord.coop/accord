defmodule AccordWeb.Router.Context do
  @behaviour Plug

  import Plug.Conn
  import Ecto.Query, only: [where: 2]

  alias Accord.Repo
  alias Accord.Session

  def init(opts), do: opts

  def call(conn, _) do
    case build_context(conn) do
      {:ok, context} ->
        Absinthe.Plug.put_options(conn, context: context)

      _ ->
        conn
    end
  end

  defp build_context(conn) do
    with ["Bearer " <> token] <- get_req_header(conn, "authorization"),
         {:ok, session} <- authorize(token) do
      {:ok,
       %{
         person_uuid:
           session
           |> Repo.get_rel(:persona)
           |> Repo.get_rel(:person)
           |> Map.get(:uuid)
       }}
    end
  end

  defp authorize(token) do
    Session
    |> where(token: ^token)
    |> Repo.one()
    |> case do
      nil -> {:error, "Invalid authorization token"}
      session -> {:ok, session}
    end
  end
end

defmodule AccordWeb.Router.Slack do
  @moduledoc """
    Pipelines & routing for Accord's Slack
    Thanks to Adam C Conrad for figuring out how to verify Slack requests in Elixir:
    https://www.userinterfacing.com/verifying-request-signatures-in-elixir-phoenix/
  """
  @behaviour Plug

  import Plug.Conn
  require Logger

  @unix_gregorian_offset 62_167_219_200

  defp signing_secrets, do: Application.get_env(:accord, AccordWeb.Router.Slack)[:signing_secrets]

  def init(opts), do: opts

  def call(conn, _) do
    #    IO.puts("[Received by Slack router]")
    #    IO.inspect(conn.params)

    case signing_secrets() do
      nil -> conn
      _ -> verify_slack_request(conn)
    end
  end

  defp verify_slack_request(conn) do
    timestamp =
      conn
      |> get_req_header("x-slack-request-timestamp")
      |> Enum.at(0)
      |> String.to_integer()

    if abs(local_timestamp() - timestamp) > 300 do
      Logger.error("Verify Slack request failed: timestamp is > 300s out of sync.")
      deny_slack_request(conn)
    else
      [remote_slack_signature] = conn |> get_req_header("x-slack-signature")

      if signing_secrets()
         |> String.split(":")
         |> Enum.any?(fn secret ->
           Plug.Crypto.secure_compare(
             local_slack_signature(secret, timestamp, conn.assigns[:raw_body]),
             remote_slack_signature
           )
         end) do
        Logger.info("Verify Slack request succeeded.")
        conn
      else
        Logger.error("Verify Slack request failed: signatures don’t match.")
        deny_slack_request(conn)
      end
    end
  end

  defp local_timestamp() do
    (:calendar.local_time()
     |> :calendar.datetime_to_gregorian_seconds()) - @unix_gregorian_offset
  end

  defp local_slack_signature(secret, timestamp, raw_body) do
    "v0=#{
      :crypto.hmac(
        :sha256,
        secret,
        "v0:#{timestamp}:#{raw_body}"
      )
      |> Base.encode16()
    }"
    |> String.downcase()
  end

  defp deny_slack_request(conn) do
    conn |> send_resp(:unauthorized, "") |> halt()
  end
end

defmodule AccordWeb.Router do
  use AccordWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :graph do
    plug AccordWeb.Router.Context
  end

  pipeline :slack do
    plug AccordWeb.Router.Slack
  end

  scope "/v1" do
    pipe_through :api

    scope "/auth" do
      post "/pw-reset", AccordWeb.AuthController, :reset_password
      put "/pw-reset", AccordWeb.AuthController, :set_password
      post "/slack", AccordWeb.AuthController, :slack_oauth
      post "/zxcvbn", AccordWeb.AuthController, :zxcvbn
    end

    scope "/slack" do
      pipe_through :slack

      post "/slash", AccordSlack.Router, :slash
      post "/event", AccordSlack.Router, :event
      post "/interaction", AccordSlack.Router, :interaction
    end

    scope "/graph" do
      pipe_through :graph

      forward "/graphiql", Absinthe.Plug.GraphiQL, schema: AccordWeb.Schema
      forward "/", Absinthe.Plug, schema: AccordWeb.Schema
    end
  end

  get "/slack", AccordSlack.Router, :add_to_slack

  get "/robots.txt", AccordWeb.SEOController, :robots
end
