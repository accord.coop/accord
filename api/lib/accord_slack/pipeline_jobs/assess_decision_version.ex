defmodule AccordSlack.PipelineJobs.AssessDecisionVersion do
  @moduledoc """
    A pipeline job for resolving details about a DecisionVersion relying on Slack
    API resources.

    ###### Stage 1
    We first query the membership as user ID’s for the channels provided to the job. This is
    resolved as a `MapSet` of user ID’s.

    ###### Stage 2
    The result of Stage 1 is diffed with the cache of Slack users. This is resolved as a
    `MapSet` of user ID’s which are missing from the cache.

    ###### Stage 3
    For each user ID not stored in the cache, we fetch that user’s stable properties, like
    `is_bot`, from Slack and store the result in the cache. This is resolved as `:ok`.

    ###### Stage 4
    The `Repo` is queried against the cache and an analysis is returned with a count of all
    unique identities and the ID’s of any known Person entities associated with
    the identities.
  """

  alias AccordWeb.Pipeline.JobsServer
  alias AccordSlack.Pipes
  alias AccordSlack.IO, as: SlackIO
  alias Broadway.Message
  alias Accord.{People, Persona, Caches, Errors}

  require Logger

  @type signature :: {
          :slack_adv_stage1 | :slack_adv_stage2 | :slack_adv_stage3,
          term()
        }

  @on_step {__MODULE__, :on_step}
  @resolve {__MODULE__, :resolve}

  @spec start(String.t(), [String.t()]) :: :ok
  def start(dv_moment_uuid, votable_people_service_ids) do
    start_stage_1(dv_moment_uuid, votable_people_service_ids)
  end

  defp start_stage_1(dv_moment_uuid, votable_people_service_ids) do
    job_signature = {:slack_adv_stage1, dv_moment_uuid}

    JobsServer.start_job(
      job_signature,
      @resolve
    )

    votable_people_service_ids
    |> Enum.map(fn people_service_id ->
      %People{app_token: app_token} =
        People.get({:slack, People.get_slack_team_id(people_service_id)})

      %Message{
        data: {
          job_signature,
          @on_step,
          {app_token, people_service_id, nil, nil}
        },
        acknowledger: {JobsServer, job_signature, nil}
      }
    end)
    |> JobsServer.enqueue(job_signature, Pipes.Tier4)
  end

  defp extend_stage_1(job_signature, app_token, people_service_id, cursor) do
    JobsServer.enqueue(
      [
        %Message{
          data: {
            job_signature,
            @on_step,
            {app_token, people_service_id, cursor, nil}
          },
          acknowledger: {JobsServer, job_signature, nil}
        }
      ],
      job_signature,
      Pipes.Tier4
    )
  end

  defp start_stage_2(dv_moment_uuid, unique_persona_service_ids) do
    job_signature = {:slack_adv_stage2, dv_moment_uuid}

    JobsServer.start_job(
      job_signature,
      @resolve
    )

    unique_persona_service_ids
    |> Enum.map(fn {app_token, service_id} ->
      %Message{
        data: {
          job_signature,
          @on_step,
          {app_token, service_id}
        },
        acknowledger: {JobsServer, job_signature, nil}
      }
    end)
    |> JobsServer.enqueue(job_signature, Pipes.Repo)
  end

  defp start_stage_3(dv_moment_uuid, votable_service_ids) do
    job_signature = {:slack_adv_stage3, dv_moment_uuid}

    missing_service_id_messages =
      votable_service_ids
      |> Enum.reduce([], fn {service_id, %{missing?: missing?} = annotation}, acc ->
        if missing? do
          [
            %Message{
              data: {
                job_signature,
                @on_step,
                annotation |> Map.put(:service_id, service_id)
              },
              acknowledger: {JobsServer, job_signature, nil}
            }
            | acc
          ]
        else
          acc
        end
      end)

    case missing_service_id_messages do
      [] ->
        resolve(job_signature, {:ok, []}, votable_service_ids)

      _ ->
        missing_service_id_messages
        |> JobsServer.start_and_enqueue_job(
          job_signature,
          Pipes.Tier4,
          @resolve,
          votable_service_ids
        )
    end
  end

  defp start_stage_4(dv_moment_uuid, votable_service_ids) do
    JobsServer.acknowledge_job({:adv, dv_moment_uuid}, [votable_service_ids])
  end

  def on_step(
        %Message{
          data: {
            {:slack_adv_stage1, _} = job_signature,
            handler_signature,
            {app_token, people_service_id, cursor, nil}
          }
        } = message
      ) do
    get_channel_users_result =
      SlackIO.get_channel_users(
        app_token,
        People.get_slack_channel_id(people_service_id),
        cursor
      )

    # Enqueue a new request if Slack returns a cursor:
    case get_channel_users_result do
      {:ok, %{cursor: cursor}} ->
        extend_stage_1(job_signature, app_token, people_service_id, cursor)

      _ ->
        :ok
    end

    # Resolve this job regardless of any cursor
    case get_channel_users_result do
      {:ok, %{user_ids: user_ids}} ->
        message
        |> Message.update_data(fn _ ->
          {
            job_signature,
            handler_signature,
            {app_token, people_service_id, nil, user_ids}
          }
        end)

      _ ->
        message |> Message.failed(Errors.get_slack_channel_users_failed())
    end
  end

  def on_step(
        %Message{
          data: {{:slack_adv_stage3, _}, _, %{service_id: service_id, app_token: app_token}}
        } = message
      ) do
    with {:ok, is_bot} <- SlackIO.is_bot(app_token, Persona.get_slack_user_id(service_id)),
         {:ok, _} <-
           Caches.SlackUser.put(%{
             service_id: service_id,
             is_bot: is_bot
           }) do
      message
      |> Message.update_data(fn {job_signature, handler_signature, annotation} ->
        {
          job_signature,
          handler_signature,
          annotation |> Map.put(:is_bot, is_bot) |> Map.put(:missing?, false)
        }
      end)
    else
      _ -> message |> Message.failed(Errors.save_failed())
    end
  end

  def on_step(
        :slack_adv_stage2,
        messages
      ) do
    votable_user_ids =
      messages
      |> Enum.reduce(
        MapSet.new(),
        fn %Message{data: {_, _, {_app_token, service_id}}}, acc ->
          MapSet.put(acc, service_id)
        end
      )

    annotated_user_ids =
      votable_user_ids
      |> Caches.SlackUser.annotate()

    known_personas =
      votable_user_ids
      |> Persona.get_known_service_ids()

    messages
    |> Enum.map(fn message ->
      message
      |> Message.update_data(fn {job_signature, handler_signature, {app_token, service_id}} ->
        # Put `nil` for any service_id’s *not* missing from the cache.
        {
          job_signature,
          handler_signature,
          annotated_user_ids
          |> Map.get(service_id)
          |> Map.put(:service_id, service_id)
          |> Map.put(:app_token, app_token)
          |> Map.put(:persona?, MapSet.member?(known_personas, service_id))
        }
      end)
    end)
  end

  @spec resolve(signature, {:error, any} | {:ok, [Message.t()]}, any) :: :ok

  def resolve({_, dv_moment_uuid}, {:error, _}, _) do
    Logger.error("Slack AssessDecisionVersion failed.")

    Task.start_link(fn ->
      JobsServer.abort_job({:adv, dv_moment_uuid})
    end)
  end

  def resolve({:slack_adv_stage1, dv_moment_uuid}, {:ok, messages}, _) do
    unique_persona_service_ids =
      Enum.reduce(
        messages,
        MapSet.new(),
        fn %Message{data: {_, _, {app_token, people_service_id, _, user_ids}}}, acc ->
          team_id = People.get_slack_team_id(people_service_id)

          MapSet.union(
            user_ids
            |> Enum.map(fn user_id ->
              {app_token, Persona.service_id({:slack, team_id, user_id})}
            end)
            |> MapSet.new(),
            acc
          )
        end
      )

    Task.start_link(fn ->
      start_stage_2(dv_moment_uuid, unique_persona_service_ids)
    end)
  end

  def resolve({:slack_adv_stage2, dv_moment_uuid}, {:ok, messages}, _) do
    Task.start_link(fn ->
      start_stage_3(dv_moment_uuid, annotated_sids_from_msgs(messages))
    end)
  end

  def resolve(
        {:slack_adv_stage3, dv_moment_uuid},
        {:ok, messages},
        votable_service_ids
      ) do
    Task.start_link(fn ->
      start_stage_4(
        dv_moment_uuid,
        annotated_sids_from_msgs(messages, votable_service_ids)
      )
    end)
  end

  defp annotated_sids_from_msgs(messages, acc_init \\ %{}) do
    Enum.reduce(
      messages,
      acc_init,
      fn %Message{data: {_, _, %{service_id: service_id} = annotation}}, acc ->
        Map.put(
          acc,
          service_id,
          annotation
          |> Map.drop([:service_id])
        )
      end
    )
  end
end
