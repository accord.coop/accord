defmodule AccordSlack.Pipes.Repo do
  alias Broadway.Message

  use Broadway
  use AccordWeb.Pipeline.Pipe

  def id(), do: :accord_slack_pipe_repo

  def processor_concurrency(), do: 1

  def producer_spec(),
    do: [
      module: {AccordSlack.Producers.Repo, []}
    ]

  def batchers(),
    do: [
      cache: [concurrency: 1, batch_size: 256, batch_timeout: 3_000]
    ]

  def handle_message(:default, %Message{} = message, _) do
    message |> Message.put_batcher(:cache)
  end

  def handle_batch(:cache, messages, _, _) do
    messages
    |> Enum.group_by(fn %Message{data: {job_signature, _, _}} -> job_signature end)
    |> Enum.reduce([], fn {{job_stage, _signature_payload}, messages}, acc ->
      # Dispatch batches of messages by job_signature, which will all have the same
      # handler (so we just grab that from the first one).
      {_job_signature, {handler_module, handler_function}, _payload} =
        messages |> List.first() |> Map.get(:data)

      acc ++ apply(handler_module, handler_function, [job_stage, messages])
    end)
  end
end

defmodule AccordSlack.Producers.Repo do
  use GenStage
  use AccordWeb.Pipeline.Producer

  def id(), do: :accord_slack_source_repo
end
