defmodule AccordSlack.Pipes.Tier4 do
  alias Broadway.Message

  use Broadway
  use AccordWeb.Pipeline.Pipe

  def id(), do: :accord_slack_pipe_tier4

  def processor_concurrency(), do: System.schedulers_online()

  def producer_spec(),
    do: [
      module: {AccordSlack.Producers.Tier4, []},
      rate_limiting: [
        allowed_messages: 100,
        interval: 60_000
      ]
    ]

  def batchers(), do: nil

  def handle_message(
        :default,
        %Message{
          data: {
            _job_signature,
            {handler_module, handler_function},
            _payload
          }
        } = message,
        _
      ) do
    apply(handler_module, handler_function, [message])
  end
end

defmodule AccordSlack.Producers.Tier4 do
  use GenStage
  use AccordWeb.Pipeline.Producer

  def id(), do: :accord_slack_source_tier4
end
