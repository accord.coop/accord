defmodule AccordSlack.Views.DelegationsModal do
  alias Accord.{Repo, Person, People, Delegation}
  alias AccordSlack

  import AccordWeb.Gettext

  use BlockBox
  use AccordSlack.View.Components

  @behaviour AccordSlack.View
  @behaviour AccordSlack.View.Modal

  @view_id "delegations_modal"

  @impl true
  def id, do: @view_id

  defp rr_delegatee_name(%Person{preferred_name: delegatee_name} = delegatee) do
    delegatee_slack_id = delegatee |> Person.get_slack_user_id()

    case delegatee_slack_id do
      nil ->
        case delegatee_name do
          "" -> "(unnamed user)"
          name -> name
        end

      slack_id ->
        "<@#{slack_id}>"
    end
  end

  defp rr_scope_name(%People{service_type: "slack︰team"}), do: gettext("all decisions")

  defp rr_scope_name(%People{title: people_title}),
    do: gettext("decisions among %{people_title}", people_title: people_title)

  defp rr_delegation_title(delegation) do
    delegatee_name = Repo.get_rel(delegation, :delegatee) |> rr_delegatee_name()
    scope_name = Delegation.get_scope(delegation) |> rr_scope_name()

    text_object(
      gettext("%{delegatee_name} for %{scope_name}",
        delegatee_name: delegatee_name,
        scope_name: scope_name
      ),
      :mrkdwn
    )
  end

  defp r_delegations(blocks, []), do: r_body(blocks, gettext("(nobody)"))

  defp r_delegations(blocks, delegations) do
    Enum.map(delegations, fn %Delegation{uuid: delegation_uuid} = delegation ->
      section(
        rr_delegation_title(delegation),
        accessory: button(gettext("End"), "end-delegation_#{delegation_uuid}")
      )
    end) ++ blocks
  end

  defp r_create_delegation_cta(blocks),
    do: [
      actions_block([
        button(
          gettext("Create delegation"),
          "new-delegation"
        )
      ])
      | blocks
    ]

  defp r_delegations_coaching(blocks),
    do: [context_block([text_object(gettext("delegations coaching"))]) | blocks]

  @impl true
  def render(
        locale,
        %{
          delegations: delegations,
          person: %Person{}
        },
        metadata
      ) do
    put_locale(locale)

    blocks =
      []
      |> r_delegations_coaching()
      |> r_div()
      |> r_title("Your vote is currently delegated to:")
      |> r_delegations(delegations)
      |> r_create_delegation_cta()
      |> Enum.reverse()

    build_view(
      :modal,
      gettext("Your delegations"),
      blocks,
      close: text_object(gettext("Okay")),
      callback_id: @view_id,
      notify_on_close: true,
      private_metadata: Jason.encode!(metadata)
    )
  end

  @impl true
  def parse_response(_) do
    {:ok, nil, nil}
  end

  @impl true
  def commit(_, _) do
    :ok
  end
end
