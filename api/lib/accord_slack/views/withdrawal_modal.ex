defmodule AccordSlack.Views.WithdrawalModal do
  alias Accord.{Repo, DecisionVersion}
  alias AccordSlack

  import AccordWeb.Gettext

  use BlockBox
  use AccordSlack.View.Components

  @behaviour AccordSlack.View
  @behaviour AccordSlack.View.Modal

  @view_id "withdrawal_modal"

  @impl true
  def id, do: @view_id

  defp r_reason_input(blocks) do
    [
      input(
        text_object(gettext("Reason")),
        plain_text_input("withdrawal_input",
          placeholder: gettext("e.g. “Has typo’s”, “No longer relevant”"),
          multiline: true
        ),
        block_id: "withdrawal",
        hint:
          text_object(
            gettext(
              "The decision’s post will be deleted and this reason will be added to the post’s thread."
            )
          )
      )
      | blocks
    ]
  end

  @impl true
  def render(
        locale,
        %DecisionVersion{
          title: title
        },
        metadata
      ) do
    put_locale(locale)

    blocks =
      []
      |> r_body(
        gettext("Please enter the reason you’re withdrawing “%{decision_version_title}” below:",
          decision_version_title: title
        )
      )
      |> r_reason_input()
      |> Enum.reverse()

    build_view(
      :modal,
      gettext("Withdraw a decision"),
      blocks,
      close: text_object(gettext("Cancel")),
      submit: text_object(gettext("Withdraw")),
      callback_id: @view_id,
      notify_on_close: true,
      private_metadata: Jason.encode!(metadata)
    )
  end

  @impl true
  def parse_response(%{
        "view" => %{
          "state" => %{
            "values" => %{
              "withdrawal" => %{"withdrawal_input" => %{"value" => withdrawal_reason}}
            }
          },
          "private_metadata" => metadata_string
        }
      }) do
    with {:ok, metadata} <- Jason.decode(metadata_string),
         %{"decision_version_service_id" => service_id} <- metadata,
         %DecisionVersion{} = version <- DecisionVersion.get_by_service_id(service_id) do
      {:ok, %{decision_version: version, withdrawal_reason: withdrawal_reason}, nil}
    else
      err -> {:withdrawal_modal, :parse_response, err}
    end
  end

  @impl true
  def parse_response(_), do: {:error, :invalid_response}

  @impl true
  def commit(
        %{
          decision_version: %DecisionVersion{} = version,
          withdrawal_reason: state_addendum
        },
        _
      ) do
    with {:ok, _} <-
           DecisionVersion.change_state(
             version,
             DecisionVersion.Types.withdrawn(:str),
             state_addendum
           )
           |> Repo.update() do
      AccordSlack.Router.Withdrawals.withdraw_decision_message(version, state_addendum)
    end
  end
end
