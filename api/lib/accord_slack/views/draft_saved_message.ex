defmodule AccordSlack.Views.DraftSavedMessage do
  import AccordWeb.Gettext

  alias AccordSlack.View

  @behaviour View
  @behaviour View.Message

  @impl true
  def render(
        locale,
        %{bot_user_id: bot_user_id},
        _
      ) do
    put_locale(locale)

    %{
      text:
        gettext(
          "A draft of your decision was saved. Go to %{at_bot}’s home tab to continue editing or to delete the draft.",
          at_bot: "<@#{bot_user_id}>"
        )
    }
  end
end
