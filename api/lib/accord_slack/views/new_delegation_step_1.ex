defmodule AccordSlack.Views.NewDelegationStep1 do
  import AccordWeb.Gettext

  use BlockBox
  use AccordSlack.View.Components

  @behaviour AccordSlack.View
  @behaviour AccordSlack.View.Modal

  @view_id "new_delegation_step_1"

  @impl true
  def id, do: @view_id

  defp r_team_or_group_input(blocks) do
    [
      input(
        text_object(
          gettext(
            "Should your vote be delegated for all decisions on the team, or only decisions in a specific group?"
          )
        ),
        radio_buttons(
          "scope_input",
          [
            option_object(
              text_object(gettext("Team")),
              "team"
            ),
            option_object(
              text_object(gettext("Group")),
              "group"
            )
          ]
        ),
        block_id: "scope",
        hint: text_object(gettext("new delegations coaching"))
      )
      | blocks
    ]
  end

  @impl true
  def render(locale, _, %{"delegations_view_id" => _} = metadata) do
    put_locale(locale)

    blocks =
      []
      |> r_team_or_group_input()
      |> Enum.reverse()

    build_view(
      :modal,
      gettext("New delegation"),
      blocks,
      close: text_object(gettext("Cancel")),
      submit: text_object("#{gettext("Next")} →"),
      callback_id: @view_id,
      notify_on_close: true,
      private_metadata: Jason.encode!(metadata)
    )
  end

  @impl true
  def parse_response(%{
        "view" => %{
          "state" => %{
            "values" => %{
              "scope" => %{"scope_input" => %{"selected_option" => %{"value" => scope_value}}}
            }
          },
          "private_metadata" => metadata_str
        }
      }) do
    {:ok, scope_value, Jason.decode!(metadata_str)}
  end

  @impl true
  def parse_response(_), do: {:error, :invalid_response}

  @impl true
  def commit(_, _) do
    :ok
  end
end
