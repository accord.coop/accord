defmodule AccordSlack.Views.NewDecisionStep1Confirm do
  alias Accord.{DecisionVersion}
  alias AccordSlack
  alias AccordSlack.Context

  import AccordWeb.Gettext

  use BlockBox
  use AccordSlack.View.Components

  @behaviour AccordSlack.View
  @behaviour AccordSlack.View.Modal

  @view_id "new_decision_step_1_confirm"

  @impl true
  def id, do: @view_id

  @impl true
  def render(
        locale,
        %DecisionVersion{} = decision_version,
        metadata
      ) do
    put_locale(locale)

    blocks =
      ([]
       |> r_body(
         gettext(
           "Make sure the title, description, and options are exactly as intended in the preview below. Once the decision is posted to the channel, it can’t be changed."
         )
       )
       |> r_div()
       |> Enum.reverse()) ++
        AccordSlack.Views.DecisionMessage.render(locale, decision_version, :preview).blocks

    build_view(
      :modal,
      gettext("Preview decision"),
      blocks,
      close: text_object("← #{gettext("Back")}"),
      submit: text_object("#{gettext("Next")} →"),
      callback_id: @view_id,
      notify_on_close: true,
      private_metadata: Jason.encode!(metadata)
    )
  end

  @impl true
  def parse_response(
        %{
          "view" => %{
            "private_metadata" => metadata_string
          }
        } = params
      ) do
    with {_, %{person: %{locale: locale}}} <- Context.resolve(params),
         {:ok, metadata} <- Jason.decode(metadata_string) do
      {:ok, %{locale: locale}, metadata}
    else
      err -> {:new_decision_step_1_confirm, :parse_response, err}
    end
  end

  @impl true
  def parse_response(_), do: {:error, :invalid_response}

  @impl true
  def commit(_, _), do: :ok
end
