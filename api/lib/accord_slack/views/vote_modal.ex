defmodule AccordSlack.Views.VoteModal do
  alias Accord.{Repo, DecisionVersion, DecisionSettings, Vote, Option}
  alias Accord.DecisionVersionMoment, as: DVMoment
  alias Accord.DecisionSettings.Types, as: DSTypes
  alias AccordSlack
  alias AccordSlack.Context

  import AccordWeb.Gettext

  use BlockBox
  use AccordSlack.View.Components

  @behaviour AccordSlack.View
  @behaviour AccordSlack.View.Modal

  @view_id "vote_modal"

  @impl true
  def id, do: @view_id

  defp r_options(blocks, options, settings, {extant_vote?, choices_state}) do
    already_voted_message =
      if extant_vote? do
        [
          context_block([
            text_object(gettext("You can still change your choices until voting closes."))
          ]),
          section(text_object("✅ " <> gettext("You’ve already voted!")))
        ]
      else
        []
      end

    rr_option_inputs(
      settings,
      options,
      choices_state
    ) ++
      already_voted_message ++
      blocks
  end

  @method_cardinal DSTypes.cardinal(:str)
  @method_plurality DSTypes.plurality(:str)
  @method_stv DSTypes.ordinal_stv(:str)

  @cardinal_resolution_star DSTypes.star(:str)

  defp rrr_stv_remove_option(option_id),
    do: option_object(gettext("Remove"), "option_choice_remove︰#{option_id}")

  defp rr_stv_move_options(option_id, 0, 0), do: [rrr_stv_remove_option(option_id)]

  defp rr_stv_move_options(option_id, 0, _),
    do: [
      option_object(gettext("Decrease preference"), "option_choice_delta︰+1︰#{option_id}"),
      rrr_stv_remove_option(option_id)
    ]

  defp rr_stv_move_options(option_id, i, j) when i == j,
    do: [
      option_object(gettext("Increase preference"), "option_choice_delta︰-1︰#{option_id}"),
      rrr_stv_remove_option(option_id)
    ]

  defp rr_stv_move_options(option_id, _, _),
    do: [
      option_object(gettext("Increase preference"), "option_choice_delta︰-1︰#{option_id}"),
      option_object(gettext("Decrease preference"), "option_choice_delta︰+1︰#{option_id}"),
      rrr_stv_remove_option(option_id)
    ]

  defp rr_option_inputs(%{method: @method_stv}, options, choices) do
    selection =
      choices
      |> Enum.sort_by(fn {_id, %{"value" => value}} -> value end, &<=/2)
      |> Enum.map(fn {option_id, _} -> option_id end)

    ([
       section(text_object("*#{gettext("Your selection:")}*", :mrkdwn))
       | case length(selection) do
           0 ->
             [
               "_#{
                 "You’ve made no selections yet. You can add to your ballot from the options below."
                 |> gettext()
               }_"
               |> text_object(:mrkdwn)
               |> section()
             ]

           _ ->
             selection
             |> Enum.reduce(
               %{i: 0, acc: []},
               fn selected_id, %{i: i, acc: acc} ->
                 %{
                   i: i + 1,
                   acc: [
                     section(
                       text_object(
                         "#{i + 1}. " <>
                           Enum.find_value(options, fn %Option{id: option_id, title: title} ->
                             if option_id === selected_id, do: title, else: false
                           end)
                       ),
                       block_id: "option_choice︰#{selected_id}",
                       accessory:
                         overflow_menu(
                           "option_choice_input︰#{selected_id}",
                           rr_stv_move_options(selected_id, i, length(selection) - 1)
                         )
                     )
                     | acc
                   ]
                 }
               end
             )
             |> Map.get(:acc)
             |> Enum.reverse()
         end
     ] ++
       [
         section(text_object("*#{gettext("Unselected:")}*", :mrkdwn))
         | options
           |> Enum.reduce([], fn
             %Option{id: option_id, title: title}, acc ->
               if option_id in selection,
                 do: acc,
                 else: [
                   section(
                     text_object(title),
                     block_id: "option_choice︰#{option_id}",
                     accessory:
                       button(
                         gettext("Select"),
                         "option_choice_select︰#{option_id}"
                       )
                   )
                   | acc
                 ]
           end)
       ])
    |> Enum.reverse()
  end

  defp rr_option_inputs(%{method: @method_plurality}, options, choices) do
    choice_id =
      choices
      |> Enum.find_value(fn {option_id, %{"value" => value}} ->
        if(value > 0, do: option_id, else: false)
      end)

    [
      input(
        gettext("Your choice:"),
        radio_buttons(
          "option_choice_input",
          options |> Enum.map(&rr_option/1),
          if(map_size(choices) > 0,
            do: [
              initial_option:
                rr_option(
                  options
                  |> Enum.find(fn %Option{id: option_id} -> option_id == choice_id end)
                )
            ],
            else: []
          )
        ),
        block_id: "option_choice"
      )
    ]
    |> Enum.reverse()
  end

  defp rr_option_inputs(
         %{
           method: @method_cardinal,
           parameter1_value: p1v,
           parameter2_value: p2v,
           value_titles: value_titles
         },
         options,
         choices
       )
       when p1v > 1 and value_titles != nil and map_size(value_titles) > 0 do
    [
      section(text_object("*#{gettext("Your choice:")}*", :mrkdwn))
      | options
        |> Enum.map(fn %Option{id: option_id, title: title} ->
          section(
            text_object(title),
            block_id: "option_choice︰#{option_id}",
            accessory:
              select_menu(
                gettext("Choose one"),
                :static_select,
                "option_choice_input︰#{option_id}",
                options:
                  0..floor(p1v)
                  |> Enum.map(&rr_value_option(option_id, &1, value_titles))
                  |> Enum.reverse(),
                initial_option:
                  rr_value_option(
                    option_id,
                    floor(
                      case get_in(choices, [option_id, "value"]) do
                        nil -> p2v
                        choice_value -> choice_value
                      end
                    ),
                    value_titles
                  )
              )
          )
        end)
    ]
    |> Enum.reverse()
  end

  defp rr_option_inputs(
         %{
           method: @method_cardinal,
           parameter1_value: p1v,
           parameter2_value: p2v,
           parameter3_value: p3v
         },
         options,
         choices
       )
       when p1v > 1 do
    [
      section(
        text_object(
          "*#{
            if p3v > 0,
              do:
                gettext(
                  "Rate the following options with a score between 0 and %{upper_bound}, inclusive; a score below %{block_score} counts as a blocking vote:",
                  block_score: s_intish(p3v),
                  upper_bound: s_intish(p1v)
                ),
              else:
                gettext(
                  "Rate the following options with a score between 0 and %{upper_bound}, inclusive:",
                  upper_bound: s_intish(p1v)
                )
          }*",
          :mrkdwn
        )
      )
      | options
        |> Enum.map(fn %Option{id: option_id, title: title} ->
          input(
            text_object(title),
            plain_text_input(
              "option_choice_input︰#{option_id}",
              initial_value:
                floor(
                  case get_in(choices, [option_id, "value"]) do
                    nil -> p2v
                    choice_value -> choice_value
                  end
                )
                |> Integer.to_string(),
              placeholder: gettext("e.g. %{example}", example: "1")
            ),
            block_id: "option_choice︰#{option_id}"
          )
        end)
    ]
    |> Enum.reverse()
  end

  defp rr_option_inputs(%{method: @method_cardinal}, options, choices) do
    option_objects = options |> Enum.map(&rr_option/1)

    initial_options =
      options
      |> Enum.filter(fn %Option{id: option_id} -> Map.get(choices, option_id, 0) > 0 end)
      |> Enum.map(&rr_option/1)

    [
      input(
        gettext("Your choice:"),
        checkboxes(
          "option_choice_input",
          option_objects,
          case initial_options do
            [] -> []
            _ -> [initial_options: initial_options]
          end
        ),
        block_id: "option_choice"
      )
    ]
    |> Enum.reverse()
  end

  defp rr_value_option(option_id, value_integer, %{} = value_titles),
    do:
      option_object(
        Map.get(
          value_titles,
          Float.to_string(value_integer / 1),
          Integer.to_string(value_integer)
        ),
        "option_choice_value︰#{option_id}︰#{Float.to_string(value_integer / 1)}"
      )

  defp rr_option(%Option{title: title, id: option_id}),
    do: option_object(title, "option_choice_value︰#{option_id}︰1.0")

  defp r_settings_coaching(blocks, %{
         method: @method_cardinal,
         resolution: @cardinal_resolution_star
       }),
       do: [context_block([text_object(gettext("star method coaching"))]) | blocks]

  defp r_settings_coaching(blocks, %{
         method: @method_cardinal,
         parameter1_value: p1v,
         parameter3_value: p3v,
         parameter4_value: p4v
       })
       when p1v == 2 and p3v > 0,
       do: [
         context_block([text_object(gettext("consensus method coaching", p4v: s_intish(p4v)))])
         | blocks
       ]

  defp r_settings_coaching(blocks, %{method: @method_cardinal, parameter1_value: p1v})
       when p1v > 1,
       do: [context_block([text_object(gettext("score method coaching"))]) | blocks]

  defp r_settings_coaching(blocks, %{method: @method_cardinal, parameter1_value: p1v})
       when p1v <= 1,
       do: [context_block([text_object(gettext("approval method coaching"))]) | blocks]

  defp r_settings_coaching(blocks, %{method: @method_plurality}),
    do: [context_block([text_object(gettext("plurality method coaching"))]) | blocks]

  defp r_settings_coaching(blocks, _), do: blocks

  defp r_general_coaching(blocks),
    do: [context_block([text_object(gettext("general vote coaching"))]) | blocks]

  @impl true
  def render(
        locale,
        %{
          decision_version: %DecisionVersion{
            title: title,
            body: body,
            options: options,
            settings: settings
          },
          extant_vote?: extant_vote?
        },
        metadata
      ) do
    put_locale(locale)

    blocks =
      []
      |> r_title(title)
      |> r_body(body)
      |> r_div()
      |> r_settings_coaching(settings)
      |> r_options(options, settings, {extant_vote?, Map.get(metadata, "choices_state")})
      |> r_div()
      |> r_general_coaching()
      |> Enum.reverse()

    build_view(
      :modal,
      gettext("Confirm your vote"),
      blocks,
      close: text_object(gettext("Cancel")),
      submit: text_object("#{gettext("Confirm")} ✓"),
      callback_id: @view_id,
      notify_on_close: true,
      private_metadata: Jason.encode!(metadata)
    )
  end

  defp parse_choice_value(
         block_id,
         "option_choice_value︰" <> choice_string,
         ballot_choices,
         %DecisionSettings{parameter1_value: p1v}
       ) do
    with [option_id, value_string] <- choice_string |> String.split("︰"),
         {value, _} <- Float.parse(value_string) do
      if value > p1v or value < 0,
        do:
          {:halt,
           {:error,
            %{block_id => gettext("error choice out of bounds", upper_bound: s_intish(p1v))}}},
        else: {:cont, Map.put(ballot_choices, option_id, value)}
    else
      _ -> {:halt, {:error, %{block_id => gettext("error parse choice")}}}
    end
  end

  defp parse_input_state(block_id, %{"selected_options" => selections}, ballot_choices, settings) do
    case selections
         |> Enum.reduce_while(
           ballot_choices,
           fn %{"value" => selected_option_value}, ballot_choices ->
             parse_choice_value(block_id, selected_option_value, ballot_choices, settings)
           end
         ) do
      {:error, errors} -> {:halt, {:error, errors}}
      ballot_choices -> {:cont, ballot_choices}
    end
  end

  defp parse_input_state(
         block_id,
         %{"selected_option" => %{"value" => selected_option_value}},
         ballot_choices,
         settings
       ),
       do: parse_choice_value(block_id, selected_option_value, ballot_choices, settings)

  defp parse_input_state(
         block_id,
         %{"value" => choice_string},
         ballot_choices,
         settings
       ) do
    "option_choice︰" <> option_id = block_id

    parse_choice_value(
      block_id,
      "option_choice_value︰#{option_id}︰#{choice_string}",
      ballot_choices,
      settings
    )
  end

  defp parse_input_state(block_id, _, _, _),
    do: {:halt, {:error, %{block_id => gettext("error parse choice")}}}

  defp parse_ballot_values(ballot_values, settings) do
    case ballot_values
         |> Enum.reduce_while(%{}, fn {block_id, option_choice_inputs}, ballot_choices ->
           case option_choice_inputs
                |> Enum.reduce_while(
                  ballot_choices,
                  fn {_input_id, input_state}, ballot_choices ->
                    parse_input_state(block_id, input_state, ballot_choices, settings)
                  end
                ) do
             {:error, errors} -> {:halt, {:error, errors}}
             ballot_choices -> {:cont, ballot_choices}
           end
         end) do
      {:error, errors} -> {:error, errors}
      ballot_choices -> {:ok, ballot_choices}
    end
  end

  @impl true
  def parse_response(
        %{
          "view" => %{
            "state" => %{
              "values" => ballot_values
            },
            "private_metadata" => metadata_string
          }
        } = params
      ) do
    with {_, %{person: %{locale: _locale} = voter}} <- Context.resolve(params),
         {:ok, metadata} <- Jason.decode(metadata_string),
         %{"decision_version_service_id" => service_id} <- metadata,
         %DecisionVersion{} = version <- DecisionVersion.get_by_service_id(service_id),
         {:ok, ballot_choices} <-
           (case map_size(ballot_values) do
              0 -> {:ok, Map.get(metadata, "choices_state")}
              _ -> parse_ballot_values(ballot_values, version.settings)
            end) do
      {:ok, %{decision_version: version, ballot_choices: ballot_choices, voter: voter}, nil}
    else
      {:error, %{} = errors} -> {:error, errors}
      err -> {:vote_modal, :parse_response, err}
    end
  end

  @impl true
  def parse_response(_), do: {:error, :invalid_response}

  @impl true
  def commit(
        %{
          decision_version: %DecisionVersion{} = version,
          ballot_choices: ballot_choices,
          voter: voter
        },
        _
      ) do
    choices =
      ballot_choices
      |> Enum.reduce(Map.new(), fn
        {option_uuid, %{"value" => _} = choice}, acc -> acc |> Map.put(option_uuid, choice)
        {option_uuid, value}, acc -> acc |> Map.put(option_uuid, %{"value" => value / 1})
      end)

    with {:ok, %{decision_version_moment: dv_moment}} <-
           (Vote.create(choices, voter, version) ++
              DVMoment.create(
                version,
                Repo.now(),
                DVMoment.Types.service_update(:str)
              ))
           |> Repo.commit(),
         :ok <-
           AccordWeb.Pipeline.Jobs.AssessDecisionVersion.start(dv_moment) do
      AccordSlack.Router.Decisions.update_decision_message(version)
    end
  end
end
