defmodule AccordSlack.Views.NewDelegationStep2 do
  alias Accord.{Repo, Delegation, Errors}
  alias AccordSlack.Context
  alias AccordWeb.Cldr

  import AccordWeb.Gettext

  use BlockBox
  use AccordSlack.View.Components

  @behaviour AccordSlack.View
  @behaviour AccordSlack.View.Modal

  @view_id "new_delegation_step_2"

  @impl true
  def id, do: @view_id

  defp r_group_selector(blocks, "group") do
    [
      input(
        text_object(gettext("For decisions in")),
        select_menu(
          gettext("Select a channel or group"),
          :conversations_select,
          "scope_people_input"
        ),
        block_id: "scope_people"
      )
      | blocks
    ]
  end

  defp r_group_selector(blocks, "team"), do: blocks

  defp s_delegatee_label("team"),
    do: gettext("For decisions in any groups on the team, delegate to")

  defp s_delegatee_label("group"), do: gettext("Delegate to")

  defp r_person_selector(blocks, scope) do
    [
      input(
        text_object(s_delegatee_label(scope)),
        select_menu(
          gettext("Select a user on your team"),
          :users_select,
          "delegatee_input"
        ),
        block_id: "delegatee"
      )
      | blocks
    ]
  end

  defp d_initial_date() do
    Repo.now()
    |> NaiveDateTime.add(604_800, :second)
    |> Date.to_iso8601()
  end

  defp dt_5am(tz) do
    with {:ok, dt} <- DateTime.now(tz),
         dt_5am <- Map.merge(dt, %{hour: 5, minute: 0, second: 0}) do
      {:ok, dt_5am}
    else
      err -> {:error, err}
    end
  end

  defp ss_5am(locale) do
    with {:ok, dt_5am} <- dt_5am(locale.tz),
         {:ok, string_5am} <- Cldr.Time.to_string(dt_5am, format: :short, locale: get_l10n()) do
      string_5am
    else
      _ -> gettext("5am")
    end
  end

  defp r_expiry(blocks, locale) do
    [
      input(
        text_object(gettext("Until")),
        datepicker(
          "expiry_input",
          initial_date: d_initial_date()
        ),
        block_id: "expiry",
        hint:
          text_object(
            gettext(
              "The delegation will expire at %{ss_5am} in your time zone (%{tz}) on the day you select.",
              ss_5am: ss_5am(locale),
              tz: locale.tz
            )
          )
      )
      | blocks
    ]
  end

  @impl true
  def render(
        locale,
        %{
          scope: scope
        },
        %{"delegations_view_id" => _} = metadata
      ) do
    put_locale(locale)

    blocks =
      []
      |> r_group_selector(scope)
      |> r_person_selector(scope)
      |> r_expiry(locale)
      |> Enum.reverse()

    build_view(
      :modal,
      gettext("New delegation"),
      blocks,
      close: text_object(gettext("Cancel")),
      submit: text_object("#{gettext("Save")} ✓"),
      callback_id: @view_id,
      notify_on_close: true,
      private_metadata: Jason.encode!(metadata)
    )
  end

  defp fail(err) do
    IO.puts("[New delegation step 2]")
    IO.inspect(err)
    {:error, err}
  end

  @impl true
  def parse_response(%{
        "team" => %{"id" => team_id},
        "user" => %{"id" => delegator_user_id},
        "view" => %{
          "state" => %{
            "values" =>
              %{
                "expiry" => %{
                  "expiry_input" => %{
                    "selected_date" => expiry_date_as_iso8601
                  }
                },
                "delegatee" => %{
                  "delegatee_input" => %{
                    "selected_user" => delegatee_user_id
                  }
                }
              } = values
          },
          "private_metadata" => metadata_str
        }
      }) do
    with {:ok, metadata} <-
           (case metadata_str do
              "" -> {:ok, %{}}
              _ -> Jason.decode(metadata_str)
            end),
         {:ok, %{team: team, person: delegatee, slack_user_cached: %{is_bot: delegatee_is_bot}}} <-
           Context.resolve({:slack, team_id, delegatee_user_id}),
         :ok <-
           (case delegatee_is_bot do
              false -> :ok
              _ -> {:error, %{"delegatee" => gettext("can't delegate to bot")}}
            end),
         {:ok, %{person: %{locale: locale} = delegator}} <-
           Context.resolve({:slack, team_id, delegator_user_id}),
         # Expiry
         {:ok, dt_5am} <- dt_5am(locale.tz),
         {:ok, dt_5am_utc} <- DateTime.shift_zone(dt_5am, "Etc/UTC"),
         {:ok, date} <- Date.from_iso8601(expiry_date_as_iso8601),
         expires <- dt_5am_utc |> Map.merge(Map.from_struct(date)) |> DateTime.to_naive(),
         # Scope:
         scope <-
           (case values do
              %{
                "scope_people" => %{
                  "scope_people_input" => %{
                    "selected_conversation" => scope_people_id
                  }
                }
              } ->
                with {:ok, %{channel: scope_people}} <-
                       Context.resolve({:slack, team_id, scope_people_id, delegator_user_id}) do
                  scope_people
                else
                  _ -> {:error, Errors.not_found()}
                end

              _ ->
                team
            end) do
      {:ok, %{expires: expires, delegatee: delegatee, delegator: delegator, scope: scope},
       metadata}
    else
      {:error, %{} = errors} -> {:error, errors}
      err -> fail(err)
    end
  end

  @impl true
  def parse_response(_), do: {:error, Errors.bad_args()}

  defp unpack_error({:error, err}), do: unpack_error(err)
  defp unpack_error(err), do: err

  defp handle_changeset_error({key, {error, _}}, acc) do
    err_circular = Errors.circular(:str)
    err_self = Errors.delegates_to_self(:str)
    err_bad_args = Errors.bad_args(:str)

    case {key, error} do
      {:delegatee, ^err_circular} ->
        Map.put(acc, "delegatee", gettext("can't make circular delegation"))

      {:delegatee, ^err_self} ->
        Map.put(acc, "delegatee", gettext("can't delegate to yourself"))

      {:scope_people, ^err_bad_args} ->
        Map.put(acc, "expiry", gettext("can't overlap delegations"))
    end
  end

  defp handle_changeset_errors({:error, :delegation, %{errors: errors}, _}) do
    Enum.reduce(errors, %{}, &handle_changeset_error/2)
  end

  defp handle_changeset_errors({:error, _} = err) do
    unpack_error(err) |> handle_changeset_errors()
  end

  @impl true
  def commit(%{delegator: delegator, delegatee: delegatee, scope: scope, expires: expires}, nil) do
    err_not_found = Errors.not_found(:str)

    with :ok <-
           (case scope do
              {:error, ^err_not_found} ->
                {:error, %{"scope_people" => gettext("bot doesn't have access")}}

              _ ->
                :ok
            end),
         {:ok, %{delegation: %Delegation{}}} <-
           Delegation.create(delegator, delegatee, scope, expires: expires) |> Repo.commit() do
      :ok
    else
      {:error, %{} = errors} -> {:error, errors}
      err -> {:error, handle_changeset_errors(err)}
    end
  end
end
