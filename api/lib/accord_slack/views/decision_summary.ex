defmodule AccordSlack.Views.DecisionSummary do
  import AccordWeb.Gettext

  alias Accord.{DecisionVersion, DecisionSettings, Option}
  alias Accord.Caches.DecisionVersionResult, as: Result

  alias AccordSlack.View

  use BlockBox
  use View.Components

  @behaviour View
  @behaviour View.Message

  @method_plurality DecisionSettings.Types.plurality(:str)
  @method_cardinal DecisionSettings.Types.cardinal(:str)
  @method_stv DecisionSettings.Types.ordinal_stv(:str)

  @cardinal_resolution_star DecisionSettings.Types.star(:str)

  defp s_title(version_title, winning_options) when length(winning_options) > 0 do
    ngettext(
      "Winning option for “%{version_title}”",
      "Winning options for “%{version_title}”",
      length(winning_options),
      version_title: version_title
    ) <>
      ":\n" <>
      (winning_options
       |> Enum.map(fn %Option{title: option_title} -> "✔︎ *#{option_title}*" end)
       |> Enum.join("\n"))
  end

  defp s_title(version_title, _) do
    gettext("*No option won for “%{version_title}”.*", version_title: version_title)
  end

  defp ss_score(score), do: s_intish(score)

  defp s_score_list_line(%{score: :blocked}, option_title, _, _),
    do: "*#{gettext("Blocked")}* #{option_title}\n"

  defp s_score_list_line(
         %{score: option_score},
         option_title,
         {n_votes, n_delegations},
         %DecisionSettings{
           method: @method_plurality
         }
       ),
       do: "#{ss_score(option_score)}/#{s_intish(n_votes + n_delegations)} #{option_title}\n"

  defp s_score_list_line(
         %{score: option_score},
         option_title,
         {n_votes, n_delegations},
         %DecisionSettings{
           method: @method_cardinal,
           parameter3_value: p3v
         }
       )
       when p3v > 0 do
    score = option_score - p3v * (n_votes + n_delegations)

    "#{ss_score(score)} #{option_title}\n"
  end

  defp s_score_list_line(
         %{score: option_score},
         option_title,
         _participation,
         %DecisionSettings{
           method: @method_cardinal
         }
       ),
       do: "#{ss_score(option_score)} #{option_title}\n"

  defp s_score_list_line(
         %{score: option_score},
         option_title,
         _participation,
         %DecisionSettings{
           method: @method_stv
         }
       ),
       do: "#{ss_score(option_score)} #{option_title}\n"

  defp ss_star_selection(
         option_title,
         preference,
         prefs_total
       ),
       do: "#{s_percent(preference, prefs_total)} #{option_title}"

  defp s_selection_line(
         selected_uuid,
         %{annotation: %{round: round, contest: contest}},
         %DecisionVersion{
           settings: %{method: @method_cardinal, resolution: @cardinal_resolution_star}
         } = version
       ) do
    prefs_total =
      contest
      |> Enum.reduce(0, fn {_, preference}, prefs_total -> prefs_total + preference end)

    contest_percentiles =
      contest
      |> Enum.sort_by(fn {_, preference} -> preference end, &>=/2)
      |> Enum.reduce(Map.new(), fn {option_uuid, preference}, acc ->
        acc
        |> Map.put(
          option_uuid,
          ss_star_selection(
            DecisionVersion.get_option_by_id(option_uuid, version) |> Map.get(:title),
            preference,
            prefs_total
          )
        )
      end)

    "#{round + 1}. #{Map.get(contest_percentiles, selected_uuid)} (vs #{
      Map.drop(contest_percentiles, [selected_uuid])
      |> Enum.map(fn {_, percentile} -> percentile end)
      |> Enum.join(", ")
    })\n"
  end

  defp s_score_section_title(%DecisionSettings{
         method: @method_cardinal
       }),
       do: gettext("Score totals:\n")

  defp s_score_section_title(%DecisionSettings{
         method: @method_stv
       }),
       do: gettext("Final allocations:\n")

  defp s_score_section_title(_), do: gettext("Breakdown:\n")

  defp fix_annotation_pattern(annotation) when is_map(annotation),
    do:
      Enum.reduce(annotation, Map.new(), fn
        {"" <> str_key, val}, acc -> Map.put(acc, String.to_atom(str_key), val)
        {key, val}, acc -> Map.put(acc, key, val)
      end)

  defp fix_annotation_pattern(annotation), do: annotation

  defp fix_breakdown_pattern(breakdown),
    do:
      breakdown
      |> Enum.reduce(Map.new(), fn {option_uuid, option_result}, acc ->
        option_score = Map.get(option_result, :score, Map.get(option_result, "score"))

        Map.put(acc, option_uuid, %{
          score:
            case option_score do
              "blocked" -> :blocked
              score -> score
            end,
          annotation:
            Map.get(option_result, :annotation, Map.get(option_result, "annotation"))
            |> fix_annotation_pattern()
        })
      end)

  defp r_score_section(
         fixed_breakdown,
         participation,
         %DecisionVersion{settings: settings} = version
       ),
       do: [
         fixed_breakdown
         |> Enum.sort_by(
           fn
             {_, %{score: :blocked}} -> 1.0e-42
             {_, %{score: score}} -> score
           end,
           &>=/2
         )
         |> Enum.reduce(
           s_score_section_title(settings),
           fn {option_id, option_result}, acc ->
             option_title =
               DecisionVersion.get_option_by_id(option_id, version)
               |> Map.get(:title)

             acc <>
               s_score_list_line(
                 option_result,
                 option_title,
                 participation,
                 settings
               )
           end
         )
         |> text_object(:mrkdwn)
         |> section()
       ]

  defp r_docs_link(%DecisionSettings{
         method: @method_cardinal,
         resolution: @cardinal_resolution_star
       }),
       do: [
         section(
           text_object(
             "\n<https://accord.coop/docs/slack/results/star|#{
               gettext("How to interpret STAR method results")
             }  ›>",
             :mrkdwn
           )
         )
       ]

  defp r_docs_link(%DecisionSettings{
         method: @method_stv
       }),
       do: [
         section(
           text_object(
             "\n<https://accord.coop/docs/slack/results/stv|#{
               gettext("How to interpret STV method results")
             }  ›>",
             :mrkdwn
           )
         )
       ]

  defp r_docs_link(_), do: []

  defp r_selection_section(
         fixed_breakdown,
         %DecisionVersion{
           settings: %{method: @method_cardinal, resolution: @cardinal_resolution_star}
         } = version
       ),
       do: [
         fixed_breakdown
         |> Enum.filter(fn
           {_, %{annotation: %{round: _}}} -> true
           _ -> false
         end)
         |> Enum.sort_by(
           fn
             {_, %{annotation: %{round: round}}} -> round
           end,
           &<=/2
         )
         |> Enum.reduce(
           gettext("\nFinalist preferences:\n"),
           fn {selected_uuid, option_result}, acc ->
             acc <>
               s_selection_line(
                 selected_uuid,
                 option_result,
                 version
               )
           end
         )
         |> text_object(:mrkdwn)
         |> section()
       ]

  defp r_selection_section(_, _), do: []

  defp r_breakdown(
         blocks,
         breakdown,
         participation,
         %DecisionVersion{settings: settings} = version
       ) do
    fixed_breakdown = breakdown |> fix_breakdown_pattern()

    r_docs_link(settings) ++
      r_selection_section(fixed_breakdown, version) ++
      r_score_section(fixed_breakdown, participation, version) ++
      blocks
  end

  @impl true
  def render(
        locale,
        %{
          decision_version_result: %Result{
            summary: %{
              winning_option_uuids: winning_option_uuids,
              breakdown: breakdown,
              n_votes: n_votes,
              n_delegations: n_delegations
            }
          },
          decision_version: %DecisionVersion{title: version_title} = version
        },
        _metadata
      ) do
    put_locale(locale)

    winning_options = version |> DecisionVersion.get_winning_options(winning_option_uuids)

    summary_title = s_title(version_title, winning_options)

    blocks =
      []
      |> r_body(summary_title)
      |> r_div()
      |> r_breakdown(breakdown, {n_votes, n_delegations}, version)
      |> Enum.reverse()

    %{
      text: summary_title,
      blocks: blocks,
      unfurl_links: false,
      unfurl_media: false
    }
  end
end
