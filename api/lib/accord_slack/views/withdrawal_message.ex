defmodule AccordSlack.Views.WithdrawalMessage do
  import AccordWeb.Gettext

  alias AccordSlack.View

  use BlockBox

  @behaviour View
  @behaviour View.Message

  defp blockquote_message(message), do: Regex.replace(~r/^(.*)/m, message, "> \\1")

  @impl true
  def render(
        locale,
        withdrawal_reason,
        _
      ) do
    put_locale(locale)

    blocks = [
      section(text_object("This decision was withdrawn by the author:")),
      section(text_object(blockquote_message(withdrawal_reason), :mrkdwn))
    ]

    %{
      text: gettext("This decision was withdrawn"),
      blocks: blocks
    }
  end
end
