defmodule AccordSlack.Views.NewDecisionStep2 do
  alias Accord.{Repo, DecisionVersion, DecisionVersionMoment, DecisionRole, Schedule}
  alias Accord.DecisionSettings.Types, as: DSTypes
  alias Accord.DecisionSettings.Templates, as: DSTemplates
  alias AccordSlack.Context
  alias AccordSlack.IO, as: SlackIO
  alias AccordSlack.Views.DecisionMessage

  import AccordWeb.Gettext
  use AccordSlack.View.Components
  use BlockBox

  @behaviour AccordSlack.View
  @behaviour AccordSlack.View.Modal

  @view_id "new_decision_step_2"

  @impl true
  def id, do: @view_id

  defp quorum_options(),
    do: [
      option_object(
        gettext("Anyone"),
        "q_any"
      ),
      option_object(
        gettext("At least 50%"),
        "q_half"
      ),
      option_object(
        gettext("At least two-thirds"),
        "q_twothirds"
      ),
      option_object(
        gettext("Everyone"),
        "q_all"
      )
    ]

  defp method_options(),
    do: [
      option_object(
        gettext("Approval"),
        "m_approval"
      ),
      option_object(
        gettext("Consensus"),
        "m_consensus"
      ),
      option_object(
        gettext("Score"),
        "m_score"
      ),
      option_object(
        gettext("STAR (Score then automatic runoff)"),
        "m_star"
      ),
      option_object(
        gettext("Single transferable vote (STV)"),
        "m_stv"
      ),
      option_object(
        gettext("Plurality"),
        "m_plurality"
      )
    ]

  defp expiry_options(),
    do: [
      option_object(
        gettext("2 minutes"),
        "c_#{2 * 60}"
      ),
      option_object(
        gettext("5 minutes"),
        "c_#{5 * 60}"
      ),
      option_object(
        gettext("15 minutes"),
        "c_#{15 * 60}"
      ),
      option_object(
        gettext("30 minutes"),
        "c_#{30 * 60}"
      ),
      option_object(
        gettext("1 hour"),
        "c_#{60 * 60}"
      ),
      option_object(
        gettext("2 hours"),
        "c_#{2 * 60 * 60}"
      ),
      option_object(
        gettext("4 hours"),
        "c_#{4 * 60 * 60}"
      ),
      option_object(
        gettext("8 hours"),
        "c_#{8 * 60 * 60}"
      ),
      option_object(
        gettext("16 hours"),
        "c_#{16 * 60 * 60}"
      ),
      option_object(
        gettext("24 hours"),
        "c_#{24 * 60 * 60}"
      ),
      option_object(
        gettext("2 days"),
        "c_#{2 * 24 * 60 * 60}"
      ),
      option_object(
        gettext("1 week"),
        "c_#{7 * 24 * 60 * 60}"
      ),
      option_object(
        gettext("2 weeks"),
        "c_#{14 * 24 * 60 * 60}"
      ),
      option_object(
        gettext("4 weeks"),
        "c_#{28 * 24 * 60 * 60}"
      )
    ]

  defp rr_conversation_select(conversation_id),
    do:
      select_menu(
        gettext("Select a channel or group"),
        :conversations_select,
        "people_votable_input",
        case conversation_id do
          nil -> []
          _ -> [initial_conversation: conversation_id]
        end
      )

  defp r_votable(blocks, active_conversation_id) do
    [
      input(
        gettext("Post in"),
        rr_conversation_select(active_conversation_id),
        block_id: "people_votable",
        hint:
          gettext("Everyone in this conversation will be able to vote on this decision")
          |> text_object()
      )
      | blocks
    ]
  end

  # Method addendum: Consensus

  defp r_method_addendum(blocks, %{"active_method" => %{value: "m_consensus"}}),
    do: [
      input(
        gettext("Minimum votes to block"),
        plain_text_input(
          "parameter4_value_input",
          placeholder: "e.g. 2" |> text_object(),
          initial_value: "1"
        ),
        block_id: "parameter4_value",
        hint: gettext("minimum block coaching") |> text_object()
      )
      | blocks
    ]

  # Method addendum: STV

  defp r_method_addendum(blocks, %{"active_method" => %{value: "m_stv"}}),
    do: blocks

  # Method addendum: Approval

  defp r_method_addendum(blocks, %{"active_method" => %{value: "m_approval"}}) do
    [
      input(
        gettext("Minimum approval"),
        plain_text_input(
          "parameter0_value_input",
          placeholder: "e.g. 4" |> text_object(),
          initial_value: "1"
        ),
        block_id: "parameter0_value",
        hint: gettext("minimum approval coaching") |> text_object()
      )
      | blocks
    ]
  end

  # Method addendum: Plurality

  defp r_method_addendum(blocks, %{"active_method" => %{value: "m_plurality"}}) do
    [
      input(
        gettext("Minimum plurality (%)"),
        plain_text_input(
          "parameter0_value_input",
          placeholder: "e.g. 67%" |> text_object(),
          initial_value: "0%"
        ),
        block_id: "parameter0_value",
        hint: gettext("minimum plurality coaching") |> text_object()
      )
      | blocks
    ]
  end

  # Method addendum: Score and STAR

  defp r_method_addendum(blocks, %{"active_method" => %{value: "m_score"}} = metadata),
    do:
      r_score_method_addendum(
        blocks,
        metadata,
        DSTemplates.score() |> Map.get(:value_titles, %{})
      )

  defp r_method_addendum(blocks, %{"active_method" => %{value: "m_star"}} = metadata),
    do: r_score_method_addendum(blocks, metadata, DSTemplates.star() |> Map.get(:value_titles))

  defp r_score_method_addendum(blocks, metadata, initial_value_titles) do
    p1v = Map.get(metadata, "parameter1_value", 5.0)
    p3v = Map.get(metadata, "parameter3_value", 0.0)

    addendum_blocks =
      [
        input(
          gettext("Blocking votes"),
          select_menu(
            gettext("Allow or disallow"),
            :static_select,
            "parameter3_value_input",
            options: rr_blocking_votes_options(),
            initial_option: rr_blocking_votes_options() |> List.first()
          ),
          block_id: "parameter3_value",
          hint: gettext("blocking score coaching") |> text_object(),
          dispatch_action: true
        )
      ] ++
        if(p3v > 0,
          do: [
            input(
              gettext("Minimum votes to block"),
              plain_text_input(
                "parameter4_value_input",
                placeholder: "e.g. 2" |> text_object(),
                initial_value: "1"
              ),
              block_id: "parameter4_value",
              hint: gettext("minimum block coaching") |> text_object()
            )
          ],
          else: []
        ) ++
        [
          input(
            gettext("Scores upper bound"),
            plain_text_input(
              "parameter1_value_input",
              placeholder: "e.g. 10" |> text_object(),
              initial_value: "5"
            )
            |> Map.merge(%{
              dispatch_action_config: %{
                trigger_actions_on: ["on_enter_pressed"]
              }
            }),
            block_id: "parameter1_value",
            hint: gettext("score upper bound coaching") |> text_object(),
            dispatch_action: true
          )
          | rr_score_names(p1v, p3v, initial_value_titles)
        ]

    Enum.reverse(addendum_blocks) ++ blocks
  end

  defp rr_blocking_votes_options(),
    do: [
      option_object(gettext("Disallow blocking votes"), "parameter3_value_0.0"),
      option_object(gettext("Allow blocking votes"), "parameter3_value_1.0")
    ]

  defp rrr_score_qualifier(score, parameter3_value) when parameter3_value == score,
    do: gettext(" ") <> gettext("(baseline)")

  defp rrr_score_qualifier(score, parameter3_value) when parameter3_value > score,
    do: gettext(" ") <> gettext("(blocking)")

  defp rrr_score_qualifier(_, _), do: ""

  defp rrr_score_name(score, parameter3_value, initial_value) do
    input(
      gettext("Name for score %{score}%{qualifier}",
        score: s_intish(score),
        qualifier: rrr_score_qualifier(score, parameter3_value)
      ),
      plain_text_input(
        "score_title_input_#{score / 1}",
        case initial_value do
          "" <> str_init_value -> [initial_value: str_init_value]
          nil -> []
        end
      ),
      block_id: "score_title_#{score / 1}",
      optional: true
    )
  end

  defp rr_score_names(
         parameter1_value,
         parameter3_value,
         initial_value_titles
       )
       when parameter1_value <= 16 do
    0..floor(parameter1_value)
    |> Enum.reduce([], fn n, acc ->
      [
        rrr_score_name(
          n,
          parameter3_value,
          initial_value_titles |> Map.get(Float.to_string(n / 1), nil)
        )
        | acc
      ]
    end)
    |> Enum.reverse()
  end

  defp rr_score_names(_, _, _), do: []

  defp s_method_coaching(%{"value" => "m_plurality"}),
    do: s_method_coaching(%{value: "m_plurality"})

  defp s_method_coaching(%{value: "m_plurality"}),
    do: gettext("plurality method breakdown")

  defp s_method_coaching(%{"value" => "m_consensus"}),
    do: s_method_coaching(%{value: "m_consensus"})

  defp s_method_coaching(%{value: "m_consensus"}),
    do: gettext("consensus method breakdown")

  defp s_method_coaching(%{"value" => "m_approval"}),
    do: s_method_coaching(%{value: "m_approval"})

  defp s_method_coaching(%{value: "m_approval"}),
    do: gettext("approval method breakdown")

  defp s_method_coaching(%{"value" => "m_score"}),
    do: s_method_coaching(%{value: "m_score"})

  defp s_method_coaching(%{value: "m_score"}),
    do: gettext("score method breakdown")

  defp s_method_coaching(%{"value" => "m_stv"}),
    do: s_method_coaching(%{value: "m_stv"})

  defp s_method_coaching(%{value: "m_stv"}),
    do: gettext("stv method breakdown")

  defp s_method_coaching(%{"value" => "m_star"}),
    do: s_method_coaching(%{value: "m_star"})

  defp s_method_coaching(%{value: "m_star"}),
    do: gettext("star method breakdown")

  defp r_method(blocks, active_method) do
    [
      input(
        gettext("Method"),
        select_menu(
          gettext("Select how the votes will be counted"),
          :static_select,
          "method_input",
          options: method_options(),
          initial_option: active_method
        ),
        block_id: "method",
        hint: s_method_coaching(active_method) |> text_object(),
        dispatch_action: true
      )
      | blocks
    ]
  end

  defp r_quorum(blocks, populate_initial_defaults) do
    [
      input(
        gettext("Quorum"),
        select_menu(
          gettext("Select how many votes this needs"),
          :static_select,
          "quorum_input",
          [options: quorum_options()]
          |> Keyword.merge(
            case populate_initial_defaults do
              true -> [initial_option: quorum_options() |> List.first()]
              _ -> []
            end
          )
        ),
        block_id: "quorum",
        hint:
          gettext(
            "Choose how much participation is needed in order for any winning options to be selected."
          )
          |> text_object(),
        dispatch_action: true
      )
      | blocks
    ]
  end

  defp r_expiry(blocks, populate_initial_defaults) do
    [
      input(
        gettext("Close voting in…"),
        select_menu(
          gettext("Select by how long from now"),
          :static_select,
          "expiry_input",
          [options: expiry_options()]
          |> Keyword.merge(
            case populate_initial_defaults do
              true -> [initial_option: expiry_options() |> Enum.at(2)]
              _ -> []
            end
          )
        ),
        block_id: "expiry",
        hint:
          gettext(
            "Choose when the voting will close. Once a vote closes, votes are tallied and the results are displayed."
          )
          |> text_object()
      )
      | blocks
    ]
  end

  defp r_n_winners(blocks, populate_initial_defaults) do
    [
      input(
        gettext("Number of winners"),
        plain_text_input(
          "n_winners_input",
          [placeholder: "e.g. 1" |> text_object()]
          |> Keyword.merge(
            case populate_initial_defaults do
              true -> [initial_value: "1"]
              _ -> []
            end
          )
        ),
        block_id: "n_winners",
        hint: gettext("number of winners coaching") |> text_object()
      )
      | blocks
    ]
  end

  defp fix_method_pattern(%{"value" => method_value} = method),
    do: Map.put(method, :value, method_value)

  defp fix_method_pattern(method), do: method

  @impl true
  def render(locale, populate_initial_defaults, metadata) do
    put_locale(locale)

    active_method =
      Map.get(metadata, "active_method", method_options() |> List.first()) |> fix_method_pattern()

    blocks =
      []
      |> r_method(active_method)
      |> r_quorum(populate_initial_defaults)
      |> r_expiry(populate_initial_defaults)
      |> r_votable(if populate_initial_defaults, do: metadata["channel_id"], else: nil)
      |> r_div()
      |> r_n_winners(populate_initial_defaults)
      |> r_method_addendum(metadata |> Map.put("active_method", active_method))
      |> Enum.reverse()

    build_view(
      :modal,
      gettext("New decision settings"),
      blocks,
      close: text_object("← #{gettext("Back")}"),
      submit: text_object("#{gettext("Publish")}"),
      callback_id: @view_id,
      notify_on_close: true,
      private_metadata: Jason.encode!(metadata)
    )
  end

  defp parse_expiry("c_never"), do: nil

  defp parse_expiry("c_" <> expiry_string) do
    {duration, _} = Integer.parse(expiry_string)

    NaiveDateTime.utc_now()
    |> NaiveDateTime.truncate(:second)
    |> NaiveDateTime.add(duration, :second)
  end

  defp parse_quorum("q_" <> quorum), do: quorum

  defp parse_method("m_" <> method), do: method

  def parse_number(nil, _), do: {:ok, nil}

  def parse_number("" <> string, block_id) do
    case Float.parse(string) do
      {number, _} -> {:ok, number}
      _ -> {:error, %{block_id => gettext("This needs to be a number.")}}
    end
  end

  @impl true
  def parse_response(%{
        "team" => %{"id" => team_id},
        "user" => %{"id" => user_id},
        "view" => %{
          "root_view_id" => root_view_id,
          "state" => %{
            "values" =>
              %{
                "expiry" => %{
                  "expiry_input" => %{"selected_option" => %{"value" => expiry_value}}
                },
                "quorum" => %{
                  "quorum_input" => %{"selected_option" => %{"value" => quorum_value}}
                },
                "method" => %{
                  "method_input" => %{"selected_option" => %{"value" => method_value}}
                },
                "people_votable" => %{
                  "people_votable_input" => %{"selected_conversation" => channel_id}
                },
                "n_winners" => %{"n_winners_input" => %{"value" => n_winners_value}}
              } = values
          },
          "private_metadata" => metadata_str
        }
      }) do
    with {:ok, metadata} <-
           (case metadata_str do
              "" -> {:ok, %{}}
              _ -> Jason.decode(metadata_str)
            end),
         {:ok, n_winners} <- parse_number(n_winners_value, "n_winners"),
         {:ok, parameter0_value} <-
           parse_number(
             get_in(values, ["parameter0_value", "parameter0_value_input", "value"]),
             "parameter0_value"
           ),
         {:ok, parameter1_value} <-
           parse_number(
             get_in(values, ["parameter1_value", "parameter1_value_input", "value"]),
             "parameter1_value"
           ),
         {:ok, parameter3_value} <-
           parse_number(
             case get_in(values, [
                    "parameter3_value",
                    "parameter3_value_input",
                    "selected_option",
                    "value"
                  ]) do
               "parameter3_value_" <> unparsed_p3v -> unparsed_p3v
               _ -> nil
             end,
             "parameter3_value"
           ),
         {:ok, parameter4_value} <-
           parse_number(
             get_in(values, ["parameter4_value", "parameter4_value_input", "value"]),
             "parameter4_value"
           ),
         method <- parse_method(method_value) do
      {:ok,
       %{
         service_id:
           case Map.get(metadata, "service_id") do
             "" <> service_id -> service_id
             nil -> DecisionVersion.service_id({:slack, team_id, root_view_id})
           end,
         team_id: team_id,
         channel_id: channel_id,
         user_id: user_id,
         expiry: parse_expiry(expiry_value),
         method: method,
         quorum: parse_quorum(quorum_value),
         n_winners: n_winners,
         parameter0_value:
           case method do
             "plurality" -> parameter0_value / 100
             _ -> parameter0_value
           end,
         parameter1_value: parameter1_value,
         parameter3_value: parameter3_value,
         parameter4_value: parameter4_value,
         value_titles:
           values
           |> Enum.reduce(%{}, fn
             {"score_title_" <> score_string, state}, acc ->
               case get_in(state, ["score_title_input_#{score_string}", "value"]) do
                 nil ->
                   acc

                 score_title ->
                   Map.put(
                     acc,
                     score_string,
                     score_title
                   )
               end

             {_, _}, acc ->
               acc
           end)
       }, nil}
    end
  end

  @impl true
  def parse_response(_), do: {:error, :invalid_response}

  defp fail(err) do
    IO.puts("[New decision step 2]")
    IO.inspect(err)
    {:error, err}
  end

  defp error_not_in_convo(locale) do
    put_locale(locale)
    {:error, %{"people_votable" => gettext("bot can't post")}}
  end

  defp resolve_method("approval", %{parameter0_value: parameter0_value}),
    do: DSTemplates.approval(parameter0_value: parameter0_value)

  defp resolve_method("plurality", %{parameter0_value: parameter0_value}),
    do:
      DSTemplates.plurality(
        parameter0_value: parameter0_value,
        parameter0_quantification: DSTypes.proportional(:str)
      )

  defp resolve_method("consensus", %{parameter4_value: parameter4_value}),
    do: DSTemplates.consensus(parameter4_value: parameter4_value)

  defp resolve_method("stv", _),
    do: DSTemplates.stv()

  defp resolve_method("score", %{
         parameter1_value: parameter1_value,
         parameter3_value: parameter3_value,
         parameter4_value: parameter4_value,
         value_titles: value_titles
       }),
       do:
         DSTemplates.score(
           # The parameter3 value is intentionally given to parameter0, parameter2 and parameter3
           [
             parameter0_value: parameter3_value,
             parameter1_value: parameter1_value,
             parameter2_value: parameter3_value,
             parameter3_value: parameter3_value,
             parameter4_value: parameter4_value
           ]
           |> Keyword.merge(if parameter1_value <= 16, do: [value_titles: value_titles], else: [])
         )

  defp resolve_method("star", %{
         parameter1_value: parameter1_value,
         parameter3_value: parameter3_value,
         parameter4_value: parameter4_value,
         value_titles: value_titles
       }),
       # This method is the same as for score, but uses the star template.
       do:
         DSTemplates.star(
           [
             parameter0_value: parameter3_value,
             parameter1_value: parameter1_value,
             parameter2_value: parameter3_value,
             parameter3_value: parameter3_value,
             parameter4_value: parameter4_value
           ]
           |> Keyword.merge(if parameter1_value <= 16, do: [value_titles: value_titles], else: [])
         )

  defp resolve_quorum("any"),
    do: %{quorum_quantification: DSTypes.absolute(:str), quorum_value: 1.0}

  defp resolve_quorum("half"),
    do: %{quorum_quantification: DSTypes.proportional(:str), quorum_value: 1 / 2}

  defp resolve_quorum("twothirds"),
    do: %{quorum_quantification: DSTypes.proportional(:str), quorum_value: 2 / 3}

  defp resolve_quorum("all"),
    do: %{quorum_quantification: DSTypes.proportional(:str), quorum_value: 1.0}

  @impl true
  def commit(
        %{
          service_id: service_id,
          team_id: team_id,
          channel_id: channel_id,
          user_id: user_id,
          expiry: expiry,
          method: method,
          quorum: quorum,
          n_winners: n_winners
        } = inputs,
        _
      ) do
    with {:ok, %{channel: %{locale: conversation_locale} = people_votable} = context} <-
           Context.resolve({:slack, team_id, channel_id, user_id}),
         step_1_version <-
           DecisionVersion.get_by_service_id(service_id) |> Repo.preload(:decision),
         {:ok, %{ts: ts}} <-
           SlackIO.post_message(
             context,
             channel_id,
             DecisionMessage.render_placeholder(conversation_locale)
           ) do
      # If that worked out, get started saving things and rendering the message to post;
      Task.start_link(fn ->
        {:ok, %{version: step_2_version, decision_version_moment: dv_moment}} =
          (DecisionVersionMoment.create(
             step_1_version,
             expiry,
             DecisionVersionMoment.Types.close(:str)
           ) ++
             DecisionRole.add_to_decision(
               step_1_version.decision,
               people_votable,
               DecisionRole.Types.votable(:str)
             ) ++
             [
               {:version, :update,
                {step_1_version
                 |> DecisionVersion.changeset(%{
                   service_id: DecisionVersion.service_id({:slack, team_id, channel_id, ts}),
                   state: DecisionVersion.Types.published(:str)
                 })
                 |> DecisionVersion.change_settings(
                   # Take method template (with method-specific inputs as needed)…
                   resolve_method(method, inputs)
                   # …merge with quorum template…
                   |> Map.merge(resolve_quorum(quorum))
                   # …merge with inputs that all decisions use.
                   |> Map.merge(%{n_winners_value: n_winners})
                 ), []}}
             ])
          |> Repo.commit()

        Schedule.on_moment_created(dv_moment)

        {:ok, %{ts: ts_2}} =
          SlackIO.update_message(
            context,
            {channel_id, ts},
            DecisionMessage.render(
              conversation_locale,
              step_2_version |> Repo.preload([:author, :decision]),
              nil
            )
          )

        # [wsn] As far as I know, ts doesn’t update when the message is updated, but just in case…
        if ts !== ts_2,
          do:
            step_2_version
            |> DecisionVersion.changeset(%{
              service_id: DecisionVersion.service_id({:slack, team_id, channel_id, ts_2})
            })
            |> Repo.update()
      end)

      :ok
    else
      {:rescue, %{person: %{locale: sender_locale}}} -> error_not_in_convo(sender_locale)
      err -> fail(err)
    end
  end
end
