defmodule AccordSlack.Views.DecisionMessage do
  import AccordWeb.Gettext
  alias AccordWeb.Cldr

  alias Accord.{DecisionVersion, DecisionSettings}
  alias DecisionSettings.Types, as: DSTypes

  alias AccordSlack.View

  use BlockBox
  use View.Components

  @behaviour View
  @behaviour View.Message

  defp r_options(blocks, options, state),
    do:
      Enum.reduce(options, %{blocks: blocks, c: 1, state: state}, &r_option/2) |> Map.get(:blocks)

  defp r_option(option, %{blocks: blocks, c: c, state: state} = acc) do
    %{
      acc
      | blocks: [
          rr_option(option, state) | blocks
        ],
        c: c + 1
    }
  end

  defp rr_option(option, state) do
    state_closed = DecisionVersion.Types.closed(:str)

    prefix =
      case state do
        # mind the em-spaces here:
        ^state_closed -> "• "
        _ -> "➤ "
      end

    section(text_object(prefix <> option.title))
  end

  defp r_cta(blocks, state) do
    state_closed = DecisionVersion.Types.closed(:str)

    case state do
      ^state_closed ->
        blocks

      _ ->
        [
          # mind the em-spaces here:
          actions_block([button("  #{gettext("Vote")}  ", "vote", style: "primary")])
          | blocks
        ]
    end
  end

  defp r_expiry(nil, _, _), do: text_object(gettext("Voting will remain open indefinitely."))

  defp r_expiry(%NaiveDateTime{} = expiry, author, state) do
    state_closed = DecisionVersion.Types.closed(:str)

    # Unfortunately Slack doesn’t offer a time zone for channels, so we’ll just use the author’s.
    with tz <- author |> get_in([:locale, :tz]),
         {:ok, lt} <- DateTime.from_naive!(expiry, "Etc/UTC") |> DateTime.shift_zone(tz),
         {:ok, expiry_string} <-
           Cldr.DateTime.to_string(lt, format: :long, locale: get_l10n()) do
      case state do
        ^state_closed ->
          text_object(gettext("Voting is closed as of %{moment}.", moment: expiry_string))

        _ ->
          text_object(gettext("Voting will remain open until %{moment}.", moment: expiry_string))
      end
    else
      _ -> text_object(gettext("Voting will remain open for now."))
    end
  end

  @method_cardinal DSTypes.cardinal(:str)
  @method_plurality DSTypes.plurality(:str)
  @method_stv DSTypes.ordinal_stv(:str)

  @quant_abs DSTypes.absolute(:str)
  @quant_prop DSTypes.proportional(:str)

  @cardinal_resolution_star DSTypes.star(:str)

  defp r_timing(blocks, author, dv, _settings, state) do
    [
      context_block([
        DecisionVersion.get_expiry(dv) |> r_expiry(author, state)
      ])
      | blocks
    ]
  end

  defp r_participation(blocks, nil) do
    [context_block([text_object(gettext("No votes yet."))]) | blocks]
  end

  defp r_participation(blocks, %{"n_voters" => nil}) do
    [
      context_block([
        text_object(gettext("(Loading total eligible to vote…)"))
      ])
      | blocks
    ]
  end

  defp r_participation(blocks, %{"n_votes" => n_votes, "n_voters" => n_voters}) do
    [
      context_block([
        text_object(
          gettext(
            "%{n_votes} of a possible %{n_voters} have voted directly.",
            n_votes: s_intish(n_votes),
            n_voters: s_intish(n_voters)
          )
        )
      ])
      | blocks
    ]
  end

  defp s_option_winners(%DecisionSettings{n_winners_value: nwv}),
    do: ngettext("option", "%{n_options} options", floor(nwv), n_options: floor(nwv))

  defp s_min_plurality(%DecisionSettings{
         parameter0_quantification: @quant_abs,
         parameter0_value: p0v
       }),
       do: s_abs_participation(p0v) <> " " <> gettext("in favor")

  defp s_min_plurality(%DecisionSettings{
         parameter0_quantification: @quant_prop,
         parameter0_value: p0v
       }),
       do:
         gettext("%{n_min_plurality}% of all votes in favor",
           n_min_plurality: Float.round(p0v * 100, 2) |> s_intish()
         )

  defp s_abs_participation(qv),
    do:
      ngettext(
        "%{n_min} vote",
        "%{n_min} votes (including delegations)",
        floor(qv),
        n_min: s_intish(qv)
      )

  defp rr_method(%DecisionSettings{method: @method_plurality} = settings),
    do:
      text_object(
        gettext(
          "The %{option_winners} with the most votes in favor, with at least %{min_plurality}, will win.",
          option_winners: s_option_winners(settings),
          min_plurality: s_min_plurality(settings)
        )
      )

  defp rr_method(
         %DecisionSettings{method: @method_cardinal, resolution: @cardinal_resolution_star} =
           settings
       ),
       do:
         text_object(
           gettext("The %{option_winners} with the most support, as evaluated by STAR, will win.",
             option_winners: s_option_winners(settings)
           )
         )

  defp rr_method(%DecisionSettings{method: @method_cardinal} = settings),
    do:
      text_object(
        gettext("The %{option_winners} with the most support will win.",
          option_winners: s_option_winners(settings)
        )
      )

  defp rr_method(%DecisionSettings{method: @method_stv} = settings),
    do:
      text_object(
        gettext("The %{option_winners} with the most support will win.",
          option_winners: s_option_winners(settings)
        )
      )

  defp rr_quorum(%DecisionSettings{quorum_quantification: @quant_abs, quorum_value: qv}),
    do:
      text_object(
        gettext("At least %{n_participation} is needed for any options to win.",
          n_participation: s_abs_participation(qv)
        )
      )

  defp rr_quorum(%DecisionSettings{quorum_quantification: @quant_prop, quorum_value: qv}),
    do:
      text_object(
        gettext(
          "At least %{n_participation}% participation (direct or delegated) is needed for any options to win.",
          n_participation: Float.round(qv * 100, 2) |> s_intish()
        )
      )

  defp r_settings(blocks, settings) do
    [
      context_block([
        rr_method(settings),
        rr_quorum(settings)
      ])
      | blocks
    ]
  end

  @impl true
  def render(
        locale,
        %DecisionVersion{
          title: title,
          body: body,
          options: options
        },
        :preview
      ) do
    put_locale(locale)

    blocks =
      []
      |> r_title(title)
      |> r_body(body)
      |> r_options(options, DecisionVersion.Types.new(:str))
      |> Enum.reverse()

    %{
      text: title,
      blocks: blocks
    }
  end

  @impl true
  def render(
        locale,
        %DecisionVersion{
          title: title,
          body: body,
          author: author,
          options: options,
          settings: settings,
          state: state
        } = dv,
        metadata
      ) do
    put_locale(locale)

    blocks =
      []
      |> r_div()
      |> r_title(title)
      |> r_body(body)
      |> r_options(options, state)
      |> r_cta(state)
      |> r_div()
      |> r_timing(author, dv, settings, state)
      |> r_settings(settings)
      |> r_participation(metadata)
      |> Enum.reverse()

    %{
      text: title,
      blocks: blocks
    }
  end

  @impl true
  def render_placeholder(locale) do
    put_locale(locale)

    %{text: gettext("Setting up a new decision…")}
  end
end
