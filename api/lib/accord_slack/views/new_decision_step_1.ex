defmodule AccordSlack.Views.NewDecisionStep1 do
  alias Accord.{Repo, Decision, DecisionVersion, Option}
  alias AccordSlack.Context

  import AccordWeb.Gettext
  use BlockBox

  @behaviour AccordSlack.View
  @behaviour AccordSlack.View.Modal

  @view_id "new_decision_step_1"

  @impl true
  def id, do: @view_id

  defp render_option(ordinal) do
    input(
      gettext("Option %{ordinal}", ordinal: ordinal),
      plain_text_input("decision_option_#{ordinal}_option"),
      block_id: "decision_option_#{ordinal}",
      optional: ordinal > 1
    )
  end

  defp render_option(ordinal, nil), do: render_option(ordinal)

  defp render_option(ordinal, %Option{title: title}) do
    input(
      gettext("Option %{ordinal}", ordinal: ordinal),
      plain_text_input("decision_option_#{ordinal}_option", initial_value: title),
      block_id: "decision_option_#{ordinal}",
      optional: ordinal > 1
    )
  end

  defp render_options(nil, n_options) do
    1..n_options
    |> Enum.reduce([], fn n, acc ->
      [render_option(n) | acc]
    end)
    |> Enum.reverse()
  end

  defp render_options(%DecisionVersion{options: options}, n_options) do
    1..n_options
    |> Enum.reduce([], fn n, acc ->
      [render_option(n, Enum.at(options, n - 1)) | acc]
    end)
    |> Enum.reverse()
  end

  # todo: DRY up the next 4 functions:

  defp r_decision_title(%DecisionVersion{title: "" <> title}) do
    input(
      gettext("Title/question"),
      plain_text_input("decision_title_input", initial_value: title),
      block_id: "decision_title"
    )
  end

  defp r_decision_title(_) do
    input(
      gettext("Title/question"),
      plain_text_input("decision_title_input"),
      block_id: "decision_title"
    )
  end

  defp r_decision_body(%DecisionVersion{body: "" <> body}) do
    input(
      gettext("Description"),
      plain_text_input("decision_body_input", multiline: true, initial_value: body),
      optional: true,
      block_id: "decision_body",
      hint: gettext("Plain text or Markdown")
    )
  end

  defp r_decision_body(_) do
    input(
      gettext("Description"),
      plain_text_input("decision_body_input", multiline: true),
      optional: true,
      block_id: "decision_body",
      hint: gettext("Plain text or Markdown")
    )
  end

  @impl true
  def render(locale, entity, %{"n_options" => n_options} = metadata) do
    put_locale(locale)

    top = [
      r_decision_title(entity),
      r_decision_body(entity)
    ]

    middle = render_options(entity, n_options)

    bottom = [
      actions_block(
        [
          button(gettext("Add option"), "add_option")
        ],
        block_id: "options_actions"
      )
    ]

    build_view(
      :modal,
      gettext("Decide on something new"),
      top ++ middle ++ bottom,
      close: text_object(gettext("Cancel")),
      submit: text_object("#{gettext("Next")} →"),
      callback_id: @view_id,
      notify_on_close: true,
      private_metadata: Jason.encode!(metadata)
    )
  end

  @impl true
  def render(locale, nil, %{} = metadata),
    do: render(locale, nil, metadata |> Map.put("n_options", 2))

  @impl true
  def render(
        locale,
        %DecisionVersion{options: options, service_id: service_id} = decision_version,
        %{} = metadata
      ),
      do:
        render(
          locale,
          decision_version,
          metadata
          |> Map.put("n_options", length(options) + 1)
          |> Map.put("service_id", service_id)
        )

  @impl true
  def parse_response(
        %{
          "team" => %{"id" => team_id},
          "view" => %{
            "id" => view_id,
            "state" => %{
              "values" =>
                %{
                  "decision_title" => %{"decision_title_input" => %{"value" => title}}
                } = values
            },
            "private_metadata" => metadata_str
          }
        } = params
      ) do
    with {:ok, metadata} <-
           (case metadata_str do
              "" -> {:ok, %{}}
              _ -> Jason.decode(metadata_str)
            end),
         {_, %{person: %{locale: locale} = author}} <- Context.resolve(params) do
      # Set title and service ID
      response = %{
        title: title,
        author: author,
        service_id:
          case Map.get(metadata, "service_id") do
            "" <> service_id -> service_id
            nil -> DecisionVersion.service_id({:slack, team_id, view_id})
          end
      }

      # Include locale so the Slack controller has it
      result = response |> Map.put(:locale, locale)

      # Set optional body
      result =
        case get_in(values, ["decision_body", "decision_body_input"]) do
          %{"value" => body} -> Map.put(result, :body, body)
          # Erase the body if it is truly blank.
          _ -> Map.put(result, :body, "")
        end

      # Set options
      result =
        result
        |> Map.put(
          :options,
          Enum.reduce(values, [], fn {k, v}, acc ->
            case get_in(v, ["#{k}_option", "value"]) do
              "" <> title when byte_size(title) > 0 -> [%{title: title} | acc]
              _ -> acc
            end
          end)
          |> Enum.reverse()
        )

      {:ok, result,
       metadata
       |> Map.drop(["n_options"])
       |> Map.put("service_id", response.service_id)}
    end
  end

  @impl true
  def parse_response(_), do: {:error, :invalid_response}

  @impl true
  def commit(response, _) do
    with {:ok, %{decision_version: _}} <-
           response
           |> Map.drop([:locale])
           |> Decision.create_with_version()
           |> Repo.commit() do
      :ok
    end
  end
end
