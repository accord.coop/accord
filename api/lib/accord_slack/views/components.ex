defmodule AccordSlack.View.Components do
  use BlockBox

  alias AccordWeb.Cldr

  defmacro __using__(_) do
    quote do
      defp r_div(blocks), do: [divider() | blocks]

      defp r_title(blocks, title),
        do: [
          section(text_object("*#{title}*", :mrkdwn))
          | blocks
        ]

      defp r_body(blocks, nil), do: blocks
      defp r_body(blocks, ""), do: blocks

      defp r_body(blocks, body),
        do: [
          section(text_object(body, :mrkdwn))
          | blocks
        ]

      defp s_intish(n) do
        with {:ok, intish} <- Cldr.Number.to_string(n, format: :standard) do
          intish
        else
          _ -> n
        end
      end

      defp s_percent(n, d, p \\ 3)
      defp s_percent(_, d, p) when d == 0, do: s_percent(0, 1, p)

      defp s_percent(n, d, p) do
        value = Float.round(n / d, p)

        with {:ok, percent} <- Cldr.Number.to_string(value, format: :percent) do
          percent
        else
          _ -> value * 100
        end
      end
    end
  end
end
