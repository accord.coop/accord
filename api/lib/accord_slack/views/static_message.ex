defmodule AccordSlack.Views.StaticMessage do
  import AccordWeb.Gettext

  alias AccordSlack.View

  @behaviour View
  @behaviour View.Message

  @impl true
  def render(
        locale,
        message,
        _
      ) do
    put_locale(locale)

    case message do
      :home_opened -> %{text: gettext("home opened")}
      :dm -> %{text: gettext("direct message")}
      :decide_help -> %{text: gettext("help response")}
      :delegate_help -> %{text: gettext("delegations coaching")}
      _ -> %{text: gettext("help response")}
    end
  end
end
