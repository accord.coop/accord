defmodule AccordSlack.Views.Home do
  alias Accord.{DecisionVersion}

  import AccordWeb.Gettext

  use BlockBox
  use AccordSlack.View.Components

  @behaviour AccordSlack.View
  @behaviour AccordSlack.View.Message

  @view_id "home"

  def id, do: @view_id

  defp s_truncated_body(body) when byte_size(body) > 140, do: String.slice(body, 0..140) <> "…"
  defp s_truncated_body(body), do: body

  defp rr_decision_version_card(%DecisionVersion{title: title, body: body}),
    do: text_object("_#{title}_\n\n#{s_truncated_body(body)}", :mrkdwn)

  defp rr_decision_version_votable(
         person_uuid,
         %DecisionVersion{uuid: uuid, author_uuid: author_uuid} = decision_version,
         copy,
         button_opts
       ) do
    section(
      rr_decision_version_card(decision_version),
      accessory:
        case person_uuid === author_uuid do
          false ->
            button("  #{copy}  ", "vote_#{uuid}", button_opts)

          true ->
            overflow_menu(
              "vote_#{uuid}",
              [
                option_object(gettext("Vote"), "vote"),
                option_object(gettext("Withdraw"), "withdraw")
              ]
            )
        end
    )
  end

  defp rr_decision_version_unvoted(person_uuid, decision_version),
    do:
      rr_decision_version_votable(person_uuid, decision_version, gettext("Vote"), style: "primary")

  defp rr_decision_version_voted(person_uuid, decision_version),
    do: rr_decision_version_votable(person_uuid, decision_version, gettext("View/edit vote"), [])

  defp rr_decision_version_draft(_person_uuid, %DecisionVersion{uuid: uuid} = decision_version) do
    section(
      rr_decision_version_card(decision_version),
      accessory:
        overflow_menu(
          "edit_#{uuid}",
          [
            option_object(gettext("Edit"), "edit"),
            option_object(gettext("Delete"), "delete")
          ]
        )
    )
  end

  defp r_decision_versions(blocks, _, [], _) do
    [gettext("None.") |> text_object |> section | blocks]
  end

  defp r_decision_versions(blocks, person_uuid, decision_versions, func) do
    Enum.map(decision_versions, fn decision_version -> func.(person_uuid, decision_version) end) ++
      blocks
  end

  @impl true
  def render(
        locale,
        %{drafts: drafts, votable: %{voted: voted, unvoted: unvoted}, person_uuid: person_uuid},
        _
      ) do
    put_locale(locale)

    blocks =
      []
      |> r_body(gettext("home intro"))
      |> r_title(gettext("𝗗𝗲𝗰𝗶𝘀𝗶𝗼𝗻𝘀 𝗻𝗲𝗲𝗱𝗶𝗻𝗴 𝘆𝗼𝘂𝗿 𝘃𝗼𝘁𝗲:"))
      |> r_decision_versions(person_uuid, unvoted, &rr_decision_version_unvoted/2)
      |> r_div()
      |> r_title(gettext("𝗗𝗲𝗰𝗶𝘀𝗶𝗼𝗻𝘀 𝘆𝗼𝘂’𝘃𝗲 𝘃𝗼𝘁𝗲𝗱 𝗼𝗻 𝘁𝗵𝗮𝘁 𝗮𝗿𝗲 𝘀𝘁𝗶𝗹𝗹 𝗼𝗽𝗲𝗻:"))
      |> r_decision_versions(person_uuid, voted, &rr_decision_version_voted/2)
      |> r_div()
      |> r_title(gettext("𝗬𝗼𝘂𝗿 𝗱𝗿𝗮𝗳𝘁 𝗱𝗲𝗰𝗶𝘀𝗶𝗼𝗻𝘀:"))
      |> r_decision_versions(person_uuid, drafts, &rr_decision_version_draft/2)
      |> r_div()
      |> Enum.reverse()

    build_view(
      :home,
      gettext("Accord: decide together"),
      blocks,
      callback_id: @view_id
    )
  end
end
