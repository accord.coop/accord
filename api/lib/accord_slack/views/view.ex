defmodule AccordSlack.View do
  alias Accord.Locale

  @doc """
    `render` should always take a Locale as the first argument and return a map which can be
    encoded as JSON to be added to a `view` key in a payload to Slack. `render` can also accept
    an entity of any kind and the decoded `private_metadata` for Slack views.
  """
  @callback render(Locale.t(), any, map | nil) :: map
end

defmodule AccordSlack.View.Modal do
  @doc "The ID to use with Slack's `view_id`."
  @callback id() :: String.t()

  @doc """
    Parses the payload received from Slack and returns a map of values to be used internally
    (called a "response") and a map of values to be passed on to subsequent views in `private_metadata`.
  """
  @callback parse_response(map) :: {:ok, map, map} | {:error, atom}

  @doc """
    Saves information to the database using the parsed response and metadata.
  """
  @callback commit(map, map) :: :ok | any
end

defmodule AccordSlack.View.Message do
  alias Accord.Locale

  @optional_callbacks render_placeholder: 1

  @doc "Renders a placeholder message to send while the full message is being prepared."
  @callback render_placeholder(Locale.t()) :: map
end
