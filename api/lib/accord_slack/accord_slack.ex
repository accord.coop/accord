defmodule AccordSlack do
  @moduledoc """
    This module is non-operative since implementing liquid democracy. Most of its logic was
    refactored to the `AccordSlack.Router` module and the other modules defined in adjacent files.
  """
end
