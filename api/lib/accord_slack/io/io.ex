defmodule AccordSlack.PayloadLogger do
  @behaviour Tesla.Middleware

  defp log_payload(%Tesla.Env{body: payload} = env) do
    IO.puts("[payload]")
    IO.inspect(payload)
    env
  end

  defp log_payload(env), do: env

  def call(env, next, _options) do
    env
    |> log_payload()
    |> Tesla.run(next)
  end
end

defmodule AccordSlack.FormClient do
  use Tesla

  #  plug AccordSlack.PayloadLogger
  plug Tesla.Middleware.FormUrlencoded
  plug Tesla.Middleware.DecodeJson
end

defmodule AccordSlack.JSONClient do
  use Tesla

  #  plug AccordSlack.PayloadLogger
  plug Tesla.Middleware.JSON
end

defmodule AccordSlack.OAuthClient do
  use Tesla

  plug Tesla.Middleware.BasicAuth,
    username: Application.get_env(:accord, AccordSlack)[:client_id],
    password: Application.get_env(:accord, AccordSlack)[:client_secret]

  plug Tesla.Middleware.FormUrlencoded
  plug Tesla.Middleware.DecodeJson
end

defmodule AccordSlack.Responder do
  require Logger

  defp fail(conn) do
    Logger.error("〘AccordSlack.Responder〙Couldn’t encode JSON")
    Plug.Conn.send_resp(conn, 500, "")
  end

  def respond(conn, %{team: %{app_token: app_token}}) do
    conn
    |> Plug.Conn.put_resp_header("authorization", "Bearer #{app_token}")
    |> Plug.Conn.put_resp_header(
      "authorization",
      "Bearer #{Application.get_env(:accord, AccordSlack)[:bot_token]}"
    )
    |> Plug.Conn.send_resp(:ok, "")
  end

  def respond(conn, %{team: %{app_token: app_token}}, payload) do
    #    IO.puts("[Respond with payload]")
    #    IO.inspect(payload)

    with {:ok, payload_string} <- Jason.encode(payload) do
      conn
      |> Plug.Conn.put_resp_header("content-type", "application/json")
      |> Plug.Conn.put_resp_header("authorization", "Bearer #{app_token}")
      |> Plug.Conn.put_resp_header(
        "authorization",
        "Bearer #{Application.get_env(:accord, AccordSlack)[:bot_token]}"
      )
      |> Plug.Conn.send_resp(:ok, payload_string)
    else
      _ -> fail(conn)
    end
  end
end

defmodule AccordSlack.IO do
  alias Accord.{Errors}
  alias AccordSlack.{FormClient, JSONClient, OAuthClient, Responder, Context}

  alias AccordSlack.Views.DraftSavedMessage

  require Logger

  @service_url Application.get_env(:accord, AccordSlack)[:url]
  @page_size 256

  defp put_token(app_token), do: [headers: [{"Authorization", "Bearer #{app_token}"}]]

  defp url(cmd), do: @service_url <> cmd

  defp fail(err, payload \\ nil)

  defp fail({:ok, %Tesla.Env{body: %{"error" => slack_error} = body}}, payload) do
    IO.inspect(body)
    IO.inspect(payload)

    Logger.error("〘IO〙 API error",
      event: %{
        slack_error: slack_error,
        messages: get_in(body, ["response_metadata", "messages"])
      }
    )

    {:error, slack_error}
  end

  defp fail(err, payload) do
    Logger.error("〘IO〙 Unanticipated error")
    IO.puts("[payload]")
    IO.inspect(payload)
    IO.puts("[error]")
    IO.inspect(err)
    {:error, err}
  end

  @spec start_modal(Context.t(), String.t(), Map.t()) :: {:ok, String.t()} | {:error, any}
  def start_modal(%{team: %{app_token: app_token}}, trigger_id, view) do
    payload = %{
      trigger_id: trigger_id,
      view: view
    }

    case JSONClient.post(
           url("views.open"),
           payload,
           put_token(app_token)
         ) do
      {:ok, %Tesla.Env{body: %{"view" => %{"id" => view_id}}}} ->
        {:ok, view_id}

      err ->
        fail(err, payload)
    end
  end

  @spec update_modal(Context.t(), String.t(), Map.t()) :: {:ok, String.t()} | {:error, any}
  def update_modal(%{team: %{app_token: app_token}}, view_id, view) do
    case JSONClient.post(
           url("views.update"),
           %{
             view_id: view_id,
             view: view
           },
           put_token(app_token)
         ) do
      {:ok, %Tesla.Env{body: %{"view" => %{"id" => view_id}}}} ->
        {:ok, view_id}

      err ->
        fail(err)
    end
  end

  @spec push_modal(Context.t(), String.t(), Map.t()) :: {:ok, String.t()} | {:error, any}
  def push_modal(%{team: %{app_token: app_token}}, trigger_id, view) do
    case JSONClient.post(
           url("views.push"),
           %{
             trigger_id: trigger_id,
             view: view
           },
           put_token(app_token)
         ) do
      {:ok, %Tesla.Env{body: %{"view" => %{"id" => view_id}}}} ->
        {:ok, view_id}

      err ->
        fail(err)
    end
  end

  @spec render_home(Context.t(), String.t(), Map.t()) :: {:ok, String.t()} | {:error, any}
  def render_home(%{team: %{app_token: app_token}}, user_id, view) do
    case JSONClient.post(
           url("views.publish"),
           %{
             user_id: user_id,
             view: view
           },
           put_token(app_token)
         ) do
      {:ok, %Tesla.Env{body: %{"view" => %{"id" => view_id}}}} ->
        {:ok, view_id}

      err ->
        fail(err)
    end
  end

  @spec reply(Context.t(), String.t(), String.t(), Map.t()) :: :ok | {:error, any}
  defp reply(%{team: %{app_token: app_token}}, url, response_type, message) do
    case JSONClient.post(
           url,
           message |> Map.put(:response_type, response_type),
           put_token(app_token)
         ) do
      {:ok, _} -> :ok
      err -> fail(err)
    end
  end

  @spec reply_in_channel(Context.t(), String.t(), Map.t()) :: :ok | {:error, any}
  def reply_in_channel(context, url, message), do: reply(context, url, "in_channel", message)

  @spec reply_ephemerally(Context.t(), String.t(), Map.t()) :: :ok | {:error, any}
  def reply_ephemerally(context, url, message), do: reply(context, url, "ephemeral", message)

  @spec respond(Plug.Conn.t(), Context.t()) :: Plug.Conn.t() | Plug.Conn.no_return()
  def respond(conn, context), do: Responder.respond(conn, context)

  @spec respond(Plug.Conn.t(), Context.t(), Map.t()) :: Plug.Conn.t() | Plug.Conn.no_return()
  def respond(conn, context, payload), do: Responder.respond(conn, context, payload)

  @spec get_user_profile(Context.t(), String.t()) :: {:ok, Map.t()} | {:error, any}
  def get_user_profile(%{team: %{app_token: app_token}}, user_id) do
    case FormClient.post(url("users.info"), %{user: user_id}, put_token(app_token)) do
      {:ok,
       %Tesla.Env{
         body: %{
           "user" => %{
             "profile" => profile
           }
         }
       }} ->
        {:ok, profile}

      err ->
        fail(err)
    end
  end

  @spec send_draft_saved_message(Context.t()) :: {:ok, any} | {:error, any}
  def send_draft_saved_message(
        %{team: %{app_token: app_token}, person: %{locale: locale}} = context
      ) do
    with %{
           persona: %{service_properties: %{"dm_id" => conversation_id}},
           team: %{bot_token: bot_user_id}
         } <- context do
      JSONClient.post(
        url("chat.postMessage"),
        DraftSavedMessage.render(locale, %{bot_user_id: bot_user_id}, nil)
        |> Map.put(:channel, conversation_id),
        put_token(app_token)
      )
    else
      _ -> fail(Errors.no_slack_dm())
    end
  end

  @spec join_conversation(Context.t(), String.t()) ::
          {:ok, :already_in_conversation | :success} | {:error, String.t()}
  def join_conversation(%{team: %{app_token: app_token}}, conversation_id) do
    case FormClient.post(
           url("conversations.join"),
           %{channel: conversation_id},
           put_token(app_token)
         ) do
      {:ok, %Tesla.Env{body: %{"ok" => true, "warning" => "already_in_channel"}}} ->
        {:ok, :already_in_conversation}

      {:ok, %Tesla.Env{body: %{"ok" => true}}} ->
        {:ok, :success}

      {:ok, %Tesla.Env{body: %{"ok" => false, "error" => slack_error}}} ->
        {:error, {:join_conversation, slack_error}}

      err ->
        fail(err)
    end
  end

  @spec post_message(Context.t(), String.t(), Map.t(), Keyword.t()) ::
          {:ok, %{channel_id: String.t(), ts: String.t()}} | {:error, any}
  def post_message(
        %{team: %{app_token: app_token}} = context,
        channel_id,
        %{} = payload,
        opts \\ []
      ) do
    # try joining just in case this helps, ignore the result
    join_conversation(context, channel_id)
    # continue posting
    with {:ok, %Tesla.Env{body: %{"ok" => true, "ts" => ts}}} <-
           JSONClient.post(
             url("chat.postMessage"),
             opts |> Enum.into(payload |> Map.put(:channel, channel_id)),
             put_token(app_token)
           ) do
      {:ok, %{channel_id: channel_id, ts: ts}}
    else
      err -> fail(err)
    end
  end

  @spec delete_message(Context.t(), {String.t(), String.t()}) ::
          {:ok, %{channel_id: String.t(), ts: String.t()}} | {:error, any}
  def delete_message(
        %{team: %{app_token: app_token}},
        {channel_id, ts}
      ) do
    with {:ok, %Tesla.Env{body: %{"ok" => true}}} <-
           FormClient.post(
             url("chat.delete"),
             %{
               channel: channel_id,
               ts: ts
             },
             put_token(app_token)
           ) do
      {:ok, %{channel_id: channel_id, ts: ts}}
    else
      err -> fail(err)
    end
  end

  @spec update_message(Context.t(), {String.t(), String.t()}, Map.t()) ::
          {:ok, %{channel_id: String.t(), ts: String.t()}} | {:error, any}
  def update_message(%{team: %{app_token: app_token}}, {channel_id, ts}, %{} = payload) do
    case JSONClient.post(
           url("chat.update"),
           payload
           |> Map.put(:channel, channel_id)
           |> Map.put(:ts, ts),
           put_token(app_token)
         ) do
      {:ok, %Tesla.Env{body: %{"ok" => true, "channel" => channel_id, "ts" => ts}}} ->
        {:ok, %{channel_id: channel_id, ts: ts}}

      err ->
        fail(err)
    end
  end

  # `is_bot` and `get_channel_users` accept `app_token` directly since they're only used
  # by `assess_decision_version`.

  @spec is_bot(String.t(), String.t()) :: {:ok, boolean} | {:error, any}
  def is_bot(app_token, user_id) do
    case FormClient.post(url("users.info"), %{user: user_id}, put_token(app_token)) do
      {:ok,
       %Tesla.Env{
         body: %{"user" => %{"is_bot" => is_bot}}
       }} ->
        {:ok, is_bot}

      err ->
        fail(err)
    end
  end

  @spec get_channel_users(String.t(), String.t() | nil) :: {:ok, MapSet.t()} | {:error, any}
  def get_channel_users(app_token, channel_id, cursor \\ nil) do
    case FormClient.post(
           url("conversations.members"),
           case cursor do
             nil -> %{channel: channel_id, limit: @page_size}
             "" <> _ -> %{channel: channel_id, limit: @page_size, cursor: cursor}
           end,
           put_token(app_token)
         ) do
      # If there's a cursor, pass it along
      {:ok,
       %Tesla.Env{
         body: %{"members" => user_ids, "response_metadata" => %{"next_cursor" => next_cursor}}
       }} ->
        case next_cursor do
          # Only pass along a nonempty, non-nil cursor
          "" <> nc when byte_size(nc) > 0 -> {:ok, %{user_ids: user_ids, cursor: next_cursor}}
          _ -> {:ok, %{user_ids: user_ids}}
        end

      # If there's not a cursor, indicate as much.
      {:ok, %Tesla.Env{body: %{"members" => user_ids}}} ->
        {:ok, %{user_ids: user_ids}}

      # If there's something wrong, error.
      err ->
        {:error, err}
    end
  end

  # All `get_{context_key}_opts` functions `app_token` directly since they're only used
  # by `AccordSlack.Context.resolve`.

  @spec get_team_opts(String.t(), String.t()) :: {:ok, [title: String.t()]} | {:error, any}
  def get_team_opts(app_token, team_id) do
    case FormClient.post(
           url("team.info"),
           %{team: team_id},
           put_token(app_token)
         ) do
      {:ok,
       %Tesla.Env{
         body: %{"team" => %{"name" => name}}
       }} ->
        {:ok, [title: name]}

      err ->
        fail(err)
    end
  end

  @spec get_channel_opts(String.t(), String.t()) ::
          {:ok, [locale: %{acceept_language: String.t()}, title: String.t()]} | {:error, any}
  def get_channel_opts(app_token, channel_id) do
    case FormClient.post(
           url("conversations.info"),
           %{channel: channel_id, include_locale: "true"},
           put_token(app_token)
         ) do
      {:ok,
       %Tesla.Env{
         body: %{"channel" => %{"locale" => accept_language, "name" => name}}
       }} ->
        {:ok, [locale: %{accept_language: accept_language}, title: "##{name}"]}

      {:ok,
       %Tesla.Env{
         body: %{"channel" => %{"locale" => accept_language, "is_im" => true}}
       }} ->
        {:ok, [locale: %{accept_language: accept_language}, title: "DM"]}

      {:ok,
       %Tesla.Env{
         body: %{"error" => "channel_not_found"}
       }} ->
        {:error, Errors.not_found()}

      err ->
        fail(err)
    end
  end

  @spec get_user_opts(String.t(), String.t()) ::
          {:ok,
           [
             locale: %{accept_language: String.t(), tz: String.t()},
             preferred_name: String.t()
           ]}
          | {:error, any}
  def get_user_opts(app_token, user_id) do
    case FormClient.post(
           url("users.info"),
           %{user: user_id, include_locale: "true"},
           put_token(app_token)
         ) do
      {:ok,
       %Tesla.Env{
         body: %{
           "user" => %{
             "locale" => accept_language,
             "tz" => tz,
             "profile" => profile,
             "is_bot" => is_bot
           }
         }
       }} ->
        {:ok,
         [
           locale: %{
             accept_language: accept_language,
             tz: tz
           },
           preferred_name:
             case Map.get(profile, "first_name") do
               "" <> name ->
                 name

               _ ->
                 case Map.get(profile, "real_name") do
                   "" <> name ->
                     name

                   _ ->
                     case Map.get(profile, "display_name") do
                       "" <> name -> name
                       _ -> nil
                     end
                 end
             end,
           is_slack_bot: is_bot
         ]}

      err ->
        fail(err)
    end
  end

  # `oauth_access` works completely differently by its nature
  @spec oauth_access(String.t()) ::
          {:ok, %{app_token: String.t(), team_id: String.t(), team_name: String.t()}}
          | {:error, any}
  def oauth_access(code) do
    case OAuthClient.post(
           url("oauth.v2.access"),
           %{
             code: code,
             redirect_uri: Application.get_env(:accord, AccordSlack)[:redirect_uri]
           }
         ) do
      {:ok,
       %Tesla.Env{
         body: %{
           "ok" => true,
           "access_token" => app_token,
           "team" => %{"id" => team_id, "name" => team_name}
         }
       }} ->
        {:ok, %{app_token: app_token, team_id: team_id, team_name: team_name}}

      err ->
        fail(err)
    end
  end
end
