defmodule AccordSlack.Context do
  alias Accord.{Repo, People, Person, Persona, PersonPeople, Errors}
  alias AccordSlack.IO, as: SlackIO

  require Logger

  # Amount of time in seconds to throttle fresh locale refreshes.
  @refresh_delta 30

  @type t :: %{
          optional(:person) => Accord.Person.t(),
          optional(:persona) => Accord.Persona.t(),
          optional(:team) => Accord.People.t(),
          optional(:channel) => Accord.People.t()
        }

  def resolve!(params) do
    case resolve(params) do
      {:ok, context} -> context
      {:rescue, context} -> context
    end
  end

  def resolve({:slack, team_id, channel_id, user_id}) do
    with %People{app_token: app_token} <- People.get({:slack, team_id}) do
      (assure_slack_peoples(app_token, {:slack, team_id, channel_id}) ++
         assure_slack_user(app_token, {:slack, team_id, user_id}) ++
         PersonPeople.associate({:user_channel_assoc, :person, :channel}))
      |> commit_assures
    else
      err -> {:error, err}
    end
  end

  def resolve({:slack, team_id, user_id}) do
    with %People{app_token: app_token} <- People.get({:slack, team_id}) do
      (assure_slack_user(app_token, {:slack, team_id, user_id}) ++
         assure_slack_team({:slack, team_id}))
      |> commit_assures
    else
      err -> {:error, err}
    end
  end

  @doc """
    Resolves :team as People, :channel as People, :person as Person, :persona as Persona for
    - flatter payloads received from Slack as for slash commands
    - deeper payloads without channel info
  """
  def resolve(%{
        "team_id" => team_id,
        "channel_id" => channel_id,
        "user_id" => user_id
      }) do
    resolve({:slack, team_id, channel_id, user_id})
  end

  def resolve(%{
        "team_id" => team_id,
        "event" => %{
          # Ignore the system DM channel, it is not useful
          # "channel" => channel_id,
          "user" => user_id
        }
      }) do
    resolve({:slack, team_id, user_id})
  end

  def resolve(%{
        "team" => %{"id" => team_id},
        "channel" => %{"id" => channel_id},
        "user" => %{"id" => user_id}
      }) do
    resolve({:slack, team_id, channel_id, user_id})
  end

  def resolve(%{
        "user" => %{"id" => user_id},
        "team" => %{"id" => team_id}
      }) do
    resolve({:slack, team_id, user_id})
  end

  defp fail(err) do
    IO.puts("[Context fail]")
    IO.inspect(err)
    {:error, err}
  end

  defp fail_conversation(actions) do
    with {:ok, rescue_context} <-
           Enum.filter(actions, fn
             {name, _, _} ->
               name == :persona or name == :person or name == :team

             {name, _, _, _} ->
               name == :persona or name == :person or name == :team
           end)
           |> Repo.commit() do
      {:rescue, rescue_context}
    end
  end

  defp commit_assures(actions) do
    err_not_found = Errors.not_found()

    with {:ok, context} <-
           (actions ++
              PersonPeople.associate({:user_team_assoc, :person, :team}))
           |> Repo.commit() do
      {:ok, context}
    else
      {:error, :channel, ^err_not_found, _} -> fail_conversation(actions)
      err -> fail(err)
    end
  end

  defp opts_needs_refresh?(get_result) do
    case get_result do
      nil ->
        true

      entity ->
        case entity.locale_updated_at do
          nil ->
            true

          %NaiveDateTime{} = updated_at ->
            NaiveDateTime.diff(Repo.now(), updated_at) > @refresh_delta
        end
    end
  end

  defp get_fresh_peoples_opts(app_token, team_id, channel_id) do
    if opts_needs_refresh?(People.get({:slack, team_id, channel_id})) do
      with {:ok, channel_opts} <- SlackIO.get_channel_opts(app_token, channel_id),
           {:ok, team_opts} <- SlackIO.get_team_opts(app_token, team_id) do
        [channel_opts: channel_opts, team_opts: team_opts]
      end
    else
      []
    end
  end

  defp get_fresh_person_opts(app_token, team_id, user_id) do
    if opts_needs_refresh?(Persona.get({:slack, team_id, user_id}) |> Repo.get_rel(:person)) do
      with {:ok, person_opts} <- SlackIO.get_user_opts(app_token, user_id), do: person_opts
    else
      []
    end
  end

  defp assure_slack_peoples(app_token, {:slack, team_id, channel_id} = signature) do
    err_not_found = Errors.not_found()

    with people_opts when is_list(people_opts) <-
           get_fresh_peoples_opts(app_token, team_id, channel_id) do
      People.assure_associated(
        signature,
        people_opts
      )
    else
      {:error, ^err_not_found} ->
        Repo.error(:channel, err_not_found) ++ People.assure(:team, {:slack, team_id}, nil)
    end
  end

  defp assure_slack_team({:slack, _team_id} = signature) do
    People.assure(:team, signature)
  end

  defp assure_slack_user(app_token, {:slack, team_id, user_id} = signature) do
    Person.assure_with_persona(
      signature,
      get_fresh_person_opts(app_token, team_id, user_id)
    )
  end
end
