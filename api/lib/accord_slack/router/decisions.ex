defmodule AccordSlack.Router.Decisions do
  alias AccordSlack.Router
  alias AccordSlack.IO, as: SlackIO

  alias Accord.{Repo, People, DecisionVersion}
  alias Accord.Caches.DecisionVersionResult, as: Result

  alias AccordSlack.Views.{
    NewDecisionStep1,
    NewDecisionStep1Confirm,
    NewDecisionStep2,
    DecisionMessage,
    DecisionSummary,
    StaticMessage
  }

  require Logger

  use AccordWeb, :controller

  def slash_decide(
        %{
          "trigger_id" => trigger_id,
          "channel_id" => channel_id,
          "text" => text,
          "response_url" => url
        },
        %{person: %{locale: locale}} = context
      ) do
    cond do
      String.match?(text, Router.help()) ->
        SlackIO.reply_ephemerally(context, url, StaticMessage.render(locale, :decide_help, nil))

      true ->
        open_decision(context, trigger_id, nil, %{"channel_id" => channel_id})
    end
  end

  def open_decision(%{person: %{locale: locale}} = context, trigger_id, version, metadata) do
    with {:ok, view_id} <-
           SlackIO.start_modal(
             context,
             trigger_id,
             NewDecisionStep1.render(locale, version, metadata)
           ) do
      Logger.info("〘AccordSlack〙 Opened decision modal", event: %{view_id: view_id})
    else
      err -> Router.fail(err, %{context: context, trigger_id: trigger_id, version: version})
    end
  end

  def save_step_1(conn, params, context) do
    with {:ok, response, metadata} <- NewDecisionStep1.parse_response(params),
         :ok <- NewDecisionStep1.commit(response, metadata) do
      SlackIO.respond(conn, context, %{
        response_action: "push",
        view:
          NewDecisionStep1Confirm.render(
            response.locale,
            DecisionVersion.get_by_service_id(Map.get(metadata, "service_id")),
            metadata
          )
      })
    else
      err -> Router.fail(err, params)
    end
  end

  def confirm_step_1(conn, params, context) do
    with {:ok, response, metadata} <- NewDecisionStep1Confirm.parse_response(params),
         :ok <- NewDecisionStep1Confirm.commit(response, metadata) do
      SlackIO.respond(conn, context, %{
        response_action: "push",
        view: NewDecisionStep2.render(response.locale, true, metadata)
      })
    else
      err -> Router.fail(err, params)
    end
  end

  def update_step_2(%{person: %{locale: locale}} = context, view_id, metadata) do
    SlackIO.update_modal(
      context,
      view_id,
      NewDecisionStep2.render(locale, nil, metadata)
    )
  end

  def save_step_2(conn, params, context) do
    with {:ok, response, metadata} <- NewDecisionStep2.parse_response(params),
         :ok <- NewDecisionStep2.commit(response, metadata) do
      Router.Homes.update_home(context)
      SlackIO.respond(conn, context, %{response_action: "clear"})
    else
      {:error, %{} = errors} ->
        SlackIO.respond(conn, context, %{response_action: "errors", errors: errors})

      err ->
        Router.fail(err, params)
    end
  end

  def delete_new_decision(
        conn,
        %{"view" => %{"private_metadata" => metadata_str}} = params,
        _context
      ) do
    with {:ok, metadata} <-
           (case metadata_str do
              "" -> {:ok, %{}}
              _ -> Jason.decode(metadata_str)
            end),
         "" <> service_id <- metadata |> Map.get("service_id"),
         %DecisionVersion{} = version <- DecisionVersion.get_by_service_id(service_id),
         {:ok, _} <- Repo.delete(version) do
      Logger.info("New DecisionVersion deleted.", event: %{metadata: metadata})
    else
      nil ->
        Logger.info("Unable to delete DecisionVersion.", event: %{metadata_str: metadata_str})

      err ->
        Router.fail(err, params)
    end

    Router.accept_slack_request(conn)
  end

  def update_decision_message(
        %DecisionVersion{service_id: service_id} = version,
        deferred_metadata \\ %{n_voters: nil}
      ) do
    with {team_id, channel_id, ts} <- DecisionVersion.get_ids_from_service_id(service_id),
         %People{app_token: app_token} <- People.get({:slack, team_id}),
         context <- %{team: %{app_token: app_token}},
         %{n_voters: n_voters} <- deferred_metadata,
         n_votes <- DecisionVersion.vote_count(version),
         {:ok, opts} <- SlackIO.get_channel_opts(app_token, channel_id),
         %{accept_language: _} = locale <- opts[:locale],
         {:ok, _} =
           SlackIO.update_message(
             context,
             {channel_id, ts},
             DecisionMessage.render(
               locale,
               version |> Repo.preload([:author, :decision]),
               %{"n_votes" => n_votes, "n_voters" => n_voters}
             )
           ) do
      Logger.info("〘AccordSlack〙 Updated decision message")
      :ok
    else
      err -> Router.fail(err, version)
    end
  end

  def send_summary(
        %DecisionVersion{service_id: service_id} = version,
        %Result{} = result
      ) do
    with {team_id, channel_id, ts} <- DecisionVersion.get_ids_from_service_id(service_id),
         %People{app_token: app_token} <- People.get({:slack, team_id}),
         context <- %{team: %{app_token: app_token}},
         {:ok, opts} <- SlackIO.get_channel_opts(app_token, channel_id),
         %{accept_language: _} = locale <- opts[:locale],
         {:ok, _} =
           SlackIO.post_message(
             context,
             channel_id,
             DecisionSummary.render(
               locale,
               %{decision_version_result: result, decision_version: version},
               nil
             ),
             thread_ts: ts,
             reply_broadcast: true
           ) do
      Logger.info("〘AccordSlack〙 Posted decision summary")
      :ok
    else
      err -> Router.fail(err, version)
    end
  end
end
