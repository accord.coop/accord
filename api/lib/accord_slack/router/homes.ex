defmodule AccordSlack.Router.Homes do
  alias AccordSlack.Router
  alias AccordSlack.IO, as: SlackIO

  alias Accord.{Repo, Persona, People, DecisionVersion, DecisionRole}

  alias AccordSlack.Views.{
    Home,
    StaticMessage
  }

  require Logger

  use AccordWeb, :controller

  def on_home_opened(
        conn,
        %{"event" => %{"user" => human_user_id, "channel" => conversation_id}},
        %{
          team: %{app_token: app_token, service_id: team_service_id},
          person: %{locale: locale},
          persona: %{service_properties: %{"home_opened" => home_opened}} = persona
        } = context
      ) do
    if not home_opened do
      SlackIO.post_message(
        context,
        conversation_id,
        StaticMessage.render(locale, :home_opened, nil)
      )
    end

    Task.start_link(fn ->
      # Refresh the bot’s user ID for this team and the conversation ID for this user
      with {:ok, %{user_ids: user_ids}} <-
             SlackIO.get_channel_users(app_token, conversation_id),
           team_id <-
             People.get_slack_team_id(team_service_id),
           bot_user_id_for_team <-
             Enum.find(user_ids, fn user_id -> user_id != human_user_id end),
           persona_changes <-
             persona
             |> Persona.change_service_properties(%{
               "home_opened" => true,
               "dm_id" => conversation_id
             }),
           {:ok, _} <-
             [
               {:persona, :update, {persona_changes, []}}
               | People.assure(:team, {:slack, team_id}, bot_token: bot_user_id_for_team)
             ]
             |> Repo.commit() do
        Logger.info("〘AccordSlack〙 Updated bot user ID for team and user DM ID")
      else
        err -> Router.fail(err, context)
      end
    end)

    update_home(context)

    Router.accept_slack_request(conn)
  end

  def update_home(
        %{
          person: %{uuid: person_uuid, locale: locale},
          persona: %{service_id: user_service_id}
        } = context
      ) do
    with user_id <- Persona.get_slack_user_id(user_service_id) do
      SlackIO.render_home(
        context,
        user_id,
        Home.render(
          locale,
          %{
            drafts: DecisionVersion.get_drafts_for_author(person_uuid),
            votable: DecisionRole.votable_decision_versions_for_person(person_uuid, nil),
            person_uuid: person_uuid
          },
          nil
        )
      )
    end
  end
end
