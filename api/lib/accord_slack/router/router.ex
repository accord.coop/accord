defmodule AccordSlack.Router do
  @moduledoc """
    HTTP requests are first routed through `AccordWeb`, which verifies the authenticity of requests
    to Slack endpoints before passing them to functions in this module, which is responsible for
    pattern matching requests from Slack until the request can be processed by a more
    domain-specific module.
  """

  alias Accord.{Repo, DecisionVersion, Caches}
  alias AccordSlack.Context
  alias AccordSlack.IO, as: SlackIO

  alias AccordSlack.Router.{
    Decisions,
    Delegations,
    Homes,
    Votes,
    Withdrawals
  }

  alias AccordSlack.Views.{
    NewDecisionStep1,
    NewDecisionStep1Confirm,
    NewDecisionStep2,
    VoteModal,
    WithdrawalModal,
    NewDelegationStep1,
    NewDelegationStep2,
    StaticMessage
  }

  @new_decision_step_1_id NewDecisionStep1.id()
  @new_decision_step_1_confirm_id NewDecisionStep1Confirm.id()
  @new_decision_step_2_id NewDecisionStep2.id()
  @vote_modal_id VoteModal.id()
  @withdrawal_modal_id WithdrawalModal.id()
  @new_delegation_step_1_id NewDelegationStep1.id()
  @new_delegation_step_2_id NewDelegationStep2.id()

  require Logger

  use AccordWeb, :controller

  @help ~r/help/i
  def help, do: @help

  def accept_slack_request(conn), do: conn |> send_resp(:ok, "")

  def fail(err, params) do
    Logger.error("〘AccordSlack〙 Unanticipated error")
    IO.inspect(params)
    IO.inspect(err)
    {:error, err}
  end

  # === === === === ===
  #  PRIMARY ROUTING
  # === === === === ===

  # The source-of-truth redirect for initiating the OAuth flow to add to Slack:

  def add_to_slack(conn, _) do
    with {:ok, %{state: %Caches.OAuthState{state: state}}} <-
           Caches.OAuthState.prune_and_new() |> Repo.commit() do
      redirect(conn,
        external:
          "https://slack.com/oauth/v2/authorize?client_id=#{
            Application.get_env(:accord, AccordSlack)[:client_id]
          }&scope=#{Application.get_env(:accord, AccordSlack)[:scopes]}&state=#{state}"
      )
    else
      err -> fail(err, nil)
    end
  end

  # Slash commands:

  def slash(conn, %{"command" => "/decide"} = params) do
    Decisions.slash_decide(params, Context.resolve!(params))
    accept_slack_request(conn)
  end

  def slash(conn, %{"command" => "/delegate"} = params) do
    Delegations.slash_delegate(params, Context.resolve!(params))
    accept_slack_request(conn)
  end

  def slash(conn, %{"command" => "/accordtestcommand", "trigger_id" => _trigger_id} = _params) do
    # Use this command in staging to open pushed modals that wouldn’t normally elicit errors
    # from Slack
    accept_slack_request(conn)
  end

  # Events:

  def event(conn, %{"challenge" => challenge}), do: conn |> send_resp(:ok, challenge)

  def event(conn, %{"event" => %{"type" => "app_home_opened"}} = params) do
    Logger.info("〘AccordSlack〙 Home opened")
    Homes.on_home_opened(conn, params, Context.resolve!(params))
  end

  def event(conn, %{"event" => %{"type" => "message", "subtype" => "message_changed"}}) do
    Logger.info("〘AccordSlack〙 Ignoring message edit")
    accept_slack_request(conn)
  end

  def event(conn, %{"event" => %{"type" => "message", "user" => _}} = params) do
    Logger.info("〘AccordSlack〙 Received DM")
    on_dm(conn, params, Context.resolve!(params))
  end

  def event(conn, %{"event" => %{"type" => "message"}}) do
    Logger.info("〘AccordSlack〙 Ignoring bot message")
    accept_slack_request(conn)
  end

  # Interactions:

  def interaction(conn, %{"payload" => payload_string} = params) do
    case Jason.decode(payload_string) do
      {:ok, payload} -> interaction_payload(conn, payload, Context.resolve!(payload))
      {:error, err} -> fail(err, params)
    end
  end

  defp interaction_payload(conn, %{"type" => "view_submission"} = params, context) do
    Logger.info("〘AccordSlack〙 Received view_submission")
    on_submit(conn, params, context)
  end

  defp interaction_payload(conn, %{"type" => "view_closed"} = params, context) do
    Logger.info("〘AccordSlack〙 Received view_closed")
    on_close(conn, params, context)
  end

  defp interaction_payload(conn, %{"type" => "shortcut"} = params, context) do
    Logger.info("〘AccordSlack〙 Received shortcut")
    on_shortcut(conn, params, context)
  end

  defp interaction_payload(
         conn,
         %{"type" => "block_actions", "actions" => actions} = params,
         context
       ) do
    Logger.info("〘AccordSlack〙 Received block_actions")
    dispatch_action(actions, params, context, conn)
  end

  defp dispatch_action(
         [%{"action_id" => "add_option"}],
         %{
           "view" => %{
             "id" => view_id,
             "private_metadata" => metadata_string
           }
         },
         %{person: %{locale: locale}} = context,
         conn
       ) do
    metadata = Jason.decode!(metadata_string)

    SlackIO.update_modal(
      context,
      view_id,
      NewDecisionStep1.render(
        locale,
        nil,
        metadata |> Map.put("n_options", metadata["n_options"] + 1)
      )
    )

    accept_slack_request(conn)
  end

  defp dispatch_action(
         [%{"action_id" => "vote"}],
         %{
           "team" => %{"id" => team_id},
           "trigger_id" => trigger_id,
           "container" => %{"channel_id" => channel_id, "message_ts" => ts}
         } = params,
         context,
         conn
       ) do
    state_published = DecisionVersion.Types.published(:str)

    with %DecisionVersion{state: ^state_published} = version <-
           DecisionVersion.get({:slack, team_id, channel_id, ts}) do
      Votes.open_vote(context, trigger_id, version)
    else
      err -> fail(err, params)
    end

    accept_slack_request(conn)
  end

  defp dispatch_action(
         [
           %{
             "action_id" => "vote_" <> decision_version_uuid,
             "selected_option" => %{"value" => "withdraw"}
           }
         ],
         %{"trigger_id" => trigger_id} = params,
         context,
         conn
       ) do
    state_published = DecisionVersion.Types.published(:str)

    with %DecisionVersion{state: ^state_published} = version <-
           DecisionVersion.get(decision_version_uuid) do
      Withdrawals.open_withdrawal(context, trigger_id, version)
    else
      %DecisionVersion{} -> Homes.update_home(context)
      err -> fail(err, params)
    end

    accept_slack_request(conn)
  end

  defp dispatch_action(
         [%{"action_id" => "vote_" <> decision_version_uuid}],
         %{"trigger_id" => trigger_id} = params,
         context,
         conn
       ) do
    state_published = DecisionVersion.Types.published(:str)

    with %DecisionVersion{state: ^state_published} = version <-
           DecisionVersion.get(decision_version_uuid) do
      Votes.open_vote(context, trigger_id, version)
    else
      %DecisionVersion{} -> Homes.update_home(context)
      err -> fail(err, params)
    end

    accept_slack_request(conn)
  end

  defp dispatch_action(
         [
           %{
             "action_id" => "edit_" <> decision_version_uuid,
             "selected_option" => %{"value" => "edit"}
           }
         ],
         %{"trigger_id" => trigger_id} = params,
         context,
         conn
       ) do
    state_new = DecisionVersion.Types.new(:str)

    with %DecisionVersion{state: ^state_new} = version <-
           DecisionVersion.get(decision_version_uuid) do
      Decisions.open_decision(context, trigger_id, version, %{"channel_id" => nil})
    else
      %DecisionVersion{} -> Homes.update_home(context)
      err -> fail(err, params)
    end

    accept_slack_request(conn)
  end

  defp dispatch_action(
         [
           %{
             "action_id" => "edit_" <> decision_version_uuid,
             "selected_option" => %{"value" => "delete"}
           }
         ],
         params,
         context,
         conn
       ) do
    state_new = DecisionVersion.Types.new(:str)

    with %DecisionVersion{state: ^state_new} = version <-
           DecisionVersion.get(decision_version_uuid),
         {:ok, _} <- Repo.delete(version) do
      Homes.update_home(context)
    else
      %DecisionVersion{} -> Homes.update_home(context)
      err -> fail(err, params)
    end

    accept_slack_request(conn)
  end

  defp dispatch_action(
         [
           %{
             "action_id" => "option_choice_input︰" <> _option_id,
             "selected_option" => %{"value" => "option_choice_" <> option_action}
           }
         ],
         %{
           "view" => %{
             "callback_id" => @vote_modal_id,
             "id" => view_id,
             "private_metadata" => metadata_string
           }
         },
         context,
         conn
       ) do
    metadata = Jason.decode!(metadata_string)
    Votes.adjust_vote(context, view_id, option_action, metadata)
    accept_slack_request(conn)
  end

  defp dispatch_action(
         [
           %{
             "action_id" => "option_choice_" <> option_action
           }
         ],
         %{
           "view" => %{
             "callback_id" => @vote_modal_id,
             "id" => view_id,
             "private_metadata" => metadata_string
           }
         },
         context,
         conn
       ) do
    metadata = Jason.decode!(metadata_string)
    Votes.adjust_vote(context, view_id, option_action, metadata)
    accept_slack_request(conn)
  end

  defp dispatch_action(
         [
           %{
             "type" => "static_select",
             "action_id" => "method_input",
             "selected_option" => next_active_method
           }
         ],
         %{
           "view" => %{
             "callback_id" => @new_decision_step_2_id,
             "id" => view_id,
             "private_metadata" => metadata_string
           }
         },
         context,
         conn
       ) do
    metadata = Jason.decode!(metadata_string)

    Decisions.update_step_2(
      context,
      view_id,
      metadata |> Map.put("active_method", next_active_method)
    )

    accept_slack_request(conn)
  end

  defp dispatch_action(
         [
           %{
             "type" => "static_select",
             "action_id" => "parameter3_value_input",
             "selected_option" => %{"value" => "parameter3_value_" <> unparsed_p3v}
           }
         ],
         %{
           "view" => %{
             "callback_id" => @new_decision_step_2_id,
             "id" => view_id,
             "private_metadata" => metadata_string
           }
         },
         context,
         conn
       ) do
    metadata = Jason.decode!(metadata_string)
    {:ok, p3v} = NewDecisionStep2.parse_number(unparsed_p3v, nil)

    Decisions.update_step_2(
      context,
      view_id,
      metadata |> Map.put("parameter3_value", p3v)
    )

    accept_slack_request(conn)
  end

  defp dispatch_action(
         [
           %{
             "type" => "plain_text_input",
             "action_id" => "parameter1_value_input",
             "block_id" => block_id,
             "value" => unparsed_p1v
           }
         ],
         %{
           "view" => %{
             "callback_id" => @new_decision_step_2_id,
             "id" => view_id,
             "private_metadata" => metadata_string
           }
         },
         context,
         conn
       ) do
    metadata = Jason.decode!(metadata_string)

    with {:ok, p1v} <- NewDecisionStep2.parse_number(unparsed_p1v, block_id) do
      Decisions.update_step_2(
        context,
        view_id,
        metadata |> Map.put("parameter1_value", p1v)
      )

      accept_slack_request(conn)
    else
      {:error, errors} ->
        SlackIO.respond(conn, context, %{response_action: "errors", errors: errors})
    end
  end

  defp dispatch_action(
         [%{"action_id" => "new-delegation"}],
         %{"trigger_id" => trigger_id, "view" => %{"id" => view_id}},
         %{person: %{locale: locale}} = context,
         conn
       ) do
    SlackIO.push_modal(
      context,
      trigger_id,
      NewDelegationStep1.render(locale, nil, %{"delegations_view_id" => view_id})
    )

    accept_slack_request(conn)
  end

  defp dispatch_action(
         [%{"action_id" => "end-delegation_" <> delegation_uuid}],
         params,
         context,
         conn
       ) do
    Delegations.end_delegation(params, context, delegation_uuid)
    accept_slack_request(conn)
  end

  defp dispatch_action(actions, _, _, conn) do
    IO.puts("[Unhandled action]")
    IO.inspect(actions)
    accept_slack_request(conn)
  end

  # === === === === ===
  #  SECONDARY ROUTING
  # === === === === ===

  # On submit decision flow:

  defp on_submit(
         conn,
         %{"view" => %{"callback_id" => @new_decision_step_1_id}} = params,
         context
       ),
       do: Decisions.save_step_1(conn, params, context)

  defp on_submit(
         conn,
         %{"view" => %{"callback_id" => @new_decision_step_1_confirm_id}} = params,
         context
       ),
       do: Decisions.confirm_step_1(conn, params, context)

  defp on_submit(
         conn,
         %{"view" => %{"callback_id" => @new_decision_step_2_id}} = params,
         context
       ),
       do: Decisions.save_step_2(conn, params, context)

  # On submit delegation flow:

  defp on_submit(
         conn,
         %{"view" => %{"callback_id" => @new_delegation_step_1_id}} = params,
         context
       ),
       do: Delegations.save_step_1(conn, params, context)

  defp on_submit(
         conn,
         %{"view" => %{"callback_id" => @new_delegation_step_2_id}} = params,
         context
       ),
       do: Delegations.save_step_2(conn, params, context)

  # On submit vote:

  defp on_submit(conn, %{"view" => %{"callback_id" => @vote_modal_id}} = params, context),
    do: Votes.save_vote(conn, params, context)

  # On submit withdrawal:

  defp on_submit(
         conn,
         %{"view" => %{"callback_id" => @withdrawal_modal_id}} = params,
         context
       ),
       do: Withdrawals.save_withdrawal(conn, params, context)

  # On close:

  defp on_close(
         conn,
         %{"view" => %{"callback_id" => @new_decision_step_1_id}} = params,
         context
       ) do
    Decisions.delete_new_decision(conn, params, context)
  end

  defp on_close(
         conn,
         %{
           "view" => %{"callback_id" => @new_decision_step_1_confirm_id},
           "is_cleared" => is_cleared?
         },
         context
       ) do
    if is_cleared?, do: SlackIO.send_draft_saved_message(context)
    accept_slack_request(conn)
  end

  defp on_close(
         conn,
         %{
           "view" => %{"callback_id" => @new_decision_step_2_id},
           "is_cleared" => is_cleared?
         },
         context
       ) do
    if is_cleared?, do: SlackIO.send_draft_saved_message(context)
    accept_slack_request(conn)
  end

  defp on_close(conn, _params, _context), do: accept_slack_request(conn)

  # On shortcut:

  defp on_shortcut(
         conn,
         %{"trigger_id" => trigger_id, "callback_id" => @new_decision_step_1_id},
         context
       ) do
    Logger.info("〘AccordSlack〙 Received new decision shortcut")
    Decisions.open_decision(context, trigger_id, nil, %{"channel_id" => nil})
    accept_slack_request(conn)
  end

  # On a direct message to the bot:

  defp on_dm(
         conn,
         %{"event" => %{"bot_id" => _}},
         _context
       ) do
    accept_slack_request(conn)
  end

  defp on_dm(
         conn,
         %{"event" => %{"channel" => conversation_id, "text" => text}},
         %{person: %{locale: locale}} = context
       ) do
    cond do
      String.match?(text, @help) ->
        SlackIO.post_message(context, conversation_id, StaticMessage.render(locale, :help, nil))

      true ->
        SlackIO.post_message(context, conversation_id, StaticMessage.render(locale, :dm, nil))
    end

    accept_slack_request(conn)
  end
end
