defmodule AccordSlack.Router.Withdrawals do
  alias AccordSlack.Router
  alias AccordSlack.IO, as: SlackIO

  alias Accord.{People, DecisionVersion}

  alias AccordSlack.Views.{
    WithdrawalModal,
    WithdrawalMessage
  }

  require Logger

  use AccordWeb, :controller

  def open_withdrawal(
        %{person: %{locale: locale}} = context,
        trigger_id,
        %DecisionVersion{service_id: decision_version_service_id} = version
      ) do
    with {:ok, view_id} <-
           SlackIO.start_modal(
             context,
             trigger_id,
             WithdrawalModal.render(locale, version, %{
               "decision_version_service_id" => decision_version_service_id
             })
           ) do
      Logger.info("〘AccordSlack〙 Opened withdrawal modal", event: %{view_id: view_id})
    else
      err -> Router.fail(err, %{context: context, trigger_id: trigger_id, version: version})
    end
  end

  def save_withdrawal(conn, params, context) do
    with {:ok, response, metadata} <- WithdrawalModal.parse_response(params),
         :ok <- WithdrawalModal.commit(response, metadata) do
      Router.Homes.update_home(context)
      SlackIO.respond(conn, context, %{response_action: "clear"})
    else
      err ->
        Router.fail(err, params)
    end
  end

  def withdraw_decision_message(
        %DecisionVersion{service_id: service_id},
        withdrawal_reason
      ) do
    with {team_id, channel_id, ts} <- DecisionVersion.get_ids_from_service_id(service_id),
         %People{app_token: app_token} <- People.get({:slack, team_id}),
         context <- %{team: %{app_token: app_token}},
         {:ok, opts} <- SlackIO.get_channel_opts(app_token, channel_id),
         %{accept_language: _} = locale <- opts[:locale],
         {:ok, _} =
           SlackIO.post_message(
             context,
             channel_id,
             WithdrawalMessage.render(locale, withdrawal_reason, nil),
             thread_ts: ts
           ),
         {:ok, _} = SlackIO.delete_message(context, {channel_id, ts}) do
      Logger.info("〘AccordSlack〙 Withdrew decision")
      :ok
    end
  end
end
