defmodule AccordSlack.Router.Votes do
  alias AccordSlack.Router
  alias AccordSlack.IO, as: SlackIO

  alias Accord.{DecisionVersion, Vote}

  alias AccordSlack.Views.{VoteModal}

  require Logger

  use AccordWeb, :controller

  defp extant_vote?(vote) do
    case(vote) do
      nil -> false
      _ -> true
    end
  end

  defp choice_value({id, %{"value" => value}}, adjust_id \\ nil, adjust_delta \\ 0.0),
    do: if(id === adjust_id, do: value + adjust_delta, else: value)

  defp assign_ordinal_values(choices_state, drop_id \\ nil) do
    choices_state
    |> Enum.reduce(%{i: 0, next_state: %{}}, fn
      {id, _}, acc when id === drop_id ->
        acc

      {id, choice_state}, %{i: i, next_state: next_state} ->
        %{
          i: i + 1,
          next_state: next_state |> Map.put(id, choice_state |> Map.put("value", i / 1))
        }
    end)
    |> Map.get(:next_state)
  end

  defp update_choices_state("delta︰+1︰" <> option_id, choices_state) do
    choices_state
    |> Enum.sort_by(&choice_value(&1, option_id, 1.5))
    |> assign_ordinal_values()
  end

  defp update_choices_state("delta︰-1︰" <> option_id, choices_state) do
    choices_state
    |> Enum.sort_by(&choice_value(&1, option_id, -1.5))
    |> assign_ordinal_values()
  end

  defp update_choices_state("remove︰" <> drop_id, choices_state) do
    choices_state |> Enum.sort_by(&choice_value/1) |> assign_ordinal_values(drop_id)
  end

  defp update_choices_state("select︰" <> option_id, choices_state) do
    choices_state |> Map.put(option_id, %{"value" => map_size(choices_state) / 1})
  end

  defp update_choices_state(_, choices_state), do: choices_state

  def open_vote(
        %{person: %{locale: locale} = voter} = context,
        trigger_id,
        %DecisionVersion{service_id: decision_version_service_id} = version
      ) do
    with extant_vote <- Vote.get(%{voter: voter, decision_version: version}),
         {:ok, view_id} <-
           SlackIO.start_modal(
             context,
             trigger_id,
             VoteModal.render(
               locale,
               %{
                 decision_version: version,
                 extant_vote?: extant_vote?(extant_vote)
               },
               %{
                 "decision_version_service_id" => decision_version_service_id,
                 "choices_state" =>
                   case extant_vote do
                     nil -> %{}
                     vote -> Vote.get_choices(vote)
                   end
               }
             )
           ) do
      Logger.info("〘AccordSlack〙 Opened vote modal", event: %{view_id: view_id})
    else
      err -> Router.fail(err, %{context: context, trigger_id: trigger_id, version: version})
    end
  end

  def adjust_vote(%{person: %{locale: locale} = voter} = context, view_id, action, %{
        "decision_version_service_id" => decision_version_service_id,
        "choices_state" => choices_state
      }) do
    with version <- DecisionVersion.get_by_service_id(decision_version_service_id),
         extant_vote <- Vote.get(%{voter: voter, decision_version: version}),
         {:ok, _} <-
           SlackIO.update_modal(
             context,
             view_id,
             VoteModal.render(
               locale,
               %{
                 decision_version: version,
                 extant_vote?: extant_vote?(extant_vote)
               },
               %{
                 "decision_version_service_id" => decision_version_service_id,
                 "choices_state" => update_choices_state(action, choices_state)
               }
             )
           ) do
      Logger.info("〘AccordSlack〙 Updated vote modal", event: %{view_id: view_id})
    else
      err -> Router.fail(err, %{context: context, view_id: view_id})
    end
  end

  def save_vote(conn, params, context) do
    with {:ok, %{decision_version: _version} = response, metadata} <-
           VoteModal.parse_response(params),
         :ok <- VoteModal.commit(response, metadata) do
      Router.Homes.update_home(context)
      SlackIO.respond(conn, context, %{response_action: "clear"})
    else
      {:error, %{} = errors} ->
        SlackIO.respond(conn, context, %{response_action: "errors", errors: errors})

      err ->
        Router.fail(err, params)
    end
  end
end
