defmodule AccordSlack.Router.Delegations do
  alias AccordSlack.Router
  alias AccordSlack.IO, as: SlackIO

  alias Accord.{Repo, Delegation}

  alias AccordSlack.Views.{
    DelegationsModal,
    NewDelegationStep1,
    NewDelegationStep2,
    StaticMessage
  }

  require Logger

  use AccordWeb, :controller

  def slash_delegate(
        %{
          "trigger_id" => trigger_id,
          "text" => text,
          "response_url" => url
        },
        %{person: %{locale: locale}} = context
      ) do
    cond do
      String.match?(text, Router.help()) ->
        SlackIO.reply_ephemerally(context, url, StaticMessage.render(locale, :delegate_help, nil))

      true ->
        open_delegations(context, trigger_id)
    end
  end

  def open_delegations(%{person: %{locale: locale} = person} = context, trigger_id) do
    with delegations <- Delegation.get_active_by_delegator(person, 3),
         {:ok, view_id} <-
           SlackIO.start_modal(
             context,
             trigger_id,
             DelegationsModal.render(locale, %{delegations: delegations, person: person}, nil)
           ) do
      Logger.info("〘AccordSlack〙 Opened delegations modal", event: %{view_id: view_id})
    else
      err -> Router.fail(err, %{context: context, trigger_id: trigger_id})
    end
  end

  def end_delegation(
        %{"view" => %{"id" => delegations_view_id}} = params,
        %{person: %{locale: locale} = person} = context,
        delegation_uuid
      ) do
    with %Delegation{} = delegation <- Delegation.get(delegation_uuid),
         {:ok, _} <- Delegation.end_now(delegation) |> Repo.update(),
         delegations <- Delegation.get_active_by_delegator(person, 3) do
      SlackIO.update_modal(
        context,
        delegations_view_id,
        DelegationsModal.render(locale, %{delegations: delegations, person: person}, nil)
      )
    else
      err -> Router.fail(err, params)
    end
  end

  def save_step_1(conn, params, %{person: %{locale: locale} = person} = context) do
    with {:ok, scope_value, metadata} <- NewDelegationStep1.parse_response(params) do
      SlackIO.respond(conn, context, %{
        response_action: "update",
        view:
          NewDelegationStep2.render(
            locale,
            %{
              person: person,
              scope: scope_value
            },
            metadata
          )
      })
    else
      err -> Router.fail(err, params)
    end
  end

  def save_step_2(conn, params, %{person: %{locale: locale} = person} = context) do
    with {:ok, response, %{"delegations_view_id" => delegations_view_id}} <-
           NewDelegationStep2.parse_response(params),
         :ok <- NewDelegationStep2.commit(response, nil),
         delegations <- Delegation.get_active_by_delegator(person, 3),
         {:ok, _new_delegations_view_id} <-
           SlackIO.update_modal(
             context,
             delegations_view_id,
             DelegationsModal.render(locale, %{delegations: delegations, person: person}, nil)
           ) do
      SlackIO.respond(conn, context)
    else
      {:error, %{} = errors} ->
        SlackIO.respond(conn, context, %{response_action: "errors", errors: errors})

      err ->
        Router.fail(err, params)
    end
  end
end
