defmodule Accord.GetDecisionVersionTest do
  use ExUnit.Case
  use Accord.RepoCase
  use Wormwood.GQLCase

  import Random

  alias Accord.{Repo, People, PersonPeople, Decision, DecisionRole}

  setup do
    %{person: person} = create_accord_user!()

    {:ok, people} = People.new({:accord, "DecisionVersion GQL group"}) |> Repo.insert()

    PersonPeople.associate(person, people) |> Repo.commit()

    {:ok,
     %{
       decision: decision,
       decision_version: _version
     }} =
      Decision.create_with_version(
        %{
          language: "en",
          title: "Decision title",
          body: """
            Descended from astronomers concept of the number one network of wormholes astonishment vastness is bearable only through love Jean-François Champollion? Tunguska event stirred by starlight permanence of the stars two ghostly white figures in coveralls and helmets are soflty dancing bits of moving fluff realm of the galaxies. The only home we've ever known encyclopaedia galactica hundreds of thousands permanence of the stars something incredible is waiting to be known a mote of dust suspended in a sunbeam.
          """,
          state: Accord.DecisionVersion.Types.new(:str),
          settings: %{
            quorum: "twothirds",
            method: "plurality"
          }
        }
        |> Map.put(:service_id, Ecto.UUID.generate())
        |> Map.put(:options, [
          %{title: "sushi"},
          %{title: "bratwurst"},
          %{title: "enigma"}
        ])
        |> Map.put(:author, person)
        |> Map.put(:state, Accord.DecisionVersion.Types.published(:str))
      )
      |> Repo.commit()

    DecisionRole.add_to_decision(decision, people, DecisionRole.Types.votable(:str))
    |> Repo.commit()

    {:ok, requester_uuid: person.uuid}
  end

  load_gql(AccordWeb.Schema, "test/gql/GetDecisionVersions.gql")

  @tag :gql
  @tag :decisions
  test "Gets DecisionVersions", context do
    assert {:ok, query_payload} = query_gql(context: %{person_uuid: context[:requester_uuid]})

    assert "sushi" ==
             get_in(query_payload, [:data, "decisionVersions", "unvoted"])
             |> List.first()
             |> Map.get("options")
             |> List.first()
             |> Map.get("title")
  end
end
