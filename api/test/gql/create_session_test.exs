defmodule Accord.CreateSessionTest do
  use ExUnit.Case
  use Accord.RepoCase
  use Wormwood.GQLCase

  alias Accord.{Repo, Person}

  setup do
    {:ok, _} =
      Person.assure_with_persona({:accord, "julia@julia.julia", "sMfWaW8269NBEjYG89ejsra7"})
      |> Repo.commit()

    {:ok, email: "julia@julia.julia"}
  end

  load_gql(AccordWeb.Schema, "test/gql/CreateSession.gql")

  @tag :gql
  @tag :sessions
  test "Creates a Session", context do
    assert {:ok, query_payload} =
             query_gql(
               variables: %{
                 "email" => context[:email],
                 "pw" => "sMfWaW8269NBEjYG89ejsra7"
               },
               context: %{}
             )

    assert "accord︰" <> context[:email] ===
             get_in(query_payload, [:data, "createSession", "persona", "serviceId"])
  end

  @tag :gql
  @tag :sessions
  test "Won't accept a wrong password", context do
    assert {:ok, query_payload} =
             query_gql(
               variables: %{
                 "email" => context[:email],
                 "pw" => "r4pBTfdGdFyapwh7QfcQmacx"
               },
               context: %{}
             )

    refute "accord︰" <> context[:email] ===
             get_in(query_payload, [:data, "createSession", "persona", "serviceId"])
  end
end
