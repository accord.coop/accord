defmodule Accord.CreatePersonTest do
  use ExUnit.Case
  use Accord.RepoCase
  use Wormwood.GQLCase

  load_gql(AccordWeb.Schema, "test/gql/CreatePerson.gql")

  @tag :gql
  @tag :persons
  test "Creates (registers) a Person" do
    assert {:ok, query_payload} = query_gql(variables: %{}, context: %{})

    assert "accord︰sandra@sandra.sandra" ===
             get_in(query_payload, [:data, "createPerson", "persona", "serviceId"])
  end
end
