defmodule Accord.GetPersonTest do
  use ExUnit.Case
  use Accord.RepoCase
  use Wormwood.GQLCase

  alias Accord.{Repo, People, PersonPeople, Person}

  setup do
    {:ok, %{person: julia}} =
      Person.assure_with_persona({:accord, "julia@julia.julia", "BVRHzeLWrKsbSUM5US5472mm"})
      |> Repo.commit()

    {:ok, %{people: party}} = People.create({:accord, "Techno party"}) |> Repo.commit()
    {:ok, %{people: circle}} = People.create({:accord, "Quilting circle"}) |> Repo.commit()

    {:ok, _assoc} = PersonPeople.associate(julia, party) |> Repo.commit()
    {:ok, _assoc} = PersonPeople.associate(julia, circle) |> Repo.commit()

    {:ok, uuid: julia.uuid}
  end

  load_gql(AccordWeb.Schema, "test/gql/GetPerson.gql")

  @tag :gql
  @tag :persons
  test "Gets a Person", context do
    assert {:ok, err_payload} = query_gql(variables: %{"uuid" => context[:uuid]}, context: %{})
    assert List.first(err_payload.errors).message == "not_found"

    assert {:ok, query_payload} =
             query_gql(
               variables: %{"uuid" => context[:uuid]},
               context: %{person_uuid: context[:uuid]}
             )

    assert "accord︰julia@julia.julia" ==
             get_in(query_payload, [:data, "person", "personas"])
             |> List.first()
             |> get_in(["serviceId"])

    assert 2 === length(get_in(query_payload, [:data, "person", "peoples"]))
  end
end
