defmodule Accord.GetPeopleTest do
  use ExUnit.Case
  use Accord.RepoCase
  use Wormwood.GQLCase

  alias Accord.{Repo, People, Person, PersonPeople}

  setup do
    {:ok, %{people: club}} = People.create({:accord, "Book club"}) |> Repo.commit()

    {:ok, %{person: renault}} =
      Person.assure_with_persona({:accord, "reno@reno.reno", "p3PZdkcZwpgg3HY6HgEQWQyf"})
      |> Repo.commit()

    {:ok, %{person: patricia}} =
      Person.assure_with_persona(
        {:accord, "patricia@patricia.patricia", "ZQVx8QcedrLncWBFbEqxHnCP"}
      )
      |> Repo.commit()

    {:ok, _assoc} = PersonPeople.associate(renault, club) |> Repo.commit()
    {:ok, _assoc} = PersonPeople.associate(patricia, club) |> Repo.commit()
    {:ok, people_uuid: club.uuid, person_uuid: renault.uuid}
  end

  load_gql(AccordWeb.Schema, "test/gql/GetPeople.gql")

  @tag :gql
  @tag :peoples
  test "Gets a People", context do
    assert {:ok, err_payload} =
             query_gql(variables: %{"uuid" => context[:people_uuid]}, context: %{})

    assert List.first(err_payload.errors).message === "not_found"

    assert {:ok, query_payload} =
             query_gql(
               variables: %{"uuid" => context[:people_uuid]},
               context: %{person_uuid: context[:person_uuid]}
             )

    assert "Book club" == get_in(query_payload, [:data, "people", "title"])
    assert 2 == length(get_in(query_payload, [:data, "people", "persons"]))
  end
end
