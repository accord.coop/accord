defmodule Accord.AuthOntologyTest do
  alias Accord.{Repo, Session}

  import Ecto.Query, only: [{:from, 2}]

  use Accord.RepoCase

  @tag :sessions
  @tag :crud
  test "CREATE Session" do
    %{persona: persona} = create_accord_user!()

    expires = NaiveDateTime.add(NaiveDateTime.utc_now(), 1, :second)

    assert {:ok, %Session{}} = Session.new(persona, expires, "test_token") |> Repo.insert()
  end
end
