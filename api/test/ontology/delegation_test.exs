defmodule Accord.DelegationOntologyTest do
  alias Accord.{
    Repo,
    Decision,
    Delegation,
    People,
    Errors,
    PeoplePeople,
    PersonPeople,
    Vote,
    DecisionRole
  }

  use Accord.RepoCase

  @default_expiry Repo.now() |> NaiveDateTime.add(6_000, :second)

  @tag :decisions
  @tag :delegations
  @tag :crud
  test "CREATE Delegation" do
    err_circular = Errors.circular(:str)
    err_bad_args = Errors.bad_args(:str)

    %{person: person1} = create_slack_user!()
    %{person: person2} = create_slack_user!()
    %{person: person3} = create_slack_user!()

    {:ok, %{team: team1}} =
      People.assure(:team, {:slack, "TR4XMERCG"},
        title: "Liquid democracy 1",
        app_token: Ecto.UUID.generate()
      )
      |> Repo.commit()

    {:ok, %{team: team2}} =
      People.assure(:team, {:slack, "TX4XMERCG"},
        title: "Liquid democracy 2",
        app_token: Ecto.UUID.generate()
      )
      |> Repo.commit()

    {:ok, %{team: team3}} =
      People.assure(:team, {:slack, "TR4XMERCX"},
        title: "Liquid democracy 3",
        app_token: Ecto.UUID.generate()
      )
      |> Repo.commit()

    assert {:ok, %Delegation{expires: %NaiveDateTime{}}} =
             Delegation.new(
               person1,
               person2,
               team1,
               expires: @default_expiry
             )
             |> Repo.insert()

    # Precedence automatically increases within the same delegator

    assert {:ok, %Delegation{expires: %NaiveDateTime{}}} =
             Delegation.new(
               person1,
               person3,
               team2,
               expires: @default_expiry
             )
             |> Repo.insert()

    # Refuse to create circular delegation

    assert {:error, %{errors: [delegatee: {^err_circular, []}]}} =
             Delegation.new(
               person2,
               person1,
               team1,
               expires: @default_expiry
             )
             |> Repo.insert()

    # Respects expiry

    expiry = Repo.now() |> NaiveDateTime.add(100, :second)

    assert {:ok, %Delegation{expires: ^expiry, check: check1} = delegation} =
             Delegation.new(
               person1,
               person3,
               team3,
               expires: expiry
             )
             |> Repo.insert()

    # Refuse to create overlapping delegation

    assert {:error, %{errors: [scope_people: {^err_bad_args, []}]}} =
             Delegation.new(
               person1,
               person2,
               team3,
               expires: @default_expiry
             )
             |> Repo.insert()

    # Can be cancelled

    assert {:ok, %Delegation{expires: new_expiry, check: check2}} =
             delegation |> Delegation.end_now() |> Repo.update()

    assert NaiveDateTime.diff(new_expiry, Repo.now()) < 1
    assert check1 != check2
  end

  @tag :decisions
  @tag :delegations
  @tag :crud
  test "Resolve Delegations" do
    %{person: person1} = create_slack_user!()
    %{person: person2} = create_slack_user!()
    %{person: person3} = create_slack_user!()

    {:ok, %{team: slack_team, channel: slack_channel}} =
      People.assure_associated({:slack, "TR4XM3RQG", "C7UE889MOX"},
        team_opts: [
          app_token: Ecto.UUID.generate(),
          title: "Slack team"
        ],
        channel_opts: [
          title: "Slack channel"
        ]
      )
      |> Repo.commit()

    {:ok, %{group: accord_group}} =
      People.assure(:group, {:accord, "Accord group"}, title: "Accord group")
      |> Repo.commit()

    {:ok, %{assoc: _}} = PeoplePeople.associate(slack_channel, accord_group) |> Repo.commit()

    {:ok, %{assoc: _}} = PersonPeople.associate(person1, slack_channel) |> Repo.commit()
    {:ok, %{assoc: _}} = PersonPeople.associate(person2, slack_channel) |> Repo.commit()
    {:ok, %{assoc: _}} = PersonPeople.associate(person3, accord_group) |> Repo.commit()

    votable_persons = MapSet.new([person1.uuid, person2.uuid, person3.uuid])

    # A simple, direct delegation works as expected

    assert {:ok, %{delegation: delegation_1}} =
             Delegation.create(
               person1,
               person2,
               slack_channel,
               expires: @default_expiry
             )
             |> Repo.commit()

    %{decision: decision_1, decision_version: version_1} = create_decision!(%{author: person2})

    DecisionRole.add_to_decision(decision_1, slack_channel, DecisionRole.Types.votable(:str))
    |> Repo.commit()

    first_option_uuid_1 = version_1.options |> List.first() |> Map.get(:id)
    first_option_choice_1 = %{first_option_uuid_1 => %{"value" => 1.0}}

    {:ok, %{vote: _}} = Vote.create(first_option_choice_1, person2, version_1) |> Repo.commit()

    assert %{breakdown: %{^first_option_uuid_1 => %{score: 2.0}}, n_delegations: 1.0, n_votes: 1} =
             version_1 |> Decision.count(3, votable_persons)

    # A delegation for a broader People that is a parent of the DV's People applies
    # when a more local delegation is not present.

    delegation_1 |> Repo.delete()

    assert {:ok, %{delegation: _}} =
             Delegation.create(
               person2,
               person1,
               slack_team,
               expires: @default_expiry
             )
             |> Repo.commit()

    %{decision: decision_2, decision_version: version_2} = create_decision!(%{author: person2})

    DecisionRole.add_to_decision(decision_2, slack_channel, DecisionRole.Types.votable(:str))
    |> Repo.commit()

    first_option_uuid_2 = version_2.options |> List.first() |> Map.get(:id)
    first_option_choice_2 = %{first_option_uuid_2 => %{"value" => 1.0}}

    {:ok, %{vote: _}} = Vote.create(first_option_choice_2, person1, version_2) |> Repo.commit()

    assert %{breakdown: %{^first_option_uuid_2 => %{score: 2.0}}, n_delegations: 1.0, n_votes: 1} =
             version_2 |> Decision.count(3, votable_persons)
  end
end
