defmodule Accord.DecisionOntologyTest do
  alias Accord.{
    Repo,
    Decision,
    DecisionVersion,
    DecisionVersionMoment,
    DecisionRole,
    Delegation,
    Option,
    Vote,
    People,
    Person,
    PersonPeople,
    Schedule
  }

  alias Accord.DecisionSettings.Types, as: DSTypes

  use Accord.RepoCase

  @tag :decisions
  @tag :crud
  test "CREATE and GET Decision with options" do
    {:ok, %{person: author}} =
      Person.assure_with_persona({:slack, "TRXQXERCG", "UXWSYHTA5"}) |> Repo.commit()

    assert %{decision: decision, decision_version: version} = create_decision!(%{author: author})

    assert Repo.preload(decision, :versions)
           |> Map.get(:versions)
           |> List.first()
           |> Map.get(:body) === version.body

    assert version.options |> length() >= 2
  end

  @tag :decisions
  @tag :crud
  test "CREATE and GET Decision without options" do
    %{person: author} = create_slack_user!()

    assert %{decision: decision, decision_version: version} =
             create_decision!(%{author: author, options: []})

    assert Repo.preload(decision, :versions)
           |> Map.get(:versions)
           |> List.first()
           |> Map.get(:body) === version.body

    assert version.options |> length() === 0
  end

  @tag :decisions
  @tag :crud
  test "PUT Decision options" do
    %{person: author} = create_slack_user!()

    assert %{decision: _, decision_version: version} = create_decision!(%{author: author})

    assert {:ok, %DecisionVersion{} = new_version} =
             DecisionVersion.change_options(version, [%{title: "Yes"}, %{title: "No"}])
             |> Repo.update()

    assert new_version.options |> List.first() |> Map.get(:title) === "Yes"
  end

  @tag :decisions
  @tag :schedule
  @tag :timed
  @tag :crud
  test "Decision will close if you wait for it" do
    %{person: author} = create_accord_user!()
    {:ok, people} = People.new({:accord, "Death panel"}) |> Repo.insert()
    PersonPeople.associate(author, people) |> Repo.commit()

    %{decision: decision, decision_version: version} =
      create_decision!(%{
        author: author,
        service_id: "accord︰#{Ecto.UUID.generate()}",
        state: DecisionVersion.Types.published(:str)
      })

    DecisionRole.add_to_decision(decision, people, DecisionRole.Types.votable(:str))
    |> Repo.commit()

    {:ok, %{decision_version_moment: dv_moment}} =
      DecisionVersionMoment.create(
        version,
        Repo.now() |> NaiveDateTime.add(1, :second),
        DecisionVersionMoment.Types.close(:str)
      )
      |> Repo.commit()

    initial_size = Schedule.stack_size()

    Schedule.on_moment_created(dv_moment)

    assert Schedule.stack_size() === initial_size + 1

    Ecto.Adapters.SQL.Sandbox.allow(
      Repo,
      self(),
      Schedule.Item.via(dv_moment.uuid)
    )

    assert DecisionVersion.get_by_service_id(version.service_id) |> Map.get(:state) ===
             DecisionVersion.Types.published(:str)

    # 1000ms is the scheduled time
    Process.sleep(1_000)

    assert DecisionVersion.get_by_service_id(version.service_id) |> Map.get(:state) ===
             DecisionVersion.Types.closed(:str)

    # 200ms is the Shutdown time for Schedule items
    Process.sleep(200)
    assert Schedule.stack_size() === initial_size
  end

  @tag :decisions
  @tag :crud
  test "Only Persons in votable Peoples can vote on a Decision" do
    %{person: person_1} = create_accord_user!()
    %{person: person_2} = create_accord_user!()
    %{person: person_3} = create_accord_user!()
    %{person: outsider_person} = create_accord_user!()

    {:ok, people} = People.new({:accord, "Quorum group"}) |> Repo.insert()

    PersonPeople.associate(person_1, people) |> Repo.commit()
    PersonPeople.associate(person_2, people) |> Repo.commit()
    PersonPeople.associate(person_3, people) |> Repo.commit()

    %{
      decision: decision,
      decision_version: %DecisionVersion{uuid: decision_version_uuid} = version
    } = create_decision!(%{author: person_1, state: DecisionVersion.Types.published(:str)})

    DecisionRole.add_to_decision(decision, people, DecisionRole.Types.votable(:str))
    |> Repo.commit()

    first_option_choice = %{
      (version.options |> List.first() |> Map.get(:id)) => %{"value" => 1.0}
    }

    _last_option_choice = %{(version.options |> List.last() |> Map.get(:id)) => %{"value" => 1.0}}

    assert {:ok, %{vote: _}} =
             Vote.create(first_option_choice, person_1, version)
             |> Repo.commit()

    assert {:error, _, _, _} =
             Vote.create(first_option_choice, outsider_person, version)
             |> Repo.commit()

    assert %{unvoted: [], voted: [%DecisionVersion{uuid: ^decision_version_uuid}]} =
             DecisionRole.votable_decision_versions_for_person(
               person_1.uuid,
               DecisionVersion.Types.published(:str)
             )
  end

  @tag :decisions
  @tag :decision_summaries
  @tag :crud
  test "Decision summaries on plurality/twothirds decisions work as expected" do
    %{person: person_1} = create_accord_user!()
    %{person: person_2} = create_accord_user!()
    %{person: person_3} = create_accord_user!()
    {:ok, people} = People.new({:accord, "Quorum group"}) |> Repo.insert()
    PersonPeople.associate(person_1, people) |> Repo.commit()
    PersonPeople.associate(person_2, people) |> Repo.commit()
    PersonPeople.associate(person_3, people) |> Repo.commit()
    votable_persons = MapSet.new([person_1.uuid, person_2.uuid, person_3.uuid])

    %{decision: decision, decision_version: version} =
      create_decision!(%{
        author: person_1,
        service_id: "accord︰#{Ecto.UUID.generate()}",
        state: DecisionVersion.Types.published(:str),
        settings: %{
          method: DSTypes.plurality(:str),
          # Two-thirds quorum
          quorum_quantification: DSTypes.proportional(:str),
          quorum_value: 2 / 3,
          # Two-thirds majority
          parameter0_quantification: DSTypes.proportional(:str),
          parameter0_value: 2 / 3
        }
      })

    DecisionRole.add_to_decision(decision, people, DecisionRole.Types.votable(:str))
    |> Repo.commit()

    %Option{id: first_option_id} = version.options |> List.first()

    assert %{breakdown: %{}, winning_option_uuids: []} =
             version |> Decision.count(3, votable_persons)

    assert {:ok, %{vote: _}} =
             Vote.create(%{first_option_id => %{"value" => 1.0}}, person_1, version)
             |> Repo.commit()

    assert %{breakdown: %{^first_option_id => %{score: 1.0}}, winning_option_uuids: []} =
             version |> Decision.count(3, votable_persons)

    assert {:ok, %{vote: _}} =
             Vote.create(%{}, person_3, version)
             |> Repo.commit()

    assert %{breakdown: %{^first_option_id => %{score: 1.0}}, winning_option_uuids: []} =
             version |> Decision.count(3, votable_persons)

    assert {:ok, %{vote: _}} =
             Vote.create(%{first_option_id => %{"value" => 1.0}}, person_2, version)
             |> Repo.commit()

    assert %{
             breakdown: %{^first_option_id => %{score: 2.0}},
             winning_option_uuids: [^first_option_id]
           } = version |> Decision.count(3, votable_persons)
  end

  @tag :decisions
  @tag :decision_summaries
  @tag :crud
  test "Decision summaries on approval decisions work as expected" do
    %{person: person_1} = create_accord_user!()
    %{person: person_2} = create_accord_user!()
    %{person: person_3} = create_accord_user!()
    {:ok, people} = People.new({:accord, "Quorum group"}) |> Repo.insert()
    PersonPeople.associate(person_1, people) |> Repo.commit()
    PersonPeople.associate(person_2, people) |> Repo.commit()
    PersonPeople.associate(person_3, people) |> Repo.commit()
    votable_persons = MapSet.new([person_1.uuid, person_2.uuid, person_3.uuid])

    %{decision: decision, decision_version: version} =
      create_decision!(%{
        author: person_1,
        service_id: "accord︰#{Ecto.UUID.generate()}",
        state: DecisionVersion.Types.published(:str),
        settings: %{
          method: DSTypes.cardinal(:str),
          resolution: DSTypes.sum(:str),
          parameter2_quantification: DSTypes.absolute(:str),
          parameter2_value: 0.0,
          # Two-thirds quorum
          quorum_quantification: DSTypes.proportional(:str),
          quorum_value: 2 / 3
        }
      })

    DecisionRole.add_to_decision(decision, people, DecisionRole.Types.votable(:str))
    |> Repo.commit()

    %Option{id: first_option_id} = version.options |> List.first()
    %Option{id: last_option_id} = version.options |> List.last()

    assert %{breakdown: %{}, winning_option_uuids: []} =
             version |> Decision.count(3, votable_persons)

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{first_option_id => %{"value" => 1.0}, last_option_id => %{"value" => 1.0}},
               person_1,
               version
             )
             |> Repo.commit()

    assert %{
             breakdown: %{^first_option_id => %{score: 1.0}, ^last_option_id => %{score: 1.0}},
             winning_option_uuids: []
           } = version |> Decision.count(3, votable_persons)

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{},
               person_3,
               version
             )
             |> Repo.commit()

    assert %{
             breakdown: %{^first_option_id => %{score: 1.0}, ^last_option_id => %{score: 1.0}},
             winning_option_uuids: two_two_option_uuids_1
           } = version |> Decision.count(3, votable_persons)

    assert first_option_id in two_two_option_uuids_1
    assert last_option_id in two_two_option_uuids_1

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{first_option_id => %{"value" => 1.0}, last_option_id => %{"value" => 1.0}},
               person_2,
               version
             )
             |> Repo.commit()

    assert %{
             breakdown: %{^first_option_id => %{score: 2.0}, ^last_option_id => %{score: 2.0}},
             winning_option_uuids: two_two_option_uuids_2
           } = version |> Decision.count(3, votable_persons)

    assert first_option_id in two_two_option_uuids_2
    assert last_option_id in two_two_option_uuids_2

    assert {:ok, %{vote: _}} =
             Vote.create(%{last_option_id => %{"value" => 1.0}}, person_3, version)
             |> Repo.commit()

    assert %{winning_option_uuids: [^last_option_id]} =
             version |> Decision.count(3, votable_persons)
  end

  @tag :decisions
  @tag :decision_summaries
  @tag :delegations
  @tag :crud
  test "\`parameter0\` works as expected" do
    %{person: person_1} = create_accord_user!()
    %{person: person_2} = create_accord_user!()
    %{person: person_3} = create_accord_user!()
    {:ok, people} = People.new({:accord, "Min score group"}) |> Repo.insert()
    PersonPeople.associate(person_1, people) |> Repo.commit()
    PersonPeople.associate(person_2, people) |> Repo.commit()
    PersonPeople.associate(person_3, people) |> Repo.commit()
    votable_persons = MapSet.new([person_1.uuid, person_2.uuid, person_3.uuid])

    # Proportional parameter0

    %{decision: plurality_decision, decision_version: plurality_version} =
      create_decision!(%{
        author: person_1,
        service_id: "accord︰#{Ecto.UUID.generate()}",
        state: DecisionVersion.Types.published(:str),
        settings: %{
          method: DSTypes.plurality(:str),
          # Two-thirds majority
          parameter0_quantification: DSTypes.proportional(:str),
          parameter0_value: 2 / 3
        }
      })

    DecisionRole.add_to_decision(plurality_decision, people, DecisionRole.Types.votable(:str))
    |> Repo.commit()

    %Option{id: plurality_first_option_id} = plurality_version.options |> List.first()
    %Option{id: plurality_last_option_id} = plurality_version.options |> List.last()

    assert %{breakdown: %{}, winning_option_uuids: []} =
             plurality_version |> Decision.count(3, votable_persons)

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{plurality_first_option_id => %{"value" => 1.0}},
               person_2,
               plurality_version
             )
             |> Repo.commit()

    # First option wins because it has all of the plurality
    assert %{
             breakdown: %{^plurality_first_option_id => %{score: 1.0}},
             winning_option_uuids: [^plurality_first_option_id]
           } = plurality_version |> Decision.count(3, votable_persons)

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{plurality_last_option_id => %{"value" => 1.0}},
               person_3,
               plurality_version
             )
             |> Repo.commit()

    # No winners because both options have less than 2/3 plurality.
    assert %{
             breakdown: %{
               ^plurality_first_option_id => %{score: 1.0},
               ^plurality_last_option_id => %{score: 1.0}
             },
             winning_option_uuids: []
           } = plurality_version |> Decision.count(3, votable_persons)

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{plurality_first_option_id => %{"value" => 1.0}},
               person_1,
               plurality_version
             )
             |> Repo.commit()

    # First option wins because it has 2/3 of the plurality
    assert %{
             breakdown: %{^plurality_first_option_id => %{score: 2.0}},
             winning_option_uuids: [^plurality_first_option_id]
           } = plurality_version |> Decision.count(3, votable_persons)

    # Absolute parameter0

    %{decision: cardinal_decision, decision_version: cardinal_version} =
      create_decision!(%{
        author: person_1,
        service_id: "accord︰#{Ecto.UUID.generate()}",
        state: DecisionVersion.Types.published(:str),
        settings: %{
          method: DSTypes.cardinal(:str),
          resolution: DSTypes.sum(:str),
          parameter2_quantification: DSTypes.absolute(:str),
          parameter2_value: 0.0,
          # Two-thirds majority
          parameter0_quantification: DSTypes.absolute(:str),
          parameter0_value: 2.8
        }
      })

    DecisionRole.add_to_decision(cardinal_decision, people, DecisionRole.Types.votable(:str))
    |> Repo.commit()

    %Option{id: first_option_id} = cardinal_version.options |> List.first()
    %Option{id: last_option_id} = cardinal_version.options |> List.last()

    assert %{breakdown: %{}, winning_option_uuids: []} =
             cardinal_version |> Decision.count(3, votable_persons)

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{first_option_id => %{"value" => 1.0}, last_option_id => %{"value" => 1.0}},
               person_2,
               cardinal_version
             )
             |> Repo.commit()

    assert %{
             breakdown: %{^first_option_id => %{score: 1.0}, ^last_option_id => %{score: 1.0}},
             winning_option_uuids: []
           } = cardinal_version |> Decision.count(3, votable_persons)

    assert {:ok, %{delegation: _}} =
             Delegation.create(
               person_3,
               person_2,
               people,
               expires: Repo.now() |> NaiveDateTime.add(100, :second)
             )
             |> Repo.commit()

    assert %{
             breakdown: %{^first_option_id => %{score: 2.0}, ^last_option_id => %{score: 2.0}},
             winning_option_uuids: []
           } = cardinal_version |> Decision.count(3, votable_persons)

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{last_option_id => %{"value" => 1.0}},
               person_1,
               cardinal_version
             )
             |> Repo.commit()

    # No options win until they have a score above 2.8.
    assert %{
             breakdown: %{^first_option_id => %{score: 2.0}, ^last_option_id => %{score: 3.0}},
             winning_option_uuids: [^last_option_id]
           } = cardinal_version |> Decision.count(3, votable_persons)
  end

  @tag :decisions
  @tag :decision_summaries
  @tag :crud
  test "\`n_winners\` works as expected" do
    %{person: person_1} = create_accord_user!()
    %{person: person_2} = create_accord_user!()
    %{person: person_3} = create_accord_user!()
    {:ok, people} = People.new({:accord, "Number of winners group"}) |> Repo.insert()
    PersonPeople.associate(person_1, people) |> Repo.commit()
    PersonPeople.associate(person_2, people) |> Repo.commit()
    PersonPeople.associate(person_3, people) |> Repo.commit()
    votable_persons = MapSet.new([person_1.uuid, person_2.uuid, person_3.uuid])

    %{decision: decision, decision_version: version} =
      create_decision!(
        %{
          author: person_1,
          service_id: "accord︰#{Ecto.UUID.generate()}",
          state: DecisionVersion.Types.published(:str),
          settings: %{
            method: DSTypes.cardinal(:str),
            resolution: DSTypes.sum(:str),
            parameter2_quantification: DSTypes.absolute(:str),
            parameter2_value: 0.0,
            # Two winners
            n_winners_quantification: DSTypes.absolute(:str),
            n_winners_value: 1.99,
            # Min 1 vote for clarity
            parameter0_quantification: DSTypes.absolute(:str),
            parameter0_value: 1
          }
        },
        min_n_opts: 3
      )

    DecisionRole.add_to_decision(decision, people, DecisionRole.Types.votable(:str))
    |> Repo.commit()

    %Option{id: first_option_id} = version.options |> Enum.at(0)
    %Option{id: second_option_id} = version.options |> Enum.at(1)
    %Option{id: third_option_id} = version.options |> Enum.at(2)

    assert %{breakdown: %{}, winning_option_uuids: []} =
             version |> Decision.count(3, votable_persons)

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{first_option_id => %{"value" => 1.0}},
               person_2,
               version
             )
             |> Repo.commit()

    # Only one option wins because it meets min score
    assert %{
             breakdown: %{^first_option_id => %{score: 1.0}},
             winning_option_uuids: [^first_option_id]
           } = version |> Decision.count(3, votable_persons)

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{first_option_id => %{"value" => 1.0}, second_option_id => %{"value" => 1.0}},
               person_3,
               version
             )
             |> Repo.commit()

    # Both first and second options win
    assert %{
             breakdown: %{^first_option_id => %{score: 2.0}, ^second_option_id => %{score: 1.0}},
             winning_option_uuids: two_winners
           } = version |> Decision.count(3, votable_persons)

    assert first_option_id in two_winners
    assert second_option_id in two_winners

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{first_option_id => %{"value" => 1.0}, third_option_id => %{"value" => 1.0}},
               person_1,
               version
             )
             |> Repo.commit()

    # All three options win because the last two are tied
    assert %{
             breakdown: %{
               ^first_option_id => %{score: 3.0},
               ^second_option_id => %{score: 1.0},
               ^third_option_id => %{score: 1.0}
             },
             winning_option_uuids: three_winners
           } = version |> Decision.count(3, votable_persons)

    assert first_option_id in three_winners
    assert second_option_id in three_winners
    assert third_option_id in three_winners
  end

  @tag :decisions
  @tag :decision_summaries
  @tag :crud
  test "Consensus-style voting works as expected" do
    %{person: person_1} = create_accord_user!()
    %{person: person_2} = create_accord_user!()
    %{person: person_3} = create_accord_user!()
    {:ok, people} = People.new({:accord, "Consensus group"}) |> Repo.insert()
    PersonPeople.associate(person_1, people) |> Repo.commit()
    PersonPeople.associate(person_2, people) |> Repo.commit()
    PersonPeople.associate(person_3, people) |> Repo.commit()
    votable_persons = MapSet.new([person_1.uuid, person_2.uuid, person_3.uuid])

    %{decision: decision, decision_version: version} =
      create_decision!(%{
        author: person_1,
        service_id: "accord︰#{Ecto.UUID.generate()}",
        state: DecisionVersion.Types.published(:str),
        settings: %{
          method: DSTypes.cardinal(:str),
          resolution: DSTypes.sum(:str),
          parameter1_quantification: DSTypes.absolute(:str),
          parameter1_value: 2.0,
          parameter2_quantification: DSTypes.absolute(:str),
          parameter2_value: 1.0,
          parameter3_quantification: DSTypes.absolute(:str),
          parameter3_value: 1.0,
          parameter4_quantification: DSTypes.absolute(:str),
          parameter4_value: 1.0
        }
      })

    DecisionRole.add_to_decision(decision, people, DecisionRole.Types.votable(:str))
    |> Repo.commit()

    %Option{id: first_option_id} = version.options |> Enum.at(0)
    %Option{id: second_option_id} = version.options |> Enum.at(1)

    assert %{breakdown: %{}, winning_option_uuids: []} =
             version |> Decision.count(3, votable_persons)

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{first_option_id => %{"value" => 2.0}},
               person_2,
               version
             )
             |> Repo.commit()

    assert %{
             breakdown: %{^first_option_id => %{score: 2.0}},
             winning_option_uuids: [^first_option_id]
           } = version |> Decision.count(3, votable_persons)

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{first_option_id => %{"value" => 0.0}, second_option_id => %{"value" => 2.0}},
               person_3,
               version
             )
             |> Repo.commit()

    # Only the second option wins because the first option was vetoed.
    assert %{
             breakdown: %{
               ^first_option_id => %{score: :blocked},
               ^second_option_id => %{score: 3.0}
             },
             winning_option_uuids: [^second_option_id]
           } = version |> Decision.count(3, votable_persons)
  end
end
