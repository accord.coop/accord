defmodule Accord.HumanOntologyTest do
  alias Accord.{
    Repo,
    Person,
    Persona,
    People,
    PersonPeople,
    PeoplePeople
  }

  use Accord.RepoCase

  @tag :persons
  @tag :crud
  test "CREATE and GET Person" do
    # Rejects invalid emails
    {:error, :persona, _, _} =
      Person.assure_with_persona({:accord, "julia", "REMQyTbsdAnCphLLtg9fu8gn"}) |> Repo.commit()

    # Rejects simple passwords
    {:error, :persona, _, _} =
      Person.assure_with_persona({:accord, "julia@julia.julia", "julia"}) |> Repo.commit()

    # Accepts valid emails and valid passwords
    {:ok, %{person: julia}} =
      Person.assure_with_persona({:accord, "julia@julia.julia", "98vM5PyAhbYm6sLZafFCt4XV"})
      |> Repo.commit()

    assert Map.equal?(julia, Person.get(julia.uuid))
    assert Map.equal?(julia, Person.get_by_email("julia@julia.julia"))
  end

  @tag :peoples
  @tag :crud
  test "CREATE and GET People" do
    {:ok, club} = People.new({:accord, "Book club"}) |> Repo.insert()
    assert club
    assert Map.equal?(club, People.get(club.uuid))
  end

  @tag :persons
  @tag :peoples
  @tag :crud
  test "CREATE and DELETE Person-People associations" do
    %{person: sandra} = create_accord_user!()

    {:ok, team} = People.new({:accord, "Rugby team"}) |> Repo.insert()

    assert {:ok, %{assoc: _}} = PersonPeople.associate(sandra, team) |> Repo.commit()
    assert {:ok, %{assoc: assoc2}} = PersonPeople.associate(sandra, team) |> Repo.commit()
    assert assoc2.id === nil

    assert {:ok, _} = PersonPeople.dissociate(sandra, team) |> Repo.commit()
    assert {:ok, _} = PersonPeople.dissociate(sandra, team) |> Repo.commit()
  end

  @tag :persons
  @tag :peoples
  @tag :crud
  test "GET multiple associations" do
    %{person: sandra} = create_accord_user!()
    %{person: billy} = create_accord_user!()

    {:ok, cadre} = People.new({:accord, "Laser cadre"}) |> Repo.insert()
    {:ok, platoon} = People.new({:accord, "Kitten platoon"}) |> Repo.insert()

    PersonPeople.associate(sandra, cadre) |> Repo.commit()
    PersonPeople.associate(billy, cadre) |> Repo.commit()
    PersonPeople.associate(billy, platoon) |> Repo.commit()

    assert 2 == length(PersonPeople.get_persons_for_people(cadre))
    assert 1 == length(PersonPeople.get_persons_for_people(platoon))

    assert 2 == length(PersonPeople.get_peoples_for_person(billy))
    assert 1 == length(PersonPeople.get_peoples_for_person(sandra))
  end

  @tag :persons
  @tag :peoples
  @tag :crud
  test "CREATE and DELETE People-People associations" do
    {:ok, tribe} = People.new({:accord, "Duwamish tribe"}) |> Repo.insert()
    {:ok, committee} = People.new({:accord, "Committee of the treasury"}) |> Repo.insert()
    {:ok, board} = People.new({:accord, "Utilities board"}) |> Repo.insert()

    assert {:ok, %{assoc: %{id: assoc1_id}}} =
             PeoplePeople.associate(committee, tribe) |> Repo.commit()

    assert {:ok, %{assoc: %{id: ^assoc1_id}}} =
             PeoplePeople.associate(committee, tribe) |> Repo.commit()

    assert {:error, :assoc, :opposite_relation_found, _} =
             PeoplePeople.associate(tribe, committee) |> Repo.commit()

    assert {:ok, %{assoc: _assoc2}} = PeoplePeople.associate(board, tribe) |> Repo.commit()

    assert 2 == length(PeoplePeople.get_child_peoples(tribe))
    assert 1 == length(PeoplePeople.get_parent_peoples(board))
    assert 0 == length(PeoplePeople.get_parent_peoples(tribe))
    assert 0 == length(PeoplePeople.get_child_peoples(board))

    assert {:ok, %{assoc: _assoc3}} = PeoplePeople.dissociate(board, tribe) |> Repo.commit()
    assert {:ok, %{assoc: nil}} = PeoplePeople.dissociate(tribe, board) |> Repo.commit()
    assert {:ok, %{assoc: _assoc4}} = PeoplePeople.dissociate(tribe, committee) |> Repo.commit()
  end

  @tag :peoples
  @tag :slack
  @tag :crud
  test "CREATE Peoples representing Slack teams & channels" do
    assert {:ok, %{channel: channel}} =
             People.assure_associated({:slack, "TR4QXERCG", "CR6LSF22K"},
               channel_opts: [locale: %{accept_language: "en"}]
             )
             |> Repo.commit()

    assert {:ok, duplicate_channel} =
             People.new({:slack, "TR4QXERCG", "CR6LSF22K"}, locale: %{accept_language: "en"})
             |> Repo.insert(on_conflict: :nothing)

    assert channel.locale.accept_language === "en"
    assert duplicate_channel.locale.accept_language === "en"

    assert manually_gotten_channel = People.get_by_service_id("slack︰TR4QXERCG︰CR6LSF22K")
    assert manually_gotten_channel.service_type === "slack︰channel"
    assert channel === manually_gotten_channel

    assert conveniently_gotten_channel = People.get({:slack, "TR4QXERCG", "CR6LSF22K"})
    assert channel === conveniently_gotten_channel

    team = PeoplePeople.get_parent_peoples(conveniently_gotten_channel) |> List.first()
    assert team.service_id === "slack︰TR4QXERCG"
    assert team.service_type === "slack︰team"
  end

  @tag :personas
  @tag :persons
  @tag :crud
  test "CREATE Personas, associate with Persons, GET by uuid and by Person association" do
    assert {:ok, %{persona: slack_persona}} =
             Person.assure_with_persona({:slack, "TR4QXERCG", "UQWSYHTA5"}) |> Repo.commit()

    # Should get the correct Persona
    assert manually_gotten_persona = Persona.get_by_service_id("slack︰TR4QXERCG︰UQWSYHTA5")
    assert manually_gotten_persona.service_type === "slack︰user"
    assert slack_persona.uuid === manually_gotten_persona.uuid

    assert conveniently_gotten_persona = Persona.get({:slack, "TR4QXERCG", "UQWSYHTA5"})

    assert slack_persona.uuid === conveniently_gotten_persona.uuid

    assert slack_persona |> Repo.get_rel(:person) ===
             conveniently_gotten_persona |> Repo.get_rel(:person)
  end
end
