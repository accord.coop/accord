defmodule Accord.SecurityTest do
  alias Accord.{Repo, Person, Persona, Session}
  alias AccordWeb.Resolvers.Auth

  use Accord.RepoCase

  @tag :auth
  test "Set and redeem password reset token" do
    # Too weak a password should fail
    assert {:error, _, _, _} =
             Person.create_with_persona({:accord, "jim@jim.jim", "jim"}) |> Repo.commit()

    # All valid should work
    assert {:ok, _} =
             Person.create_with_persona({:accord, "jim@jim.jim", "u9yqksFyvFc8uVfvLjgvqqnJ"})
             |> Repo.commit()

    # Existing email should fail
    assert {:error, _, _, _} =
             Person.create_with_persona({:accord, "jim@jim.jim", "7U6bMsjYHsCaa2D4XqYACS4S"})
             |> Repo.commit()

    # Attempting to update password should fail with invalid token
    assert {:error, _} =
             Persona.get_by_service_id("accord︰jim@jim.jim")
             |> Persona.change_pw("", "ShT3hgqP89AEe7TY2GSucpQJ")
             |> Repo.update()

    # Logging in with existing password should succeed
    assert {:ok, %Session{}} =
             Auth.login(
               nil,
               %{email: "jim@jim.jim", pw: "u9yqksFyvFc8uVfvLjgvqqnJ"},
               nil
             )

    # Setting reset token should succeed
    assert {:ok, %Persona{native_credentials: %{pw_reset_token: token, pw_reset_token_expiry: _}}} =
             Persona.get_by_service_id("accord︰jim@jim.jim")
             |> Persona.change_pw_reset_token()
             |> Repo.update()

    # Should fail to update with a weak password
    assert {:error, _} =
             Persona.get_by_service_id("accord︰jim@jim.jim")
             |> Persona.change_pw(token, "jim")
             |> Repo.update()

    # Should successfully update password
    assert {:ok, _} =
             Persona.get_by_service_id("accord︰jim@jim.jim")
             |> Persona.change_pw(token, "bvmFTwbvYJG4X6f6Cu28cgvw")
             |> Repo.update()

    # Should fail to update before another reset
    assert {:error, _} =
             Persona.get_by_service_id("accord︰jim@jim.jim")
             |> Persona.change_pw(token, "7U6bMsjYHsCaa2D4XqYACS4S")
             |> Repo.update()

    # Logging in with old password should fail
    assert {:error, _} =
             Auth.login(
               nil,
               %{email: "jim@jim.jim", pw: "u9yqksFyvFc8uVfvLjgvqqnJ"},
               nil
             )

    # Logging in with new password should succeed
    assert {:ok, %Session{}} =
             Auth.login(
               nil,
               %{email: "jim@jim.jim", pw: "bvmFTwbvYJG4X6f6Cu28cgvw"},
               nil
             )
  end
end
