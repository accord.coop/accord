defmodule Accord.OrdinalDecisionOntologyTest do
  alias Accord.{
    Repo,
    Decision,
    DecisionVersion,
    DecisionRole,
    Option,
    Vote,
    People,
    PersonPeople
  }

  alias Accord.DecisionSettings.Types, as: DSTypes

  use Accord.RepoCase

  @tag :decisions
  @tag :decision_summaries
  @tag :ordinal
  @tag :crud
  test "STV-Scott basic scenario works" do
    %{person: person_1} = create_accord_user!()
    %{person: person_2} = create_accord_user!()
    %{person: person_3} = create_accord_user!()

    {:ok, people} = People.new({:accord, "Scott basic group"}) |> Repo.insert()

    PersonPeople.associate(person_1, people) |> Repo.commit()
    PersonPeople.associate(person_2, people) |> Repo.commit()
    PersonPeople.associate(person_3, people) |> Repo.commit()

    votable_persons = MapSet.new([person_1.uuid, person_2.uuid, person_3.uuid])

    %{decision: decision, decision_version: version} =
      create_decision!(
        %{
          author: person_1,
          service_id: "accord︰#{Ecto.UUID.generate()}",
          state: DecisionVersion.Types.published(:str),
          settings: %{
            method: DSTypes.ordinal_stv(:str),
            resolution: DSTypes.scott(:str),
            quota: DSTypes.droop(:str),
            # Five winners
            n_winners_quantification: DSTypes.absolute(:str),
            n_winners_value: 5.0,
            # Min 1 vote for clarity
            parameter0_quantification: DSTypes.absolute(:str),
            parameter0_value: 1.0
          }
        },
        min_n_opts: 10,
        max_n_opts: 10
      )

    DecisionRole.add_to_decision(decision, people, DecisionRole.Types.votable(:str))
    |> Repo.commit()

    assert %{breakdown: %{}, winning_option_uuids: []} =
             version |> Decision.count(3, votable_persons)

    %Option{id: second_option_id} = version.options |> Enum.at(1)

    # Ballots are indifferent to absolute value, only relative values within the ballot.
    # Single-option ballots are essentially plurality votes.

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{second_option_id => %{"value" => 0.0}},
               person_1,
               version
             )
             |> Repo.commit()

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{second_option_id => %{"value" => 1.0}},
               person_2,
               version
             )
             |> Repo.commit()

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{second_option_id => %{"value" => -69.69}},
               person_3,
               version
             )
             |> Repo.commit()

    # STV stops at 1.0 because it's enough to win and the ballots leave nothing to be redistributed.

    assert %{
             breakdown: %{^second_option_id => %{score: 1.0}},
             winning_option_uuids: [^second_option_id]
           } = version |> Decision.count(3, votable_persons)
  end

  @tag :decisions
  @tag :decision_summaries
  @tag :ordinal
  @tag :crud
  test "STV-Scott fruit scenario works" do
    persons = 1..20 |> Enum.map(fn _ -> create_accord_user!() |> Map.get(:person) end)

    {:ok, people} = People.new({:accord, "Scott fruit group"}) |> Repo.insert()

    persons |> Enum.each(fn person -> PersonPeople.associate(person, people) |> Repo.commit() end)

    votable_persons = persons |> Enum.map(fn %{uuid: uuid} -> uuid end) |> MapSet.new()

    %{decision: decision, decision_version: version} =
      create_decision!(%{
        author: persons |> List.first(),
        service_id: "accord︰#{Ecto.UUID.generate()}",
        state: DecisionVersion.Types.published(:str),
        settings: %{
          method: DSTypes.ordinal_stv(:str),
          resolution: DSTypes.scott(:str),
          quota: DSTypes.droop(:str),
          # Three winners
          n_winners_quantification: DSTypes.absolute(:str),
          n_winners_value: 3.0,
          # Min 1 vote for clarity
          parameter0_quantification: DSTypes.absolute(:str),
          parameter0_value: 1.0
        },
        options: [
          %{title: "orange"},
          %{title: "pear"},
          %{title: "chocolate"},
          %{title: "strawberry"},
          %{title: "burger"}
        ]
      })

    orange = version.options |> Enum.at(0) |> Map.get(:id)
    pear = version.options |> Enum.at(1) |> Map.get(:id)
    chocolate = version.options |> Enum.at(2) |> Map.get(:id)
    strawberry = version.options |> Enum.at(3) |> Map.get(:id)
    burger = version.options |> Enum.at(4) |> Map.get(:id)

    DecisionRole.add_to_decision(decision, people, DecisionRole.Types.votable(:str))
    |> Repo.commit()

    assert %{breakdown: %{}, winning_option_uuids: []} =
             version |> Decision.count(3, votable_persons)

    Repo.commit(
      (Enum.map(1..4, fn i ->
         Vote.create(
           %{orange => %{"value" => 0.0}},
           persons |> Enum.at(i - 1),
           version,
           name: :"vote_#{i}"
         )
       end) ++
         Enum.map(5..6, fn i ->
           Vote.create(
             %{
               pear => %{"value" => 0.0},
               orange => %{"value" => 1.0}
             },
             persons |> Enum.at(i - 1),
             version,
             name: :"vote_#{i}"
           )
         end) ++
         Enum.map(7..14, fn i ->
           Vote.create(
             %{
               chocolate => %{"value" => 0.0},
               strawberry => %{"value" => 1.0}
             },
             persons |> Enum.at(i - 1),
             version,
             name: :"vote_#{i}"
           )
         end) ++
         Enum.map(15..18, fn i ->
           Vote.create(
             %{
               chocolate => %{"value" => 0.0},
               burger => %{"value" => 1.0}
             },
             persons |> Enum.at(i - 1),
             version,
             name: :"vote_#{i}"
           )
         end) ++
         Enum.map(19..19, fn i ->
           Vote.create(
             %{
               strawberry => %{"value" => 0.0}
             },
             persons |> Enum.at(i - 1),
             version,
             name: :"vote_#{i}"
           )
         end) ++
         Enum.map(20..20, fn i ->
           Vote.create(
             %{
               burger => %{"value" => 0.0}
             },
             persons |> Enum.at(i - 1),
             version,
             name: :"vote_#{i}"
           )
         end))
      |> List.flatten()
    )

    assert %{
             breakdown: breakdown,
             winning_option_uuids: winning_option_uuids
           } = version |> Decision.count(3, votable_persons)

    assert orange in winning_option_uuids
    assert chocolate in winning_option_uuids
    assert strawberry in winning_option_uuids

    assert %{
             ^orange => %{score: 6.0},
             ^chocolate => %{score: 6.0},
             ^strawberry => %{score: 5.0},
             ^burger => %{score: 0.0}
           } = breakdown
  end
end
