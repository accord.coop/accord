defmodule Accord.StarDecisionTest do
  alias Accord.{
    Repo,
    Decision,
    DecisionVersion,
    DecisionRole,
    Option,
    Vote,
    People,
    PersonPeople
  }

  alias Accord.DecisionSettings.Types, as: DSTypes

  use Accord.RepoCase

  @star_settings %{
    method: DSTypes.cardinal(:str),
    resolution: DSTypes.star(:str),
    # Scores 0 to 5, default 0
    parameter1_quantification: DSTypes.absolute(:str),
    parameter1_value: 5.0,
    parameter2_quantification: DSTypes.absolute(:str),
    parameter2_value: 0.0,
    # No blocking:
    parameter3_quantification: DSTypes.absolute(:str),
    parameter3_value: 0.0,
    parameter4_quantification: DSTypes.absolute(:str),
    parameter4_value: 1.0
  }

  @tag :decisions
  @tag :star
  @tag :crud
  test "STAR voting works like score voting where condorcet preference is equivalent" do
    %{person: person_1} = create_accord_user!()
    %{person: person_2} = create_accord_user!()
    %{person: person_3} = create_accord_user!()
    {:ok, people} = People.new({:accord, "Star group"}) |> Repo.insert()
    PersonPeople.associate(person_1, people) |> Repo.commit()
    PersonPeople.associate(person_2, people) |> Repo.commit()
    PersonPeople.associate(person_3, people) |> Repo.commit()
    votable_persons = MapSet.new([person_1.uuid, person_2.uuid, person_3.uuid])

    %{decision: decision, decision_version: version} =
      create_decision!(%{
        author: person_1,
        service_id: "accord︰#{Ecto.UUID.generate()}",
        state: DecisionVersion.Types.published(:str),
        settings: @star_settings
      })

    DecisionRole.add_to_decision(decision, people, DecisionRole.Types.votable(:str))
    |> Repo.commit()

    %Option{id: option_id_1} = version.options |> Enum.at(0)
    %Option{id: option_id_2} = version.options |> Enum.at(1)

    assert %{breakdown: %{}, winning_option_uuids: []} =
             version |> Decision.count(3, votable_persons)

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{option_id_1 => %{"value" => 5.0}, option_id_2 => %{"value" => 1.0}},
               person_1,
               version
             )
             |> Repo.commit()

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{option_id_1 => %{"value" => 3.0}, option_id_2 => %{"value" => 2.0}},
               person_2,
               version
             )
             |> Repo.commit()

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{option_id_1 => %{"value" => 2.0}, option_id_2 => %{"value" => 1.0}},
               person_3,
               version
             )
             |> Repo.commit()

    assert %{
             breakdown: %{
               ^option_id_1 => %{
                 score: 10.0,
                 annotation: %{round: 0, contest: %{^option_id_1 => 3.0, ^option_id_2 => 0.0}}
               },
               ^option_id_2 => %{score: 4.0, annotation: nil}
             },
             winning_option_uuids: one_winner
           } = version |> Decision.count(3, votable_persons)

    assert option_id_1 in one_winner
  end

  @tag :decisions
  @tag :star
  @tag :crud
  test "STAR voting works unlike score voting where condorcet preference is not equivalent" do
    %{person: person_1} = create_accord_user!()
    %{person: person_2} = create_accord_user!()
    %{person: person_3} = create_accord_user!()
    {:ok, people} = People.new({:accord, "Star group"}) |> Repo.insert()
    PersonPeople.associate(person_1, people) |> Repo.commit()
    PersonPeople.associate(person_2, people) |> Repo.commit()
    PersonPeople.associate(person_3, people) |> Repo.commit()
    votable_persons = MapSet.new([person_1.uuid, person_2.uuid, person_3.uuid])

    %{decision: decision, decision_version: version} =
      create_decision!(%{
        author: person_1,
        service_id: "accord︰#{Ecto.UUID.generate()}",
        state: DecisionVersion.Types.published(:str),
        settings: @star_settings
      })

    DecisionRole.add_to_decision(decision, people, DecisionRole.Types.votable(:str))
    |> Repo.commit()

    %Option{id: option_id_1} = version.options |> Enum.at(0)
    %Option{id: option_id_2} = version.options |> Enum.at(1)

    assert %{breakdown: %{}, winning_option_uuids: []} =
             version |> Decision.count(3, votable_persons)

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{option_id_1 => %{"value" => 5.0}, option_id_2 => %{"value" => 1.0}},
               person_1,
               version
             )
             |> Repo.commit()

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{option_id_1 => %{"value" => 2.0}, option_id_2 => %{"value" => 3.0}},
               person_2,
               version
             )
             |> Repo.commit()

    assert {:ok, %{vote: _}} =
             Vote.create(
               %{option_id_1 => %{"value" => 1.0}, option_id_2 => %{"value" => 2.0}},
               person_3,
               version
             )
             |> Repo.commit()

    assert %{
             breakdown: %{
               ^option_id_1 => %{score: 8.0, annotation: nil},
               ^option_id_2 => %{
                 score: 6.0,
                 annotation: %{round: 0, contest: %{^option_id_2 => 2.0, ^option_id_1 => 1.0}}
               }
             },
             winning_option_uuids: one_winner
           } = version |> Decision.count(3, votable_persons)

    assert option_id_2 in one_winner
  end
end
