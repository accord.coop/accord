defmodule AccordWeb.TestRegistry do
  @moduledoc """
    Registers `gproc` process identifiers for all the servers.
  """

  defp via(term) do
    {:via, :gproc, {:n, :l, term}}
  end

  def passer(), do: via(:passer)
end

defmodule AccordWeb.PayloadPasser do
  use GenServer

  def start_link(parent_pid) do
    GenServer.start_link(__MODULE__, [parent_pid], name: AccordWeb.TestRegistry.passer())
  end

  def pass_payload(path, payload) do
    GenServer.cast(
      AccordWeb.TestRegistry.passer(),
      {:pass, path, payload}
    )
  end

  @impl true
  def init([parent_pid]) do
    {:ok,
     %{
       parent_pid: parent_pid
     }}
  end

  @impl true
  def handle_cast({:pass, path, payload}, state) do
    send(state.parent_pid, {path, payload})
    {:noreply, state}
  end
end
