defmodule Random do
  @moduledoc """
  Generates random things for tests.
  """
  @letters_Latn "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  @letters_Grek "ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩ"
  @letters_Cyrl "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
  @letters_Arab "ئابةتثجحخسشصضطظعغفقكلمنهوىي"
  @letters_Hebr "אבגדהוזחטיךכלםמןנסעףפץצקרשת"

  @numbers "0123456789"
  @emoji "⏹🇦🇶😍🙎🏽‍♀️"

  defp set(sets),
    do: Enum.reduce(sets, "", fn set, acc -> set <> acc end) |> String.split("", trim: true)

  defp all_chars(),
    do:
      set([
        @numbers,
        @emoji,
        @letters_Latn,
        String.downcase(@letters_Latn),
        @letters_Grek,
        String.downcase(@letters_Grek),
        @letters_Cyrl,
        String.downcase(@letters_Cyrl),
        @letters_Arab,
        @letters_Hebr
      ])

  def random_string(length), do: random_string(length, all_chars())
  def random_string(length, "" <> str), do: random_string(length, set([str]))

  def random_string(length, set) do
    1..length
    |> Enum.reduce([], fn _, acc -> [Enum.random(set) | acc] end)
    |> Enum.join("")
  end

  def random_bool(threshold \\ 0.5) do
    :rand.uniform() < threshold
  end

  def random_slack_id(prefix),
    do: prefix <> random_string(10, set([@letters_Latn, @numbers]))

  def random_email(),
    do:
      "#{random_string(12, @letters_Latn)}@#{random_string(8, @letters_Latn)}.#{
        random_string(2, @letters_Latn)
      }"

  def create_slack_user!() do
    {:ok, person_persona} =
      Accord.Person.assure_with_persona({:slack, random_slack_id("T"), random_slack_id("U")})
      |> Accord.Repo.commit()

    person_persona
  end

  def create_accord_user!() do
    {:ok, person_persona} =
      Accord.Person.assure_with_persona(
        {:accord, random_email(), random_string(16, set([@letters_Latn, @numbers]))}
      )
      |> Accord.Repo.commit()

    person_persona
  end

  def create_decision!(version_parameters, opts \\ []) do
    {:ok, create_decision_result} =
      Accord.Decision.create_with_version(
        %{
          language: "en",
          title: random_string(80),
          body: "#{random_string(240)}\n\n#{random_string(240)}",
          state: Accord.DecisionVersion.Types.new(:str),
          settings: %{
            method: Accord.DecisionSettings.Types.plurality(:str),
            quorum_quantification: Accord.DecisionSettings.Types.proportional(:str),
            quorum_value: 2 / 3
          },
          service_id: Ecto.UUID.generate(),
          options:
            0..Enum.random(
              (Keyword.get(opts, :min_n_opts, 2) - 1)..(Keyword.get(opts, :max_n_opts, 8) - 1)
            )
            |> Enum.map(fn _ -> %{title: random_string(12)} end)
        }
        |> Map.merge(version_parameters)
      )
      |> Accord.Repo.commit()

    create_decision_result
  end
end
