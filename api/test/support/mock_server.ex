defmodule AccordWeb.MockServer do
  use Plug.Router
  import Plug.Conn

  alias AccordSlack.Payloads

  plug Plug.Logger, log: :debug

  plug Plug.Parsers,
    parsers: [:json, :urlencoded],
    json_decoder: Jason

  plug :match
  plug :dispatch

  defp pass_payload(conn) do
    AccordWeb.PayloadPasser.pass_payload(conn.request_path, conn.params)
    conn
  end

  get "/ping" do
    conn |> pass_payload |> send_resp(200, "pong")
  end

  post "/commands/*_cmd" do
    conn |> pass_payload |> send_resp(200, "")
  end

  CR6LSF22K

  post "/views.publish" do
    conn
    |> pass_payload
    |> put_resp_header("content-type", "application/json")
    |> send_resp(200, Jason.encode!(%{"ok" => true, "view" => %{"id" => "V011M9W1GKF"}}))
  end

  post "/users.info" do
    conn
    |> pass_payload
    |> put_resp_header("content-type", "application/json")
    |> send_resp(
      200,
      Jason.encode!(Payloads.UserInfo.will())
    )
  end

  post "/conversations.info" do
    conn
    |> pass_payload
    |> put_resp_header("content-type", "application/json")
    |> send_resp(
      200,
      Jason.encode!(Payloads.ChannelInfo.bot_test())
    )
  end

  post "/conversations.members" do
    cursor =
      case conn.body_params |> Map.get("cursor") do
        "" <> str when byte_size(str) > 0 -> str
        _ -> nil
      end

    conn
    |> pass_payload
    |> put_resp_header("content-type", "application/json")
    |> send_resp(
      200,
      Jason.encode!(Payloads.ChannelMembers.paginated(cursor))
    )
  end

  post "/team.info" do
    conn
    |> pass_payload
    |> put_resp_header("content-type", "application/json")
    |> send_resp(
      200,
      Jason.encode!(Payloads.TeamInfo.accord())
    )
  end

  post "/views.open" do
    conn
    |> pass_payload
    |> put_resp_header("content-type", "application/json")
    |> send_resp(
      200,
      Jason.encode!(%{"ok" => true, "view" => %{"id" => "V011M9W1GKF"}})
    )
  end

  post "/views.update" do
    conn
    |> pass_payload
    |> put_resp_header("content-type", "application/json")
    |> send_resp(
      200,
      Jason.encode!(%{"ok" => true, "view" => %{"id" => "V011M9W1GKF"}})
    )
  end

  post "/chat.postMessage" do
    conn
    |> pass_payload
    |> put_resp_header("content-type", "application/json")
    |> send_resp(
      200,
      Jason.encode!(%{"ok" => true, "ts" => "240598250948.2945937", "channel" => "CR6LSF22K"})
    )
  end

  post "/chat.update" do
    conn
    |> pass_payload
    |> put_resp_header("content-type", "application/json")
    |> send_resp(
      200,
      Jason.encode!(%{"ok" => true, "ts" => "240598250948.2945937", "channel" => "CR6LSF22K"})
    )
  end

  post "/conversations.join" do
    conn
    |> pass_payload
    |> put_resp_header("content-type", "application/json")
    |> send_resp(
      200,
      Jason.encode!(%{"ok" => true, "channel" => %{"id" => "CR6LSF22K"}})
    )
  end
end
