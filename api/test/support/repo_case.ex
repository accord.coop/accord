defmodule Accord.RepoCase do
  use ExUnit.CaseTemplate

  using do
    quote do
      alias Accord.Repo

      import Ecto
      import Ecto.Query
      import Accord.RepoCase
      import Random
    end
  end

  setup tags do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(Accord.Repo)

    unless tags[:async] do
      Ecto.Adapters.SQL.Sandbox.mode(Accord.Repo, {:shared, self()})
    end

    :ok
  end
end
