defmodule AccordSlack.Payloads.ViewSubmission do
  def step_1 do
    %{
      "payload" =>
        Jason.encode!(%{
          "api_app_id" => "A01189AQQBH",
          "response_urls" => [],
          "team" => %{"domain" => "accord-cooperative", "id" => "TR4QXERCG"},
          "token" => "PdCBnWA1BVEjJNwbYNHRXN4i",
          "trigger_id" => "1056738213813.854847501424.2ae0039cb726925aa14d5329014ccd8b",
          "type" => "view_submission",
          "user" => %{
            "id" => "U011B1U8APR",
            "name" => "wsn",
            "team_id" => "TR4QXERCG",
            "username" => "wsn"
          },
          "view" => %{
            "app_id" => "A01189AQQBH",
            "app_installed_team_id" => "TR4QXERCG",
            "blocks" => [
              %{
                "block_id" => "decision_title",
                "element" => %{
                  "action_id" => "decision_title_input",
                  "type" => "plain_text_input"
                },
                "label" => %{
                  "emoji" => true,
                  "text" => "Question / Title",
                  "type" => "plain_text"
                },
                "optional" => false,
                "type" => "input"
              },
              %{
                "block_id" => "decision_body",
                "element" => %{
                  "action_id" => "decision_body_input",
                  "multiline" => true,
                  "type" => "plain_text_input"
                },
                "hint" => %{
                  "emoji" => true,
                  "text" => "Plain text or Markdown",
                  "type" => "plain_text"
                },
                "label" => %{
                  "emoji" => true,
                  "text" => "Description",
                  "type" => "plain_text"
                },
                "optional" => true,
                "type" => "input"
              }
            ],
            "bot_id" => "B010TFEEM7C",
            "callback_id" => "new_decision_step_1",
            "clear_on_close" => false,
            "close" => %{"emoji" => true, "text" => "Cancel", "type" => "plain_text"},
            "external_id" => "",
            "hash" => "1586543377.cb517cd1",
            "id" => "V011XJ9QQ3U",
            "notify_on_close" => true,
            "previous_view_id" => nil,
            "private_metadata" => "{\"n_options\":5,\"channel_id\":\"CR6LSF22K\"}",
            "root_view_id" => "V011XJ9QQ3U",
            "state" => %{
              "values" => %{
                "decision_body" => %{
                  "decision_body_input" => %{"type" => "plain_text_input"}
                },
                "decision_title" => %{
                  "decision_title_input" => %{
                    "type" => "plain_text_input",
                    "value" => "Payload test"
                  }
                },
                "decision_option_1" => %{
                  "decision_option_1_option" => %{
                    "type" => "plain_text_input",
                    "value" => "Red"
                  }
                },
                "decision_option_2" => %{
                  "decision_option_2_option" => %{
                    "type" => "plain_text_input",
                    "value" => "Green"
                  }
                },
                "decision_option_3" => %{
                  "decision_option_3_option" => %{
                    "type" => "plain_text_input",
                    "value" => ""
                  }
                },
                "decision_option_4" => %{
                  "decision_option_4_option" => %{
                    "type" => "plain_text_input",
                    "value" => "Blue"
                  }
                },
                "decision_option_5" => %{
                  "decision_option_4_option" => %{
                    "type" => "plain_text_input"
                  }
                }
              }
            },
            "submit" => %{"emoji" => true, "text" => "Next →", "type" => "plain_text"},
            "team_id" => "TR4QXERCG",
            "title" => %{
              "emoji" => true,
              "text" => "Start a new decision",
              "type" => "plain_text"
            },
            "type" => "modal"
          }
        })
    }
  end

  def step_2 do
    %{
      "payload" =>
        Jason.encode!(%{
          "api_app_id" => "A01189AQQBH",
          "response_urls" => [],
          "team" => %{"domain" => "accord-cooperative", "id" => "TR4QXERCG"},
          "token" => "PdCBnWA1BVEjJNwbYNHRXN4i",
          "trigger_id" => "1064242107381.854847501424.c5e1c343b4a94b111fcdf990ffe56d96",
          "type" => "view_submission",
          "user" => %{
            "id" => "U011B1U8APR",
            "name" => "wsn",
            "team_id" => "TR4QXERCG",
            "username" => "wsn"
          },
          "view" => %{
            "app_id" => "A01189AQQBH",
            "app_installed_team_id" => "TR4QXERCG",
            "blocks" => [
              %{
                "block_id" => "people_votable",
                "element" => %{
                  "action_id" => "people_votable_input",
                  "initial_channel" => "CR6LSF22K",
                  "placeholder" => %{
                    "emoji" => true,
                    "text" => "Select a channel",
                    "type" => "plain_text"
                  },
                  "type" => "channels_select"
                },
                "hint" => %{
                  "emoji" => true,
                  "text" => "Everyone in this conversation will be able to vote on this decision",
                  "type" => "plain_text"
                },
                "label" => %{
                  "emoji" => true,
                  "text" => "Post in",
                  "type" => "plain_text"
                },
                "optional" => false,
                "type" => "input"
              },
              %{
                "block_id" => "quorum",
                "element" => %{
                  "action_id" => "quorum_input",
                  "initial_option" => %{
                    "text" => %{
                      "emoji" => true,
                      "text" => "Anyone",
                      "type" => "plain_text"
                    },
                    "value" => "q_any"
                  },
                  "options" => [
                    %{
                      "text" => %{
                        "emoji" => true,
                        "text" => "Anyone",
                        "type" => "plain_text"
                      },
                      "value" => "q_any"
                    },
                    %{
                      "text" => %{
                        "emoji" => true,
                        "text" => "At least 50%",
                        "type" => "plain_text"
                      },
                      "value" => "q_half"
                    },
                    %{
                      "text" => %{
                        "emoji" => true,
                        "text" => "At least two-thirds",
                        "type" => "plain_text"
                      },
                      "value" => "q_twothirds"
                    },
                    %{
                      "text" => %{
                        "emoji" => true,
                        "text" => "Everyone",
                        "type" => "plain_text"
                      },
                      "value" => "q_all"
                    }
                  ],
                  "placeholder" => %{
                    "emoji" => true,
                    "text" => "Select how many votes this needs",
                    "type" => "plain_text"
                  },
                  "type" => "static_select"
                },
                "hint" => %{
                  "emoji" => true,
                  "text" => "The decision is only considered valid if it gets this many votes.",
                  "type" => "plain_text"
                },
                "label" => %{
                  "emoji" => true,
                  "text" => "Quorum",
                  "type" => "plain_text"
                },
                "optional" => false,
                "type" => "input"
              },
              %{
                "block_id" => "expiry",
                "element" => %{
                  "action_id" => "expiry_input",
                  "initial_option" => %{
                    "text" => %{
                      "emoji" => true,
                      "text" => "15 minutes",
                      "type" => "plain_text"
                    },
                    "value" => "c_900"
                  },
                  "options" => [
                    %{
                      "text" => %{
                        "emoji" => true,
                        "text" => "15 minutes",
                        "type" => "plain_text"
                      },
                      "value" => "c_900"
                    },
                    %{
                      "text" => %{
                        "emoji" => true,
                        "text" => "30 minutes",
                        "type" => "plain_text"
                      },
                      "value" => "c_1800"
                    },
                    %{
                      "text" => %{
                        "emoji" => true,
                        "text" => "1 hour",
                        "type" => "plain_text"
                      },
                      "value" => "c_3600"
                    },
                    %{
                      "text" => %{
                        "emoji" => true,
                        "text" => "2 hours",
                        "type" => "plain_text"
                      },
                      "value" => "c_7200"
                    },
                    %{
                      "text" => %{
                        "emoji" => true,
                        "text" => "4 hours",
                        "type" => "plain_text"
                      },
                      "value" => "c_14400"
                    },
                    %{
                      "text" => %{
                        "emoji" => true,
                        "text" => "8 hours",
                        "type" => "plain_text"
                      },
                      "value" => "c_28800"
                    },
                    %{
                      "text" => %{
                        "emoji" => true,
                        "text" => "16 hours",
                        "type" => "plain_text"
                      },
                      "value" => "c_57600"
                    },
                    %{
                      "text" => %{
                        "emoji" => true,
                        "text" => "24 hours",
                        "type" => "plain_text"
                      },
                      "value" => "c_86400"
                    },
                    %{
                      "text" => %{
                        "emoji" => true,
                        "text" => "2 days",
                        "type" => "plain_text"
                      },
                      "value" => "c_172800"
                    },
                    %{
                      "text" => %{
                        "emoji" => true,
                        "text" => "1 week",
                        "type" => "plain_text"
                      },
                      "value" => "c_604800"
                    },
                    %{
                      "text" => %{
                        "emoji" => true,
                        "text" => "never",
                        "type" => "plain_text"
                      },
                      "value" => "c_never"
                    }
                  ],
                  "placeholder" => %{
                    "emoji" => true,
                    "text" => "Select by how long from now",
                    "type" => "plain_text"
                  },
                  "type" => "static_select"
                },
                "hint" => %{
                  "emoji" => true,
                  "text" =>
                    "The decision will wrap-up when everyone who can vote does vote, or by this time, whichever is sooner.",
                  "type" => "plain_text"
                },
                "label" => %{
                  "emoji" => true,
                  "text" => "Close voting in…",
                  "type" => "plain_text"
                },
                "optional" => false,
                "type" => "input"
              }
            ],
            "bot_id" => "B010TFEEM7C",
            "callback_id" => "new_decision_step_2",
            "clear_on_close" => false,
            "close" => %{"emoji" => true, "text" => "← Back", "type" => "plain_text"},
            "external_id" => "",
            "hash" => "1586998088.d3dfcb45",
            "id" => "V011XQG11SP",
            "notify_on_close" => true,
            "previous_view_id" => "V011XJ9QQ3U",
            "private_metadata" => "{\"channel_id\":\"CR6LSF22K\"}",
            "root_view_id" => "V011XJ9QQ3U",
            "state" => %{
              "values" => %{
                "expiry" => %{
                  "expiry_input" => %{
                    "selected_option" => %{
                      "text" => %{
                        "emoji" => true,
                        "text" => "15 minutes",
                        "type" => "plain_text"
                      },
                      "value" => "c_900"
                    },
                    "type" => "static_select"
                  }
                },
                "people_votable" => %{
                  "people_votable_input" => %{
                    "selected_conversation" => "CR6LSF22K",
                    "type" => "channels_select"
                  }
                },
                "quorum" => %{
                  "quorum_input" => %{
                    "selected_option" => %{
                      "text" => %{
                        "emoji" => true,
                        "text" => "Anyone",
                        "type" => "plain_text"
                      },
                      "value" => "q_any"
                    },
                    "type" => "static_select"
                  }
                },
                "method" => %{
                  "method_input" => %{
                    "selected_option" => %{
                      "text" => %{
                        "emoji" => true,
                        "text" => "Two-thirds",
                        "type" => "plain_text"
                      },
                      "value" => "m_approval"
                    },
                    "type" => "static_select"
                  }
                },
                "parameter0_value" => %{
                  "parameter0_value_input" => %{
                    "type" => "plain_text_input",
                    "value" => "3.3"
                  }
                },
                "n_winners" => %{
                  "n_winners_input" => %{
                    "type" => "plain_text_input",
                    "value" => "3.3"
                  }
                }
              }
            },
            "submit" => %{"emoji" => true, "text" => "Next →", "type" => "plain_text"},
            "team_id" => "TR4QXERCG",
            "title" => %{
              "emoji" => true,
              "text" => "New decision settings",
              "type" => "plain_text"
            },
            "type" => "modal"
          }
        })
    }
  end

  def vote(type, options) do
    slack_options =
      Enum.map(options, fn option ->
        %{
          "text" => %{
            "emoji" => true,
            "text" => option.title,
            "type" => "plain_text"
          },
          "value" => "option_choice_value︰#{option.id}︰1.0"
        }
      end)

    [first_slack_option | last_slack_options] = slack_options

    %{
      "payload" =>
        Jason.encode!(%{
          "api_app_id" => "A01189AQQBH",
          "response_urls" => [],
          "team" => %{"domain" => "accord-cooperative", "id" => "TR4QXERCG"},
          "token" => "PdCBnWA1BVEjJNwbYNHRXN4i",
          "trigger_id" => "1080497886164.854847501424.bf1de34da430a8d70ecf12acc05c59c7",
          "type" => "view_submission",
          "user" => %{
            "id" => "U011B1U8APR",
            "name" => "wsn",
            "team_id" => "TR4QXERCG",
            "username" => "wsn"
          },
          "view" => %{
            "app_id" => "A01189AQQBH",
            "app_installed_team_id" => "TR4QXERCG",
            "blocks" => [
              %{
                "block_id" => "9f9I",
                "text" => %{
                  "text" => "*Plurality test 4*",
                  "type" => "mrkdwn",
                  "verbatim" => false
                },
                "type" => "section"
              },
              %{"block_id" => "ixU", "type" => "divider"},
              %{
                "block_id" => "9UZr",
                "elements" => [
                  %{
                    "emoji" => true,
                    "text" => "Choose the option you think should win.",
                    "type" => "plain_text"
                  }
                ],
                "type" => "context"
              },
              %{
                "block_id" => "option_choice",
                "dispatch_action" => false,
                "element" => %{
                  "action_id" => "option_choice_input",
                  "options" => slack_options,
                  "type" => "radio_buttons"
                },
                "label" => %{
                  "emoji" => true,
                  "text" => "Your choice:",
                  "type" => "plain_text"
                },
                "optional" => false,
                "type" => "input"
              },
              %{"block_id" => "XMnVq", "type" => "divider"},
              %{
                "block_id" => "jLnYp",
                "elements" => [
                  %{
                    "emoji" => true,
                    "text" => "You can change your choices until voting closes.",
                    "type" => "plain_text"
                  }
                ],
                "type" => "context"
              }
            ],
            "bot_id" => "B010TFEEM7C",
            "callback_id" => "vote_modal",
            "clear_on_close" => false,
            "close" => %{"emoji" => true, "text" => "Cancel", "type" => "plain_text"},
            "external_id" => "",
            "hash" => "1587404827.b50b7bfa",
            "id" => "V012W0E59RN",
            "notify_on_close" => true,
            "previous_view_id" => nil,
            "private_metadata" =>
              "{\"decision_version_service_id\":\"slack︰TR4QXERCG︰CR6LSFXXK︰1587151303.333300\"}",
            "root_view_id" => "V012W0E59RN",
            "state" => %{
              "values" => %{
                "option_choice" => %{
                  "option_choice_input" =>
                    case type do
                      :multiple ->
                        %{
                          "selected_options" => last_slack_options,
                          "type" => "checkboxes"
                        }

                      :single ->
                        %{
                          "selected_option" => first_slack_option,
                          "type" => "radio_buttons"
                        }
                    end
                }
              }
            },
            "submit" => %{
              "emoji" => true,
              "text" => "Confirm ✓",
              "type" => "plain_text"
            },
            "team_id" => "TR4QXERCG",
            "title" => %{
              "emoji" => true,
              "text" => "Confirm your vote",
              "type" => "plain_text"
            },
            "type" => "modal"
          }
        })
    }
  end
end
