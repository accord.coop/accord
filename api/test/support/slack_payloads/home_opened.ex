defmodule AccordSlack.Payloads.HomeOpened do
  def pl do
    %{
      "api_app_id" => "A01189AQQBH",
      "event" => %{
        "channel" => "D011B1U8CB1",
        "event_ts" => "1586544154.111645",
        "tab" => "home",
        "type" => "app_home_opened",
        "user" => "U011B1U8APR",
        "view" => %{
          "app_id" => "A01189AQQBH",
          "app_installed_team_id" => "TR4QXERCG",
          "blocks" => [
            %{
              "block_id" => "decision_title",
              "text" => %{
                "text" =>
                  "Hi there! :wave:\n\nYou can create a new decision by typing `/decide` without anything else and hitting send — you’ll be guided through the process of crafting a new decision step-by-step.\n\nA message will be posted to this channel only when you’re ready – you can go back or cancel any time in the process, and you can delete a decision after it’s been posted if you need to.\n\nVisit <https://accord.coop/help> for the full documentation.",
                "type" => "mrkdwn",
                "verbatim" => false
              },
              "type" => "section"
            }
          ],
          "bot_id" => "B010TFEEM7C",
          "callback_id" => "home",
          "clear_on_close" => false,
          "close" => nil,
          "external_id" => "",
          "hash" => "1586482460.f451018d",
          "id" => "V011M9W1GKF",
          "notify_on_close" => false,
          "previous_view_id" => nil,
          "private_metadata" => "",
          "root_view_id" => "V011M9W1GKF",
          "state" => %{"values" => %{}},
          "submit" => nil,
          "team_id" => "TR4QXERCG",
          "title" => %{
            "emoji" => true,
            "text" => "View Title",
            "type" => "plain_text"
          },
          "type" => "home"
        }
      },
      "event_id" => "Ev0127QDAE64",
      "event_time" => 1_586_544_154,
      "team_id" => "TR4QXERCG",
      "token" => "PdCBnWA1BVEjJNwbYNHRXN4i",
      "type" => "event_callback"
    }
  end
end
