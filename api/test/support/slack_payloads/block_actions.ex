defmodule AccordSlack.Payloads.BlockActions do
  def add_option do
    %{
      "payload" =>
        Jason.encode!(%{
          "actions" => [
            %{
              "action_id" => "add_option",
              "action_ts" => "1586543814.674615",
              "block_id" => "options_actions",
              "text" => %{
                "emoji" => true,
                "text" => "Add option",
                "type" => "plain_text"
              },
              "type" => "button"
            }
          ],
          "api_app_id" => "A01189AQQBH",
          "container" => %{"type" => "view", "view_id" => "V011NMQ6GCD"},
          "team" => %{"domain" => "accord-cooperative", "id" => "TR4QXERCG"},
          "token" => "PdCBnWA1BVEjJNwbYNHRXN4i",
          "trigger_id" => "1062641649748.854847501424.a0598d1373f332ee0665f5b30d2d90e1",
          "type" => "block_actions",
          "user" => %{
            "id" => "U011B1U8APR",
            "name" => "wsn",
            "team_id" => "TR4QXERCG",
            "username" => "wsn"
          },
          "view" => %{
            "app_id" => "A01189AQQBH",
            "app_installed_team_id" => "TR4QXERCG",
            "blocks" => [
              %{
                "block_id" => "NvN8",
                "text" => %{
                  "text" => "Question / Title:\n\n&gt; Payload test",
                  "type" => "mrkdwn",
                  "verbatim" => false
                },
                "type" => "section"
              },
              %{
                "block_id" => "decision_option_1",
                "element" => %{
                  "action_id" => "decision_option_1_input",
                  "type" => "plain_text_input"
                },
                "label" => %{
                  "emoji" => true,
                  "text" => "Option 1",
                  "type" => "plain_text"
                },
                "optional" => false,
                "type" => "input"
              },
              %{
                "block_id" => "decision_option_2",
                "element" => %{
                  "action_id" => "decision_option_2_input",
                  "type" => "plain_text_input"
                },
                "label" => %{
                  "emoji" => true,
                  "text" => "Option 2",
                  "type" => "plain_text"
                },
                "optional" => true,
                "type" => "input"
              },
              %{
                "block_id" => "options_actions",
                "elements" => [
                  %{
                    "action_id" => "add_option",
                    "text" => %{
                      "emoji" => true,
                      "text" => "Add option",
                      "type" => "plain_text"
                    },
                    "type" => "button"
                  }
                ],
                "type" => "actions"
              }
            ],
            "bot_id" => "B010TFEEM7C",
            "callback_id" => "new_decision_step_2",
            "clear_on_close" => false,
            "close" => %{"emoji" => true, "text" => "← Back", "type" => "plain_text"},
            "external_id" => "",
            "hash" => "1586543385.84951eed",
            "id" => "V011NMQ6GCD",
            "notify_on_close" => true,
            "previous_view_id" => "V011NMQ01KP",
            "private_metadata" => "{\"n_options\":2}",
            "root_view_id" => "V011NMQ01KP",
            "state" => %{"values" => %{}},
            "submit" => %{"emoji" => true, "text" => "Next →", "type" => "plain_text"},
            "team_id" => "TR4QXERCG",
            "title" => %{
              "emoji" => true,
              "text" => "New decision options",
              "type" => "plain_text"
            },
            "type" => "modal"
          }
        })
    }
  end

  def vote() do
    %{
      "payload" =>
        Jason.encode!(%{
          "actions" => [
            %{
              "action_id" => "vote",
              "action_ts" => "1587245818.508152",
              "block_id" => "Srgi8",
              "text" => %{"emoji" => true, "text" => "Choose", "type" => "plain_text"},
              "type" => "button"
            }
          ],
          "api_app_id" => "A01189AQQBH",
          "channel" => %{"id" => "CR6LSF22K", "name" => "bot-test"},
          "container" => %{
            "channel_id" => "CR6LSF22K",
            "is_ephemeral" => false,
            "message_ts" => "1587151303.000300",
            "type" => "message"
          },
          "message" => %{
            "blocks" => [
              %{
                "block_id" => "axRe",
                "text" => %{
                  "text" => "*Is the following approved:*",
                  "type" => "mrkdwn",
                  "verbatim" => false
                },
                "type" => "section"
              },
              %{
                "block_id" => "Ig8e",
                "text" => %{
                  "text" =>
                    "The arrows could be said to resemble cheeky step-brothers. A legal of the sunshine is assumed to be a pliant servant. A sardine of the suede is assumed to be a haunting prison. Some assert that a lanky attention's stick comes with it the thought that the foolproof mirror is a possibility.\n\nSome skyward flames are thought of simply as screens. A gracious study is a trumpet of the mind. Recent controversy aside, a dippy passbook without crabs is truly a tom-tom of scrubby bangles. This could be, or perhaps an approval can hardly be considered an unraised fowl without also being a viola.",
                  "type" => "mrkdwn",
                  "verbatim" => false
                },
                "type" => "section"
              },
              %{"block_id" => "LwT", "type" => "divider"},
              %{
                "block_id" => "+PBag",
                "elements" => [
                  %{"emoji" => true, "text" => "Option 1", "type" => "plain_text"}
                ],
                "type" => "context"
              },
              %{
                "block_id" => "l0Rk",
                "text" => %{"emoji" => true, "text" => "Etwas", "type" => "plain_text"},
                "type" => "section"
              },
              %{
                "block_id" => "oTJ",
                "elements" => [
                  %{"emoji" => true, "text" => "Option 2", "type" => "plain_text"}
                ],
                "type" => "context"
              },
              %{
                "block_id" => "Srgi8",
                "text" => %{"emoji" => true, "text" => "Eetwas", "type" => "plain_text"},
                "type" => "section"
              },
              %{
                "block_id" => "459",
                "elements" => [
                  %{"emoji" => true, "text" => "Option 3", "type" => "plain_text"}
                ],
                "type" => "context"
              },
              %{
                "block_id" => "9cx",
                "text" => %{
                  "emoji" => true,
                  "text" => "Eeetwas",
                  "type" => "plain_text"
                },
                "type" => "section"
              },
              %{"block_id" => "6RsZ", "type" => "divider"},
              %{
                "block_id" => "6RXZ",
                "elements" => [
                  %{
                    "action_id" => "vote",
                    "text" => %{"text" => "Vote", "type" => "plain_text"},
                    "type" => "button"
                  }
                ],
                "type" => "actions"
              },
              %{
                "block_id" => "mhp2",
                "elements" => [
                  %{
                    "emoji" => true,
                    "text" => "This version created by ",
                    "type" => "plain_text"
                  },
                  %{
                    "alt_text" => "wsn",
                    "fallback" => "24x24px image",
                    "image_bytes" => 1143,
                    "image_height" => 24,
                    "image_url" =>
                      "https://avatars.slack-edge.com/2020-04-05/1039989735763_f9d25056c54da3a3acc5_24.jpg",
                    "image_width" => 24,
                    "type" => "image"
                  },
                  %{"emoji" => true, "text" => "@wsn", "type" => "plain_text"}
                ],
                "type" => "context"
              },
              %{
                "block_id" => "WB4z",
                "elements" => [
                  %{
                    "emoji" => true,
                    "text" => "Voting will remain open until Apr 17, 2020, 7:36:43 PM.",
                    "type" => "plain_text"
                  }
                ],
                "type" => "context"
              }
            ],
            "bot_id" => "B010TFEEM7C",
            "edited" => %{"ts" => "1587151303.000000", "user" => "B010TFEEM7C"},
            "team" => "TR4QXERCG",
            "text" => "Is the following approved:",
            "ts" => "1587151303.000300",
            "type" => "message",
            "user" => "U01168M78GY"
          },
          "response_url" =>
            "https://hooks.slack.com/actions/TR4QXERCG/1071616939538/bd6pWRboiLoNejPIt0XX7r9f",
          "team" => %{"domain" => "accord-cooperative", "id" => "TR4QXERCG"},
          "token" => "PdCBnWA1BVEjJNwbYNHRXN4i",
          "trigger_id" => "1070237005109.854847501424.13ba4e9ed3212171b8c7d4e58608eed3",
          "type" => "block_actions",
          "user" => %{
            "id" => "U011B1U8APR",
            "name" => "wsn",
            "team_id" => "TR4QXERCG",
            "username" => "wsn"
          }
        })
    }
  end
end
