defmodule AccordSlack.Payloads.ChannelInfo do
  def bot_test do
    %{
      ok: true,
      channel: %{
        id: "CR6LSF22K",
        name: "bot-test",
        is_channel: true,
        is_group: false,
        is_im: false,
        created: 1_575_148_184,
        is_archived: false,
        is_general: false,
        unlinked: 0,
        name_normalized: "bot-test",
        is_shared: false,
        parent_conversation: nil,
        creator: "UQWSYHTA5",
        is_ext_shared: false,
        is_org_shared: false,
        shared_team_ids: [
          "TR4QXERCG"
        ],
        pending_shared: [],
        pending_connected_team_ids: [],
        is_pending_ext_shared: false,
        is_member: false,
        is_private: false,
        is_mpim: false,
        topic: %{
          value: "",
          creator: "",
          last_set: 0
        },
        purpose: %{
          value: "This is the test channel for the bot!",
          creator: "UQWSYHTA5",
          last_set: 1_585_947_798
        },
        previous_names: [
          "mod-poesie",
          "poesie"
        ],
        locale: "en-US",
        num_members: 5
      }
    }
  end
end
