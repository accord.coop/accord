defmodule AccordSlack.Payloads.ViewClosed do
  def step_1 do
    %{
      "payload" =>
        Jason.encode!(%{
          "api_app_id" => "A01189AQQBH",
          "is_cleared" => false,
          "team" => %{"domain" => "accord-cooperative", "id" => "TR4QXERCG"},
          "token" => "PdCBnWA1BVEjJNwbYNHRXN4i",
          "type" => "view_closed",
          "user" => %{
            "id" => "U011B1U8APR",
            "name" => "wsn",
            "team_id" => "TR4QXERCG",
            "username" => "wsn"
          },
          "view" => %{
            "app_id" => "A01189AQQBH",
            "app_installed_team_id" => "TR4QXERCG",
            "blocks" => [
              %{
                "block_id" => "decision_title",
                "element" => %{
                  "action_id" => "decision_title_input",
                  "type" => "plain_text_input"
                },
                "label" => %{
                  "emoji" => true,
                  "text" => "Question / Title",
                  "type" => "plain_text"
                },
                "optional" => false,
                "type" => "input"
              },
              %{
                "block_id" => "decision_body",
                "element" => %{
                  "action_id" => "decision_body_input",
                  "multiline" => true,
                  "type" => "plain_text_input"
                },
                "hint" => %{
                  "emoji" => true,
                  "text" => "Plain text or Markdown",
                  "type" => "plain_text"
                },
                "label" => %{
                  "emoji" => true,
                  "text" => "Description",
                  "type" => "plain_text"
                },
                "optional" => true,
                "type" => "input"
              }
            ],
            "bot_id" => "B010TFEEM7C",
            "callback_id" => "new_decision_step_1",
            "clear_on_close" => false,
            "close" => %{"emoji" => true, "text" => "Cancel", "type" => "plain_text"},
            "external_id" => "",
            "hash" => "1586543323.8416d35f",
            "id" => "V011JSPFSLW",
            "notify_on_close" => true,
            "previous_view_id" => nil,
            "private_metadata" => "",
            "root_view_id" => "V011JSPFSLW",
            "state" => %{"values" => %{}},
            "submit" => %{"emoji" => true, "text" => "Next →", "type" => "plain_text"},
            "team_id" => "TR4QXERCG",
            "title" => %{
              "emoji" => true,
              "text" => "Start a new decision",
              "type" => "plain_text"
            },
            "type" => "modal"
          }
        })
    }
  end
end
