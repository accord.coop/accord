defmodule AccordSlack.Payloads.TeamInfo do
  def accord do
    %{
      ok: true,
      team: %{
        id: "TR4QXERCG",
        name: "Accord",
        domain: "accord-cooperative",
        email_domain: "accord.coop",
        icon: %{
          image_34:
            "https://avatars.slack-edge.com/2019-11-30/854859879717_eae43a3684a47d9be48b_34.png",
          image_44:
            "https://avatars.slack-edge.com/2019-11-30/854859879717_eae43a3684a47d9be48b_44.png",
          image_68:
            "https://avatars.slack-edge.com/2019-11-30/854859879717_eae43a3684a47d9be48b_68.png",
          image_88:
            "https://avatars.slack-edge.com/2019-11-30/854859879717_eae43a3684a47d9be48b_88.png",
          image_102:
            "https://avatars.slack-edge.com/2019-11-30/854859879717_eae43a3684a47d9be48b_102.png",
          image_132:
            "https://avatars.slack-edge.com/2019-11-30/854859879717_eae43a3684a47d9be48b_132.png",
          image_230:
            "https://avatars.slack-edge.com/2019-11-30/854859879717_eae43a3684a47d9be48b_230.png",
          image_original:
            "https://avatars.slack-edge.com/2019-11-30/854859879717_eae43a3684a47d9be48b_original.png"
        }
      }
    }
  end
end
