defmodule AccordSlack.Payloads.UserInfo do
  def will do
    %{
      ok: true,
      user: %{
        id: "U011B1U8APR",
        team_id: "TR4QXERCG",
        name: "wsn",
        deleted: false,
        color: "684b6c",
        real_name: "Will",
        tz: "America/Chicago",
        tz_label: "Central Daylight Time",
        tz_offset: -18000,
        profile: %{
          title: "",
          phone: "",
          skype: "",
          real_name: "Will",
          real_name_normalized: "Will",
          display_name: "wsn",
          display_name_normalized: "wsn",
          status_text: "",
          status_emoji: "",
          status_expiration: 0,
          avatar_hash: "f9d25056c54d",
          image_original:
            "https://avatars.slack-edge.com/2020-04-05/1039989735763_f9d25056c54da3a3acc5_original.jpg",
          is_custom_image: true,
          first_name: "Will",
          last_name: "",
          image_24:
            "https://avatars.slack-edge.com/2020-04-05/1039989735763_f9d25056c54da3a3acc5_24.jpg",
          image_32:
            "https://avatars.slack-edge.com/2020-04-05/1039989735763_f9d25056c54da3a3acc5_32.jpg",
          image_48:
            "https://avatars.slack-edge.com/2020-04-05/1039989735763_f9d25056c54da3a3acc5_48.jpg",
          image_72:
            "https://avatars.slack-edge.com/2020-04-05/1039989735763_f9d25056c54da3a3acc5_72.jpg",
          image_192:
            "https://avatars.slack-edge.com/2020-04-05/1039989735763_f9d25056c54da3a3acc5_192.jpg",
          image_512:
            "https://avatars.slack-edge.com/2020-04-05/1039989735763_f9d25056c54da3a3acc5_512.jpg",
          image_1024:
            "https://avatars.slack-edge.com/2020-04-05/1039989735763_f9d25056c54da3a3acc5_1024.jpg",
          status_text_canonical: "",
          team: "TR4QXERCG"
        },
        is_admin: true,
        is_owner: true,
        is_primary_owner: false,
        is_restricted: false,
        is_ultra_restricted: false,
        is_bot: false,
        is_app_user: false,
        updated: 1_586_107_750,
        locale: "en-US"
      }
    }
  end
end
