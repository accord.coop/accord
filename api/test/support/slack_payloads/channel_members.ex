defmodule AccordSlack.Payloads.ChannelMembers do
  def paginated(nil) do
    %{
      ok: true,
      members: [
        "UQWSYHTA5",
        "U01168M78GY",
        "U011B1U824F",
        "U011B1U85DM"
      ],
      response_metadata: %{
        next_cursor: "dXNlcl9pZDoxMDQ1MDYyMjgwNDg3"
      }
    }
  end

  def paginated(_cursor) do
    %{
      ok: true,
      members: [
        "U011B1U88EB",
        "U011B1U8APR",
        "U011DCTK5ST",
        "U0139P6NRFC"
      ],
      response_metadata: %{
        next_cursor: ""
      }
    }
  end
end
