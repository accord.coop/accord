defmodule AccordWeb.SlackTest do
  alias AccordSlack.Payloads.{
    BlockActions,
    HomeOpened,
    Slash,
    ViewSubmission,
    ViewClosed
  }

  alias Accord.{
    Repo,
    Decision,
    DecisionVersion,
    DecisionRole,
    Person,
    Persona,
    People,
    PersonPeople,
    Caches
  }

  alias AccordSlack.IO, as: SlackIO
  alias Accord.DecisionSettings.Types, as: DSTypes

  use AccordWeb.ConnCase

  setup do
    People.assure(:team, {:slack, "TR4QXERCG"}, title: "Accord", app_token: Ecto.UUID.generate())
    |> Repo.commit()

    {:ok, pid} = AccordWeb.PayloadPasser.start_link(self())
    {:ok, pid: pid}
  end

  @tag :integrations
  @tag :slack
  test "Returns a home view" do
    conn =
      build_conn()
      |> put_req_header("accept", "application/json")
      |> put_req_header("content-type", "application/json")
      |> post("/v1/slack/event", Jason.encode!(HomeOpened.pl()))

    # Home view is sent in a follow-up API call
    assert conn.resp_body === ""
    assert conn.status === 200
    assert_receive {"/views.publish", %{"view" => %{"type" => "home"}}}
  end

  @tag :integrations
  @tag :slack
  test "Responds to view closures" do
    conn =
      build_conn()
      |> put_req_header("accept", "application/json")
      |> put_req_header("content-type", "application/json")
      |> post("/v1/slack/interaction", Jason.encode!(ViewClosed.step_1()))

    assert conn.resp_body === ""
    assert conn.status === 200
  end

  @tag :integrations
  @tag :slack
  test "Responds to a slash command for help and responds in a different language after changing locale" do
    conn_en =
      build_conn()
      |> put_req_header("accept", "application/json")
      |> put_req_header("content-type", "application/json")
      |> post("/v1/slack/slash", Jason.encode!(Slash.help()))

    assert conn_en.resp_body === ""
    assert conn_en.status === 200
    assert_receive {"/commands" <> _, %{"response_type" => "ephemeral", "text" => text_en}}

    uuid_1 = Persona.get({:slack, "TR4QXERCG", "U011B1U8APR"}) |> Map.get(:uuid)

    {:ok,
     [
       locale: %{
         accept_language: _,
         tz: tz
       },
       preferred_name: name,
       is_slack_bot: false
     ]} = SlackIO.get_user_opts(Ecto.UUID.generate(), "U011B1U8APR")

    Person.assure_with_persona(
      {:slack, "TR4QXERCG", "U011B1U8APR"},
      locale: %{accept_language: "de-DE", tz: tz},
      preferred_name: name
    )
    |> Repo.commit()

    uuid_2 = Persona.get({:slack, "TR4QXERCG", "U011B1U8APR"}) |> Map.get(:uuid)

    assert uuid_1 === uuid_2

    conn_de =
      build_conn()
      |> put_req_header("accept", "application/json")
      |> put_req_header("content-type", "application/json")
      |> post("/v1/slack/slash", Jason.encode!(Slash.help()))

    assert conn_de.resp_body === ""
    assert conn_de.status === 200
    assert_receive {"/commands" <> _, %{"response_type" => "ephemeral", "text" => text_de}}

    assert text_en !== text_de
  end

  @tag :integrations
  @tag :slack
  test "Responds to a slash command to create a new decision" do
    conn =
      build_conn()
      |> put_req_header("accept", "application/json")
      |> put_req_header("content-type", "application/json")
      |> post("/v1/slack/slash", Jason.encode!(Slash.decide()))

    assert conn.resp_body === ""
    assert conn.status === 200
    assert_receive {"/views.open", %{"trigger_id" => _, "view" => _}}
  end

  @tag :integrations
  @tag :slack
  test "Responds to new decision modal view submissions" do
    add_option =
      build_conn()
      |> put_req_header("accept", "application/json")
      |> put_req_header("content-type", "application/json")
      |> post("/v1/slack/interaction", Jason.encode!(BlockActions.add_option()))

    assert add_option.resp_body === ""
    assert add_option.status === 200

    assert_receive {"/views.update",
                    %{"view_id" => _, "view" => %{"private_metadata" => metadata}}}

    assert Jason.decode!(metadata) |> Map.get("n_options") === 3

    step_1 =
      build_conn()
      |> put_req_header("accept", "application/json")
      |> put_req_header("content-type", "application/json")
      |> post("/v1/slack/interaction", Jason.encode!(ViewSubmission.step_1()))

    assert {:ok, %{"response_action" => "push", "view" => _}} = Jason.decode(step_1.resp_body)
    assert step_1.status === 200

    step_2 =
      build_conn()
      |> put_req_header("accept", "application/json")
      |> put_req_header("content-type", "application/json")
      |> post("/v1/slack/interaction", Jason.encode!(ViewSubmission.step_2()))

    assert_receive {"/chat.postMessage", %{"text" => _}}
    assert_receive {"/chat.update", %{"text" => _}}

    assert {:ok, %{"response_action" => "clear"}} = Jason.decode(step_2.resp_body)
    assert step_2.status === 200

    assert %DecisionVersion{settings: settings, options: options} =
             DecisionVersion.get({:slack, "TR4QXERCG", "CR6LSF22K", "240598250948.2945937"})
             |> Repo.preload([:decision, :author])

    assert settings.quorum_value == 1
    assert settings.quorum_quantification === DSTypes.absolute(:str)
    assert settings.method === DSTypes.cardinal(:str)
    assert settings.parameter0_quantification === DSTypes.absolute(:str)
    assert settings.parameter0_value == 3.3
    assert length(options) === 3
  end

  @tag :integrations
  @tag :slack
  test "Clicking 'Vote' invokes the voting modal" do
    {:ok, %{person: author}} =
      Person.assure_with_persona({:slack, "TR4QXERCG", "U011B1U8APR"})
      |> Repo.commit()

    {:ok, %{decision_version: %DecisionVersion{options: [%{id: _option_id} | _]}}} =
      Decision.create_with_version(%{
        service_id: "slack︰TR4QXERCG︰CR6LSF22K︰1587151303.000300",
        title: random_string(20),
        author: author,
        options: [
          %{title: random_string(20)},
          %{title: random_string(20)}
        ],
        settings: %{
          method: DSTypes.cardinal(:str),
          resolution: DSTypes.sum(:str),
          parameter1_quantification: DSTypes.absolute(:str),
          parameter1_value: 1.0
        },
        state: Accord.DecisionVersion.Types.published(:str)
      })
      |> Repo.commit()

    choose =
      build_conn()
      |> put_req_header("accept", "application/json")
      |> put_req_header("content-type", "application/json")
      |> post("/v1/slack/interaction", Jason.encode!(BlockActions.vote()))

    assert choose.resp_body === ""
    assert choose.status === 200
    assert_receive {"/views.open", %{"trigger_id" => _, "view" => _}}
  end

  @tag :integrations
  @tag :slack
  test "Confirming choices in the voting modal creates a vote" do
    {:ok, %{person: person}} =
      Person.assure_with_persona({:slack, "TR4QXERCG", "U011B1U8APR"})
      |> Repo.commit()

    {:ok, %{channel: people}} =
      People.assure_associated({:slack, "TR4QXERCG", "CR6LSF22K"})
      |> Repo.commit()

    PersonPeople.associate(person, people) |> Repo.commit()

    {:ok, %{decision_version: %DecisionVersion{options: options}, decision: decision}} =
      Decision.create_with_version(%{
        service_id: "slack︰TR4QXERCG︰CR6LSFXXK︰1587151303.333300",
        title: random_string(20),
        author: person,
        options: [
          %{title: random_string(20)},
          %{title: random_string(20)},
          %{title: random_string(20)}
        ],
        settings: %{
          method: DSTypes.cardinal(:str),
          resolution: DSTypes.sum(:str)
        }
      })
      |> Repo.commit()

    DecisionRole.add_to_decision(decision, people, DecisionRole.Types.votable(:str))
    |> Repo.commit()

    vote =
      build_conn()
      |> put_req_header("accept", "application/json")
      |> put_req_header("content-type", "application/json")
      |> post("/v1/slack/interaction", Jason.encode!(ViewSubmission.vote(:single, options)))

    assert {:ok, %{"response_action" => "clear"}} = Jason.decode(vote.resp_body)
    assert vote.status === 200

    votes =
      DecisionVersion.get_by_service_id("slack︰TR4QXERCG︰CR6LSFXXK︰1587151303.333300")
      |> Repo.preload(:votes)
      |> Map.get(:votes)

    assert length(votes) === 1

    voter = votes |> List.first() |> Repo.preload(:person) |> Map.get(:person)

    assert voter ===
             Persona.get({:slack, "TR4QXERCG", "U011B1U8APR"})
             |> Repo.preload(:person)
             |> Map.get(:person)

    # Happens twice: once when the vote is first updated…
    assert_receive {"/chat.update", %{"ts" => "1587151303.333300"}}, 30_000
    # …and again when quorum is counted by pipeline.
    assert_receive {"/chat.update", %{"ts" => "1587151303.333300"}}, 30_000
  end

  @tag :integrations
  @tag :slack
  @tag :caches
  @tag :crud
  test "Caches work as expected" do
    team_id = random_slack_id("T")

    [uid_1 | [uid_2 | uid_rest]] =
      user_ids = 1..300 |> Enum.reduce([], fn _, acc -> [random_slack_id("U") | acc] end)

    to_cache = [
      %{
        service_id: Persona.service_id({:slack, team_id, uid_1}),
        is_bot: true
      },
      %{
        service_id: Persona.service_id({:slack, team_id, uid_2}),
        is_bot: false
      }
    ]

    to_cache =
      to_cache ++
        (uid_rest
         |> Enum.map(fn user_id ->
           %{
             service_id: Persona.service_id({:slack, team_id, user_id}),
             is_bot: random_bool(0.1)
           }
         end))

    to_cache |> Enum.map(fn attrs -> Caches.SlackUser.put(attrs) end)

    from_cache =
      user_ids
      |> Enum.map(fn user_id -> Persona.service_id({:slack, team_id, user_id}) end)
      |> MapSet.new()
      |> Caches.SlackUser.get_cached()

    assert MapSet.size(from_cache) === 300

    assert Caches.SlackUser.get_missing(
             [uid_1, uid_2, random_slack_id("U"), random_slack_id("U")]
             |> Enum.map(fn user_id -> Persona.service_id({:slack, team_id, user_id}) end)
             |> MapSet.new()
           )
           |> MapSet.size() === 2
  end
end
