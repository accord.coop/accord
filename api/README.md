# Accord API

Accord's chat app API.

Built with [Elixir](https://elixir-lang.org/) on [Phoenix](https://www.phoenixframework.org/).

## Develop

1. Get dependencies: `$ mix deps.get`

1. Run a PostgreSQL server & database at `postgresql://localhost:5432/accord_dev` with username and password `postgres` (for development)

1. Run `mix ecto.create && mix ecto.migrate`

You're now ready to either:

- Run a local development server: `mix phx.server`

- Run tests: `mix test`

See associated automations in the `.scripts` module, a sibling to this module.

## Deploy

1. Start a PostgreSQL server. Accord is a secure-only application, so configure the database to accept only secure connections.

1. Put the following production secrets in `.scripts/gigalixir/.secrets`:
  - set `URNE_KEY` to the output of either `guardian.gen.secret` or `phx.gen.secret`
  - set `GUARDIAN_KEY` to the output of `guardian.gen.secret`
  - set `SLACK_SIGNING_SECRET` to the signing secret available in Slack
  - set `SLACK_DEVELOPMENT_TEAM` to the development account’s team ID and the bot token assigned by api.slack.com joined by `:`, e.g. `TRXQXEXCG:xoxb-1234567890`.
  - set `SLACK_CLIENT_ID` to the Slack app client ID 
  - set `SLACK_CLIENT_SECRET` to the Slack app client secret 
  - set `PG_URL` to `ecto://postgres:${password}@${ip or host}:5432/${database}`, replacing the variables with specifics
  - set `SQL_SERVER_CA`, `SQL_CLIENT_KEY`, and `SQL_CLIENT_CERT` each to the output of `cat ${.pem file} | tr ' ' '@' | tr '\n' ':'`, where `.pem file` is the relevant file offered by the SQL provider
  - set `SENDGRID_API_KEY` if you will use Sendgrid for production.

1. Run `.scripts/gigialixir/create.sh` (you must be logged in to Gigalixir)

1. Run `.scripts/gigalixir/deploy.sh`

1. Run `.scripts/gigalixir/resume.sh`

The `.scripts` module contains other basic operations for CI in Gitlab and hosting in Gigalixir.

## License

Accord’s top-level apps, `api` and `www`, are licensed `CC-BY-SA-4.0`, Creative Commons Attribution-ShareAlike 4.0 International.
